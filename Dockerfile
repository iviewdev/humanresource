# stage1 as builder
FROM node:10-alpine as builder
ARG MAX_OLD_SPACE_SIZE=8192
ENV NODE_OPTIONS=--max-old-space-size=${MAX_OLD_SPACE_SIZE}


WORKDIR /app

# copy the package.json to install dependencies
COPY package*.json /app/

# Install the dependencies and make the folder
RUN npm install && mv ./node_modules ./app


COPY ./ /app/

ARG configuration=production

# Build the project and copy the files
RUN npm run ng build -- --output-path=./dist/out --configuration $configuration


FROM nginx:alpine

#!/bin/sh

COPY nginx.conf /etc/nginx/nginx.conf
COPY toovix.key /etc/ssl/toovix.key
COPY ssl-bundle.crt /etc/ssl/ssl-bundle.crt

## Remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*

# Copy from the stahg 1
COPY --from=builder /app/dist/out /usr/share/nginx/html

EXPOSE 4200 80

ENTRYPOINT ["nginx", "-g", "daemon off;"]
