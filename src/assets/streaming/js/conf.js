// var websocketURL = "ws://domain-name.com:5443/WebRTCAppEE/websocket";
var baseUrl = '139.59.243.230';
//var loc = 'webstream1.westus.cloudapp.azure.com:5080/WebRTCAppEE';
var websocketURL = "ws://" + baseUrl + ":5080/WebRTCAppEE/websocket";

if (location.protocol.startsWith("https")) {
    websocketURL = "wss://" + baseUrl + ":5443/WebRTCAppEE/websocket";
}
var pc_config = { 'name': 'yogesh' };

var sdpConstraints = {
    OfferToReceiveAudio: false,
    OfferToReceiveVideo: false

};

var mediaConstraints = {
    video: {
        width: { min: 260, ideal: 260 },
        height: { min: 260, ideal: 260 },
        frameRate: { max: 10 },
        aspectRatio: { ideal: 1.7777777778 }
    },
    audio: {
        sampleSize: 16,
        channelCount: 2
    }
};
var streamId = "stream1";
var webRTCAdaptor = new WebRTCAdaptor({
    websocket_url: websocketURL,
    mediaConstraints: mediaConstraints,
    peerconnection_config: pc_config,
    sdp_constraints: sdpConstraints,
    localVideoId: "localVideo",
    remoteVideoId: "remoteVideo",
    callback: function (info) {
        debugger
        if (info == "initialized") {
            console.log("initialized");
        }
        else if (info == "joined") {
            //joined the stream
            console.log("joined");
        }
        else if (info == "leaved") {
            //leaved the stream
            console.log("leaved");
        }
    },
    callbackError: function (error) {
        //some of the possible errors, NotFoundError, SecurityError,PermissionDeniedError

        console.log("error callback: " + error);
        alert(error);
    }
});
function join() {
    webRTCAdaptor.join(streamId);
}