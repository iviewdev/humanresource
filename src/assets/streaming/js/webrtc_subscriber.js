﻿var streamId_sub = '183638566009850003536541';
var token = '';

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var name = getUrlParameter("name");
if (name !== "undefined") {
    streamNameBox.value = name;
}


function startPlaying() {
    debugger

    webRTCAdaptor_sub.play(streamId_sub, token);
}

function stopPlaying() {
    playing = false;
    webRTCAdaptor_sub.stop(streamId_sub);
}

var pc_config_sub = null;

var sdpConstraints_sub = {
    OfferToReceiveAudio: true,
    OfferToReceiveVideo: true

};
var mediaConstraints_sub = {
    video: false,
    audio: false
};

var path = "webstream2.westus.cloudapp.azure.com:5080/WebRTCAppEE/websocket";
var websocketURL_sub = "ws://" + path;

if (location.protocol.startsWith("https")) {
    websocketURL_sub = "wss://webstream2.westus.cloudapp.azure.com:5443/WebRTCAppEE/websocket"
}
var playing = false;
var webRTCAdaptor_sub = new WebRTCAdaptor({
    websocket_url: websocketURL_sub,
    mediaConstraints: mediaConstraints_sub,
    peerconnection_config: pc_config_sub,
    sdp_constraints: sdpConstraints_sub,
    remoteVideoId: "remoteVideo",
    isPlayMode: true,
    debug: true,
    callback: function (info, description) {
        console.log(info + ":" + description);
        if (info == "initialized") {
            console.log("initialized");
            start_play_button.disabled = false;
            stop_play_button.disabled = true;
        } else if (info == "play_started") {
            //joined the stream
            console.log("play started");
            start_play_button.disabled = true;
            stop_play_button.disabled = false;
            playing = true;


        }
        else if (info == "streamInformation") {
            if (!playing) {
                playing = true;
                startPlaying();

            }
            console.log(JSON.stringify(description));
        }
        else if (info == "play_finished") {
            //leaved the stream
            console.log("play finished");
            start_play_button.disabled = false;
            stop_play_button.disabled = true;
            playing = false;
        } else if (info == "closed") {
            playing = false;
            //console.log("Connection closed");
            if (typeof description != "undefined") {
                console.log("Connecton closed: "
                        + JSON.stringify(description));
            }
        }
    },

    callbackError: function (error) {
        //some of the possible errors, NotFoundError, SecurityError,PermissionDeniedError

        console.log("error callback: " + JSON.stringify(error));
        // alert(JSON.stringify(error));
    }
});


window.setInterval(function () {
    if (!playing) {
        var stream = webRTCAdaptor.getStreamInfo(streamId);
        console.log(stream);
    }
}, 3000);