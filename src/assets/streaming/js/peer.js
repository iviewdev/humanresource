var join_button = document.getElementById("join_button");
	var leave_button = document.getElementById("leave_button");


	var streamNameBox = document.getElementById("streamName");

	function join() {
		webRTCAdaptor.join(streamNameBox.value);
	}

	function leave() {
		webRTCAdaptor.leave(streamNameBox.value);
	}
	
	function turnOffLocalCamera() {
		webRTCAdaptor.turnOffLocalCamera();
	}
	
	function turnOnLocalCamera() {
		webRTCAdaptor.turnOnLocalCamera();
	}
	
	function muteLocalMic(){
		webRTCAdaptor.muteLocalMic();
	}
	
	function unmuteLocalMic() {
		webRTCAdaptor.unmuteLocalMic();
	}
	
	
	  
	var pc_config = null;
	/*{
		'iceServers' : [ {
			'urls' : 'stun:stun.l.google.com:19302'
		} ]
	};
	*/

	var sdpConstraints = 
	{
		OfferToReceiveAudio : true,
		OfferToReceiveVideo : true
			
	};
	var mediaConstraints = {
	          video: true,
	          audio: true
	        };
	var baseUrl = '139.59.243.230';
	 var websocketURL = "ws://" + baseUrl + ":5080/WebRTCAppEE/websocket";

    if (location.protocol.startsWith("https")) {
        websocketURL = "wss://" + baseUrl + ":5443/WebRTCAppEE/websocket";
    }
	
	var webRTCAdaptor = new WebRTCAdaptor({
		  websocket_url: websocketURL,
		  mediaConstraints: mediaConstraints,
		  peerconnection_config: pc_config,
		  sdp_constraints: sdpConstraints,
		  localVideoId: "localVideo",
		  remoteVideoId: "remoteVideo",
		  callback: function(info) {
			  if (info == "initialized") {
				  console.log("initialized");
				  join_button.disabled = false;
				  leave_button.disabled = true;
			  }
			  else if (info == "joined") {
				  //joined the stream
				  console.log("joined");
				  join_button.disabled = true;
				  leave_button.disabled = false;
			  }
			  else if (info == "leaved") {
				  //leaved the stream
				  console.log("leaved");
				  join_button.disabled = false;
				  leave_button.disabled = true;
			  }
		  },
		  callbackError: function(error) {
			  //some of the possible errors, NotFoundError, SecurityError,PermissionDeniedError
			  
			  console.log("error callback: " + error);
			  alert(error);
		  }
	  });
	
