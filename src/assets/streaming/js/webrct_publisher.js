﻿var pub_streamId = '183638566009850003536541dfdfsfd';

function startPublishing() {
    //  pub_streamId = streamNameBox.value;
    webRTCAdaptor_pub.publish(pub_streamId);
}

function stopPublishing() {
    webRTCAdaptor_pub.stop(pub_streamId);
}

function startAnimation() {

    $("#broadcastingInfo").fadeIn(800, function () {
        $("#broadcastingInfo").fadeOut(800, function () {
            var state = webRTCAdaptor.signallingState(pub_streamId);
            if (state != null && state != "closed") {
                var iceState = webRTCAdaptor.iceConnectionState(pub_streamId);
                if (iceState != null && iceState != "failed" && iceState != "disconnected") {
                    startAnimation();
                }
            }
        });
    });

}

var pc_config_pub = { "name": "yogesh" };

var sdpConstraints_pub = {
    OfferToReceiveAudio: false,
    OfferToReceiveVideo: false

};

var mediaConstraints_pub = {
    video: {
        width: { min: 260, ideal: 260 },
        height: { min: 260, ideal: 260 },
        aspectRatio: { ideal: 1.7777777778 }
    },
    audio: {
        sampleSize: 16,
        channelCount: 2
    }
};
//var loc = 'webstream1.westus.cloudapp.azure.com:5080/WebRTCAppEE';
var websocketURL_pub = "ws://webstream2.westus.cloudapp.azure.com:5080/WebRTCAppEE/websocket";

if (location.protocol.startsWith("https")) {
    websocketURL_pub = "wss://webstream2.westus.cloudapp.azure.com:5443/WebRTCAppEE/websocket";
}

 
var webRTCAdaptor_pub = new WebRTCAdaptor({
    websocket_url: websocketURL_pub,
    mediaConstraints: mediaConstraints_pub,
    peerconnection_config: pc_config_pub,
    sdp_constraints: sdpConstraints_pub,
    localVideoId: "localVideo",
    debug: true,
    callback: function (info, description) {
        debugger
        if (info == "initialized") {
            console.log("initialized");
            start_publish_button.disabled = false;
            stop_publish_button.disabled = true;
        } else if (info == "publish_started") {
            //stream is being published
            console.log("publish started");
            start_publish_button.disabled = true;
            stop_publish_button.disabled = false;
            startAnimation();
        } else if (info == "publish_finished") {
            //stream is being finished
            console.log("publish finished");
            start_publish_button.disabled = false;
            stop_publish_button.disabled = true;
        }
        else if (info == "closed") {
            //console.log("Connection closed");
            if (typeof description != "undefined") {
                console.log("Connecton closed: " + JSON.stringify(description));
            }
        }
    },
    callbackError: function (error, message) {
        //some of the possible errors, NotFoundError, SecurityError,PermissionDeniedError
        debugger
        console.log("error callback: " + JSON.stringify(error));
        var errorMessage = JSON.stringify(error);
        if (typeof message != "undefined") {
            errorMessage = message;
        }
        var errorMessage = JSON.stringify(error);
        if (error.indexOf("NotFoundError") != -1) {
            errorMessage = "Camera or Mic are not found or not allowed in your device.";
        }
        else if (error.indexOf("NotReadableError") != -1 || error.indexOf("TrackStartError") != -1) {
            errorMessage = "Camera or Mic is being used by some other process that does not not allow these devices to be read.";
        }
        else if (error.indexOf("OverconstrainedError") != -1 || error.indexOf("ConstraintNotSatisfiedError") != -1) {
            errorMessage = "There is no device found that fits your video and audio constraints. You may change video and audio constraints."
        }
        else if (error.indexOf("NotAllowedError") != -1 || error.indexOf("PermissionDeniedError") != -1) {
            errorMessage = "You are not allowed to access camera and mic.";
        }
        else if (error.indexOf("TypeError") != -1) {
            errorMessage = "Video/Audio is required.";
        }
        else if (error.indexOf("UnsecureContext") != -1) {
            errorMessage = "Fatal Error: Browser cannot access camera and mic because of unsecure context. Please install SSL and access via https";
        }
        else if (error.indexOf("WebSocketNotSupported") != -1) {
            errorMessage = "Fatal Error: WebSocket not supported in this browser";
        }

        alert(errorMessage);
    }
});