//debugger
var token = 'sdfdsfsf';//"<%= request.getParameter("token") %>";
var start_publish_button = document.getElementById("start_publish_button");
var stop_publish_button = document.getElementById("stop_publish_button");

var streamNameBox = document.getElementById("streamName");

var streamId = streamNameBox.value;

function startPublishing() {
    streamId = streamNameBox.value;
    webRTCAdaptor.publish(streamId,token);
}


function stopPublishing() {
    webRTCAdaptor.stop(streamId,);
    // webRTCAdaptor.close(streamId);
}

function startAnimation() {

    $("#broadcastingInfo").fadeIn(800, function () {
        $("#broadcastingInfo").fadeOut(800, function () {
            var state = webRTCAdaptor.signallingState(streamId);
            if (state != null && state != "closed") {
                var iceState = webRTCAdaptor.iceConnectionState(streamId);
                if (iceState != null && iceState != "failed" && iceState != "disconnected") {
                    startAnimation();
                   
                }
            }
        });
    });

}

var pc_config = { 'name': 'yogesh' };

var sdpConstraints = {
    OfferToReceiveAudio: false,
    OfferToReceiveVideo: false

};

var mediaConstraints = {
    video: {
        width: { min: 260, ideal: 260 },
        height: { min: 260, ideal: 260 },
        frameRate: { max: 10 },
        aspectRatio: { ideal: 1.7777777778 }
    },
    audio: {
        sampleSize: 16,
        channelCount: 2
    }
};
//rtmp://128.199.72.142/WebRTCApp/361112691341675399761447
var baseUrl = '139.59.243.230';
//var loc = 'webstream1.westus.cloudapp.azure.com:5080/WebRTCAppEE';
var websocketURL = "ws://" + baseUrl + ":5080/WebRTCAppEE/websocket";

if (location.protocol.startsWith("https")) {
    console.log("in wss ");
    websocketURL = "wss://" + baseUrl + ":5443/WebRTCAppEE/websocket";
}
function getPeerConnection() {
     
    this.webRTCAdaptor.initPeerConnection(streamId);
    var state = this.webRTCAdaptor.signallingState(streamId);
    var dataChannelOptions = {
        ordered: true, // do not guarantee order
    };
  
  //  var dataChannel = webRTCAdaptor.remotePeerConnection[streamId].createDataChannel(streamId, dataChannelOptions);
}
 
var webRTCAdaptor = new WebRTCAdaptor({
    websocket_url: websocketURL,
    mediaConstraints: mediaConstraints,
    peerconnection_config: pc_config,
    sdp_constraints: sdpConstraints,
    localVideoId: "localVideo",
    debug: true,
    callback: function (info, description) {
         
        console.log(info + ':' + JSON.stringify(description));
        if (info == "initialized") {
            console.log("initialized");
            start_publish_button.disabled = false;
            stop_publish_button.disabled = true;
            getPeerConnection();
        } else if (info == "publish_started") {
            //stream is being published
            console.log("publish started");
            start_publish_button.disabled = true;
            stop_publish_button.disabled = false;
            startAnimation();
        } else if (info == "publish_finished") {
            //stream is being finished
            console.log("publish finished");
            start_publish_button.disabled = false;
            stop_publish_button.disabled = true;
        }
        else if (info == "closed") {
            //console.log("Connection closed");
            if (typeof description != "undefined") {
                console.log("Connecton closed: " + JSON.stringify(description));
            }
        }
       else if (info == "data_channel_opened") {

            console.log("data channel is open");

        }
        else if (info == "data_received") {

            console.log("Message received ", description.data);

            handleData(description);
        }

        else if (info == "data_channel_error") {

            handleError(description);

        } else if (info == "data_channel_closed") {

            console.log("Data channel closed ");
        }
    },
    callbackError: function (error, message) {
        //some of the possible errors, NotFoundError, SecurityError,PermissionDeniedError
        debugger
        console.log("error callback: " + JSON.stringify(error));
        var errorMessage = JSON.stringify(error);
        if (typeof message != "undefined") {
            errorMessage = message;
        }
        var errorMessage = JSON.stringify(error);
        if (error.indexOf("NotFoundError") != -1) {
            errorMessage = "Camera or Mic are not found or not allowed in your device.";
        }
        else if (error.indexOf("NotReadableError") != -1 || error.indexOf("TrackStartError") != -1) {
            errorMessage = "Camera or Mic is being used by some other process that does not not allow these devices to be read.";
        }
        else if (error.indexOf("OverconstrainedError") != -1 || error.indexOf("ConstraintNotSatisfiedError") != -1) {
            errorMessage = "There is no device found that fits your video and audio constraints. You may change video and audio constraints."
        }
        else if (error.indexOf("NotAllowedError") != -1 || error.indexOf("PermissionDeniedError") != -1) {
            errorMessage = "You are not allowed to access camera and mic.";
        }
        else if (error.indexOf("TypeError") != -1) {
            errorMessage = "Video/Audio is required.";
        }
        else if (error.indexOf("UnsecureContext") != -1) {
            errorMessage = "Fatal Error: Browser cannot access camera and mic because of unsecure context. Please install SSL and access via https";
        }
        else if (error.indexOf("WebSocketNotSupported") != -1) {
            errorMessage = "Fatal Error: WebSocket not supported in this browser";
        }

        alert(errorMessage);
    }
});

