﻿angular.module('Sort').controller('TrainingCntrl', function ($scope, $http, $state, $location, $cookies, $timeout, ngAudio, $mdDialog, $sce, APICallService, $mdToast, $rootScope,AuthService) {
    
    //debugger;
    $rootScope.securitytime = 0;
    if ($scope.LoggedInUser != null) {
        $scope.UserName = $scope.LoggedInUser.FirstName;
        $scope.UserId = $scope.LoggedInUser.UserID;
    }
    $scope.IsPrivateChat = 1;

    var startTimer = function () {
        $rootScope.securitytime++;
        $timeout(startTimer, 1000);
    }
    startTimer();

    $scope.GetSelectedCourseModuleNameById = function () {

        //alert($state.params.CourseID + $state.params.ModuleId)
        var Packages = new $.Courses({ CourseID: $state.params.CourseID, ModuleId: $state.params.ModuleId, UserId: $scope.LoggedInUser.UserID, AngularHTTP: $http });
        Packages.GetSelectedCourseModuleNameByIdQuery(function (e) {
            $scope.GetMandateCourseModuleNameById = e;

        })
    }


    $scope.ReLoadSlide = function (pageid, hasQuiz) {

        var Packages = new $.Courses({ PageId: pageid, UserId: $scope.LoggedInUser.UserID, AngularHTTP: $http });
        Packages.GetPageDetail(function (e) {
            $('.mobile-view').removeClass('nav-show');
            $('.overlay').hide();
            $scope.SelectedPage = e.Table[0];
            if ($scope.SelectedPage.hasQuiz) {
                $state.go('Training.QuickCheck', { PageId: pageid }, { reload: 'Training.QuickCheck' });
            }
            else {
                $state.go('Training.Slide', { PageId: pageid }, { reload: 'Training.Slide' });
            }
        })

    }


    $scope.selectTraining = function (isSelect) {
        
            //debugger
            if (!isSelect) {
                $scope.isSelectedAll = false;
            }
            else {
                var selectAll = true;
                $.grep($scope.Trainings, function (value, index) {
                    if (value.Selected == false) {
                        selectAll = false;
                    }
                });
                if (selectAll) {
                    $scope.isSelectedAll = true;
                } else {
                    $scope.isSelectedAll = false;
                }
            }
        
        

    }

    $scope.LoadSlide = function (pageid, hasQuiz, DisplaySequence, o) {

         

        //alert('Total Pages :' + $scope.Pages.length + '   Current Page: ' + DisplaySequence)
		if(pageid == undefined){
			var toast = $mdToast.simple().content("instructor has not moved the slide.").position('right bottom').action('OK').highlightAction(false).hideDelay(2000);
			$mdToast.show(toast);
		}
        else if ($scope.currPageId != pageid) {
            var Packages = new $.Courses({ PageId: pageid, UserId: $scope.LoggedInUser.UserID, AngularHTTP: $http });
            Packages.GetPageDetail(function (e) {
                $('.mobile-view').removeClass('nav-show');
                $('.overlay').hide();
                $scope.SelectedPage = e.Table[0];
                if ($scope.SelectedPage.hasQuiz) {
                    $state.go('Training.QuickCheck', { PageId: pageid }, { reload: 'Training.QuickCheck' });
                }
                else {

                    $state.go('Training.Slide', { PageId: pageid }, { reload: 'Training.Slide' });
		
                     
                    if ($scope.Pages.length == DisplaySequence) {

                         
                        var UserSession = new $.Courses({ PageId: pageid, UserId: $scope.LoggedInUser.UserID, ModuleId: $state.params.ModuleId, AngularHTTP: $http });
                        UserSession.SaveUserTrainingSessionQuery(function (e) {
                            $scope.SelectedPage.TrainingStatusId = 3

                            try {

                                //jQuery.grep($scope.Pages, function (a) {
                                //    return a.PageId == $scope.SelectedPage.NextPageNumber;
                                //})[0].isEnable = true;
                                 
                                $state.go('Training.Slide', { PageId: pageid }, { reload: 'Training.Slide' });
                                o.call();

                            } catch (e) {
                                return false;
                            }

                        });

                    }
                    else
                    {
                        $state.go('Training.Slide', { PageId: pageid }, { reload: 'Training.Slide' });

                    }


                }
            })
        }
    }



    var Packages = new $.Courses({ ModuleId: $state.params.ModuleId, UserId: $scope.LoggedInUser.UserID, AngularHTTP: $http });
    $scope.GetSelectedCourseModuleNameById();
    Packages.GetSelectedPagesByModuleIdQuery(function (e) {
        $scope.Pages = e;

    })







    //Ask instructor js implemented in training



    ///      Manoranjan Dikshit (30 Sep 2019)


    /////////////////////////////////////////////////////////////////////

    $scope.UserMails = [];
    $scope.UserMail = {};

    $scope.QueryType = '1';

    $scope.test = 0;




    $scope.GetUserMails = function () {

        var obj = {
            UserId: $scope.LoggedInUser.UserID
        };

        APICallService.Post('AskInstructor', 'GetAskInstructor', obj).then(function (e) {

            $scope.UserMails = e.data.Table;

            for (var i = 0; i < e.data.Table1.length; i++) {
                $scope.UserMails.push(e.data.Table1[i]);
            }



        });

    }



    $scope.GetUserCourses = function () {
        //debugger
        APICallService.Get('Packages', 'GetUserCourses').then(function (data) {
            //debugger
            $scope.UserCourses = data.data;

        });

    }

    $scope.GetUserCourses();

    $scope.GetUserMailsBySupport = function () {

        $timeout.cancel(timer);


        var obj = {
            UserId: 0
        };

        APICallService.Post('AskInstructor', 'GetAskInstructor', obj).then(function (e) {

            $scope.UserMailsUnattended = e.data.Table;
            $scope.UserMailsResolved = e.data.Table1;

            $scope.UserMails = $scope.UserMailsUnattended;

        });


    }






    $scope.ViewMailTrail = function (UserMail) {


        $state.go('UserAskInstructorLookupMail', { 'Token': UserMail.Token });
    }

    $scope.ViewMailTrail2 = function (UserMail) {


        $state.go('SupportAskInstructorLookupMail', { 'Token': UserMail.Token });
    }


    $scope.GetTrailMails = function (o) {


        if (($state.current.name == 'SupportAskInstructorLookupMail') || ($state.current.name == 'UserAskInstructorLookupMail')) {
            var obj = {
                Token: $state.params.Token
            };

            APICallService.Post('AskInstructor', 'GetAskInstructorByToken', obj).then(function (e) {

                $scope.ViewTrailMailList = e.data;

                $scope.SelectedUserId = $scope.ViewTrailMailList[0].UserId;
                //console.log($scope.ViewTrailMailList);

                o.call();

            });



        }
        else {
            $timeout.cancel(timer);
        }




    }

    $scope.time = 0;

    //timer callback
    var timer = function () {

        $scope.GetTrailMails(function () {
            $scope.time += 1000;
            $('.mesgs').animate({ scrollTop: 10000 }, 1000);
            $timeout(timer, 10000);
        });


        //if ($scope.time < 50000) {
        //    $scope.time += 5000;
        //    console.log($scope.time);
        //    $timeout(timer, 1000);
        //}
    }

    //run!!




    if (($state.current.name == 'SupportAskInstructorLookupMail') || ($state.current.name == 'UserAskInstructorLookupMail')) {

        $timeout(timer, 10000);
    }
    else {
        $timeout.cancel(timer);
    }




    $scope.SubmitUserNewMail = function (CurrentIndex) {
        //debugger


        $scope.test = CurrentIndex;

        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;




        var obj = {
            UserId: $scope.LoggedInUser.UserID,
            Token: '0',
            //Subject: $scope.UserMail.Subject,
            Subject: $scope.UserMail.CourseName.CourseName,
            QueryDesc: $scope.UserMail.QueryDesc != undefined ? $scope.UserMail.QueryDesc : '',
            QueryBy: $scope.LoggedInUser.UserID,
            Status: 1,
            QueryOn: 1,
            SubmittedDate: dateTime
        };


        $scope.UserMailSubmitPromise = APICallService.Post('AskInstructor', 'SaveAskInstructor', obj).then(function (e) {
            //debugger

            if (e.data != '') {



                $scope.UserMail.QueryDesc = '';

                $scope.frmusermail2.$setPristine();  //reset 
                $scope.frmusermail2.$setValidity();
                $scope.frmusermail2.$setUntouched(); //reset 	

				$('#myModal').modal('hide');

                var toast = $mdToast.simple().content("Message sent successfully.").position('right bottom').action('OK').highlightAction(false).hideDelay(2000);
                $mdToast.show(toast);


                $timeout(function () {
                    $state.go('UserAskInstructorLookupMail', { 'Token': e.data });
                }, 2000);

            }



        });






    }


    $scope.btnvisible = true;

    $scope.fnFocus = function () {
        if ($scope.UserMail.QueryDesc != null && $scope.UserMail.QueryDesc != undefined && $scope.UserMail.QueryDesc != '') {
            $scope.btnvisible = false;
        }
        else {
            $scope.btnvisible = true;
        }
    }



    $scope.SubmitTrailMail = function (Status, QueryOn, CurrentIndex) {
        $scope.test = CurrentIndex;


        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;

        if ($scope.ViewTrailMailList[$scope.ViewTrailMailList.length - 1].Status == 2) {
            Status = $scope.ViewTrailMailList[$scope.ViewTrailMailList.length - 1].Status;
        }



        var obj = {
            UserId: $scope.ViewTrailMailList[0].UserId,
            Token: $scope.ViewTrailMailList[0].Token,
            Subject: $scope.ViewTrailMailList[0].Subject,
            QueryDesc: $scope.UserMail.QueryDesc,
            QueryBy: $scope.LoggedInUser.UserID,
            Status: Status,
            QueryOn: QueryOn,
            SubmittedDate: dateTime
        };

        if ($scope.UserMail.QueryDesc != null && $scope.UserMail.QueryDesc != undefined && $scope.UserMail.QueryDesc != '') {

            $scope.UserMailSubmitPromise = APICallService.Post('AskInstructor', 'SaveAskInstructor', obj).then(function (e) {


                if (e.data != '') {

                    $scope.UserMail.QueryDesc = '';

                    $scope.frmusermail2.$setPristine();  //reset 
                    $scope.frmusermail2.$setValidity();
                    $scope.frmusermail2.$setUntouched(); //reset 	

                    $scope.GetTrailMails(function () {
                        $('.mesgs').animate({ scrollTop: 10000 }, 1000);
                    });


                    var toast = $mdToast.simple().content("Message sent successfully.").position('right bottom').action('OK').highlightAction(false).hideDelay(2000);
                    $mdToast.show(toast);



                    //$timeout(function () {
                    //    if (QueryOn == 1) {
                    //        //$state.go('AskInstructorDashboard');
                    //    }
                    //    else if (QueryOn == 2) {
                    //        //$state.go('SupportAskInstructorDashboard');

                    //    }
                    //}, 2000);

                }



            });
        }
    }


    $scope.SubmitTrailMail2 = function (Status, QueryOn, CurrentIndex) {

        //debugger

        $scope.test = CurrentIndex;

        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;





        if (Status == 2) {

            var obj = {
                UserId: $scope.ViewTrailMailList[0].UserId,
                Token: $scope.ViewTrailMailList[0].Token,
                Subject: $scope.ViewTrailMailList[0].Subject,
                QueryDesc: $scope.UserMail.QueryDesc,
                QueryBy: $scope.LoggedInUser.UserID,
                Status: Status,
                QueryOn: QueryOn,
                SubmittedDate: dateTime
            };


            if ($scope.UserMail.QueryDesc != null && $scope.UserMail.QueryDesc != undefined && $scope.UserMail.QueryDesc != '') {

                $scope.UserMailSubmitPromise = APICallService.Post('AskInstructor', 'SaveAskInstructor', obj).then(function (e) {


                    if (e.data != '') {


                        $scope.UserMail.QueryDesc = '';

                        $scope.frmusermail2.$setPristine();  //reset 
                        $scope.frmusermail2.$setValidity();
                        $scope.frmusermail2.$setUntouched(); //reset 	

                        $scope.GetTrailMails(function () {
                            $('.mesgs').animate({ scrollTop: 10000 }, 1000);
                        });

                        var toast = $mdToast.simple().content("Message sent successfully.").position('right bottom').action('OK').highlightAction(false).hideDelay(2000);
                        $mdToast.show(toast);

                    }



                });

            }

        }
        else if (Status == 3) {


            var _testConfirm = confirm('Are you sure, you want to close the ticket?');


            if (_testConfirm) {




                var obj = {
                    UserId: $scope.ViewTrailMailList[0].UserId,
                    Token: $scope.ViewTrailMailList[0].Token,
                    Subject: $scope.ViewTrailMailList[0].Subject,
                    QueryDesc: 'Ticket Closed',
                    QueryBy: $scope.LoggedInUser.UserID,
                    Status: Status,
                    QueryOn: QueryOn,
                    SubmittedDate: dateTime
                };

                $scope.UserMailSubmitPromise = APICallService.Post('AskInstructor', 'SaveAskInstructor', obj).then(function (e) {

                    if (e.data != '') {


                        $scope.UserMail.QueryDesc = '';

                        $scope.frmusermail2.$setPristine();  //reset 
                        $scope.frmusermail2.$setValidity();
                        $scope.frmusermail2.$setUntouched(); //reset 	

                        $scope.GetTrailMails(function () {
                            $('.mesgs').animate({ scrollTop: 10000 }, 1000);
                        });

                        var toast = $mdToast.simple().content("Ticket closed successfully.").position('right bottom').action('OK').highlightAction(false).hideDelay(2000);
                        $mdToast.show(toast);



                        //$timeout(function () {
                        //    if (QueryOn == 1) {
                        //        //$state.go('AskInstructorDashboard');
                        //    }
                        //    else if (QueryOn == 2) {
                        //        //$state.go('SupportAskInstructorDashboard');

                        //    }
                        //}, 2000);

                    }



                });

            }

        }


    }




    $scope.accordianclk = function (CurrentIndex) {

        //alert(1);


        $('.panel-heading').removeClass('active');
        $('.panel-heading:eq(' + CurrentIndex + ')').addClass('active');

        $('.panel-collapse').removeClass('in');
        $('.panel-collapse:eq(' + CurrentIndex + ')').addClass('in');


    }

    /* Code for Support Section  */

    $scope.selectedIndex = 0;
    $scope.CompleteTabclk = function () {
        $scope.selectedIndex = 1;
    }
    $scope.ActiveTabclk = function () {
        $scope.selectedIndex = 0;
    }

    /* Code for Support Section  */



    $scope.DisplayDataByQueryType = function () {

        $scope.displayData($scope.QueryType)
    }


    $scope.displayData = function (QueryType) {


        if ($scope.QueryType == 1) {
            $scope.UserMails = [];
            $scope.UserMails = $scope.UserMailsUnattended;
        }
        else if ($scope.QueryType == 2) {

            $scope.UserMails = [];
            $.grep($scope.UserMailsResolved, function (element, index) {

                if (element.Status == 2) {
                    $scope.UserMails.push(element);
                }



            });

        }
        else if ($scope.QueryType == 3) {
            $scope.UserMails = [];
            $.grep($scope.UserMailsResolved, function (element, index) {

                if (element.Status == 3) {
                    $scope.UserMails.push(element);
                }
            });
        }

    }

    $scope.OpenModal = function () {
        $('#myModal').modal('show');
    }
	//--face recognition
	 var CapturedImage;
        $rootScope.faceRecog = 1;
        $scope.takepicture = function () {

            var context = canvas.getContext('2d');
            width = 480;
            height = 480
            if (width && height) {
                canvas.width = width;
                canvas.height = height;
                context.drawImage(video, 0, 0, width, height);
                CapturedImage = canvas.toDataURL('image/png');
                console.log(CapturedImage);
                uploadPic();
                // photo.setAttribute('src', data);
            } else {
                clearphoto();
            }
        }
        if (AuthService.GetCredential().FaceDetection) {
            //processImage(2, SERVER_PATH + 'upload/UserProfile/User_' + $scope.LoggedInUser.UserID + ".jpg");
            //processImage(2, 'https://iasir.sortelearn.com/api/upload/temp/UserTraing_10087.jpg');
            //$rootScope.TrainingPicInterval = setTimeout($scope.takepicture, 15000);

        }
		
        function uploadPic() {
            CapturedImage = CapturedImage.replace('data:image/png;base64,', '');
			$rootScope.profileMatching = 'Uploading new picture';
            APICallService.Post('User', 'UploadUserPic', { 'Image': CapturedImage }).then(function (res) {

					$rootScope.profileMatching = 'Detecting you';
               // processImage(1, 'https://upload.wikimedia.org/wikipedia/commons/c/c3/RH_Louise_Lillian_Gish.jpg', checkProfilePic);
               processImage(1, SERVER_PATH + 'upload/temp/' + res.data + '?d=' + new Date(), compare);
			  //processImage(1, 'https://iasir.sortelearn.com/api/upload/UserProfile/User_10087.jpg', compare);
            }).then(function (error) {
                 
            });
        }

        // function checkProfilePic() {
			// debugger
            // var url = SERVER_PATH + '/';
           // //processImage(2, 'https://iasir.sortelearn.com/api/upload/UserProfile/User_10073.jpg', compare);
            // //processImage(2, SERVER_PATH + 'upload/UserProfile/User_' + $scope.LoggedInUser.UserID +".jpg", compare);
			
        // }
        function compare() {

            CompareFaces(function (d) {
				//debugger
				//alert(JSON.stringify(d));
				if(d) {
                if (d.confidence * 100 < 40) {
                    ProfileNotMatched();
                }
                else {
                   
					
					$rootScope.profileMatching = 'Profile Matching'
                    $('#faceProfile').removeClass('profile-not-mathing');
                    $('#faceProfile').addClass('profile-mathing');
                }
				}
				else  {
				ProfileNotMatched();
				}
				if($rootScope.faceRecog == 1) {
					setTimeout( $scope.takepicture,15000);
				}
               
			   
            });
        }
		
		function ProfileNotMatched() {
		 
					$rootScope.profileMatching = 'Profile not Matching'
                    $('#faceProfile').addClass('profile-not-mathing');
                    $('#faceProfile').removeClass('profile-mathing');
                    $('#faceProfile').css('color', 'red');
                   // $('#profileMatch').modal('show');
		}
		
		$scope.LoadNextModule = function (NextModuleID, pageid, DisplaySequence, o) {
        //debugger
        //var Packages = new $.Courses({ CourseID: $state.params.CourseID, ModuleId: NextModuleID, UserId: $scope.LoggedInUser.UserID, AngularHTTP: $http });
        //Packages.GetSelectedCourseModuleNameByIdQuery(function (e) {
        //    //debugger
        //    $scope.GetMandateCourseModuleNameById = e;



        //    //var UserSession = new $.Courses({ PageId: NextModulePageId, UserId: $scope.LoggedInUser.UserID, ModuleId: NextModuleID, AngularHTTP: $http });
        //    //UserSession.UpdateCurrentPageNo();

        //    $state.go('Training.QuickCheck', { PageId: NextModulePageId }, { reload: 'Training.QuickCheck' });




        //})




        if ($scope.currPageId != pageid) {
            var Packages = new $.Courses({ PageId: pageid, UserId: $scope.LoggedInUser.UserID, AngularHTTP: $http });
            Packages.GetPageDetail(function (e) {
                //debugger
                $('.mobile-view').removeClass('nav-show');
                $('.overlay').hide();
                $scope.SelectedPage = e.Table[0];
                if ($scope.SelectedPage.hasQuiz) {
                    $state.go('Training.QuickCheck', { PageId: pageid }, { reload: 'Training.QuickCheck' });
                }
                else {

                    //debugger 
                    //$state.go('Training.Slide', { PageId: pageid }, { reload: 'Training.Slide' });
                    $state.go('Training.Slide', { CourseID: $state.params.CourseID, ModuleId: NextModuleID, PageId: pageid })
                }
            })
        }

    }
	//debugger;
	var today = new Date();
    var dd = today.getDate();

    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
       dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    } 
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyCMTnt5DVidcB-MoKoPsImSCAVr3NtXvvk",
        authDomain: "sort-4c7be.firebaseapp.com",
        databaseURL: "https://sort-4c7be.firebaseio.com",
        projectId: "sort-4c7be",
        storageBucket: "sort-4c7be.appspot.com",
        messagingSenderId: "745978722531",
        appId: "1:745978722531:web:93d675f65616679d96add6",
        measurementId: "G-NDL7YHVN50"
    };
	$scope.firebase = firebase;
	if($scope.firebase.apps.length ==0){
		$scope.firebase.initializeApp(config);
	}
	else{
		// firebase.app().delete().then(function() {
			// firebase.initializeApp(config);
		// });
	}
    
	// get firebase database reference...
    $scope.firebasedb_ref = $scope.firebase.database().ref('/');
    $scope.tsid_node_ref = $scope.firebasedb_ref.child(localStorage.getItem("TSId"));
	$scope.chatUsers = [];
	var msgHistory = document.querySelector('#history');
	
    var user = $scope.firebase.auth().signInAnonymously();

    $scope.firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            // User is signed in.
            var isAnonymous = user.isAnonymous;
            user_id = user.uid;
			$scope.tsid_node_ref.on('child_added', function (data) {         
				if (data != undefined && data.val() != undefined) {
					var msg_info = data.val();
					if (msg_info.slide_no != undefined && parseInt(msg_info.slide_no) > 0) {
						var notice = document.getElementById('notice');                
						$scope.notice = 'Instructor - ' + msg_info.slide_no;
						notice.innerHTML = 'Instructor - ' + msg_info.slide_no;
						$scope.PageId = msg_info.page_id;
						$scope.hasQuiz = msg_info.has_quiz;
						$scope.LoadSlide(msg_info.page_id, msg_info.has_quiz, msg_info.slide_no);
						//showNotice(event.data);
					}
					else if ($scope.UserId == msg_info.userId || msg_info.userId == 0) {
						var msg = document.createElement('p');
						msg.textContent = msg_info.userName + ":" + msg_info.message;
						msg.className = msg_info.user_id == user_id ? 'mine' : 'theirs';
						msgHistory.appendChild(msg);
						msg.scrollIntoView();

						//Show Active User List                
						var existCount = 0;
						$.grep($scope.chatUsers, function (ele, ind) {
							if (ele.UserId == parseInt(msg_info.userId)) {
								existCount = 1;
							}
						})

						if (existCount == 0 && parseInt(msg_info.userId) > 0) {
							$scope.chatUsers.push({ UserId: parseInt(msg_info.userId), UserName: msg_info.userName });
						}
						$scope.activeUser = $scope.chatUsers.length;
					}

				}
				
			});

        } else {
            // User is signed out.
        }
    });
    
    var uName = document.querySelector('.name');
    var msgTxt = document.querySelector('#msgTxt');

    // Send a signal once the user enters data in the form
    var btn = document.getElementById('btnChat');
    btn.addEventListener('click', function (e) {
		if(msgTxt.value != ''){
			$scope.tsid_node_ref.push({
				user_id: user_id,
				userId: $scope.UserId,
				userName: uName.innerHTML,
				message: msgTxt.value
			});
			//SAVE HISTORY IN DATABASE
			$scope.setChatHistory($scope.UserId + '-' + uName.innerHTML + ': ' + msgTxt.value);
			//CLEAR MESSAGE TEXT
			msgTxt.value = '';
		}
    });           
  


    $scope.setChatHistory = function (msg) {
        var obj = {
            TSId: localStorage.getItem('TSId'),
            MSG: msg,
            UserId: $scope.LoggedInUser.UserID,
        };
        APICallService.Post('AskInstructor', 'setChatHistory', obj).then(function (e) {
            //Inserted            
        });

    }

 ////   var apiKey = "46582492";
 ////   //var sessionId = "1_MX40NjU4MjQ5Mn5-MTU4NDY5ODI3NzY5MH5kN1RiNU1MT2F4NFF0Y240c1VNb21CQ3l-fg";
	//////var token = "T1==cGFydG5lcl9pZD00NjU4MjQ5MiZzaWc9ODAwMzcxMGE2ZWYwMDEwMDIyOTIzY2I1NmMwNmJhNTlkOGY5NDU5YzpzZXNzaW9uX2lkPTFfTVg0ME5qVTRNalE1TW41LU1UVTRORFk1T0RJM056WTVNSDVrTjFSaU5VMU1UMkY0TkZGMFkyNDBjMVZOYjIxQ1EzbC1mZyZjcmVhdGVfdGltZT0xNTg3MzgxMjI3Jm5vbmNlPTAuNjI2OTE0NDY5OTY1NDcyNyZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNTg5OTczMjI1JmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9";  
	////var sessionId = localStorage.getItem('SessionId');
	////var token = localStorage.getItem('TokenId');
 ////   if ($scope.LoggedInUser != null) {
 ////       $scope.UserName = $scope.LoggedInUser.FirstName;
 ////   }
 ////   // Handling all of our errors here by alerting them
 ////   function handleError(error) {
 ////       if (error) {
 ////           alert(error.message);
 ////       }
 ////   }

 ////   // (optional) add server code here
 ////   var pb_session;

 ////   $scope.setChatHistory = function (msg) {
 ////       var obj = {
 ////           TSId: localStorage.getItem('TSId'),
 ////           MSG: msg,
 ////           UserId: $scope.LoggedInUser.UserID,
 ////       };
 ////       APICallService.Post('AskInstructor', 'setChatHistory', obj).then(function (e) {
 ////           //Inserted            
 ////       });

 ////   }

 ////   $scope.initializeSession = function () {
 ////       debugger;
 ////       if (pb_session) {
 ////           if (pb_session.isConnected) {
 ////               pb_session.disconnect();
 ////           }
 ////       }
 ////       pb_session = OT.initSession(apiKey, sessionId);
 ////       $rootScope.tokSession = pb_session;
 ////       // Subscribe to a newly created stream
 ////       pb_session.on('streamCreated', function (event) {
 ////           // pb_session.subscribe(event.stream, 'subscriber', {
 ////               // insertMode: 'replace',
 ////               // width: '150px',
 ////               // height: '150px'
 ////           // }, handleError);
 ////       });
	////	 pb_session.on("streamDestroyed", function(event) {
	////	   // alert("Stream " + event.stream.name + " ended. " + event.reason);
	////	   // $('#videos').append('<div id="subscriber" ></div>');        
	////		// dragElement(document.getElementById("subscriber"));
	////	}).connect(token,function(error) {
	////		debugger
	////		if (error) {
	////					handleError(error);
	////				} else {
	////					//  session.publish(publisher, handleError);
	////					//$(btn).removeAttr('disabled');
	////					$scope.startChat();
	////				}
	////	});
 ////       // Create a publisher
 ////       //var publisher = OT.initPublisher('publisher', {
 ////       //    insertMode: 'append',
 ////       //    width: '100%',
 ////       //    height: '100%'
 ////       //}, handleError);


 ////       // Connect to the session
 ////       pb_session.connect(token, function (error) {
 ////           // If the connection is successful, initialize a publisher and publish to the session
 ////           if (error) {
 ////               handleError(error);
 ////           } else {
 ////               //  session.publish(publisher, handleError);
 ////               //$(btn).removeAttr('disabled');
 ////               $scope.startChat();
 ////           }
 ////       });
 ////   }
 ////   debugger;
 ////   if (localStorage.getItem('SchedDate') == mm + '/' + dd + '/' + yyyy) {
 ////       $scope.initializeSession();
 ////   }
    

 ////   $scope.startChat = function () {
 ////       debugger
 ////       // Receive a message and append it to the history
 ////       var msgHistory = document.querySelector('#history');

 ////       var obj = {
 ////           TSId: localStorage.getItem('TSId'),
 ////       };
 ////       APICallService.Post('AskInstructor', 'GetChatHistory', obj).then(function (e) {
 ////           $.grep(e.data, function (element, index) {
 ////               var chatText = element.MSG.split('-');
 ////               if (chatText.length > 0) {
 ////                   if ($scope.UserId == chatText[0] || chatText[0] == 0) {
 ////                       var msg = document.createElement('p');
 ////                       msg.textContent = chatText[1];
 ////                       msgHistory.appendChild(msg);            
 ////                   }
 ////               }
               
 ////           });
 ////       });

 ////       var notice = document.getElementById('notice');
 ////       pb_session.on('signal:msg', function signalCallback(event) {
 ////         debugger
 ////           var slideNo = event.data.split('slide:');
 ////           if (slideNo.length > 1) {
 ////               var notice = document.getElementById('notice');
 ////               var slideInfo = event.data.split(':')[1].split('-');
 ////               $scope.notice = 'Instructor - ' + slideInfo[0];
 ////               notice.innerHTML = 'Instructor - ' + slideInfo[0];
 ////               $scope.PageId = slideInfo[1];
 ////               $scope.hasQuiz = slideInfo[2];
 ////               $scope.LoadSlide(slideInfo[1],slideInfo[2],slideInfo[0]);
 ////               //showNotice(event.data);
 ////           } else {
 ////               var chatText = event.data.split('-');
 ////               if ($scope.IsPrivateChat == 1) {
 ////                   if (chatText.length > 0) {
 ////                       if ($scope.UserId == chatText[0] || chatText[0] == 0) {
 ////                           var msg = document.createElement('p');
 ////                           msg.textContent = chatText[1];

 ////                           msg.className = event.from.connectionId === pb_session.connection.connectionId ? 'mine' : 'theirs';
 ////                           msgHistory.appendChild(msg);
 ////                           msg.scrollIntoView();
 ////                       }
 ////                   }
 ////               }
 ////               else {
 ////                   var msg = document.createElement('p');
 ////                   msg.textContent = chatText[1];
 ////                   msg.className = event.from.connectionId === pb_session.connection.connectionId ? 'mine' : 'theirs';
 ////                   msgHistory.appendChild(msg);
 ////                   msg.scrollIntoView();
 ////               }
 ////           }
 ////       });


 ////       // Text chat
 ////       var form = document.querySelector('form');
 ////       var uName = document.querySelector('.name');
 ////       debugger
 ////       var msgTxt = document.querySelector('#msgTxt');

 ////       // Send a signal once the user enters data in the form
 ////       var btn = document.getElementById('btnChat');
 ////       btn.addEventListener('click', function (e) {
 ////           pb_session.signal({
 ////               type: 'msg',
 ////               data: $scope.UserId + '-' + uName.innerHTML + ': ' + msgTxt.value
 ////           }, function signalCallback(error) {
 ////               if (error) {
 ////                   console.error('Error sending signal:', error.name, error.message);
 ////               } else {
 ////                   var chatText = $scope.UserId + '-' + $scope.UserName + ': ' + msgTxt.value
 ////                   $scope.setChatHistory(chatText);
 ////                   msgTxt.value = '';
 ////               }
 ////           });
 ////       });
 ////   }
	
	
	
	debugger
	//New Code
	if (localStorage.getItem('SchedDate') == mm + '/' + dd + '/' + yyyy) {
		$("#videos").css("display","block");
		var token = 'sdfdsfsf';//"<%= request.getParameter("token") %>";
		//var start_play_button = document.getElementById("start_play_button");
		//var stop_play_button = document.getElementById("stop_play_button");

		//var streamNameBox = document.getElementById("streamName");

		$scope.streamId = localStorage.getItem('StreamId');

		function getUrlParameter(sParam) {
			var sPageURL = decodeURIComponent(window.location.search.substring(1)),
				sURLVariables = sPageURL.split('&'),
				sParameterName,
				i;

			for (i = 0; i < sURLVariables.length; i++) {
				sParameterName = sURLVariables[i].split('=');

				if (sParameterName[0] === sParam) {
					return sParameterName[1] === undefined ? true : sParameterName[1];
				}
			}
		};

		var name = getUrlParameter("name");
		if (name !== "undefined") {
			//streamNameBox.value = name;
		}

		//streamId = streamNameBox.value;
		$scope.startPlaying = function() {
			debugger
			//streamId = streamNameBox.value;
			$scope.webRTCAdaptor.play($scope.streamId, token);
		}

		function stopPlaying() {
			playing = false;
			$scope.webRTCAdaptor.stop($scope.streamId);
		}

		var pc_config = null;

		var sdpConstraints = {
			OfferToReceiveAudio: true,
			OfferToReceiveVideo: true

		};
		var mediaConstraints = {
			video: false,
			audio: false
		};

		var path = "webstream1.westus.cloudapp.azure.com:5080/WebRTCAppEE/websocket";
		var websocketURL = "ws://" + path;

		if (location.protocol.startsWith("https")) {
			websocketURL = "wss://webstream1.westus.cloudapp.azure.com:5443/WebRTCAppEE/websocket"
		}
		var playing = false;
		$rootScope.UserTypeId =  $scope.LoggedInUser.UserTypeId;
		
		$scope.checkStream = function (){
			if (!playing) {
				var stream = $scope.webRTCAdaptor.getStreamInfo($scope.streamId);
				console.log(stream);
			}
		}
		$scope.webRTCAdaptor = new WebRTCAdaptor({
			websocket_url: websocketURL,
			mediaConstraints: mediaConstraints,
			peerconnection_config: pc_config,
			sdp_constraints: sdpConstraints,
			remoteVideoId: "subscriber",
			isPlayMode: true,
			debug: true,
			callback: function (info, description) {
				console.log(info + ":" + description);
				if (info == "initialized") {
					console.log("initialized");
					//start_play_button.disabled = false;
					//stop_play_button.disabled = true;
				} else if (info == "play_started") {
					//joined the stream
					console.log("play started");
					//start_play_button.disabled = true;
					//stop_play_button.disabled = false;
					playing = true;
					

				}
				else if (info == "streamInformation") {
					if (!playing) {
						playing = true;
						$scope.startPlaying();
					   
					}
					console.log(JSON.stringify(description));
				}
				else if (info == "play_finished") {
					//leaved the stream
					console.log("play finished");
					//start_play_button.disabled = false;
					//stop_play_button.disabled = true;
					playing = false;
				} else if (info == "closed") {
					playing = false;
					//console.log("Connection closed");
					if (typeof description != "undefined") {
						console.log("Connecton closed: "
								+ JSON.stringify(description));
					}
				}
			},

			callbackError: function (error) {
				//some of the possible errors, NotFoundError, SecurityError,PermissionDeniedError

				console.log("error callback: " + JSON.stringify(error));
			   // alert(JSON.stringify(error));
			}
		});
		 
		debugger
		// setTimeout(function () {
			// debugger
			// if (!playing) {
				// var stream = $scope.webRTCAdaptor.getStreamInfo(streamId);
				// console.log(stream);
			// }
		// }, 3000);
		$scope.InterVal = window.setInterval($scope.checkStream,3000);
	
	
	
       $scope.startPlaying();
    }	
	
	$scope.$on("$destroy",function (event) 
	{
		if($scope.InterVal !=null){
			clearInterval($scope.InterVal);
		}
		if($scope.webRTCAdaptor != null && $scope.webRTCAdaptor != undefined){
			$scope.webRTCAdaptor.leave($scope.streamId);
		}
	});
	
	var chkaudio = document.querySelector('audio');
	
	var chkaudioconstraints = window.constraints = {
	  audio: true,
	  video: false
	};
	
	$scope.CheckAudioVideo = function(){
		$('#mdCheckDevice').modal('show');
	}
	$scope.CallTestAudio = function(){
		debugger;
		if($scope.isStartAudioTest == undefined || $scope.isStartAudioTest == false){
			$scope.TestAudio();
		}
		else{
			$scope.StopTestAudio();
		}
	}
	$scope.TestAudio = function(){
		navigator.mediaDevices.getUserMedia(chkaudioconstraints).then(function(stream){
			const audioTracks = stream.getAudioTracks();
			stream.oninactive = function() {
			console.log('Stream ended');
			};
			window.stream = stream; // make variable available to browser console
			chkaudio.srcObject = stream;
		}).catch(function(error){
			const errorMessage = 'navigator.MediaDevices.getUserMedia error: ' + error.message + ' ' + error.name;
			errorMsgElement.innerHTML = errorMessage;
			console.log(errorMessage);
		});	
		$scope.isStartAudioTest = true;
	}
	$scope.StopTestAudio = function(){
		navigator.mediaDevices.getUserMedia(chkaudioconstraints).then(function(stream){			
			window.stream = null;
			chkaudio.srcObject = null;
		}).catch(function(error){});
		$scope.isStartAudioTest = false;		
	}
	$('#mdCheckDevice').on('hidden.bs.modal', function () {			
		$scope.StopTestAudio();		
	});
	//End COde

		
});