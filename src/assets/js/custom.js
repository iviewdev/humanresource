jQuery(function(){
   // alert('inside loads');
});

function showPanel(id) {
    var targePanel = id;
    $(targePanel).addClass('show-p');
    localStorage.setItem("data-target", targePanel);
    $('.backdrop').addClass('show-p');
}
// Hide all Panels
function hidePanel() {
    var targetPanel = localStorage.getItem("data-target");
    $(targetPanel).removeClass('show-p');
    $('.backdrop').removeClass('show-p');
    localStorage.removeItem("data-target");
}