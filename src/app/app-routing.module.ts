import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { RootComponent } from './components/root/root.component';
import { HomeComponent } from './website/home/home.component';
import { SearchDetailComponent } from './website/search/search-detail/search-detail.component';
import { SearchComponent } from './website/search/search.component';
const routes: Routes = [
  {
    path: '',
    component: RootComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'search', component: SearchComponent },
      { path: 'search-detail/:id', component: SearchDetailComponent },
    ]
  },

  {
    path: 'jobseeker',
    loadChildren: () => import('./modules/jobseeker/jobseeker.module').then(m => m.JobseekerModule)
  },
  {
    path: 'freelancer',
    loadChildren: () => import('./modules/freelancer/freelancer.module').then(m => m.FreelancerModule)
  },
  {
    path: 'account',
    loadChildren: () => import('./modules/account/account.module').then(m => m.AccountModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule)
  },
  {
    path: 'marketplace',
    loadChildren: () => import('./modules/marketplace/marketplace.module').then(m => m.MarketplaceModule)
  },
  {
    path: 'employer',
    loadChildren: () => import('./modules/employer/employer.module').then(m => m.EmployerModule)
  },
  {
    path: 'job-search',
    component: RootComponent,
    children: [
      {
        path: 'job',
        loadChildren: () => import('./job-search/job-search.module').then(m => m.JobSearchModule)
      }
    ]

  }


];






// const routes: Routes = [
//   { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
//   { path: 'dashboard', component: DashboardComponent },
//   { path: 'widgets', component: WidgetsComponent },
//   { path: 'accordions', component: AccordionsComponent },
//   { path: 'buttons', component: ButtonsComponent },
//   { path: 'badges', component: BadgesComponent },
//   { path: 'breadcrumbs', component: BreadcrumbsComponent },
//   { path: 'dropdowns', component: DropdownsComponent },
//   { path: 'dropdowns', component: DropdownsComponent },
//   { path: 'modals', component: ModalsComponent },
//   { path: 'progressbar', component: ProgressbarComponent },
//   { path: 'pagination', component: PaginationComponent },
//   { path: 'tabs', component: TabsComponent },
//   { path: 'typography', component: TypographyComponent },
//   { path: 'tooltips', component: TooltipsComponent },
//   { path: 'dragula', component: DragulaComponent },
//   { path: 'clipboard', component: ClipboardComponent },
//   { path: 'context-menu', component: MyContextMenuComponent },
//   { path: 'slider', component: SliderComponent },
//   { path: 'carousel', component: CarouselComponent },
//   { path: 'loaders', component: LoadersComponent },
//   { path: 'basic-elements', component: BasicElementsComponent },
//   { path: 'advanced-elements', component: AdvancedElementsComponent },
//   { path: 'validation', component: ValidationComponent },
//   { path: 'wizard', component: WizardComponent },
//   { path: 'text-editor', component: TextEditorComponent },
//   { path: 'code-editor', component: CodeEditorComponent },
//   { path: 'chartjs', component: ChartjsComponent },
//   { path: 'chartist', component: ChartistComponent },
//   { path: 'morris', component: MorrisComponent },
//   { path: 'basic-table', component: BasicTableComponent },
//   { path: 'data-table', component: DataTableComponent },
//   { path: 'popups', component: PopupsComponent },
//   { path: 'notifications', component: NotificationsComponent },
//   { path: 'flag-icons', component: FlagIconsComponent },
//   { path: 'mdi', component: MdiComponent },
//   { path: 'font-awesome', component: FontAwesomeComponent },
//   { path: 'simple-line-icons', component: SimpleLineIconsComponent },
//   { path: 'themify', component: ThemifyComponent },
//   { path: 'google-map', component: GoogleMapComponent },
//   { path: 'login', component: LoginComponent },
//   { path: 'login-2', component: Login2Component },
//   { path: 'register', component: RegisterComponent },
//   { path: 'register-2', component: Register2Component },
//   { path: 'lock-screen', component: LockscreenComponent },
//   { path: 'error-500', component: Page500Component },
//   { path: 'error-404', component: Page404Component },
//   { path: 'blank-page', component: BlankPageComponent },
//   { path: 'profile', component: ProfileComponent },
//   { path: 'faq', component: FaqComponent },
//   { path: 'faq-2', component: Faq2Component },
//   { path: 'news-grid', component: NewsGridComponent },
//   { path: 'timeline', component: TimelineComponent },
//   { path: 'search-results', component: SearchResultsComponent },
//   { path: 'portfolio', component: PortfolioComponent },
//   { path: 'invoice', component: InvoiceComponent },
//   { path: 'pricing', component: PricingComponent },
//   { path: 'orders', component: OrdersComponent },
//   { path: 'email', component: EmailComponent },
//   { path: 'calendar', component: CalendarComponent },
//   { path: 'todo-list', component: TodoListComponent },
//   { path: 'documentation', component: DocumentationComponent },
//   { path: '**', redirectTo: '/error-404'}
// ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
