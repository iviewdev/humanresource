import { Injectable } from '@angular/core';
import { DataService } from '../../../shared/services/data.service';

import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StorageService } from 'src/app/shared/services/storage.service';
@Injectable({
  providedIn: 'root'
})
export class EmployerService {
  public tenantid: string = '';
  constructor(private dataService: DataService, private _http: HttpClient,
    private storage: StorageService) {
    this.tenantid = this.storage.getItem('tenantid');
  }

  headerOptions = {
    headers: new HttpHeaders({
      'X-TenantID': this.storage.getItem('tenantid')
    })
  };

  updatePassword(d: any) {
    return this.dataService.put('corpidentity/updatepassword', d, this.headerOptions);
  }

  createbasicprofile(d: any) {
    return this.dataService.put('corpregister/createbasicprofile', d, this.headerOptions);
  }

  getbasicprofile(d: any) {
    return this.dataService.get('corpregister/getbasicprofile/' + d, this.headerOptions);
  }


  getAllCountries() {
    return this.dataService.get('elasticsearch/getAllCountries');
  }
  getAllStatesForCountry(countryName: string) {
    return this.dataService.get('elasticsearch/getAllStatesForCountry/' + countryName);
  }

  getAllDistrictsForState(stateName: string) {
    return this.dataService.get('elasticsearch/getAllDistrictsForState/' + stateName);
  }

  uploadFile(addressProof: any) {
    return this.dataService.put('corpregister/uploadFile', addressProof, this.headerOptions);
  }

  updateAddress(data: any) {
    return this.dataService.put('corpregister/updateAddress/', data, this.headerOptions);
  }

  updateOtherCorporateDetails(data: any) {
    return this.dataService.put('corpregister/updateOtherCorporateDetails', data, this.headerOptions);
  }

}
