import { HttpResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { element } from 'protractor';
import { SortPipe } from 'src/app/shared/pipes/sort.pipe';
import { StorageService } from 'src/app/shared/services/storage.service';
import { EmployerService } from '../../service/employer.service';
import { SpinnerService } from '../../service/spinner.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  @ViewChild('pTabs', { static: true }) pTabs: NgbTabset;
  public employerRecord: any = {};
  public username: string;
  public basicProfileForm: FormGroup;
  public addressForm: FormGroup;
  public otherDetailsForm: FormGroup;
  public submitted_basic_form: boolean = false;
  public submitted_address_form: boolean = false;
  public submitted_other_details = false;
  public basicProfileId: any;
  constructor(private fb: FormBuilder,
    private storageService: StorageService,
    private toastr: ToastrService, private empService: EmployerService,
    private spinner: SpinnerService,
    private sortPipe: SortPipe) { }

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.createBasicProfile();
    this.createAddressForm();
    this.createOtherDetailsForm();
    this.getAllCountries();
    this.getBasicProfile();

  }

  //go previous
  navigateTabById(tabid) {
    if (tabid) {
      this.pTabs.select(tabid);
    }
  }

  //Create Basic Profile form

  createBasicProfile() {
    this.basicProfileForm = this.fb.group({
      uid: [''],
      username: [this.username],
      orgname: ['', Validators.required],
      orgtype: ['', Validators.required],
      registeredphone: ['', Validators.required],
      registeredfax: ['', Validators.required],
      website: ['', Validators.required],
    });
  }

  //get Basic Profile

  getBasicProfile() {
    this.empService.getbasicprofile(this.username).subscribe((res: any) => {
      if (res) {
        this.employerRecord = res;
        this.basicProfileId = res.uid;
        this.basicProfileForm.patchValue(res);
        this.addressForm.patchValue(res);
        this.otherDetailsForm.patchValue(res);
        this.getAllStatesForCountry(res.country);
        this.getAllDistrictsForState(res.state);
      } else {
        this.employerRecord = {};
      }
    }, err => {
      console.log(err);
    })
  }

  // Get Basic Form Control

  get basicControl() {
    return this.basicProfileForm.controls;
  }

  //Submit Basic Form

  submitBasicForm(tabid?) {
    this.submitted_basic_form = true;
    if (this.basicProfileForm.invalid) {
      return;
    }
    this.spinner.showSpinner();
    const basicObj = Object.assign(this.employerRecord, this.basicProfileForm.value);
    this.empService.createbasicprofile(basicObj).subscribe((res: any) => {
      if (res) {
        this.employerRecord = res;
        this.toastr.success('Saved successfully');
        this.basicProfileForm.patchValue(res);
        this.navigateTabById(tabid);
      }
      this.spinner.hideSpinner();
    }, err => {
      console.log(err);
      this.spinner.hideSpinner();
    });

  }

  // Create Address Form

  createAddressForm() {
    this.addressForm = this.fb.group({
      adline1: ['', Validators.required],
      adline2: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required],
      district: ['', Validators.required],
      zipcode: ['', Validators.required],
      addproofurl: ['', Validators.required],
    });
  }

  // Return address Control 
  get addressControl() {
    return this.addressForm.controls;
  }

  // Submit Address

  submitAddress(tabid?) {
    debugger;
    this.submitted_address_form = true;
    if (this.addressForm.invalid) {
      return;
    }
    this.spinner.showSpinner();
    if (this.addressProof != null) {
      this.empService.uploadFile(this.addressProof).toPromise()
        .then(event => {

          var url = event;
          this.addressForm.get('addproofurl').patchValue(url);
          this.addressProof = null;
          const addressObj = Object.assign(this.employerRecord, this.addressForm.value);
          this.empService.updateAddress(addressObj).subscribe(res => {
            this.spinner.hideSpinner();
            this.toastr.success('Address saved successfully.');
            this.employerRecord = res;
            this.addressForm.patchValue(res);
            this.navigateTabById(tabid);
          },
            err => {
              this.spinner.hideSpinner();
            });

        }).catch(err => {
          console.log(err);
          this.spinner.hideSpinner();
        });

    }
    else {
      // This Edit details when address file in not upload
      const addressObj = Object.assign(this.employerRecord, this.addressForm.value);
      this.empService.updateAddress(addressObj).subscribe(res => {
        this.spinner.hideSpinner();
        this.employerRecord = res;
        this.toastr.success('Address saved successfully.');
        this.addressForm.patchValue(res);
        this.navigateTabById(tabid);
      },
        err => {
          this.spinner.hideSpinner();
        });
    }

  }


  public countryList: any = [];
  getAllCountries() {
    debugger
    this.empService.getAllCountries().subscribe((data: any) => {
      if (data) {

        this.countryList = this.sortPipe.transform(data.content, "asc", "countryname");

      }
    },
      (err) => {

      });
  }

  onChangeCountry(countryCode: string) {
    if (countryCode) {
      this.getAllStatesForCountry(countryCode);
    }
  }

  public stateList: any = [];
  getAllStatesForCountry(countryCode: string) {
    this.empService.getAllStatesForCountry(countryCode).subscribe((data: any) => {
      this.stateList = this.sortPipe.transform(data, "asc", "statename");
    },
      (err) => {
      });
  }

  onChangeState(stateCode: string) {
    if (stateCode) {
      //this.getAllCitiesForState(stateCode);
      this.getAllDistrictsForState(stateCode);
    }
  }
  public districtList: any = [];
  getAllDistrictsForState(stateName: string) {
    this.empService.getAllDistrictsForState(stateName).subscribe((data: any) => {

      this.districtList = this.sortPipe.transform(data, "asc", "districtname");

    },
      (err) => {

      });
  }

  // Upload Address proof
  public addressProof: FormData;
  uploadFile(event: any) {
    if (event.target.files.length > 0) {
      const uploadfile: File = event.target.files[0];
      if (this.validateFile(uploadfile)) {
        this.addressForm.patchValue({
          addproofurl: uploadfile.name
        });
        this.addressForm.updateValueAndValidity();
        this.addressProof = this.getFormData(uploadfile, 'addressproof');
      }
    }
  }

  //validate file;

  validateFile(file: File): boolean {
    let IsValid = true;
    const extension = file.name.substr(file.name.lastIndexOf('.') + 1);
    if (file.size > (1024 * 1024)) {
      this.toastr.error('Please select file size less than 1 MB');
      IsValid = false;
    }
    if (extension.toLowerCase() != 'doc' && extension.toLowerCase() != 'docx' &&
      extension.toLowerCase() != 'pdf' && extension.toLowerCase() != 'jpeg'
      && extension.toLowerCase() != 'jpg' && extension.toLowerCase() != 'png') {
      this.toastr.error("Only doc, docx, JPEG, PNG and PDF files are allowed.");
      IsValid = false;
    }
    return IsValid;
  }

  getFormData(uploadfile: File, filetype): FormData {
    const formData: FormData = new FormData();
    formData.append('file', uploadfile);
    formData.append('bucket', 'iviewid' + this.basicProfileId);
    formData.append('username', this.username);
    formData.append('filetype', filetype);
    return formData;
  }

  getEscapeStringUrl(url: string): string {
    if (url) {
      return url.replace('\ "', '').replace('"', '').replace('"', '');
    }
    return '';
  }


  // Create Other Details Form Data
  createOtherDetailsForm() {
    this.otherDetailsForm = this.fb.group({
      employeesnumber: ['', Validators.required],
      industrysector: ['', Validators.required],
      subsector: ['', Validators.required],
      registrationno: ['', Validators.required],
      registrationurl: ['', Validators.required],
      revenue: ['', Validators.required],
      annualstatementlink: ['', Validators.required],
      pan: ['', Validators.required],
      pan_url: ['', Validators.required],
      gst: ['', Validators.required],
    });
  }

  // Return Other Details Control 
  get otherDetailsControls() {
    return this.otherDetailsForm.controls;
  }


  // Check form Data is Null or Not




  // Submit Other Details

  async submitOtherDetails(tabid?) {
    debugger;
    this.submitted_other_details = true;
    if (this.otherDetailsForm.invalid) {
      return;
    }
    this.spinner.showSpinner();
    var f = ['mca', 'annual', 'pan'];
    f.forEach(element => {
      if (element == 'mca' && this.mcaCertificate != null) {
        this.empService.uploadFile(this.mcaCertificate).toPromise().then(event => {
          this.otherDetailsForm.get('registrationurl').patchValue(event);
          this.mcaCertificate = null;
          const otherDetailsObj = Object.assign(this.employerRecord, this.otherDetailsForm.value);
          this.empService.updateOtherCorporateDetails(otherDetailsObj).subscribe(res => {
            this.employerRecord = res;
            this.otherDetailsForm.patchValue(res);
          },
            err => {
              this.spinner.hideSpinner();
            });
        }).catch(err => {
          console.log(err);
        })
      }


      if (element == 'annual' && this.annualResult != null) {
        this.empService.uploadFile(this.annualResult).toPromise().then(event => {
          this.otherDetailsForm.get('annualstatementlink').patchValue(event);
          this.annualResult = null;

          const otherDetailsObj = Object.assign(this.employerRecord, this.otherDetailsForm.value);
          this.empService.updateOtherCorporateDetails(otherDetailsObj).subscribe(res => {
            this.employerRecord = res;
            this.otherDetailsForm.patchValue(res);
          },
            err => {
              this.spinner.hideSpinner();
            });

        }).catch(err => {
          console.log(err);
          this.spinner.hideSpinner();
        });
      }


      if (element == 'pan' && this.panUrl != null) {
        this.empService.uploadFile(this.panUrl).toPromise().then(event => {
          this.otherDetailsForm.get('pan_url').patchValue(event);
          this.panUrl = null;

          const otherDetailsObj = Object.assign(this.employerRecord, this.otherDetailsForm.value);
          this.empService.updateOtherCorporateDetails(otherDetailsObj).subscribe(res => {

            this.employerRecord = res;
            this.otherDetailsForm.patchValue(res);

          },
            err => {
              this.spinner.hideSpinner();
            });

        }).catch(err => {
          console.log(err);
          this.spinner.hideSpinner();
        });
      }


    });
    
    if (this.mcaCertificate == null && this.annualResult == null && this.panUrl == null) {

      const otherDetailsObj = Object.assign(this.employerRecord, this.otherDetailsForm.value);
      this.empService.updateOtherCorporateDetails(otherDetailsObj).subscribe(res => {
        this.spinner.hideSpinner();
        this.toastr.success('Other details saved successfully.');
        this.employerRecord = res;
        this.otherDetailsForm.patchValue(res);
        this.navigateTabById(tabid);
      },
        err => {
          this.spinner.hideSpinner();
        });

    }
    else {
      const otherDetailsObj = Object.assign(this.employerRecord, this.otherDetailsForm.value);
      this.empService.updateOtherCorporateDetails(otherDetailsObj).subscribe(res => {
        this.spinner.hideSpinner();
        this.toastr.success('Other details saved successfully.');
        this.employerRecord = res;
        this.otherDetailsForm.patchValue(res);
        this.navigateTabById(tabid);
      },
        err => {
          this.spinner.hideSpinner();
        });
    }

  }



  // Upload MCA Certificate
  public mcaCertificate: FormData;
  uploadMcaCertificate(event: any) {
    if (event.target.files.length > 0) {
      const uploadfile: File = event.target.files[0];
      if (this.validateFile(uploadfile)) {
        this.otherDetailsForm.patchValue({
          registrationurl: uploadfile.name
        });
        this.otherDetailsForm.updateValueAndValidity();
        this.mcaCertificate = this.getFormData(uploadfile, 'registrationurl');
      }
    }
  }


  // Upload AnnalResult Statement
  public annualResult: FormData;
  uploadAnnualResult(event: any) {
    if (event.target.files.length > 0) {
      const uploadfile: File = event.target.files[0];
      if (this.validateFile(uploadfile)) {
        this.otherDetailsForm.patchValue({
          annualstatementlink: uploadfile.name
        });
        this.otherDetailsForm.updateValueAndValidity();
        this.annualResult = this.getFormData(uploadfile, 'annualstatement');
      }
    }
  }

  // Upload Pan url
  public panUrl: FormData;
  uploadPanUrl(event: any) {
    if (event.target.files.length > 0) {
      const uploadfile: File = event.target.files[0];
      if (this.validateFile(uploadfile)) {
        this.otherDetailsForm.patchValue({
          pan_url: uploadfile.name
        });
        this.otherDetailsForm.updateValueAndValidity();
        this.panUrl = this.getFormData(uploadfile, 'pan');
      }
    }
  }

}
