import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobseekerRoutingModule } from './jobseeker-routing.module';
import { AddProfileComponent } from './pages/add-profile/add-profile.component';
import { LayoutComponent } from './components/layout/layout.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormWizardModule } from 'angular2-wizard';
import { NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker'
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { InterviewSchedulesComponent } from './pages/interview-schedules/interview-schedules.component';
import { ScheduleInterviewDetailsComponent } from './pages/schedule-interview-details/schedule-interview-details.component';
import { InterviewResultComponent } from './pages/interview-result/interview-result.component';
import { InterviewResultDetailsComponent } from './pages/interview-result-details/interview-result-details.component';
import { UpdatePwdComponent } from './pages/update-pwd/update-pwd.component';
import { SearchListComponent } from './pages/search-list/search-list.component';
import { SearchOptionComponent } from './pages/search-option/search-option.component';
import { SearchDetailsComponent } from './pages/search-details/search-details.component';
import { MyCalendarComponent } from './pages/my-calendar/my-calendar.component';
import { ScheduleModule, WeekService, MonthService, DayService, DragAndDropService, ResizeService } from '@syncfusion/ej2-angular-schedule';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { SendInterviewRequestComponent } from './pages/send-interview-request/send-interview-request.component';
import { ViewProfileComponent } from './pages/view-profile/view-profile.component';
import { MyDocumentApprovalComponent } from './pages/my-document-approval/my-document-approval.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { QuickRegistrationComponent } from './pages/quick-registration/quick-registration.component';
import { JoinMeetingComponent } from './pages/join-meeting/join-meeting.component';
import { VideoModule } from 'src/app/video/video.module';
import { PlayRecordingsComponent } from './pages/play-recordings/play-recordings.component';
import { VideoResumeComponent } from './pages/add-profile/video-resume/video-resume.component';
import { RateYourExperienceComponent } from './pages/rate-your-experience/rate-your-experience.component';
import { BarRatingModule } from 'ngx-bar-rating';
import { CartComponent } from './pages/order/cart/cart.component';
import { OrderSummaryComponent } from './pages/order/order-summary/order-summary.component';
import { DataTablesModule } from 'angular-datatables';
import { OrderConfirmationComponent } from './pages/order/order-confirmation/order-confirmation.component';
import { FooterComponent } from './components/footer/footer.component';
import { BasicInfoComponent } from './pages/add-profile/basic-info/basic-info.component';
import { JobInfoComponent } from './pages/add-profile/job-info/job-info.component';
import { IdproofComponent } from './pages/add-profile/idproof/idproof.component';
import { AddressComponent } from './pages/add-profile/address/address.component';
import { ExperienceComponent } from './pages/add-profile/experience/experience.component';
import { EducationComponent } from './pages/add-profile/education/education.component';
import { CompetencyComponent } from './pages/add-profile/competency/competency.component';
import { CareerDetailsComponent } from './pages/add-profile/career-details/career-details.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { SpinnerService } from '../employer/service/spinner.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AddCompetencyComponent } from './pages/add-profile/competency/add-competency/add-competency.component';
import { ReviewComponent } from './pages/add-profile/review/review.component';
import { SearchComponent } from './pages/search/search.component';
import { FilterPipe } from './pages/search/filter.pipe';
import { SearchDetailComponent } from './pages/search/search-detail/search-detail.component';
import { AddExperienceComponent } from './pages/add-profile/experience/add-experience/add-experience.component';
import { AddEducationComponent } from './pages/add-profile/education/add-education/add-education.component';
import { JobInterviewsComponent } from './pages/job-interviews/job-interviews.component';


 
@NgModule({
  declarations: [
    AddProfileComponent,
    LayoutComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    InterviewSchedulesComponent,
    ScheduleInterviewDetailsComponent,
    InterviewResultComponent,
    InterviewResultDetailsComponent,
    UpdatePwdComponent,
    SearchListComponent,
    SearchOptionComponent,
    SearchDetailsComponent,
    MyCalendarComponent,
    SendInterviewRequestComponent,
    ViewProfileComponent,
    MyDocumentApprovalComponent,
    DashboardComponent,
    QuickRegistrationComponent,
    JoinMeetingComponent,
    PlayRecordingsComponent,
    VideoResumeComponent,
    RateYourExperienceComponent,
    CartComponent,
    OrderSummaryComponent,
    OrderConfirmationComponent,
    BasicInfoComponent,
    JobInfoComponent,
    IdproofComponent,
    AddressComponent,
    ExperienceComponent,
    EducationComponent,
    CompetencyComponent,
    CareerDetailsComponent,
    AddExperienceComponent,
    AddEducationComponent,
    AddCompetencyComponent,
    ReviewComponent,
    SearchComponent,
    FilterPipe,
    SearchDetailComponent,
    JobInterviewsComponent,
  ],
  imports: [
    CommonModule,
    JobseekerRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormWizardModule,
    NgbModule,
    NgMultiSelectDropDownModule,
    BsDatepickerModule.forRoot(),
    AutocompleteLibModule,
    ScheduleModule,
    AngularDateTimePickerModule,
    VideoModule,
    NgbModalModule,
    BarRatingModule,
    DataTablesModule,
    AngularMultiSelectModule,
    NgxSpinnerModule
  ],
  entryComponents: [PlayRecordingsComponent, RateYourExperienceComponent,AddExperienceComponent,AddEducationComponent,AddCompetencyComponent],
  providers: [DayService, WeekService, MonthService, DragAndDropService, ResizeService,SpinnerService]

})
export class JobseekerModule { }
