import { Injectable } from '@angular/core';
import { DataService } from '../../../shared/services/data.service';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class JobSeekerService {
  // baseUrl = 'auth/';
  // private _controllerName: string = "jobseeker/";
  // _url = 'http://157.245.101.207:8080/'; 
  // _methodName: string;
  // _param: number;

  constructor(private dataService: DataService, private _http: HttpClient) { }


  // ******************gte basic profile*****************************
  getbasicprofile(username: string) {
    return this.dataService.get('jobseekerreg/getbasicprofile/' + username);
  }

  // ******************gte basic profile*****************************
  getbasicprofilebyid(id: number) {
    return this.dataService.get('jobseekerreg/getprofile/' + id);
  }

  getLanguages() {
    return this.dataService.get('elasticsearch/getLanguages');
  }
  createbasicprofile(basicprofile: any) {
    return this.dataService.put('jobseekerreg/createbasicprofile/', basicprofile);
  }
  addjobinfo(basicprofile: any) {
    return this.dataService.put('jobseekerreg/updateJobInfo/', basicprofile);
  }

  addIdDetails(basicprofile: any) {
    return this.dataService.put('jobseekerreg/addIdDetails/', basicprofile);
  }
  uploadIDfile(idProof: any) {
    return this.dataService.uploadFile('jobseekerreg/uploadIDfile/', idProof);
  }
  uploadTaxfile(taxFile: any) {
    return this.dataService.uploadFile('jobseekerreg/uploadTaxfile/', taxFile);
  }
  updateAddress(contact: any) {
    return this.dataService.put('jobseekerreg/updateAddress/', contact);
  }
  uploadAddFile(addressProof: any) {
    return this.dataService.uploadFile('jobseekerreg/uploadAddFile', addressProof);
  }
  getEducation(username: string) {
    return this.dataService.get('jobseekerreg/getEducation/' + username);
  }
  updateEducation(education: any) {
    return this.dataService.put('jobseekerreg/updateEducation', education);
  }
  updateEducationCertificate(educationCertificate: any) {
    return this.dataService.uploadFile('jobseekerreg/updateEducationCertificate', educationCertificate);
  }
  getExperience(username: string) {
    return this.dataService.get('jobseekerreg/getExperience/' + username);
  }
  updateOtherDetails(data: any) {
    return this.dataService.put('jobseekerreg/updateOtherDetails', data);
  }
  getOtherDetails(username: string) {
    return this.dataService.get('jobseekerreg/getOtherDetails/' + username);
  }
  updateExperienceDetails(jobDetails: any) {
    return this.dataService.put('jobseekerreg/updateExperience', jobDetails);
  }
  updateExperienceCertificate(experienceCertificate: any) {
    return this.dataService.uploadFile('jobseekerreg/updateExperienceCertificate', experienceCertificate);
  }
  updateJoiningLetter(joiningLetter: any) {
    return this.dataService.uploadFile('jobseekerreg/updateJoiningLetter', joiningLetter);
  }
  updateSkillDetails(skillDetails: any) {
    return this.dataService.put('jobseeker/updateSkillDetails/', skillDetails);
  }

  uploadResume(resume: any) {
    return this.dataService.uploadFile('jobseekerreg/uploadResume/', resume);
  }

  updateResume(resume: any) {
    return this.dataService.uploadFile('jobseekerreg/updateResume/', resume);
  }
  submitDetails(username: string) {
    debugger
    return this.dataService.put('jobseekerreg/submitDetails/' + username, username);
  }

  downloadFile(data: any) {
    return this.dataService.downloadFile('jobseekerreg/DownloadFile', data);
  }

  getJobrolebyName(modeller: string) {
    return this.dataService.get('elasticsearch/getJobrolebyName/' + modeller);
  }
  deleteEducation(eid: number) {
    return this.dataService.delete('jobseekerreg/deleteEducation/' + eid);
  }
  deleteExperience(exid: number) {
    return this.dataService.delete('jobseekerreg/deleteExperience/' + exid);
  }

  getSkillsCompetencies(username: string) {
    return this.dataService.get('jobseekerreg/getSkillsCompetencies/' + username);
  }
  submitSkills(username: string, skills: any) {
    return this.dataService.put('jobseekerreg/submitSkills/' + username, skills);
  }
  submitCompetencySkills(skills: any) {
    return this.dataService.put('jobseekerreg/submitSkills', skills);
  }
  deleteSkillCompetency(competencyId: number) {
    return this.dataService.delete('jobseekerreg/DeleteSkillCompetency/' + competencyId);
  }
  updatePassword(d: any) {
    return this.dataService.put('identity/updatepassword', d);
  }
  getCompetencyByName(competenctyName: string) {
    return this.dataService.get('elasticsearch/getCompetencyByName/' + competenctyName);
  }
  getAllSkills(skillName: string) {
    return this.dataService.get('elasticsearch/getAllSkills/' + skillName);
  }


  //Searching Services 

  getJobRole(term: string) {
    return this.dataService.get('elasticsearch/getJobrolebyName/' + term);
  }

  getCurrentIndustry() {
    return this.dataService.get('elasticsearch/getindustry');
  }

  getJobRoleByIndustryName(industry) {
    return this.dataService.get('elasticsearch/getJobrolebyIndustry/' + industry);
  }

  getCompetencyByJobRole(jobrole) {
    return this.dataService.get('elasticsearch/getJobrolebyName/' + jobrole);
  }

  getAllCountry() {
    return this.dataService.get('elasticsearch/getAllCountries');
  }

  getAllStateForCountry(countrycode) {
    return this.dataService.get('elasticsearch/getAllStatesForCountry/' + countrycode);
  }
  getAllCityForState(statecode) {
    return this.dataService.get('elasticsearch/getAllCitiesForState/' + statecode);
  }
  getAllLanguage() {
    return this.dataService.get('elasticsearch/getLanguages');
  }

  search(d: any) {
    return this.dataService.post('elasticsearch/getfreelancerbySearchParams', d);
  }
  getSkillbyName(skillName: string) {
    return this.dataService.get('elasticsearch/getSkillbyName/' + skillName);
  }
  // getCountry(name: string) {
  //   return this.dataService.get('elasticsearch/getCountry/' + name);
  // }
  // getState(name: string) {
  //   return this.dataService.get('elasticsearch/getstate/' + name);
  // }
  // getCity(name: string) {
  //   return this.dataService.get('elasticsearch/getcity/' + name);
  // }
  // getDistrict(name: string) {
  //   return this.dataService.get('elasticsearch/getdistrict/' + name);
  // }
  getAllCountries() {
    return this.dataService.get('elasticsearch/getAllCountries');
  }
  getAllStatesForCountry(countryName: string) {
    return this.dataService.get('elasticsearch/getAllStatesForCountry/' + countryName);
  }
  getAllCitiesForState(stateName: string) {
    return this.dataService.get('elasticsearch/getAllCitiesForState/' + stateName);
  }
  getAllDistrictsForState(stateName: string) {
    return this.dataService.get('elasticsearch/getAllDistrictsForState/' + stateName);
  }

  getFreelancerById(id) {
    return this.dataService.get('elasticsearch/getFreelancerbyId/' + id);
  }

  // get work day hours

  getWorkDayHours(username) {
    return this.dataService.get('freelanceriviewer/getWorkdayHours/' + username);
  }

  getFreelancerCurrentEvent(username: string) {
    return this.dataService.get('freelanceriviewer/getCurrentCalendarEvents/' + username);
  }

  sendInterviewRequest(data: any) {
    return this.dataService.post('jobseekerinterviewer/sendInterviewRequest', data);
  }

  getAllMyInterviews(id) {
    return this.dataService.get('jobseekerinterviewer/getAllMyInterviews/' + id);
  }

  getinterviewresultsummaries(id) {
    return this.dataService.get('jobseeker/getinterviewresultsummaries/' + id);
  }

  interviewresultdetail(id) {
    return this.dataService.get('jobseeker/interviewresultdetail/' + id);
  }

  getJobrolebyIndustry(industryname) {
    return this.dataService.get('elasticsearch/getJobrolebyIndustry/' + industryname);
  }

  getAllFunctionalAreas() {
    return this.dataService.get('elasticsearch/getAllFunctionalAreas');
  }


  getAllSkillList() {
    return this.dataService.get('elasticsearch/getAllSkills');
  }

  getAllCompetanceList() {
    return this.dataService.get('elasticsearch/getAllCompetencies');
  }

  getAllRoles() {
    return this.dataService.get('elasticsearch/getallRoles');
  }

  getInterviewScheduleById(id) {
    return this.dataService.get('jobseekerinterviewer/getInterviewScheduleById/' + id);
  }

  saveVideoRecordings(data: any) {
    return this.dataService.post('jobseekerinterviewer/saveMyRecording', data);
  }

  getVideoRecordings(id) {
    return this.dataService.get('jobseekerinterviewer/interviewrecordings/' + id);
  }

  getSurveyTemplate(id) {

    return this.dataService.get('jobseeker/getSurveyTemplate/' + id);

  }

  saveSurveyFeedback(data: any) {

    return this.dataService.put('jobseeker/saveSurveyFeedback', data);

  }


  saveConsolidateFeedback(data: any) {

    return this.dataService.put('jobseeker/saveConsolidateFeedback/' + data, []);

  }

  createNewOrder(data: any) {

    return this.dataService.put('cart/createNewOrder', data);

  }

  createNewOrderItem(data: any) {

    return this.dataService.put('cart/createNewOrderItem', data);

  }


  getOrder(id) {
    return this.dataService.get('cart/getOrder/' + id);
  }


  getOrderitems(id) {
    return this.dataService.get('cart/getOrderitems/' + id);
  }

  publishSuccessfulOrder(data: any) {
    return this.dataService.put('cart/publishSuccessfulOrder', data);
  }

  cancelOrder(id: any) {
    return this.dataService.put('cart/cancelOrder/' + id, []);
  }

  getjobseekerorders(id) {
    return this.dataService.get('cart/getjobseekerorders/' + id);
  }

  getProfileCompleteness(username: string) {
    return this.dataService.get('jobseekerreg/getprofilecompleteness/' + username);
  }


  uploadVideoResume(videoResume: any) {
    return this.dataService.uploadFile('jobseeker/uploadVideo', videoResume);
  }

  getAllMyCorpInterviews(id: number) {
    return this.dataService.get('jobseekerinterviewer/getAllMyCorpInterviews/' + id);
  }


}