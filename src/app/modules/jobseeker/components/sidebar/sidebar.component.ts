import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/shared/services/storage.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  public role: string;
  public username: string;
  public userId: any;
  public profilePage: boolean = false;
  constructor(private storageService: StorageService) { }

  ngOnInit() {
    this.role = this.storageService.getItem('role');
    this.username = this.storageService.getItem('username');
    this.userId = this.storageService.getItem('id');
  }


}
