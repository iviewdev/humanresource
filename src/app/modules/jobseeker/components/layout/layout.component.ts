import { Component, OnInit } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  public show_side_menu: boolean = true;
  constructor(private router: Router) {
    this.router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        var url = event['url'];
        url = url.toLocaleLowerCase();
        if (url.includes('freelancer-search') || url.includes('join-meeting')) {
          this.show_side_menu = false;
        } else {
          this.show_side_menu = true;
        }
      }
    });
  }

  ngOnInit() {
    var u = window.location.href;
    u = u.toLocaleLowerCase();
    if (u.includes('freelancer-search') || u.includes('join-meeting')) {
      this.show_side_menu = false;
    } else {
      this.show_side_menu = true;
    }
  }

}
