import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendInterviewRequestComponent } from './send-interview-request.component';

describe('SendInterviewRequestComponent', () => {
  let component: SendInterviewRequestComponent;
  let fixture: ComponentFixture<SendInterviewRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendInterviewRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendInterviewRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
