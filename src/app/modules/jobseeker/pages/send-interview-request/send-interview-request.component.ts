import { Component, OnInit } from '@angular/core';
import { DatePipe, Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../service/jobseeker.service';
import { ToastrService } from 'ngx-toastr';
import { getHours } from 'ngx-bootstrap/chronos/utils/date-getters';

@Component({
  selector: 'app-send-interview-request',
  templateUrl: './send-interview-request.component.html',
  styleUrls: ['./send-interview-request.component.scss'],
  providers: [DatePipe]
})
export class SendInterviewRequestComponent implements OnInit {

  datePickersettings = {
    bigBanner: true,
    timePicker: true,
    format: 'yyyy-MM-dd HH:mm',
    defaultOpen: false,
    minDate: new Date()
  }
  public fsid: any;
  public per_hour_rate: any;
  public scheduleRequestForm: FormGroup;
  constructor(private location: Location, private fb: FormBuilder,
    private datePipe: DatePipe,
    private route: ActivatedRoute,
    private storageService: StorageService,
    private jobseekerService: JobSeekerService,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.fsid = params['fsid'];
      this.per_hour_rate = params['per_hour_rate'];
    });
    this.createForm();
    this.scheduleRequestForm.controls.start_date_time.valueChanges.subscribe((v) => {
      var date = new Date(v);
      this.scheduleRequestForm.controls.end_date_time.patchValue(this.datePipe.transform(date.setHours(date.getHours() + 1), 'yyyy-MM-dd HH:mm'));
    });
  }

  createForm() {
    var date = new Date();
    this.scheduleRequestForm = this.fb.group({
      start_date_time: [date.setDate(date.getDate() + 1), Validators.required],
      end_date_time: [this.datePipe.transform(date.setHours(date.getHours() + 1), 'yyyy-MM-dd HH:mm')]
    });
  }

  back() {
    this.location.back();
  }

  submit() {
    if (this.scheduleRequestForm.invalid) {
      return;
    }
    debugger;
    //  this.createNewOrder(1);
    let d = {
      scheduleid: "",
      freelancerid: this.fsid,
      jobseekerid: this.storageService.getItem('id'),
      starttime: new Date(this.scheduleRequestForm.controls.start_date_time.value),
      endtime: new Date(this.scheduleRequestForm.controls.end_date_time.value),
      status: "Interview Requested"
    }
    console.log(d);
    this.jobseekerService.sendInterviewRequest(d).subscribe((res: any) => {
      if (res) {
        console.log(res);
        this.createNewOrder(res.interviewid);
        // this.toastr.success('Request send successfully');
        // this.router.navigate(['jobseeker/checkout-order',data.fsid]);
      }
    });
  }


  createNewOrder(id) {

    let d = {
      "orderdate": new Date(),
      "ordercompletedate": new Date(),
      "customertype": "Jobseeker",
      "customerid": this.storageService.getItem('id'),
      "subtotal": (this.per_hour_rate * 1),
      "gst": "18",
      "grandtotal": ((this.per_hour_rate * 1) * 18 / 100) + (this.per_hour_rate * 1),
      "refundstatus": null,
      "scheduleid": id
    };

    this.jobseekerService.createNewOrder(d).subscribe((res: any) => {
      if (res) {
        this.createOrderItem(res.orderid);
      }
    }, err => {
      console.log(err);
    });
  }

  createOrderItem(id) {

    let d = {
      "itemdescreption": "Interview with freelancer " + this.fsid,
      "mrp": (this.per_hour_rate * 1),
      "discount": 0,
      "gstrate": 18.0,
      "itemtotal": ((this.per_hour_rate * 1) * 18 / 100) + (this.per_hour_rate * 1),
      "orderid": id
    };

    this.jobseekerService.createNewOrderItem(d).subscribe((res: any) => {
      if (res) {
        console.log(res);
        this.router.navigate(['jobseeker/checkout-order'], { queryParams: { fsid: this.fsid, orderid: id } });
      }
    }, err => {

    });

  }

  convertDate(val) {
    var date = new Date(val);
    var v = date.getFullYear() + "-" + (+date.getMonth() + 1).toString().padStart(2, '0') + "-" + date.getDate().toString().padStart(2, '0') + "T" + date.getHours().toString().padStart(2, '0') + ":" + date.getMinutes().toString().padStart(2, '0') + ":" + "00";
    return v;
  }


  // onDateSelect(val) {
  //   if (new Date(val) > new Date()) {
  //     alert('yes');
  //   }
  // }



}
