import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ConfirmedValidator } from 'src/app/modules/account/pages/page-reset-password/confirm-validator';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../service/jobseeker.service';

@Component({
  selector: 'app-update-pwd',
  templateUrl: './update-pwd.component.html',
  styleUrls: ['./update-pwd.component.scss']
})
export class UpdatePwdComponent implements OnInit {

  updatePwdForm: FormGroup;
  loginStart = false;
  submitted = false;
  constructor(private toastr: ToastrService,
    private jobseekerService: JobSeekerService,
    private fb: FormBuilder,
    private router: Router,
    private storageService: StorageService
  ) {
    this.updatePwdForm = this.fb.group({
      password: ['', Validators.required],
      new_password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]],
      confirm_password: ['', [Validators.required]],
    }, {
      validators: ConfirmedValidator('new_password', 'confirm_password')
    });
  }

  ngOnInit() {

  }


  get f() {
    return this.updatePwdForm.controls;
  }

  //***********************Verify  Here**************************/

  updatePassword() {

    this.submitted = true;
    if (this.updatePwdForm.invalid) {
      return;
    }
    this.loginStart = true;
    let d = {
      username: this.storageService.getItem('username'),
      password: this.updatePwdForm.get('password').value,
      newpass: this.updatePwdForm.get('new_password').value,
      email_confirmed: null,
      role: null,
      reset_link: null,
      jdbc_url: null
    }
    this.jobseekerService.updatePassword(d).subscribe((res: any) => {
      console.log(res);
      this.loginStart = false;
      console.log(res);
      if (res) {
        this.toastr.success('Password updated successfully');
        this.router.navigateByUrl('/jobseeker');
      }
      else {
        this.toastr.error('update Password Fail Please try Again!');
      }

    }, (err) => {
      this.loginStart = false;
      if (err.error.status == 401) {
        this.toastr.error('Invalid username');
      } else {
        this.toastr.error(err.error.message);
      }

    });
  }

}
