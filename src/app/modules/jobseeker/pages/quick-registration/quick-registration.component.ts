import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../service/jobseeker.service';

@Component({
  selector: 'app-quick-registration',
  templateUrl: './quick-registration.component.html',
  styleUrls: ['./quick-registration.component.scss']
})
export class QuickRegistrationComponent implements OnInit {
  public quickRegistrationForm: FormGroup;
  public attachedResume: FormData;
  public registraionData: any;
  public username: string;
  constructor(private fb: FormBuilder,
    private jobseekerService: JobSeekerService,
    private storage: StorageService, private toastr: ToastrService) { }

  ngOnInit() {
    this.username = this.storage.getItem('username');
    this.createForm();
    this.getBasicProfile();
  }

  createForm() {
    this.quickRegistrationForm = this.fb.group({
      id: [''],
      title: ['', Validators.required],
      fname: ['', Validators.required],
      lname: ['', Validators.required],
      gender: ['', Validators.required],
      mobile: ['', Validators.required],
      username: ['', Validators.required],
      attachedResume:['']
    });
  }

  getBasicProfile() {
    this.jobseekerService.getbasicprofile(this.username).subscribe((res: any) => {
      console.log(res);
      this.quickRegistrationForm.patchValue(res);
    }, err => {
      console.log(err);
    })
  }

  submit() {
    if (this.quickRegistrationForm.invalid) {
      return;
    }
    this.registraionData.title = this.quickRegistrationForm.get('title').value;
    this.registraionData.fname = this.quickRegistrationForm.get('fname').value;
    this.registraionData.lname = this.quickRegistrationForm.get('lname').value;
    this.registraionData.gender = this.quickRegistrationForm.get('gender').value;
    this.registraionData.mobile = this.quickRegistrationForm.get('mobile').value;
    this.registraionData.username = this.quickRegistrationForm.get('username').value;

    this.jobseekerService.createbasicprofile(this.registraionData).subscribe((res: any) => {
      this.jobseekerService.updateResume(this.attachedResume).toPromise()
              .then(event => {
                if (event instanceof HttpResponse) {
                  let d = {
                    resume_url:this.getEscapeStringUrl(event.body as string),
                    username:this.username
                  }
                  this.attachedResume = null;
                  this.jobseekerService.updateSkillDetails(d).subscribe(res => {
                    this.toastr.success('Registration completed successfully');
                  },
                    err => {
                      
                    });
                }
              }).catch(err => {
                
              });

     
    }, err => {
      console.log(err);
    });

  }

  uploadResumeFile(event: any) {
    if (event.target.files.length > 0) {
      const uploadfile: File = event.target.files[0];
      if (this.validateFile(uploadfile)) {
        this.quickRegistrationForm.patchValue({
          attachedResume: uploadfile.name
        });
       // this.quickRegistrationForm.updateValueAndValidity();
        const formData: FormData = new FormData();
        formData.append('file', uploadfile);
        formData.append('bucket', 'iviewid' + this.registraionData.id);
        formData.append('username', this.username);
        this.attachedResume = formData; // this.getFormData(uploadfile);
      }

    }
  }

  validateFile(file: File): boolean {
    // let iFileSize = Number($event.target.files[0].size);
    // let extension = $event.target.files[0].name.substr(($event.target.files[0].name.lastIndexOf('.') + 1));
    let IsValid = true;
    const extension = file.name.substr(file.name.lastIndexOf('.') + 1);
    // if (file.size > 2101038) {
    //   this.toastr.error('Please select file size less than 2 MB');
    //   IsValid = false;
    // }
    if (file.size > (1024 * 1024)) {
      this.toastr.error('Please select file size less than 1 MB');
      IsValid = false;
    }
    if (extension.toLowerCase() != 'doc' && extension.toLowerCase() != 'docx' &&
      extension.toLowerCase() != 'pdf' && extension.toLowerCase() != 'jpeg'
      && extension.toLowerCase() != 'jpg' && extension.toLowerCase() != 'png') {
      this.toastr.error("Only doc, docx, JPEG, PNG and PDF files are allowed.");
      IsValid = false;
    }
    return IsValid;
  }


  getEscapeStringUrl(url: string): string {
    if (url) {
      return url.replace('\ "', '').replace('"', '').replace('"', '');
    }
    return '';
  }



}
