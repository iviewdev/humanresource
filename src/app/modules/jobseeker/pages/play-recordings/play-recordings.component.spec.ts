import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayRecordingsComponent } from './play-recordings.component';

describe('PlayRecordingsComponent', () => {
  let component: PlayRecordingsComponent;
  let fixture: ComponentFixture<PlayRecordingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayRecordingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayRecordingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
