import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import videojs from 'video.js';
declare var require: any;
require('videojs-contrib-quality-levels');
require('videojs-hls-quality-selector');
@Component({
  selector: 'app-play-recordings',
  templateUrl: './play-recordings.component.html',
  styleUrls: ['./play-recordings.component.scss']
})
export class PlayRecordingsComponent implements OnInit {
  @Input() url;

  public player: videojs.Player;
  constructor(private modelService: NgbModal) { }

  ngOnInit() {
   // videojs.options.autoSetup = false;
    console.log(this.url);
  }

  ngAfterViewInit() {
    const options = {
      inactivityTimeout: 0,
      controls: true,
      autoplay: true,
      preload: "auto",
    };
    this.player = videojs('my-video', options, function onPlayerReady() {
      console.log('Player ready');
      var myPlayer = this, id = myPlayer.id();
      myPlayer.hlsQualitySelector();

    });


  }

  ngOnDestroy(): void {
    if (this.player != null) {
      this.player.dispose();
    }
  }

  close() {
    this.modelService.dismissAll();
  }

}
