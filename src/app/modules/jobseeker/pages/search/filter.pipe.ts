import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'aFilter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any[], args: string, title: string): any {
    if (!value) {
      return [];
    }
    if (!args) {
      return value;
    }

    args = args.toLowerCase();

    return value.filter(it => {
      if (title == 'Country') {
        return it.countryname.toLowerCase().includes(args);
      }
      if (title == 'State') {
        return it.statename.toLowerCase().includes(args);
      }

      if (title == 'City') {
        return it.cityname.toLowerCase().includes(args);
      }

    });
  }

}
