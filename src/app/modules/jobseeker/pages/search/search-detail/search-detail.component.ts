import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../../service/jobseeker.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-search-detail',
  templateUrl: './search-detail.component.html',
  styleUrls: ['./search-detail.component.scss']
})
export class SearchDetailComponent implements OnInit {

  public fsid;
  public freelancerData: any;
  date: Date = new Date();
  datePickersettings = {
    bigBanner: true,
    timePicker: true,
    format: 'dd-MM-yyyy hh:mm a',
    defaultOpen: false
  }
  constructor(private route: ActivatedRoute,
    private jobseekerService: JobSeekerService, private router: Router,
    private modalService: NgbModal,
    private location: Location,
    private storage: StorageService) { }

  ngOnInit() {
    this.fsid = this.route.snapshot.paramMap.get('id');
    this.getFreelancerById(this.fsid);
  }

  //get freelancer by id

  getFreelancerById(id) {
    this.jobseekerService.getFreelancerById(id).subscribe((res: any) => {
      this.freelancerData = res;
      console.log(res);
    });
  }

  viewCalendar(username) {
    if (username) {
      this.router.navigate(['jobseeker/view-my-calendar', username])
    }
  }


  bookInterview(data: any) {
    console.log('data ' + JSON.stringify(data));
    this.router.navigate(['jobseeker/send-interview-request'], { queryParams: { fsid: data.fsid, per_hour_rate: data.per_hour_rate } });

  }

  back() {
    this.location.back();
  }


  counter(i: number) {
    return new Array(i);
  }

 
}
