import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as $ from 'jquery'
import { OpenapiService } from 'src/app/website/openapi.service';
declare function showPanel(id);
declare function hidePanel();
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  public searchRecords: any = [];
  public search_keywords: string = '';
  public org_type: string = '';
  public search: string;
  public country;
  public d = {
    "currindustry": "",
    "keywords": "",
    "totalexperience": "",
    "state": "",
    "country": "",
    "city": "",
    "currrole": "",
    "languages": "",
    "skills": []
  };
  public offset: number = 0;
  public limit: number = 5;
  public applied: number = 0;
  constructor(private route: ActivatedRoute,
    private openapi: OpenapiService) { }

  ngOnInit() {
    // $.getScript("assets/js/custom.js");
    this.route.queryParams.subscribe(params => {
      this.org_type = params['org_type'];
      this.d.keywords = params['search_keyword'];
      this.getfreelancerbySearchParams();
    });
    this.currentIndustryRecord();
    this.getAllCountries();
  }

  // next page
  nextPage() {
    this.offset = this.offset + 1;
    this.getfreelancerbySearchParams();
  }

  //previous page

  priviousPage() {
    if (this.offset > 0) {
      this.offset = this.offset - 1;
      this.getfreelancerbySearchParams();
    }
  }


  // Get Freelancer By Search params
  public total_page: number = 0;
  getfreelancerbySearchParams() {
    this.openapi.getfreelancerbySearchParams(this.d, this.offset, this.limit).subscribe((res: any) => {
      console.log(res);
      if (res.length > 0) {
        this.openapi.getQueryHits(this.d).subscribe((q: any) => {
          if (q) {
            this.total_page = q;
            this.searchRecords = res;
          }
        }, err => {

        })

      } else {
        this.total_page = 0;
        this.offset = 0;
        this.searchRecords = [];
      }

    }, err => {
      this.searchRecords = [];
      console.log(err);
    });
  }

  forwardDisable() {
    if ((this.total_page - (this.limit * this.offset)) > this.limit) {
      return false;
    } else {
      return true;
    }
  }

  backwardDisable() {
    if (this.offset != 0) {
      return false;
    } else {
      return true;
    }
  }

  // Filter Education
  getEducation(records) {
    return Array.prototype.map.call(records, function (item) { return item.education_type; }).join(",")
  }


  //######################### Clear All Filters####################

  clearAllFilter() {

    // Clear Current industries
    this.currentIndustryData.map(item => {
      return item.checked = false;
    });
    this.jobRoleData.map(item => {
      return item.checked = false;
    });
    this.CompetencyData.map(item => {
      return item.checked = false;
    });
    this.countryData.map(item => {
      return item.checked = false;
    });
    this.stateData.map(item => {
      return item.checked = false;
    });
    this.cityData.map(item => {
      return item.checked = false;
    })
    this.applied = 0
    this.d = {
      "currindustry": "",
      "keywords": this.search_keywords,
      "totalexperience": "",
      "state": "",
      "country": "",
      "city": "",
      "currrole": "",
      "languages": "",
      "skills": []
    }
    this.searchRecords = [];
    this.total_page = 0;
    this.offset = 0;
    this.getfreelancerbySearchParams();
  }


  //Current Industry Record
  public currentIndustryData: any = [];
  currentIndustryRecord() {
    this.openapi.getCurrentIndustry().subscribe((res: any) => {
      console.log(res);
      if (res) {
        this.currentIndustryData = res.content;
      } else {
        this.currentIndustryData = [];
      }
    });

  }

  //On Industry Select 
  public jobRoleData: any = [];
  public currentindustry: any;
  onIndustrySelectSelect($event) {

    if (!this.d.currindustry) {
      this.applied = this.applied + 1;
    }

    this.currentIndustryData.forEach((element, index) => {
      if (element.industryname == $event.target.value) {
        this.currentIndustryData[index].checked = true;
      } else {
        this.currentIndustryData[index].checked = false;
      }
    });
    var i = this.currentIndustryData.findIndex(x => x.industryname == $event.target.value);
    if (i >= 0) {
      if (i > 4) {
        var element = this.currentIndustryData[i];
        element.checked = true;
        this.currentIndustryData.splice(i, 1);
        this.currentIndustryData.unshift(element);
      }
    }

    this.d.currindustry = $event.target.value;
    this.getfreelancerbySearchParams();
    hidePanel();
    this.openapi.getJobRoleByIndustryName($event.target.value).subscribe((res: any) => {
      console.log(res);
      if (res) {

        this.jobRoleData = res;
      } else {
        this.jobRoleData = [];
      }
    });
  }


  //On Job Role Selection 
  public CompetencyData: any = [];
  public jobrole: any;
  onJobRoleSelect($event) {
    this.jobRoleData.forEach((element, index) => {
      if (element.jobrolename == $event.target.value) {
        this.jobRoleData[index].checked = true;
      } else {
        this.jobRoleData[index].checked = false;
      }
    });
    var i = this.jobRoleData.findIndex(x => x.jobrolename == $event.target.value);
    if (i >= 0) {
      if (i > 4) {
        var element = this.jobRoleData[i];
        element.checked = true;
        this.jobRoleData.splice(i, 1);
        this.jobRoleData.unshift(element);
      }
    }
    if (!this.d.currrole) {
      this.applied = this.applied + 1;
    }
    this.d.currrole = $event.target.value;
    this.getfreelancerbySearchParams();
    hidePanel();
    var CompetencyData = [];
    this.openapi.getCompetencyByJobRole($event.target.value).subscribe((res: any) => {
      if (res) {
        res.forEach(element => {
          element.competencies.forEach(ele => {
            CompetencyData.push(ele);
          });
        });
        this.CompetencyData = CompetencyData;
      } else {
        this.CompetencyData = [];
      }
    });
  }

  // On Competance Select
  public competency: any;
  onSelectCompetance(event) {
    debugger;
    if (this.d.skills.length == 0) {
      this.applied = this.applied + 1;
    }
    if (event.target.checked) {

      this.d.skills.push({ 'competency_name': event.target.value });

      this.getfreelancerbySearchParams();
    }
    if (!event.target.checked) {

      let index = this.d.skills.findIndex(x => x.competency_name === event.target.value);

      if (index > -1) {

        this.d.skills.splice(index, 1);
        this.applied = this.applied - 1;
        this.getfreelancerbySearchParams();
      }
    }
    hidePanel();
  }


  //Get All Country
  public countryData: any = [];
  getAllCountries() {
    this.openapi.getAllCountry().subscribe((res: any) => {
      // console.log(res);
      if (res) {
        res.content.forEach((element, index) => {
          if (element.country_code == "IN") {
            res.content.splice(index, 1);
            res.content.unshift(element);
          }
        });
        this.countryData = res.content;
      } else {
        this.countryData = [];
      }
    });
  }

  //ON Country Select 
  public stateData: any = [];
  onCountrySelect($event, country_code) {
    if (!this.d.country) {
      this.applied = this.applied + 1;
    }
    this.countryData.forEach((element, index) => {
      if (element.id == $event.target.value) {
        this.countryData[index].checked = true;
      } else {
        this.countryData[index].checked = false;
      }
    });
    var i = this.countryData.findIndex(x => x.id == $event.target.value);
    if (i >= 0) {
      if (i > 4) {
        var element = this.countryData[i];
        element.checked = true;
        this.countryData.splice(i, 1);
        this.countryData.unshift(element);
      }
    }
    this.d.country = country_code;
    this.getfreelancerbySearchParams();
    hidePanel();
    this.openapi.getAllStateForCountry($event.target.value).subscribe((res: any) => {
      if (res) {
        this.stateData = res;
      } else {
        this.stateData = [];
      }
    });
  }

  //ON State Select 
  public cityData: any = [];
  public state: any;
  onStateSelect($event, statename) {

    if (!this.d.state) {
      this.applied = this.applied + 1;
    }

    this.stateData.forEach((element, index) => {
      if (element.statecode == $event.target.value) {
        this.stateData[index].checked = true;
      } else {
        this.stateData[index].checked = false;
      }
    });
    var i = this.stateData.findIndex(x => x.statecode === $event.target.value);
    if (i >= 0) {
      if (i > 4) {
        var element = this.stateData[i];
        element.checked = true;
        this.stateData.splice(i, 1);
        this.stateData.unshift(element);
      }
    }
    this.d.state = statename;
    this.getfreelancerbySearchParams();
    hidePanel();
    this.openapi.getAllCityForState($event.target.value).subscribe((res: any) => {
      if (res) {
        this.cityData = res;
      } else {
        this.cityData = [];
      }
    });
  }
  // City select
  public city: any;
  onCitySelect($event, cityname) {

    if (!this.d.city) {
      this.applied = this.applied + 1;
    }

    this.cityData.forEach((element, index) => {
      if (element.citycode == $event.target.value) {
        this.cityData[index].checked = true;
      } else {
        this.cityData[index].checked = false;
      }
    });
    var i = this.cityData.findIndex(x => x.citycode == $event.target.value);
    if (i >= 0) {
      if (i > 4) {
        var element = this.cityData[i];
        element.checked = true;
        this.cityData.splice(i, 1);
        this.cityData.unshift(element);
      }
    }

    this.d.city = cityname;
    this.getfreelancerbySearchParams();
    hidePanel();
  }



  //show Panel
  public title: string;
  public popuprecords: any = [];
  showPan(id, title) {
    this.search = '';
    hidePanel();
    if (title == 'Country') {
      this.title = title;
      this.popuprecords = this.countryData;
    }
    if (title == 'State') {
      this.title = title;
      this.popuprecords = this.stateData;
    }
    if (title == 'City') {
      this.title = title;
      this.popuprecords = this.cityData;
    }

    if (title == 'Current Industry') {
      this.title = title;
      this.popuprecords = this.currentIndustryData;
    }

    if (title == 'Job Role') {
      this.title = title;
      this.popuprecords = this.jobRoleData;
    }

    if (title == 'Competency') {
      this.title = title;
      this.popuprecords = this.CompetencyData;
    }

    showPanel(id);
  }




}
