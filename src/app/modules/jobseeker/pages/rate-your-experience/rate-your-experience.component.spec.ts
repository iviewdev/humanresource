import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateYourExperienceComponent } from './rate-your-experience.component';

describe('RateYourExperienceComponent', () => {
  let component: RateYourExperienceComponent;
  let fixture: ComponentFixture<RateYourExperienceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RateYourExperienceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateYourExperienceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
