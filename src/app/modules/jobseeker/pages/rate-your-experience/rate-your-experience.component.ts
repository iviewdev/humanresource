import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { JobSeekerService } from '../../service/jobseeker.service';

@Component({
  selector: 'app-rate-your-experience',
  templateUrl: './rate-your-experience.component.html',
  styleUrls: ['./rate-your-experience.component.scss']
})
export class RateYourExperienceComponent implements OnInit {
  @Input() jobseekerid;
  @Input() freelancerid;
  @Input() interviewid;
  public ratingForm: FormGroup;
  public questionTemplates: any = [];
  constructor(private fb: FormBuilder,
    private jobseeker: JobSeekerService,
    private toastr: ToastrService, private modelService: NgbModal) { }

  ngOnInit() {
    //  this.create();
    this.getQuestionTemplates();
  }

  getQuestionTemplates() {

    this.jobseeker.getSurveyTemplate('interviewexperiencefeedback').subscribe((res: any) => {
      if (res) {
        res.forEach(element => {
          this.questionTemplates.push({
            "templatename": element.templatename,
            "interviewid": this.interviewid,
            "questionid": element.questionid,
            "questiontext": element.questiontext,
            "feedback": '',
            "jobseekerid": this.jobseekerid,
            "freelancerid": this.freelancerid
          });
        });
      }
    }, err => {
      console.log(err);
    });
  }



  create() {
    this.ratingForm = this.fb.group({
      polite_rating: ['', Validators.required],
      relevant_role_question: ['', Validators.required],
      complexity: ['', Validators.required],
      u_had: ['', Validators.required]
    });
  }

  submit() {
    this.questionTemplates.forEach(element => {
      this.jobseeker.saveSurveyFeedback(element).subscribe((res: any) => {

      });
    });
    this.jobseeker.saveConsolidateFeedback(this.interviewid).subscribe((res: any) => {
      this.modelService.dismissAll();
      this.toastr.success('Feedback submitted and closed');
    }, err => {

    });

  }
}
