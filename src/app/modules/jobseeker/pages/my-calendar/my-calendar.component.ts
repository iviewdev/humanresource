import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EventSettingsModel, View, DayService, MonthService, WeekService, ScheduleComponent, WorkHoursModel } from '@syncfusion/ej2-angular-schedule';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../service/jobseeker.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-my-calendar',
  templateUrl: './my-calendar.component.html',
  styleUrls: ['./my-calendar.component.scss'],
  providers: [DayService, WeekService, MonthService]
})
export class MyCalendarComponent implements OnInit {
  @ViewChild('scheduleObj', { static: true })
  public scheduleObj: ScheduleComponent;
  public scheduleViews: View[] = ['Day', 'Week', 'Month'];
  public newViewMode: View = 'Week';
  public workDay: number[] = [];
  public scheduleHours: WorkHoursModel;
  public username: string;
  public eventData: EventSettingsModel = {
    // dataSource: [{
    //   Id: 1,
    //   Subject: 'Board Meeting',
    //   StartTime: new Date(),
    //   EndTime: new Date(),
    // },
    // {
    //   Subject: "Interview Request From Jobseeker:ranjeet@yahoo.com",
    //   StartTime: new Date(2020, 10, 6, 8, 0),
    //   EndTime: new Date(2020, 9, 6, 9, 0),
    // },
    // {
    //   Subject: 'Sprint Planning with Team members',
    //   StartTime: new Date(2020, 9, 20, 9, 0),
    //   EndTime: new Date(2020, 9, 20, 11, 0)
    // },
    // {
    //   "Id": 4,
    //   "Subject": "vik_shar@yahoo.com",
    //   "StartTime": "2020-10-18T10:00:00.000+0000",
    //   "EndTime": "2020-10-18T12:00:00.000+0000",
    //   "RecurrenceRule": "FREQ=DAILY;INTERVAL=1;COUNT=4;",
    // }
    // ]
  }

  constructor(private jobseeker: JobSeekerService, private storageService: StorageService,
    private route: ActivatedRoute, private location: Location) { }

  ngOnInit() {
    this.username = this.route.snapshot.paramMap.get('username');
    this.getFreelancerCalendarRecord(this.username);
    this.getWorkDayHours(this.username);
  }

  onActionBegin($event) {


    //  console.log($event);
  }

  onNavigating(event): void {
    console.log(event);
  }


  getWorkDayHours(username) {
    this.jobseeker.getWorkDayHours(username).subscribe((res: any) => {
      debugger;
      if (res) {
        var arrList = [];
        res.dayofweek.split(',').forEach(element => {
          arrList.push(+element);
        });
        this.workDay = arrList;
        this.scheduleHours = { highlight: true, start: res.starthour, end: res.endhour }
      }
    });
  }



  public eventRecord: any = [];
  getFreelancerCalendarRecord(username) {

    this.jobseeker.getFreelancerCurrentEvent(username).subscribe((res: any) => {
      debugger;
      if (res) {
        res.forEach(element => {
          this.eventRecord.push(
            {
              "Id": element.id,
              "Subject": element.subject,
              "Description": element.description,
              "StartTime": element.starttime,
              "EndTime": element.endtime,
              "RecurrenceRule": element.recurrenceRule,
              "RecurrenceException": element.recurranceException ? element.recurranceException : "",
              "IsAllDay": (element.recurrenceRule) ? "" : element.isallday,

            }
          )
        });
      }
      this.scheduleObj.eventSettings.dataSource = this.eventRecord;
      // console.log(this.eventRecord);
    });
  }

  back() {
    this.location.back()
  }

}
