import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JobSeekerService } from '../../service/jobseeker.service';



@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.scss']
})
export class SearchListComponent implements OnInit {
  public searchData: any = [];
  public isLoading: boolean = false;
  constructor(private jobseekerService: JobSeekerService,
    private router: Router) {

  }

  ngOnInit() {

  }

  pushSearchData($event) {

    this.isLoading = true;
    this.jobseekerService.search($event).subscribe((res: any) => {
      this.isLoading = false;
      if (res) {
        this.searchData = res;
        console.log('this is the search list');
        console.log(this.searchData);
      }
    }, err => {
      console.log(err);
      this.isLoading = false;
    });
  }


  openSearchDetailPage(fsid) {
    this.router.navigate(['jobseeker/search-details', fsid]);
  }



}
