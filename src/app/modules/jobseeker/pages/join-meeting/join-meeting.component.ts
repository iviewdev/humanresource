import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from 'src/app/shared/services/storage.service';

@Component({
  selector: 'app-join-meeting',
  templateUrl: './join-meeting.component.html',
  styleUrls: ['./join-meeting.component.scss']
})
export class JoinMeetingComponent implements OnInit {
  public user_role: string;
  public inteviewid;
  public userId: number;
  public freelancerid;
  public jobseekerid;
  public interviewstarttime;
  public interviewendtime;
  public scheduleid;
  constructor(private storage: StorageService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.inteviewid = params.interviewid;
      this.freelancerid = params.freelancerid;
      this.jobseekerid = params.jobseekerid;
      this.interviewstarttime = params.starttime;
      this.interviewendtime = params.endtime;
      this.scheduleid = params.scheduleid;
    });
    this.user_role = this.storage.getItem('role');
    this.userId = +this.storage.getItem('id');
   
  }



}
