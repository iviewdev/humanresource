import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../service/jobseeker.service';

@Component({
  selector: 'app-interview-result',
  templateUrl: './interview-result.component.html',
  styleUrls: ['./interview-result.component.scss']
})
export class InterviewResultComponent implements OnInit {
  public InterviewResultSummary: any = [];
  constructor(private jobseekerService: JobSeekerService,
    private storageService: StorageService) { }

  ngOnInit() {
    console.log(this.storageService.getItem('id'));
    this.getInterResultSummary(this.storageService.getItem('id'));
    
  }

  getInterResultSummary(id) {
    this.jobseekerService.getinterviewresultsummaries(id).subscribe((res: any) => {
      if (res) {
        console.log(res);
        this.InterviewResultSummary = res;
      } else {

      }
    }, err => {

    });
  }

}
