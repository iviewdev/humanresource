import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../service/jobseeker.service';
import { saveAs } from 'file-saver';
@Component({
  selector: 'app-my-document-approval',
  templateUrl: './my-document-approval.component.html',
  styleUrls: ['./my-document-approval.component.scss']
})
export class MyDocumentApprovalComponent implements OnInit {
  public experienceData: Observable<any>;
  public educationData: Observable<any>;
  public basicProfile: any;
  constructor(private jobseekerService: JobSeekerService,
    private storageService: StorageService) { }

  ngOnInit() {
    this.getExperience(this.storageService.getItem('username'));
    this.getEducation(this.storageService.getItem('username'));
    this.getBasicProfile(this.storageService.getItem('username'));
  }

  //get Education


  getEducation(username) {
    this.educationData = this.jobseekerService.getEducation(username);

  }

  //get Experience


  getExperience(username) {
    this.experienceData = this.jobseekerService.getExperience(username);
    this.experienceData.subscribe((res: any) => {
      console.log(res);
    })
  }

  // get basic profile

  getBasicProfile(username) {
    this.jobseekerService.getbasicprofile(username).subscribe((res: any) => {
      console.log(res);
      if (res) {
        this.basicProfile = res;
      }
    });
  }

  downloadFile(url) {
    var v = url.split('/');
    var n = v[v.length - 1];
    var f = new FormData();
    f.append('bucket', 'iviewid' + this.basicProfile.id);
    f.append('key', n);
    this.jobseekerService.downloadFile(f).subscribe((res: any) => {
      saveAs(res, n);
    }, err => {
      console.log(err);
    })
  }

}
