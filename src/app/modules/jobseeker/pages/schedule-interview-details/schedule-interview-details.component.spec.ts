import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleInterviewDetailsComponent } from './schedule-interview-details.component';

describe('ScheduleInterviewDetailsComponent', () => {
  let component: ScheduleInterviewDetailsComponent;
  let fixture: ComponentFixture<ScheduleInterviewDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleInterviewDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleInterviewDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
