import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JobSeekerService } from '../../service/jobseeker.service';
import { PlayRecordingsComponent } from '../play-recordings/play-recordings.component';

@Component({
  selector: 'app-schedule-interview-details',
  templateUrl: './schedule-interview-details.component.html',
  styleUrls: ['./schedule-interview-details.component.scss']
})
export class ScheduleInterviewDetailsComponent implements OnInit {
  public interviewDetails: any;
  public interviewid: any;
  public videoRecording: any = [];
  constructor(private router: Router,
    private route: ActivatedRoute,
    private jobseeker: JobSeekerService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.interviewid = this.route.snapshot.paramMap.get('interviewid');
    this.getInterviewScheduleByInterviewId(this.interviewid);
  }

  getInterviewScheduleByInterviewId(interviewid) {
    this.jobseeker.getInterviewScheduleById(interviewid).subscribe((res: any) => {
      if (res) {
        console.log(res);
        this.interviewDetails = res;
        this.getVideoRecordings(this.interviewid);
      }
    }, err => {
      console.log(err);
    });
  }

  getVideoRecordings(id) {

    this.jobseeker.getVideoRecordings(id).subscribe((res: any) => {
      if (res) {
        this.videoRecording = res;
      } else {
        this.videoRecording = [];
      }
    }, err => {

    });
  }

  play(url) {

    const modelRef = this.modalService.open(PlayRecordingsComponent);
    modelRef.componentInstance.url = url;

  }

  join_meeting() {
    this.router.navigate(['/jobseeker/join-meeting'],{queryParams:this.interviewDetails});
  }

}
