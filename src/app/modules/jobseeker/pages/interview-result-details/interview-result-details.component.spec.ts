import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterviewResultDetailsComponent } from './interview-result-details.component';

describe('InterviewResultDetailsComponent', () => {
  let component: InterviewResultDetailsComponent;
  let fixture: ComponentFixture<InterviewResultDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterviewResultDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterviewResultDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
