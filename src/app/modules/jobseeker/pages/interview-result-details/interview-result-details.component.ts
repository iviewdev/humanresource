import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { JobSeekerService } from '../../service/jobseeker.service';

@Component({
  selector: 'app-interview-result-details',
  templateUrl: './interview-result-details.component.html',
  styleUrls: ['./interview-result-details.component.scss']
})
export class InterviewResultDetailsComponent implements OnInit {
  public interviewsummaryresult: any;
  public interviewResultDetails: any = [];
  constructor(private route: ActivatedRoute,
    private jobseekerService: JobSeekerService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.interviewsummaryresult = params;
    });
    console.log(this.interviewsummaryresult);
    this.getInterviewResultDetail(this.interviewsummaryresult.resultid);
  }

  getInterviewResultDetail(id) {
    debugger;
    this.jobseekerService.interviewresultdetail(id).subscribe((res: any) => {
      if (res) {
        this.interviewResultDetails = res;
      }
    }, err => {

    });
  }

}
