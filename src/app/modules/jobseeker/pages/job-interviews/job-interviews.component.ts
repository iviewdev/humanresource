import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../service/jobseeker.service';

@Component({
  selector: 'app-job-interviews',
  templateUrl: './job-interviews.component.html',
  styleUrls: ['./job-interviews.component.scss']
})
export class JobInterviewsComponent implements OnInit {
  public jobInterviews: any = [];
  constructor(private jobseekerService: JobSeekerService,
    private storateService: StorageService) { }

  ngOnInit() {
    this.getAllJobInterViews(+this.storateService.getItem('id'));
  }

  getAllJobInterViews(id) {
    this.jobseekerService.getAllMyCorpInterviews(54).subscribe((res: any) => {
      this.jobInterviews = res;
      console.log(res);
    }, (err) => {
      console.log(err);
    });
  }

}
