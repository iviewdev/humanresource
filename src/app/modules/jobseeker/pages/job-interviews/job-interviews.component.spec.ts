import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobInterviewsComponent } from './job-interviews.component';

describe('JobInterviewsComponent', () => {
  let component: JobInterviewsComponent;
  let fixture: ComponentFixture<JobInterviewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobInterviewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobInterviewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
