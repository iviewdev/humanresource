import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { JobSeekerService } from '../../../service/jobseeker.service';
import { ToastrService } from 'ngx-toastr';
declare var Razorpay: any;
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  public fsid: any;
  public orderid: any;
  public orderItems: any;
  public order: any;
  constructor(private location: Location,
    private route: ActivatedRoute, private router: Router,
    private jobseeker: JobSeekerService,
    private toastr: ToastrService) {

  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.fsid = params['fsid'];
      this.orderid = params['orderid'];
    });
    this.getOrderItem();
    this.getOrder();
  }

  getOrderItem() {
    this.jobseeker.getOrderitems(this.orderid).subscribe((res: any) => {
      //  console.log(res);
      this.orderItems = res;
    }, err => {
      console.log(err);
    });
  }

  getOrder() {
    this.jobseeker.getOrder(this.orderid).subscribe((res: any) => {
      console.log(res);
      this.order = res;
    }, err => {
      console.log(err);
    });
  }

  publishSuccessfullOrder() {
    debugger;
    let d = {
      "orderid": this.orderid,
      "orderdate": this.order.orderdate,
      "ordercompletedate": this.order.ordercompletedate,
      "customertype": this.order.customertype,
      "orderstatus": "Paid",
      "customerid": this.order.customerid,
      "subtotal": this.order.subtotal,
      "gst": this.order.gst,
      "grandtotal": this.order.grandtotal,
      "refundstatus": null,
      "scheduleid": this.order.scheduleid
    };
    this.jobseeker.publishSuccessfulOrder(d).subscribe((res: any) => {
      this.toastr.success('Paid successfully');
      this.router.navigate(['jobseeker/order-confirmation'],{queryParams:{orderid:this.orderid,interviewid:this.order.scheduleid}});
    }, err => {
      console.log(err);
    });
  }

  cancelOrder() {
    this.jobseeker.cancelOrder(this.orderid).subscribe((res: any) => {
      if (res) {
        this.toastr.error('Order Canceled successfully');
        this.location.back();
      }
    }, err => {

    })
  }

  back() {
    this.location.back();
  }

  navigateToSendInterviewRequest(fsid) {
    this.router.navigate(['jobseeker/send-interview-request', fsid]);
  }


  payNow() {

    var options = {
      "key": "rzp_test_dYcSSon3xxWzKA", // Enter the Key ID generated from the Dashboard
      "amount": "100", // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
      "currency": "INR",
      "name": "Acme Corp",
      "description": "Test Transaction",
      "image": "",
      // "order_id": "1", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
      "handler": function (response) {
        // alert(response.razorpay_payment_id);
        // alert(response.razorpay_order_id);
        // alert(response.razorpay_signature)
        console.log(response);
        this.toastr.success('Paid successfully');
        this.publishSuccessfullOrder();
      }.bind(this),
      "prefill": {
        "name": "Ranjeet kumar Tiwari",
        "email": "ranjeet@gmail.com",
        "contact": "8448724145"
      },
      "notes": {
        "address": "G-76 Ali vihar New delhi"
      },
      "theme": {
        "color": "#F37254"
      }
    };

    var rzp = new Razorpay(options);
    rzp.open();
  }


}
