import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../../service/jobseeker.service';

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.scss']
})
export class OrderSummaryComponent implements OnInit {
  public myOrders: any = [];
  public user_id: any;
  constructor(private jobseeker: JobSeekerService, private storage: StorageService,
    private toastr:ToastrService) { }

  ngOnInit() {
    this.user_id = this.storage.getItem('id');
    this.getMyOrders();
  }

  getMyOrders() {
    this.jobseeker.getjobseekerorders(this.user_id).subscribe((res: any) => {
      console.log(res);
      if (res) {
        this.myOrders = res;
      }
    }, err => {

    });
  }

  cancelOrder(id) {
    this.jobseeker.cancelOrder(id).subscribe((res: any) => {
      if (res) {
        this.toastr.error('Order Canceled successfully');
    //    this.router.navigate(['jobseeker/search-interviewers']);
      }
    }, err => {

    })
  }
}
