import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../service/jobseeker.service';

@Component({
  selector: 'app-interview-schedules',
  templateUrl: './interview-schedules.component.html',
  styleUrls: ['./interview-schedules.component.scss'],
})
export class InterviewSchedulesComponent implements OnInit {
  public scheduleInterview: any = [];
  public completedInterview: any = [];
  constructor(private jobseekerService: JobSeekerService,
    private storateService: StorageService) { }

  ngOnInit() {
    this.getAllMyInterviews(this.storateService.getItem('id'));
  }

  getAllMyInterviews(id) {
    this.jobseekerService.getAllMyInterviews(id).subscribe((res: any) => {
      if (res) {
        this.scheduleInterview = [];
        this.completedInterview = [];
        res.forEach(element => {
          if (element.status == 'Completed & Closed') {
            this.completedInterview.push(element);
          } else {
            this.scheduleInterview.push(element);
          }
        });
      //  this.scheduleInterview = res;
        console.log(this.scheduleInterview);
      } else {
        this.scheduleInterview = [];
      }
    }, err => {
      console.log(err);
    });

  }

}
