import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SortPipe } from 'src/app/shared/pipes/sort.pipe';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../../service/jobseeker.service';
import { saveAs } from 'file-saver'
import { SpinnerService } from 'src/app/modules/employer/service/spinner.service';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';
import { ToastrService } from 'ngx-toastr';
import { ProfileService } from '../../../service/profile.service';
@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
  public username: any;
  constructor(private jobSeekerService: JobSeekerService,
    private storageService: StorageService,
    private sortPipe: SortPipe,
    private spinner: SpinnerService,
    private router: Router,
    private confirmDialogService: ConfirmationDialogService,
    private toastr: ToastrService,
    private profileService:ProfileService) { }

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.profileService.setProfileCompleteness(this.username);
    this.getAllCountries();
    this.getAllCompetency(this.username);
    this.getOtherDetiails(this.username);
  }

  public basicProfile: any;
  getbasicprofile(username) {
    this.spinner.showSpinner();
    //   debugger;
    if (username) {
      this.jobSeekerService.getbasicprofile(username).subscribe((data: any) => {
        console.log(data);
        if (data) {
          this.getAllStatesForCountry(data.country);
          this.getAllCitiesForState(data.state);
          data.country = this.countryList.find(x => x.id == data.country)['countryname'];

          // console.log(v);
          this.basicProfile = data;
          this.spinner.hideSpinner();
        }
      },
        err => {
          console.log(err);
          this.spinner.hideSpinner();
        });

    }
  }


  //##################################
  public countryList: any = [];
  getAllCountries() {
    this.spinner.showSpinner();
    this.jobSeekerService.getAllCountries().subscribe((data: any) => {
      console.log(data);
      if (data) {
        this.countryList = this.sortPipe.transform(data.content, "asc", "countryname");
        this.getbasicprofile(this.username);
        this.spinner.hideSpinner();
      }
    },
      (err) => {
        console.log(err);
        this.spinner.hideSpinner();
      });
  }
  onChangeCountry(countryCode: string) {
    if (countryCode) {
      this.getAllStatesForCountry(countryCode);
    }
  }
  public stateList: any = [];
  getAllStatesForCountry(countryCode: string) {
    this.jobSeekerService.getAllStatesForCountry(countryCode).subscribe((data: any) => {
      console.log(data);
      this.stateList = this.sortPipe.transform(data, "asc", "statename");
      this.basicProfile.state = this.stateList.find(x => x.statecode == this.basicProfile.state)['statename'];
    },
      (err) => {
        console.log(err);
      });
  }
  onChangeState(stateCode: string) {
    if (stateCode) {
      this.getAllCitiesForState(stateCode);
    }
  }


  public cityList: any = [];
  getAllCitiesForState(stateCode: string) {
    this.jobSeekerService.getAllCitiesForState(stateCode).subscribe((data: any) => {
      this.cityList = this.sortPipe.transform(data, "asc", "cityname");
      this.basicProfile.city = this.cityList.find(x => x.citycode == this.basicProfile.city)['cityname'];
    },
      (err) => {
        console.log(err);
      });
  }

  //##################################

  public competencies: any = [];
  getAllCompetency(username) {
    // this.spinner.showSpinner();
    this.jobSeekerService.getSkillsCompetencies(username).subscribe((res: any) => {
      if (res) {
        this.competencies = res;
      } else {
        this.competencies = [];
      }
      // this.spinner.hideSpinner();
    }, err => {
      this.competencies = [];
      //  this.spinner.hideSpinner();
    });
  }


  public otherDetails: any;
  getOtherDetiails(username) {
    this.jobSeekerService.getOtherDetails(username).subscribe((res: any) => {
      console.log(res);
      if (res != null) {
        this.otherDetails = res;

      }
    }, err => {
      console.log(err);
    })
  }


  submit() {
    this.confirmDialogService.confirm(`Please confirm`, `All The Documents You have Attached would be submitted for verification to Admin. Your Profile would be Visible for Search your Profile is 90% completed. Once your docs are admin verified, your profile would be visible with "Verified Internally" Label. This would improve your Prospects for Shortlisting by Employees.`,
      `Submit Details`, 'Cancel', 'md')
      .then((confirmed) => {
        if (confirmed) {
          this.spinner.showSpinner();
          this.jobSeekerService.submitDetails(this.username).subscribe((res: any) => {
            console.log(res);
            this.basicProfile.status = res.status;
            this.spinner.hideSpinner();
            this.toastr.success('Details submitted successfully.');
          },
            err => {
              this.toastr.error('Error in submitted details.');
              console.log(err);
              this.spinner.hideSpinner();
            });
        }
      }).catch((err) => {
        console.log(err);
        this.spinner.hideSpinner();
      });
  }

  previous() {
    this.router.navigate(['/jobseeker/profile/career-details']);
  }

  downloadFile(url) {
    var v = url.split('/');
    var n = v[v.length - 1];
    var f = new FormData();
    f.append('bucket', 'iviewid' + this.basicProfile.id);
    f.append('key', n);
    this.jobSeekerService.downloadFile(f).subscribe((res: any) => {
      saveAs(res, n);
    }, err => {
      console.log(err);
    })
  }

}
