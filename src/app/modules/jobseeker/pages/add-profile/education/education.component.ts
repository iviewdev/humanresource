import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { SpinnerService } from 'src/app/modules/employer/service/spinner.service';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../../service/jobseeker.service';
import { ProfileService } from '../../../service/profile.service';
import { AddEducationComponent } from './add-education/add-education.component';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent implements OnInit {
  public username: any;
  constructor(private router: Router,
    private modalService: NgbModal,
    private jobSeekerService: JobSeekerService,
    private storageService: StorageService,
    private spinner: SpinnerService,
    private toastr:ToastrService,
    private profileService:ProfileService,
    private confirmDailogService:ConfirmationDialogService) { }

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.profileService.setProfileCompleteness(this.username);
    this.getbasicprofile(this.username);
    this.getEducationList(this.username);
  }


  //################################
  public basicProfile: any;
  getbasicprofile(username) {
    if (username) {
      this.jobSeekerService.getbasicprofile(username).subscribe((data: any) => {
        if (data) {
          if (data.status == 'Pending_Approval') {
            this.confirmDailogService.confirm(`Information`, `You cannot make changes to  Documents as they are pending admin approval.`,
              `OK`, '', 'md')
              .then((confirmed) => {
                if (confirmed) {

                }
              }).catch((err) => {
                console.log(err);

              });
          }
          this.basicProfile = data;
        }
      },
        err => {
          console.log(err);
        });

    }
  }

  //##########################
  public educations: any = [];
  getEducationList(username) {
    this.spinner.showSpinner();
    this.jobSeekerService.getEducation(username).subscribe((res: any) => {
      if (res) {
        this.educations = res;
      } else {
        this.educations = [];
      }
      this.spinner.hideSpinner();
    }, err => {
      this.spinner.hideSpinner();
      this.educations = [];
      console.log(err);
    })
  }

  openEducation(edu?) {
    const modelRef = this.modalService.open(AddEducationComponent, {
      size: 'md',
      backdrop: 'static'
    });
    modelRef.componentInstance.username = this.username;
    modelRef.componentInstance.basicProfileId = this.basicProfile.id;
    modelRef.componentInstance.edu = edu;
    modelRef.result.then((res: any) => {
      this.getEducationList(this.username);
    }, dissmiss => {

    });
  }

  next() {
    this.router.navigate(['/jobseeker/profile/competency']);
  }

  previous() {
    this.router.navigate(['/jobseeker/profile/experience']);
  }



  delete(eid) {
    this.spinner.showSpinner();
    this.jobSeekerService.deleteEducation(eid).subscribe((res: any) => {
      console.log(res);
      this.educations = this.educations.filter(x => x.eid !== eid);
      this.spinner.hideSpinner();
      this.toastr.success('Deleted successfully');
    }, err => {
      this.spinner.hideSpinner();
      console.log(err);
    });
  }

}
