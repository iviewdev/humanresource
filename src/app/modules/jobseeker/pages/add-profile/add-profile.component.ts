import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../service/jobseeker.service';
import { ProfileService } from '../../service/profile.service';


@Component({
  selector: 'app-add-profile',
  templateUrl: './add-profile.component.html',
  styleUrls: ['./add-profile.component.scss']
})
export class AddProfileComponent implements OnInit {
  public username: any;
  public complete: number = 0;
  constructor(private jobSeekerService: JobSeekerService,
    private storage: StorageService,
    private profileService: ProfileService) {

  }

  ngOnInit() {
    this.username = this.storage.getItem('username');
    this.profileService.profilecompleteness.subscribe(res => {
      
      this.complete = res;
    });
  }


}
