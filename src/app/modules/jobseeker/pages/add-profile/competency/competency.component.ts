import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { SpinnerService } from 'src/app/modules/employer/service/spinner.service';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../../service/jobseeker.service';
import { ProfileService } from '../../../service/profile.service';
import { AddCompetencyComponent } from './add-competency/add-competency.component';

@Component({
  selector: 'app-competency',
  templateUrl: './competency.component.html',
  styleUrls: ['./competency.component.scss']
})
export class CompetencyComponent implements OnInit {
  public username: any;
  constructor(private router: Router, private modelService: NgbModal,
    private jobseekerService: JobSeekerService,
    private storageService: StorageService,
    private spinner: SpinnerService, private toastr: ToastrService,
    private profileService:ProfileService,
    private confirmDailogService:ConfirmationDialogService) { }

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.profileService.setProfileCompleteness(this.username);
    this.getAllCompetency(this.username);
    this.getbasicprofile(this.username);
  }


  //################################
  public basicProfile: any;
  getbasicprofile(username) {
    if (username) {
      this.jobseekerService.getbasicprofile(username).subscribe((data: any) => {
        if (data) {
          if (data.status == 'Pending_Approval') {
            this.confirmDailogService.confirm(`Information`, `You cannot make changes to  Documents as they are pending admin approval.`,
              `OK`, '', 'md')
              .then((confirmed) => {
                if (confirmed) {

                }
              }).catch((err) => {
                console.log(err);

              });
          }
          this.basicProfile = data;
        }
      },
        err => {
          console.log(err);
        });

    }
  }


  competanceAddUpdate(compe?) {
    const modelRef = this.modelService.open(AddCompetencyComponent, {
      size: 'md',
      backdrop: 'static'
    });
    modelRef.componentInstance.username = this.username;
    modelRef.componentInstance.compe = compe;
    modelRef.result.then((res: any) => {
      this.getAllCompetency(this.username);
    }, dismiss => {

    });
  }

  next() {
    this.router.navigate(['/jobseeker/profile/career-details']);
  }

  previous() {
    this.router.navigate(['/jobseeker/profile/education']);
  }


  public competencies: any = [];
  getAllCompetency(username) {
    this.spinner.showSpinner();
    this.jobseekerService.getSkillsCompetencies(username).subscribe((res: any) => {
      if (res) {
        this.competencies = res;
        console.log(JSON.stringify(this.competencies));
      } else {
        this.competencies = [];
      }
      this.spinner.hideSpinner();
    }, err => {
      this.competencies = [];
      this.spinner.hideSpinner();
    });
  }

  delete(competencyid) {
    this.spinner.showSpinner();
    this.jobseekerService.deleteSkillCompetency(competencyid).subscribe((res: any) => {
      console.log(res);
      this.competencies = this.competencies.filter(x => x.competencyid !== competencyid);
      this.spinner.hideSpinner();
      this.toastr.success('Deleted successfully');
    }, err => {
      this.spinner.hideSpinner();
      console.log(err);
    });
  }

}
