import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SpinnerService } from 'src/app/modules/employer/service/spinner.service';
import { JobSeekerService } from 'src/app/modules/jobseeker/service/jobseeker.service';

@Component({
  selector: 'app-add-competency',
  templateUrl: './add-competency.component.html',
  styleUrls: ['./add-competency.component.scss']
})
export class AddCompetencyComponent implements OnInit {
  public competency: FormGroup;
  public submitted: boolean = false;
  @Input() username;
  @Input() compe;
  constructor(private modelService: NgbActiveModal,
    private fb: FormBuilder,
    private jobSeekerService: JobSeekerService,
    private spinner: SpinnerService) { }

  ngOnInit() {
    this.create();
    this.getAllCompetancelList();
    if (this.compe) {
      this.competency.patchValue(this.compe);
    }
  }


  create() {
    this.competency = this.fb.group({
      username: [this.username],
      competencyid: [''],
      competencyname: ['', Validators.required],
      jobcategory: [''],
      skillid: [''],
      skillname: [''],
      skillweightage: [''],
      years_of_exp: ['', Validators.required],
      competencyscale: ['', Validators.required],
    });
  }

  get f() {
    return this.competency.controls;
  }


  submit() {
    this.submitted = true;
    if (this.competency.invalid) {
      return;
    }
    this.spinner.showSpinner();
    this.jobSeekerService.submitCompetencySkills(this.competency.value).subscribe((res: any) => {
      this.spinner.hideSpinner();
      if (res) {
        this.modelService.close();
      }
    }, err => {
      this.spinner.hideSpinner();
      console.log(err);
    });
  }



  //Get Competance List
  public competanceData: any = [];
  getAllCompetancelList() {
    this.jobSeekerService.getAllCompetanceList().subscribe((data: any) => {
      this.competanceData = data.content;
    },
      (err) => {
        console.log(err);
      });
  }

  close() {
    this.modelService.dismiss();
  }

}
