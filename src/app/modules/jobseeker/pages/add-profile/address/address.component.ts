import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SpinnerService } from 'src/app/modules/employer/service/spinner.service';
import { SortPipe } from 'src/app/shared/pipes/sort.pipe';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../../service/jobseeker.service';
import { ProfileService } from '../../../service/profile.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {

  public addressForm: FormGroup;
  public submitted: boolean = false;
  public username: string = '';
  constructor(private router: Router, private fb: FormBuilder,
    private jobSeekerService: JobSeekerService,
    private sortPipe: SortPipe, private spinner: SpinnerService,
    private toastr: ToastrService,
    private storageService: StorageService,
    private profileService:ProfileService,
    private confirmDailogService:ConfirmationDialogService) { }

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.profileService.setProfileCompleteness(this.username);
    this.create();
    this.getAllCountries();
    this.getbasicprofile(this.username);
  }

  //#########################################
  create() {
    this.addressForm = this.fb.group({
      username:[this.username],
      address1: ['', Validators.required],
      address: [''],
      country: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      zip: ['', Validators.required],
      addressurl: ['', Validators.required],
    });
  }

  //################################
  public basicProfile: any;
  getbasicprofile(username) {
    if (username) {
      this.jobSeekerService.getbasicprofile(username).subscribe((data: any) => {
        if (data) {
          if (data.status == 'Pending_Approval') {
            this.confirmDailogService.confirm(`Information`, `You cannot make changes to  Documents as they are pending admin approval.`,
              `OK`, '', 'md')
              .then((confirmed) => {
                if (confirmed) {

                }
              }).catch((err) => {
                console.log(err);

              });
          }
          delete data.jobSeekerOther;
          delete data.experiences;
          delete data.educations;
          this.basicProfile = data;
          if (data.country) {
            this.getAllStatesForCountry(data.country);
          }
          if (data.state) {
            this.getAllCitiesForState(data.state);
          }
          this.addressForm.patchValue(data);
        }
      },
        err => {

        });

    }
  }

  //##################################
  get f() {
    return this.addressForm.controls;
  }

  //#####################################
  submit() {
    this.submitted = true;
    if (this.addressForm.invalid) {
      return;
    }
    if (!this.addressForm.touched) {
      this.next();
      return false;
    }

    this.spinner.showSpinner();
    const addressObj = Object.assign(this.basicProfile, this.addressForm.value);
    if (this.attachedAddress != null) {
      this.jobSeekerService.uploadAddFile(this.attachedAddress).toPromise()
        .then(event => {
          if (event instanceof HttpResponse) {
            var url = this.getEscapeStringUrl(event.body as string);
            addressObj.addressurl = url;
            this.attachedAddress = null;
            this.jobSeekerService.updateAddress(addressObj).subscribe(res => {
              this.spinner.hideSpinner();
              this.toastr.success('Address saved successfully.');
              this.next();
            },
              err => {
                this.spinner.hideSpinner();
                this.toastr.error('Error in Address saved.');
                console.log(err);
              });
          }
        }).catch(err => {
          this.spinner.hideSpinner();
          console.log(err);
        });
    } else {
      this.jobSeekerService.updateAddress(addressObj).subscribe(res => {
        this.spinner.hideSpinner();
        this.toastr.success('Address saved successfully.');
        this.next();
      },
        err => {
          this.spinner.hideSpinner();
          this.toastr.error('Error in Address saved.');
          console.log(err);
        });
    }
  }

  //##################################
  public countryList: any = [];
  getAllCountries() {
    this.jobSeekerService.getAllCountries().subscribe((data: any) => {
      if (data) {
        this.countryList = this.sortPipe.transform(data.content, "asc", "countryname");
      }
    },
      (err) => {
        console.log(err);
      });
  }
  onChangeCountry(countryCode: string) {
    if (countryCode) {
      this.getAllStatesForCountry(countryCode);
    }
  }
  public stateList: any = [];
  getAllStatesForCountry(countryCode: string) {
    this.jobSeekerService.getAllStatesForCountry(countryCode).subscribe((data: any) => {
      this.stateList = this.sortPipe.transform(data, "asc", "statename");
    },
      (err) => {
        console.log(err);
      });
  }
  onChangeState(stateCode: string) {
    if (stateCode) {
      this.getAllCitiesForState(stateCode);
    }
  }


  public cityList: any = [];
  getAllCitiesForState(stateCode: string) {
    this.jobSeekerService.getAllCitiesForState(stateCode).subscribe((data: any) => {
      this.cityList = this.sortPipe.transform(data, "asc", "cityname");
    },
      (err) => {
        console.log(err);
      });
  }

  //##################################

  next() {
    this.router.navigate(['/jobseeker/profile/experience']);
  }

  previous() {
    this.router.navigate(['/jobseeker/profile/idproof']);
  }

  //#######################

  //########################################
  public attachedAddress: FormData;

  attachAddressProof(event: any) {
    if (event.target.files.length > 0) {
      const uploadfile: File = event.target.files[0];
      if (this.validateFile(uploadfile)) {
        this.addressForm.patchValue({
          addressurl: uploadfile.name
        });
        this.addressForm.markAsTouched();
        const formData: FormData = new FormData();
        formData.append('file', uploadfile);
        formData.append('bucket', 'iviewid' + this.basicProfile.id);
        formData.append('username', this.username);
        this.attachedAddress = formData; // this.getFormData(uploadfile);
      }

    }
  }

  validateFile(file: File): boolean {

    let IsValid = true;
    const extension = file.name.substr(file.name.lastIndexOf('.') + 1);
    if (file.size > (1024 * 1024)) {
      this.toastr.error('Please select file size less than 1 MB');
      IsValid = false;
    }
    if (extension.toLowerCase() != 'doc' && extension.toLowerCase() != 'docx' &&
      extension.toLowerCase() != 'pdf' && extension.toLowerCase() != 'jpeg'
      && extension.toLowerCase() != 'jpg' && extension.toLowerCase() != 'png') {
      this.toastr.error("Only doc, docx, JPEG, PNG and PDF files are allowed.");
      IsValid = false;
    }
    return IsValid;
  }



  //##########################
  getEscapeStringUrl(url: string): string {
    if (url) {
      return url.replace('\ "', '').replace('"', '').replace('"', '');
    }
    return '';
  }


}
