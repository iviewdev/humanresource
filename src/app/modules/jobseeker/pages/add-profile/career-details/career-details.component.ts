import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SpinnerService } from 'src/app/modules/employer/service/spinner.service';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../../service/jobseeker.service';
import { ProfileService } from '../../../service/profile.service';

@Component({
  selector: 'app-career-details',
  templateUrl: './career-details.component.html',
  styleUrls: ['./career-details.component.scss']
})
export class CareerDetailsComponent implements OnInit {
  public edit_mode: boolean = false;
  public username: string;
  public desiredindustry: string;
  public desiredprofile: string;
  public desiredlocation: string;
  public jobtype: string;
  public keywords: string;
  constructor(private router: Router,
    private jobSeekerService: JobSeekerService,
    private spinner: SpinnerService,
    private storageService: StorageService,
    private profileService: ProfileService,
    private confirmDailogService: ConfirmationDialogService) { }

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.profileService.setProfileCompleteness(this.username);
    this.getOtherDetiails(this.username);
    this.getbasicprofile(this.username);
    this.getindustry();
    this.getroles();
  }

  public industries: any = [];
  getindustry() {
    this.jobSeekerService.getCurrentIndustry().subscribe((res: any) => {
      if (res) {
        this.industries = res.content;
      } else {
        this.industries = [];
      }
    }, err => {
      this.industries = [];
      console.log(err);
    })
  }

  public roles: any = [];
  getroles() {
    this.jobSeekerService.getAllRoles().subscribe((res: any) => {
      if (res) {
        this.roles = res.content;
      } else {
        this.roles = [];
      }
    }, err => {
      this.roles = [];
      console.log(err);
    })
  }

  //################################
  public basicProfile: any;
  getbasicprofile(username) {
    if (username) {
      this.jobSeekerService.getbasicprofile(username).subscribe((data: any) => {
        if (data) {
          if (data.status == 'Pending_Approval') {
            this.confirmDailogService.confirm(`Information`, `You cannot make changes to  Documents as they are pending admin approval.`,
              `OK`, '', 'md')
              .then((confirmed) => {
                if (confirmed) {

                }
              }).catch((err) => {
                console.log(err);

              });
          }
          this.basicProfile = data;
         // this.basicProfile.status = 'Draft';

        }
      },
        err => {
          console.log(err);
        });

    }
  }


  //Get Other Details
  public otherDetails: any;
  getOtherDetiails(username) {
    this.jobSeekerService.getOtherDetails(username).subscribe((res: any) => {
      console.log(res);
      if (res) {
        this.otherDetails = res;
        this.desiredindustry = res.desiredindustry;
        this.desiredprofile = res.desiredprofile;
        this.desiredlocation = res.desiredlocation;
        this.jobtype = res.jobtype;
        this.keywords = res.keywords;
      }
    }, err => {
      console.log(err);
    });
  }

  //#################################
  toggle_edit_mode(val) {
    if (val) {
      this.updateOtherDetails();
    } else {
      this.edit_mode = true;
    }
  }

  // Update Profile Title

  updateOtherDetails() {
    this.spinner.showSpinner();
    this.otherDetails.desiredindustry = this.desiredindustry;
    this.otherDetails.desiredprofile = this.desiredprofile;
    this.otherDetails.desiredlocation = this.desiredlocation;
    this.otherDetails.jobtype = this.jobtype;
    this.otherDetails.keywords = this.keywords;
    this.jobSeekerService.updateOtherDetails(this.otherDetails).subscribe((res: any) => {
      this.spinner.hideSpinner();
      this.edit_mode = false;
    }, err => {
      this.spinner.hideSpinner();
      console.log(err);
    });
  }

  next() {
    this.router.navigate(['/jobseeker/profile/review']);
  }

  previous() {
    this.router.navigate(['/jobseeker/profile/competency']);
  }

}
