import { HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { SpinnerService } from 'src/app/modules/employer/service/spinner.service';
import { JobSeekerService } from 'src/app/modules/jobseeker/service/jobseeker.service';

@Component({
  selector: 'app-add-experience',
  templateUrl: './add-experience.component.html',
  styleUrls: ['./add-experience.component.scss']
})
export class AddExperienceComponent implements OnInit {
  public experience: FormGroup;
  public submitted: boolean = false;
  @Input() username;
  @Input() basicProfileId;
  @Input() exp: any;
  @Input() disable: boolean;
  public exp_id: any;
  constructor(private modalService: NgbActiveModal,
    private fb: FormBuilder,
    private jobSeekerService: JobSeekerService,
    private toastr: ToastrService,
    private spinner: SpinnerService) { }

  ngOnInit() {
    this.create();
    this.getAllRoles();
    if (this.exp) {
      this.exp.startdate = new Date(this.exp.startdate).toISOString().split('T')[0];
      this.exp.endDate = new Date(this.exp.endDate).toISOString().split('T')[0];
      this.experience.patchValue(this.exp);
      this.currentJobOnCheck(this.exp.currentjob);
    }
  }

  //##########################
  create() {
    this.experience = this.fb.group({
      exid: [''],
      username: [this.username],
      orgname: ['', Validators.required],
      currentjob: [false],
      startdate: ['', Validators.required],
      endDate: ['', Validators.required],
      designation: ['', Validators.required],
      role_name: ['', Validators.required],
      rolensrep: ['', Validators.required],
      expcert_url: ['', Validators.required],
      joining_letter_url: ['', Validators.required]
    });
  }

  //##############################
  get f() {
    return this.experience.controls;
  }

  //###########################

  submit() {
    this.submitted = true;
    if (this.experience.invalid) {
      return;
    }

    this.spinner.showSpinner();
    this.experience.patchValue({
      currentjob: (this.experience.get('currentjob')) ? 'Y' : 'N',
      startdate: this.formatYYYYMMDD(this.experience.get('startdate').value),
      endDate: this.experience.get('endDate').value ? this.formatYYYYMMDD(this.experience.get('endDate').value) : ''
    });
    debugger;
    this.jobSeekerService.updateExperienceDetails(this.experience.value).subscribe(async (res: any) => {
      await this.uploadExperienceCertificate(res.exid);
    }, err => {
      this.spinner.hideSpinner();
      console.log(err);
    })

  }

  //#############################
  uploadExperienceCertificate(exp_id) {
    if (this.attachedExpCertificate != null) {
      this.attachedExpCertificate.append('exid', exp_id);
      this.jobSeekerService.updateExperienceCertificate(this.attachedExpCertificate).toPromise()
        .then(event => {
          if (event instanceof HttpResponse) {
            this.attachedExpCertificate = null;
            if (this.attachedJoinLetter != null) {
              this.attachedJoinLetter.append('exid', exp_id);
              this.jobSeekerService.updateJoiningLetter(this.attachedJoinLetter).toPromise()
                .then(event => {
                  if (event instanceof HttpResponse) {
                    this.attachedJoinLetter = null;
                    this.spinner.hideSpinner();
                    this.close();
                  }
                }).catch(err => {
                  this.spinner.hideSpinner();
                  console.log(err);
                  this.close();
                })
            } else {
              this.spinner.hideSpinner();
              this.close();
            }
          }

        }).catch(err => {
          this.spinner.hideSpinner();
          console.log(err);
        });
    } else {
      if (this.attachedJoinLetter != null) {
        this.attachedJoinLetter.append('exid', exp_id);
        this.jobSeekerService.updateJoiningLetter(this.attachedJoinLetter).toPromise()
          .then(event => {
            if (event instanceof HttpResponse) {
              this.attachedJoinLetter = null;
              this.spinner.hideSpinner();
              this.close();
            }
          }).catch(err => {
            this.spinner.hideSpinner();
            console.log(err);
            this.close();
          })
      } else {
        this.spinner.hideSpinner();
        this.close();
      }
    }

  }



  //#################################
  formatYYYYMMDD(date: string): string {
    const d = new Date(date);
    return d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2);
    // + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);

  }

  //#####################################3

  public roleData: any = [];
  getAllRoles() {
    this.jobSeekerService.getAllRoles().subscribe((data: any) => {
      console.log(data);
      this.roleData = data.content;
    },
      (err) => {
        console.log(err);
      });
  }



  //########################################
  public attachedExpCertificate: FormData;

  attachExpCert(event: any) {
    if (event.target.files.length > 0) {
      const uploadfile: File = event.target.files[0];
      if (this.validateFile(uploadfile)) {
        this.experience.patchValue({
          expcert_url: uploadfile.name
        });
        this.experience.markAsTouched();
        const formData: FormData = new FormData();
        formData.append('file', uploadfile);
        formData.append('bucket', 'iviewid' + this.basicProfileId);
        formData.append('username', this.username);
        this.attachedExpCertificate = formData; // this.getFormData(uploadfile);
      }

    }
  }


  //########################################
  public attachedJoinLetter: FormData;

  attachJoinLetter(event: any) {
    if (event.target.files.length > 0) {
      const uploadfile: File = event.target.files[0];
      if (this.validateFile(uploadfile)) {
        this.experience.patchValue({
          joining_letter_url: uploadfile.name
        });
        this.experience.markAsTouched();
        const formData: FormData = new FormData();
        formData.append('file', uploadfile);
        formData.append('bucket', 'iviewid' + this.basicProfileId);
        formData.append('username', this.username);
        this.attachedJoinLetter = formData; // this.getFormData(uploadfile);
      }

    }
  }

  validateFile(file: File): boolean {

    let IsValid = true;
    const extension = file.name.substr(file.name.lastIndexOf('.') + 1);
    if (file.size > (1024 * 1024)) {
      this.toastr.error('Please select file size less than 1 MB');
      IsValid = false;
    }
    if (extension.toLowerCase() != 'doc' && extension.toLowerCase() != 'docx' &&
      extension.toLowerCase() != 'pdf' && extension.toLowerCase() != 'jpeg'
      && extension.toLowerCase() != 'jpg' && extension.toLowerCase() != 'png') {
      this.toastr.error("Only doc, docx, JPEG, PNG and PDF files are allowed.");
      IsValid = false;
    }
    return IsValid;
  }



  //##########################
  getEscapeStringUrl(url: string): string {
    if (url) {
      return url.replace('\ "', '').replace('"', '').replace('"', '');
    }
    return '';
  }


  close() {
    this.modalService.close();
  }
  dismiss() {
    this.modalService.dismiss();
  }


  public end_date_readonly: boolean;
  currentJobOnCheck(val) {
    if (val) {
      this.end_date_readonly = true;
      this.experience.get('endDate').patchValue('');
      this.experience.get('endDate').clearValidators();
      this.experience.get('endDate').updateValueAndValidity();
    } else {
      this.end_date_readonly = false;
      this.experience.get('endDate').setValidators(Validators.required);
      this.experience.get('endDate').updateValueAndValidity();
      this.experience.get('endDate').markAsTouched();

    }
  }

}
