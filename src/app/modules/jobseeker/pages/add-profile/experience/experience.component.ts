import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ThemeService } from 'ng2-charts';
import { ToastrService } from 'ngx-toastr';
import { SpinnerService } from 'src/app/modules/employer/service/spinner.service';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../../service/jobseeker.service';
import { ProfileService } from '../../../service/profile.service';
import { AddExperienceComponent } from './add-experience/add-experience.component';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {
  public username: any;
  public experiences: any = [];
  public title: string = '';
  // Senior Architect Looking for a Leadership Role
  public edit_mode: boolean = false;
  constructor(private router: Router, private modalService: NgbModal,
    private jobSeekerService: JobSeekerService, public storageService: StorageService,
    private spinner: SpinnerService,
    private toastr: ToastrService,
    private profileService: ProfileService,
    private confirmDailogService: ConfirmationDialogService) { }

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.profileService.setProfileCompleteness(this.username);
    this.getExperiences(this.username);
    this.getbasicprofile(this.username);
    this.getOtherDetails(this.username);
  }

  //Get Other Details
  public otherDetails: any;
  getOtherDetails(username) {
    this.jobSeekerService.getOtherDetails(username).subscribe((res: any) => {
      if (res) {
        this.otherDetails = res;
        this.title = res.profiletitle;
      }
    }, err => {
      console.log(err);
    })
  }

  //#################################
  toggle_edit_mode(val) {
    if (val) {

      this.updateProfileTitle();
    } else {
      this.edit_mode = true;
    }
  }

  // Update Profile Title 

  updateProfileTitle() {
    this.spinner.showSpinner();
    let d = {
      other_id: this.otherDetails ? this.otherDetails.other_id : '',
      username: this.username,
      profiletitle: this.title
    }
    if (d.profiletitle == '') {
      this.toastr.error('Enpty value');
      this.spinner.hideSpinner();
      return false;
    }
    const obj = Object.assign(this.otherDetails, d);
    this.jobSeekerService.updateOtherDetails(obj).subscribe((res: any) => {
      this.spinner.hideSpinner();
      this.edit_mode = false;
    }, err => {
      this.spinner.hideSpinner();
      console.log(err);
    })
  }

  //#################################

  getExperiences(username) {
    this.spinner.showSpinner();
    this.jobSeekerService.getExperience(username).subscribe((res: any) => {
      //console.log(res);
      if (res) {
        res = res.map(function (x) {
          if (x.currentjob == null || x.currentjob == 'N') {
            x.currentjob = false;
            return x;
          } else {
            x.currentjob = true;
            return x;
          }
        });
        res.unshift(res.splice(res.findIndex(item => item.currentjob == true), 1)[0])
        this.experiences = res;

      } else {
        this.experiences = [];
      }
      this.spinner.hideSpinner();
    }, err => {
      this.spinner.hideSpinner();
      console.log(err);
      this.experiences = [];
    })
  }

  //################################
  public basicProfile: any;
  getbasicprofile(username) {
    if (username) {
      this.jobSeekerService.getbasicprofile(username).subscribe((data: any) => {
        if (data) {
          if (data.status == 'Pending_Approval') {
            this.confirmDailogService.confirm(`Information`, `You cannot make changes to  Documents as they are pending admin approval.`,
              `OK`, '', 'md')
              .then((confirmed) => {
                if (confirmed) {

                }
              }).catch((err) => {
                console.log(err);

              });
          }
          this.basicProfile = data;
          this.title = data.jobSeekerOther.profiletitle;

        }
      },
        err => {
          console.log(err);
        });

    }
  }



  next() {
    this.router.navigate(['/jobseeker/profile/education']);
  }

  previous() {
    this.router.navigate(['/jobseeker/profile/address']);
  }

  addNew(exp?) {
    const modelRef = this.modalService.open(AddExperienceComponent, {
      size: 'md',
      backdrop: 'static'
    });
    var disable: boolean;
    debugger;
    var i = this.experiences.findIndex(x => x.currentjob == true);
    if (i >= 0 && exp && exp.currentjob == true) {
      disable = false;
    } else if (i >= 0 && exp && exp.currentjob == false) {
      disable = true;
    } else if (i < 0 && exp && exp.currentjob == false) {
      disable = false;
    } else if (i >= 0 && !exp) {
      disable = true;
    } else if (i < 0 && !exp) {
      disable = false;
    } else {
      disable = true;
    }

    modelRef.componentInstance.exp = exp;
    modelRef.componentInstance.basicProfileId = this.basicProfile.id;
    modelRef.componentInstance.username = this.username;
    modelRef.componentInstance.disable = disable;

    modelRef.result.then((res: any) => {
      this.getExperiences(this.username);
    }, dismiss => {
      console.log(dismiss);
    })
  }

  delete(exid) {
    this.spinner.showSpinner();
    this.jobSeekerService.deleteExperience(exid).subscribe((res: any) => {
      console.log(res);
      this.experiences = this.experiences.filter(x => x.exid !== exid);
      this.spinner.hideSpinner();
      this.toastr.success('Deleted successfully');
    }, err => {
      this.spinner.hideSpinner();
      console.log(err);
    });
  }

}
