import { HttpResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SpinnerService } from 'src/app/modules/employer/service/spinner.service';
import { SortPipe } from 'src/app/shared/pipes/sort.pipe';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { JobSeekerService } from '../../../service/jobseeker.service';
import { ProfileService } from '../../../service/profile.service';

@Component({
  selector: 'app-basic-info',
  templateUrl: './basic-info.component.html',
  styleUrls: ['./basic-info.component.scss']
})
export class BasicInfoComponent implements OnInit {
  public basicForm: FormGroup;
  public submitted: boolean = false;
  public username: any;
  public languageList = [];
  public dropdownSettings = {
    singleSelection: false,
    text: "Select Language",
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: true,
    classes: "language-select custom-class"
  };

  constructor(private router: Router, private fb: FormBuilder,
    private jobSeekerService: JobSeekerService,
    private sortPipe: SortPipe, private cd: ChangeDetectorRef,
    private toastr: ToastrService,
    private storageService: StorageService,
    private spinner: SpinnerService,
    private profileService: ProfileService,
    private confirmDailogService: ConfirmationDialogService) { }

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.profileService.setProfileCompleteness(this.username);
    this.getLanguageList();
    this.getAllCountries();
    this.createNumbers();
    this.create();
  }

  //################################
  public basicProfile: any;
  getbasicprofile(username) {
    if (username) {
      this.jobSeekerService.getbasicprofile(username).subscribe((data: any) => {
        if (data) {
          if (data.status == 'Pending_Approval') {
            this.confirmDailogService.confirm(`Information`, `You cannot make changes to  Documents as they are pending admin approval.`,
              `OK`, '', 'md')
              .then((confirmed) => {
                if (confirmed) {

                }
              }).catch((err) => {
                console.log(err);

              });
          }

          delete data.jobSeekerOther;
          delete data.experiences;
          delete data.educations;
          this.basicProfile = data;
          var av = [];
          var a = data.language.split(',');
          a.forEach(element => {
            var v = this.languageList.find(x => x.itemName === element);
            console.log(v);
            av.push(v);
          });
          data.language = av;
          data.dob = new Date(data.dob).toISOString().split('T')[0];
          this.basicForm.patchValue(data);
          this.basicForm.updateValueAndValidity();
          this.basicForm.markAsUntouched();
          this.basicForm.markAsPristine();
          this.cd.detectChanges();
        }
      },
        err => {

        });

    }
  }

  //************************* Create Basic form  ***********/
  create() {
    this.basicForm = this.fb.group({
      title: ['', Validators.required],
      gender: ['Male', Validators.required],
      nationality: ['', Validators.required],
      language: ['', Validators.required],
      dob: ['', Validators.required],
      fname: ['', Validators.required],
      lname: ['', Validators.required],
      mobile: ['', Validators.required],
      mobilevalid: [''],
      currsallac: [''],
      currsalth: ['', Validators.required],
      resumeurl: [''],
    });
  }


  get f() {
    return this.basicForm.controls;
  }

  submit() {
    debugger;
    this.submitted = true;
    if (this.basicForm.invalid) {
      return;
    }
    if (!this.basicForm.touched) {
      this.next();
      return false;
    }
    console.log("spiineer=" + this.spinner);
    this.spinner.showSpinner();
    const basicObj = Object.assign(this.basicProfile, this.basicForm.value);
    basicObj.language = this.getSelecteLanguage(this.basicForm.get('language').value);
    if (this.basicForm.get('dob').value != null) {
      basicObj.dob = this.formatYYYYMMDD(this.basicForm.get('dob').value);
    }

    if (this.attachedResume != null) {
      this.jobSeekerService.uploadResume(this.attachedResume).toPromise()
        .then(event => {
          if (event instanceof HttpResponse) {
            var url = this.getEscapeStringUrl(event.body as string);
            basicObj.resumeurl = url;
            this.attachedResume = null;
            this.jobSeekerService.createbasicprofile(basicObj).subscribe(res => {
              this.spinner.hideSpinner();
              this.toastr.success('Personal details saved successfully.');
              this.next();
            },
              err => {
                this.spinner.hideSpinner();
                this.toastr.error('Error in personal details saved.');
                console.log(err);
              });
          }
        }).catch(err => {
          this.spinner.hideSpinner();
          console.log(err);
        });
    } else {
      this.jobSeekerService.createbasicprofile(basicObj).subscribe(res => {
        this.spinner.hideSpinner();
        this.toastr.success('Personal details saved successfully.');
        this.next();
      },
        err => {
          this.spinner.hideSpinner();
          this.toastr.error('Error in personal details saved.');
          console.log(err);
        });
    }

  }

  //#######################################

  getSelecteLanguage(value) {
    var v = [];
    value.forEach(element => {
      v.push(element.itemName);
    });
    return v.join();
  }


  //#################################
  formatYYYYMMDD(date: string): string {
    const d = new Date(date);
    return d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2);
    // + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);

  }

  //**********************************select box function */

  onItemSelect(item: any) {
    console.log(item);
  }
  OnItemDeSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onDeSelectAll(items: any) {
    console.log(items);
  }

  //*****************************************end****************************/



  next() {
    this.router.navigate(['/jobseeker/profile/jobinfo']);
  }


  // ############################
  public countryList: any = [];
  getAllCountries() {
    this.jobSeekerService.getAllCountries().subscribe((data: any) => {
      if (data) {
        console.log(this.countryList);
        this.countryList = this.sortPipe.transform(data.content, "asc", "countryname");

      }
    },
      (err) => {
        console.log(err);
      });
  }


  //###############################
  public numbers: any = [];
  createNumbers() {
    for (var i = 0; i <= 100; i++) {
      this.numbers.push(i);
    }
  }

  //#######################################
  getLanguageList() {
    this.jobSeekerService.getLanguages().subscribe((res: any) => {
      res.content.forEach(element => {
        var d = {
          id: element.langid,
          itemName: element.language
        };
        this.languageList.push(d);
      });
      this.getbasicprofile(this.username);
    },
      err => {
        console.log(err);
      });
  }


  //########################################
  public attachedResume: FormData;

  uploadResumeFile(event: any) {
    if (event.target.files.length > 0) {
      const uploadfile: File = event.target.files[0];
      if (this.validateFile(uploadfile)) {
        this.basicForm.patchValue({
          resumeurl: uploadfile.name
        });
        this.basicForm.updateValueAndValidity();
        const formData: FormData = new FormData();
        formData.append('file', uploadfile);
        formData.append('bucket', 'iviewid' + this.basicProfile.id);
        formData.append('username', this.username);
        this.attachedResume = formData; // this.getFormData(uploadfile);
      }

    }
  }

  validateFile(file: File): boolean {

    let IsValid = true;
    const extension = file.name.substr(file.name.lastIndexOf('.') + 1);
    if (file.size > (1024 * 1024)) {
      this.toastr.error('Please select file size less than 1 MB');
      IsValid = false;
    }
    if (extension.toLowerCase() != 'doc' && extension.toLowerCase() != 'docx' &&
      extension.toLowerCase() != 'pdf' && extension.toLowerCase() != 'jpeg'
      && extension.toLowerCase() != 'jpg' && extension.toLowerCase() != 'png') {
      this.toastr.error("Only doc, docx, JPEG, PNG and PDF files are allowed.");
      IsValid = false;
    }
    return IsValid;
  }



  //##########################
  getEscapeStringUrl(url: string): string {
    if (url) {
      return url.replace('\ "', '').replace('"', '').replace('"', '');
    }
    return '';
  }



}
