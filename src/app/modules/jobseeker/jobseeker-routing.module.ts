import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/auth/auth.guard';
// import { VideoComponent } from 'src/app/common/video/video.component';
import { LayoutComponent } from './components/layout/layout.component';
import { AddProfileComponent } from './pages/add-profile/add-profile.component';
import { AddressComponent } from './pages/add-profile/address/address.component';
import { BasicInfoComponent } from './pages/add-profile/basic-info/basic-info.component';
import { CareerDetailsComponent } from './pages/add-profile/career-details/career-details.component';
import { CompetencyComponent } from './pages/add-profile/competency/competency.component';
import { EducationComponent } from './pages/add-profile/education/education.component';
import { ExperienceComponent } from './pages/add-profile/experience/experience.component';
import { IdproofComponent } from './pages/add-profile/idproof/idproof.component';
import { JobInfoComponent } from './pages/add-profile/job-info/job-info.component';
import { ReviewComponent } from './pages/add-profile/review/review.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { InterviewResultDetailsComponent } from './pages/interview-result-details/interview-result-details.component';
import { InterviewResultComponent } from './pages/interview-result/interview-result.component';
import { InterviewSchedulesComponent } from './pages/interview-schedules/interview-schedules.component';
import { JobInterviewsComponent } from './pages/job-interviews/job-interviews.component';
import { JoinMeetingComponent } from './pages/join-meeting/join-meeting.component';
import { MyCalendarComponent } from './pages/my-calendar/my-calendar.component';
import { MyDocumentApprovalComponent } from './pages/my-document-approval/my-document-approval.component';
import { CartComponent } from './pages/order/cart/cart.component';
import { OrderConfirmationComponent } from './pages/order/order-confirmation/order-confirmation.component';
import { OrderSummaryComponent } from './pages/order/order-summary/order-summary.component';
import { QuickRegistrationComponent } from './pages/quick-registration/quick-registration.component';
import { ScheduleInterviewDetailsComponent } from './pages/schedule-interview-details/schedule-interview-details.component';
import { SearchDetailsComponent } from './pages/search-details/search-details.component';
import { SearchListComponent } from './pages/search-list/search-list.component';
import { SearchDetailComponent } from './pages/search/search-detail/search-detail.component';
import { SearchComponent } from './pages/search/search.component';
import { SendInterviewRequestComponent } from './pages/send-interview-request/send-interview-request.component';
import { UpdatePwdComponent } from './pages/update-pwd/update-pwd.component';
import { ViewProfileComponent } from './pages/view-profile/view-profile.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'profile',
        component: AddProfileComponent,
        canActivate: [AuthGuard],
        children: [
          { 'path': '', redirectTo: 'basicinfo', pathMatch: 'full' },
          { 'path': 'basicinfo', component: BasicInfoComponent },
          { 'path': 'jobinfo', component: JobInfoComponent },
          { 'path': 'idproof', component: IdproofComponent },
          { 'path': 'address', component: AddressComponent },
          { 'path': 'experience', component: ExperienceComponent },
          { 'path': 'education', component: EducationComponent },
          { 'path': 'competency', component: CompetencyComponent },
          { 'path': 'career-details', component: CareerDetailsComponent },
          { 'path': 'review', component: ReviewComponent }
        ]
      },
      {
        path: 'quick-profile',
        component: QuickRegistrationComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'interview-schedules',
        component: InterviewSchedulesComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'interview-schedule-details/:interviewid',
        component: ScheduleInterviewDetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'my-interview-result',
        component: InterviewResultComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'my-interview-result-details',
        component: InterviewResultDetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'update-pwd',
        component: UpdatePwdComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'search-interviewers',
        component: SearchListComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'search-detail/:id',
        component: SearchDetailComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'search-details/:id',
        component: SearchDetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'view-my-calendar/:username',
        component: MyCalendarComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'send-interview-request',
        component: SendInterviewRequestComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'join-meeting',
        component: JoinMeetingComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'view-update-profile',
        component: ViewProfileComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'my-document-approval',
        component: MyDocumentApprovalComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'checkout-order',
        component: CartComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'order-summary',
        component: OrderSummaryComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'order-confirmation',
        component: OrderConfirmationComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'freelancer-search',
        component: SearchComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'job-interviews',
        component: JobInterviewsComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobseekerRoutingModule { }
