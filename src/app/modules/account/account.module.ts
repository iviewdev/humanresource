import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { PageLoginComponent } from './pages/page-login/page-login.component';
import { LayoutComponent } from './components/layout/layout.component';
import { PageRegisterComponent } from './pages/page-register/page-register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { PageResetPasswordComponent } from './pages/page-reset-password/page-reset-password.component';
import { VerifyUserComponent } from './pages/verify-user/verify-user.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { JobseekerComponent } from './pages/jobseeker/jobseeker.component';
import { FreelancerComponent } from './pages/freelancer/freelancer.component';
import { EmployerComponent } from './pages/employer/employer.component';
import { CorpResetPasswordComponent } from './pages/employer/corp-reset-password/corp-reset-password.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { SpinnerService } from '../employer/service/spinner.service';
import { NgxSpinnerModule } from 'ngx-spinner';



@NgModule({
  declarations: [PageLoginComponent, LayoutComponent, PageRegisterComponent,PageResetPasswordComponent, VerifyUserComponent, JobseekerComponent, FreelancerComponent, EmployerComponent, CorpResetPasswordComponent,
  HeaderComponent,FooterComponent],
  imports: [
    CommonModule,
    AccountRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxCaptchaModule,
    NgxSpinnerModule
  ],
  providers:[SpinnerService]
  
})
export class AccountModule { }
