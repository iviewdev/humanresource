import { Injectable } from '@angular/core';
import { DataService } from '../../../shared/services/data.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  baseUrl = 'auth/';
  constructor(private dataService: DataService) { }



  update(userModel: any) {
    return this.dataService.post("ForgetPassword/update", userModel);
  }




  ValidateLink(id: any) {
    return this.dataService.get("ForgetPassword/" + id);
  }

  // ************************check and send reset link
  checkAndSendPasswordLink(email: any, link: any) {
    return this.dataService.put("identity/resetToken/" + email, link);
  }

  // ******************check already email register in the database or not
  checkEmailAllReadyRegisteredOrNot(email: string) {
    return this.dataService.getWithParam('identity/getEmailValid/' + email, '');
  }


  // ******************check already mobile register in the database or not
  checkMobileAllReadyRegisteredOrNot(mobile: string) {
    return this.dataService.getWithParam('identity/getMobileValid/' + mobile, '');
  }


  //send mobile OTP ********************************
  sendmobileOtp(code: any) {
    return this.dataService.put('identity/sendMobileOTP', code);
  }

  //*******************Verify mobile OTP********************** */
  verify_mobile_otp(mobile: string, otp: string) {
    debugger;
    return this.dataService.getWithParam('identity/validateMobileOTP/' + mobile + '/' + otp, '');
  }


  //Validate email ********************************
  sendEmailOtp(code: any) {
    return this.dataService.put('identity/sendEmailOTP', code);
  }

  //*******************Verify email OTP********************** */
  verify_email_otp(email: string, otp: string) {
    return this.dataService.getWithParam('identity/validateEmailOTP/' + email + '/' + otp, '');
  }

  //*****************************register user***********************/
  register(code: any) {
    debugger
    return this.dataService.post('identity/registerUser', code);
  }
  // ***********************login to your account********************
  login(data: any) {
    return this.dataService.login('identity/authenticate', data);
  }

  // ***********************************reset your password*************
  resetPassword(data: any) {
    return this.dataService.put('identity/resetPassword', data);
  }

  findtenant(data: any) {
    return this.dataService.get('corpidentity/findtenant/' + data);
  }

  coporateLongin(data: any, tenantid: string) {
    return this.dataService.post('corpidentity/authenticate/' + tenantid, data);
  }


  // ************************check and send reset link
  corpCheckAndSendPasswordLink(email: any, tenentid, link: any) {
    return this.dataService.put("corpidentity/resetToken/" + tenentid + "/" + email, link);
  }

  // ***********************************Corp reset your password*************
  corpresetPassword(data: any, tenentid: any) {
    return this.dataService.put('corpidentity/resetPassword/' + tenentid, data);
  }


}
