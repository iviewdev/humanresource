import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from '../../service/account.service';
import { StorageService } from 'src/app/shared/services/storage.service';

import { Observable, of } from "rxjs";
import { delay } from 'rxjs/operators';

export interface Role {
  Roleid: number;
  name: string;
}


@Component({
  selector: 'app-page-register',
  templateUrl: './page-register.component.html',
  styleUrls: ['./page-register.component.scss']
})
export class PageRegisterComponent implements OnInit {
  form: FormGroup;
  public verified: boolean = false;
  public mobileverified: boolean = false;

  //public otpSendStart: boolean = false;
  public mobileotpSendStart: boolean = false;


  RoleData: Role[] = [];

  // RoleData: Role[] = [
  //   { id: 0, name: 'Job Seeker' },
  //   { id: 1, name: 'Freelancer' },
  //   { id: 2, name: 'Corportate User' },

  // ];


  public loginStart = false;
  public submitted = false;
  public validate: boolean = true;
  public mvaldate: boolean = true;

  emailOTP: boolean = false;
  mobileOTP: boolean = false;
  isChecked;
  isCheckedName;


  theterms = false;

  constructor(
    public router: Router,
    public toastr: ToastrService,
    public accountService: AccountService,
    public storageService: StorageService,
    public fb: FormBuilder,
  ) {

    this.form = this.fb.group({
      'email': ['', [Validators.required, Validators.email]],
      'mobile': ['', [Validators.required, Validators.minLength(10)]],
      'code': ['', [Validators.required]],
      'mobilecode': ['', [Validators.required]],
      'role': ['1', Validators.required],

      //checkArray: new FormArray([], minSelectedCheckboxes(1))
      //languages: this._formBuilder.array([], minSelectedCheckboxes(1)),
    });
  }


  //************************get field*************/
  get f() {
    return this.form.controls;
  }

  Chechterm(e) {
    this.theterms = e.target.checked;
  }

  //*********************************Register********** */


  onCheckboxChange(index, e) {
    debugger

    const checkArray: FormArray = this.form.get('checkArray') as FormArray;

    checkArray.controls.forEach((control, i) => {
      debugger
      if (i == index) {
        // (<FormArray>this.form.controls['checkArray']).at(i).setValue({
        //   i: true
        // });
        e.target.checked = true;
      }
      else {
        // (<FormArray>this.form.controls['checkArray']).at(i).setValue({
        //   i: false
        // });
        //e.target.checked=false;
      }

    });

    // if (e.target.checked) {
    //   checkArray.push(new FormControl(e.target.value));
    // } else {
    //   let i: number = 0;
    //   checkArray.controls.forEach((item: FormControl) => {

    //     if (item.value == e.target.value) {
    //       checkArray.removeAt(i);
    //       return;
    //     }
    //     i++;
    //   });
    // }
  }



  register() {
    debugger;
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    if (!this.theterms) {
      this.showMessage('Please check the terms and conditions');
      return;

    }
    //const selectedOrderIds = this.form.value.checkArray;

    //const selectedOrderIds = this.form.value.checkArray.map((checked, i) => checked ? this.RoleData[i].Roleid : null).filter(v => v !== null);

    //return;
    //this.registerStart = true;.
    var r = ["jobseeker", "freelancer", "employer"]
    let d = {
      "username": this.form.get('email').value,
      "mobile": this.form.get('mobile').value,
      "email_confirmed": 'Y',
      "mobile_confirmed": "Y",
      "role": { "role_id": this.form.get('role').value, "role_name": r[this.form.get('role').value] },
      "reset_link": null,
      "jdbc_url": null
    }
    debugger
    this.accountService.register(d).subscribe((res: any) => {
      //this.registerStart = false;
      if (res) {
        this.toastr.success('Register successfully! Please login');
        this.router.navigateByUrl('/account/login');
      }
      else {
        this.toastr.error('Not register! Please try again');
      }

    }, (err) => {
      //this.registerStart = false;
      this.toastr.error(err.error);
    });
  }


  //*****************************Validate Email && call checkEmailAlreadyExist function************/
  validateAndCheckEmail() {
    if (this.f.email.errors) {
      if (this.f.email.errors.required) {
        this.showMessage('Email required');
      } else {
        this.showMessage('Invalid Email');
      }
    } else {
      this.checkEmailAlreadyExist();
    }
  }

  //*****************************Check Email Already Exist in the database or not*********************/
  checkEmailAlreadyExist() {
    //this.otpSendStart = true;
    this.accountService.checkEmailAllReadyRegisteredOrNot(this.form.get('email').value).subscribe((res: any) => {
      if (res == false) {
        this.sendemailOtp();
      } else {
        ///this.otpSendStart = false;
        this.showMessage('This email already registered');
      }
    }, error => {
      //this.otpSendStart = false;
      this.toastr.error(error);
    });
  }

  //*****************************send Email OTP*********************/
  sendemailOtp() {
    let d = {
      "username": this.form.value.email
    }
    this.accountService.sendEmailOtp(d).subscribe((res: any) => {
      if (res) {
        this.toastr.success('OTP has been sent');
        this.emailOTP = true;
        //this.otpSendStart = false;

      } else {
        //this.otpSendStart = false;
        this.emailOTP = false;
        //this.validate = true;
        this.showMessage('OTP has not been sent');
      }
    }, error => {
      // this.otpSendStart = false;

      //this.validate = true;
      this.toastr.error(error);
    });
  }

  /*****************************verify email OTP*********************/
  verifyEmailOtp() {
    debugger
    //this.submitted = true;

    if (this.f.code.errors) {
      if (this.f.code.errors.required) {
        this.showMessage('Email otp required');
      }
    }
    else {
      this.accountService.verify_email_otp(this.form.value.email, this.form.get('code').value).subscribe((res: any) => {
        debugger
        if (res == 0) {
          this.verified = true;
          this.emailOTP = false;
          this.validate = false;
        } else if (res == 1) {
          this.toastr.error('OTP expired! Please try again');

        } else if (res == 2) {

          this.toastr.error('Invalid OTP');
        } else {
          this.toastr.error('Exception Error');
        }
      }, (err) => {
        this.toastr.error(err.error);
      });
    }


  }
  //*****************************Validate mobile && call checkMobileAlreadyExist function************/
  validateAndCheckMobile() {
    debugger;
    if (this.f.mobile.errors) {
      if (this.f.mobile.errors.required) {
        this.showMessage('mobile required');
      } else {
        this.showMessage('Invalid mobile');
      }
    } else {
      this.checkMobileAlreadyExist();
    }
  }



  //*****************************Check mobile Already Exist in the database or not*********************/
  checkMobileAlreadyExist() {
    debugger;
    this.mobileotpSendStart = true;
    this.accountService.checkMobileAllReadyRegisteredOrNot(this.form.get('mobile').value).subscribe((res: any) => {

      if (res == false) {
        this.sendMobileOtp();
      } else {
        this.mobileotpSendStart = false;
        this.showMessage('This mobile already registered');
      }
    }, error => {
      this.mobileotpSendStart = false;
      this.toastr.error(error);
    });
  }

  //*****************************send mobile OTP************/
  sendMobileOtp() {

    debugger;
    let d = {
      "mobile": this.form.value.mobile
    }
    this.accountService.sendmobileOtp(d).subscribe((res: any) => {
      if (res) {
        this.toastr.success('OTP has been sent');
        this.mobileOTP = true;
        //this.otpSendStart = false;
      } else {
        this.mobileotpSendStart = false;
        this.mobileOTP = false;
        //this.validate = true;
        this.showMessage('OTP has not been sent');
      }
    }, error => {
      this.mobileotpSendStart = false;

      //this.validate = true;
      this.toastr.error(error);
    });
  }
  /*****************************verify email OTP*********************/
  verifyMobileOtp() {
    debugger
    //this.submitted = true;

    if (this.f.mobilecode.errors) {
      if (this.f.mobilecode.errors.required) {
        this.showMessage('mobile otp required');
      }
    }
    else {
      this.accountService.verify_mobile_otp(this.form.value.mobile, this.form.get('mobilecode').value).subscribe((res: any) => {
        debugger
        if (res == 0) {
          this.mobileverified = true;
          this.mobileOTP = false;
          this.mvaldate = false;
        } else if (res == 1) {
          this.toastr.error('OTP expired! Please try again');

        } else if (res == 2) {

          this.toastr.error('Invalid OTP');
        } else {
          this.toastr.error('Exception Error');
        }
      }, (err) => {
        this.toastr.error(err.error);
      });
    }


  }

  onChange(e) {
    debugger
    this.isChecked = !this.isChecked;
    this.isCheckedName = e;
  }

  // onChange(name: string, isChecked: boolean) {
  //   debugger;
  //   const cartoons = (this.form.controls.name as FormArray);

  //   if (isChecked) {
  //     cartoons.push(new FormControl(name));
  //   } else {
  //     const index = cartoons.controls.findIndex(x => x.value === name);
  //     cartoons.removeAt(index);
  //   }
  // }
  get RoleFormArray() {
    return this.form.controls.checkArray as FormArray;
  }

  ngOnInit() {
    // this.getrole().subscribe(role => {
    //   this.RoleData = role;
    //   this.RoleData.forEach(() => this.RoleFormArray.push(new FormControl(false)));


    // })
  }

  getrole(): Observable<Role[]> {
    let RoleData: Role[] = [
      { Roleid: 2, name: 'Job Seeker' },
      { Roleid: 1, name: 'Freelancer' },
      { Roleid: 3, name: 'Corportate User' },
      { Roleid: 4, name: 'Admin' },
    ];
    return of(RoleData).pipe(delay(10));
  }

  //******************************Show Error message*/
  showMessage(str) {
    this.toastr.error(str);
  }

}

function minSelectedCheckboxes(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      // get a list of checkbox values (boolean)
      .map(control => control.value)
      // total up the number of checked checkboxes
      .reduce((prev, next) => next ? prev + next : prev, 0);

    // if the total is not greater than the minimum, return the error message
    return totalSelected >= min ? null : { required: true };
  };

  return validator;
}

