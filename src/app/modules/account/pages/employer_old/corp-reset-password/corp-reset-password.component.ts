import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ConfirmedValidator } from '../../page-reset-password/confirm-validator';
import { AccountService } from '../../../service/account.service';
import { StorageService } from 'src/app/shared/services/storage.service';

@Component({
  selector: 'app-corp-reset-password',
  templateUrl: './corp-reset-password.component.html',
  styleUrls: ['./corp-reset-password.component.scss']
})
export class CorpResetPasswordComponent implements OnInit {

  public username: string;
  public reset_token: string;
  resetPwdForm: FormGroup;
  loginStart = false;
  submitted = false;
  public tenentid:string = '';
  constructor(private route: ActivatedRoute,
    private toastr: ToastrService,
    private accountService: AccountService,
    private storageService: StorageService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.resetPwdForm = this.fb.group({
      password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]],
      confirm_password: ['', [Validators.required]],
    }, {
      validators: ConfirmedValidator('password', 'confirm_password')
    });
  }

  ngOnInit() {
    this.username = this.route.snapshot.paramMap.get('username');
    this.reset_token = this.route.snapshot.paramMap.get('reset_token');
    this.reset_token = this.route.snapshot.paramMap.get('reset_token');
    this.tenentid = this.route.snapshot.paramMap.get('tenentid');
  }


  get f() {
    return this.resetPwdForm.controls;
  }

  //***********************Verify  Here**************************/

  resetPassword() {

    this.submitted = true;
    if (this.resetPwdForm.invalid) {
      return;
    }
    this.loginStart = true;
    let d = {
      username: this.username,
      reset_link: this.reset_token,
      newpass: this.resetPwdForm.get('password').value
    }
    this.accountService.corpresetPassword(d,this.tenentid).subscribe((res: any) => {
      console.log(res);
      this.loginStart = false;
      //console.log(res);
      if (res) {
        this.toastr.success('Password Reset successfully');
        this.router.navigateByUrl('/account/login/employer');
      }
      else {
        this.toastr.error('Reset Password Fail Please try Again!');
      }

    }, (err) => {
      console.log(err);
      this.loginStart = false;
      if (err.status == 408) {
        this.toastr.error('Reset password link expired');
      } else {
        this.toastr.error(err.error.message);
      }

    });
  }

}
