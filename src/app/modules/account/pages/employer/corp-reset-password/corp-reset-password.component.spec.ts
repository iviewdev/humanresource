import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorpResetPasswordComponent } from './corp-reset-password.component';

describe('CorpResetPasswordComponent', () => {
  let component: CorpResetPasswordComponent;
  let fixture: ComponentFixture<CorpResetPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorpResetPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorpResetPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
