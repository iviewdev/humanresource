import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StorageService } from '../../../../shared/services/storage.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from '../../service/account.service';

@Component({
  selector: 'app-verify-user',
  templateUrl: './verify-user.component.html',
  styleUrls: ['./verify-user.component.scss']
})
export class VerifyUserComponent implements OnInit {

  verifyForm: FormGroup;
  loginStart = false;
  submitted = false;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private accountService: AccountService,
    private storageService: StorageService,
    private fb: FormBuilder,
  ) {

    this.verifyForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
    });
  }

  ngOnInit() {

  }
  get f() {
    return this.verifyForm.controls;
  }

  //***********************Verify  Here**************************/

  verifyUser() {

    this.submitted = true;
    if (this.verifyForm.invalid) {
      return;
    }
    this.loginStart = true;
    let d = {
      "reset_link":"http://localhost:4200/account/reset-password"
    }
    this.accountService.checkAndSendPasswordLink(this.f.username.value, d).subscribe((res: any) => {
      this.loginStart = false;
      //console.log(res);
      if (res) {
        this.toastr.success('Reset link has been sent to your email');
        this.router.navigateByUrl('/account/login');
      }
      else {
        this.toastr.error('Invalid user name');
      }

    }, (err) => {
      this.loginStart = false;
      if (err.error.status == 401) {
        this.toastr.error('Invalid username');
      } else {
        this.toastr.error(err.error.message);
      }

    });
  }

}
