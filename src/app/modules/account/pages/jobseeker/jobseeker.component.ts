import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from '../../service/account.service';
import { StorageService } from 'src/app/shared/services/storage.service';

import { Observable, of } from "rxjs";
import { delay } from 'rxjs/operators';
import { SpinnerService } from 'src/app/modules/employer/service/spinner.service';
declare var $: any;
export interface Role {
  Roleid: number;
  name: string;
}

@Component({
  selector: 'app-jobseeker',
  templateUrl: './jobseeker.component.html',
  styleUrls: ['./jobseeker.component.scss']
})

export class JobseekerComponent implements OnInit {

  form: FormGroup;
  public verified: boolean = false;
  public mobileverified: boolean = false;

  //public otpSendStart: boolean = false;
  public mobileotpSendStart: boolean = false;


  RoleData: Role[] = [];
  public loginStart = false;
  public submitted = false;
  public validate: boolean = true;
  public mvaldate: boolean = true;

  emailOTP: boolean = false;
  mobileOTP: boolean = false;
  isChecked;
  isCheckedName;
  theterms = false;

  loginForm: FormGroup;
  loginStart1 = false;
  submitted1 = false;
  siteKey: string;

  verifyForm: FormGroup;
  loginStart2 = false;
  submitted2 = false;

  constructor(
    public router: Router,
    public toastr: ToastrService,
    public accountService: AccountService,
    public storageService: StorageService,
    public fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private spinner: SpinnerService
  ) {

    this.siteKey = '6LcI7doZAAAAAGFNWcwuxd2wVmxbKB-vDBWX0F_n';
    this.loginForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      recaptcha: ['', Validators.required]
    });
    setTimeout(() => { this.cd.detectChanges(); }, 2000);
  }


  ngOnInit() {
    this.creageRegisterForm();
    this.createEmailVerifyForm();
  }

  creageRegisterForm() {
    this.form = this.fb.group({
      'email': ['', [Validators.required, Validators.email]],
      'mobile': ['', [Validators.required, Validators.minLength(10)]],
      'code': ['', [Validators.required]],
      'mobilecode': ['', [Validators.required]],
      'role': ['2', Validators.required],

      //checkArray: new FormArray([], minSelectedCheckboxes(1))
      //languages: this._formBuilder.array([], minSelectedCheckboxes(1)),
    });
  }

  //************************get field*************/
  get f() {
    return this.form.controls;
  }
  //*********************************login form control  */
  get l() {
    return this.loginForm.controls;
  }

  //*********************************Get Verify User Email */
  get v() {
    return this.verifyForm.controls;
  }



  //***********************Verify  Here**************************/

  verifyUser() {

    this.submitted2 = true;
    if (this.verifyForm.invalid) {
      return;
    }
    this.loginStart2 = true;
    let d = {
      "reset_link": "http://localhost:4200/account/reset-password"
    }
    this.accountService.checkAndSendPasswordLink(this.v.username.value, d).subscribe((res: any) => {
      this.loginStart2 = false;
      //console.log(res);
      if (res) {
        this.toastr.success('Reset link has been sent to your email');
        document.getElementById('forgotPass').click();
        this.verifyForm.reset();
        this.verifyForm.markAsPristine();
        this.verifyForm.markAsUntouched();
        this.verifyForm.updateValueAndValidity();
        this.submitted2 = false;
        //  this.router.navigateByUrl('/account/login/jobseeker');
      }
      else {
        this.toastr.error('Invalid user name');
      }

    }, (err) => {
      this.loginStart2 = false;
      if (err.error.status == 401) {
        this.toastr.error('Invalid username');
      } else {
        this.toastr.error(err.error.message);
      }

    });
  }


  //***************************create verify form ******* */
  createEmailVerifyForm() {
    this.verifyForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
    });
  }



  //***********************Login Here**************************/

  login() {
    debugger;
    this.submitted1 = true;
    if (this.loginForm.invalid) {
      return;
    }


    this.loginStart1 = true;
    this.spinner.showSpinner();
    this.accountService.login(this.loginForm.value).subscribe((res: any) => {
      this.loginStart1 = false;
      this.spinner.hideSpinner();
      // console.log(res);
      if (res) {
        if (res.role.toLowerCase() == 'jobseeker') {
          debugger;

          this.storageService.setItem("token", res.jwttoken);
          this.storageService.setItem('username', res.username);
          this.storageService.setItem('id', res.userid);
          this.storageService.setItem('mobile', res.mobile);
          this.storageService.setItem('role', res.role);

          var nav = localStorage.getItem('nav');
          if (nav == 'viewcalender') {
            localStorage.removeItem('nav');
            localStorage.removeItem('intdata');
            this.router.navigate(['jobseeker/view-my-calendar', res.username]);
            return false;
          }

          if (nav == 'bookinterview') {
            var data = JSON.parse(localStorage.getItem('intdata'));
            localStorage.removeItem('nav');
            localStorage.removeItem('intdata');
            this.router.navigate(['jobseeker/send-interview-request'], { queryParams: { fsid: data.fsid, per_hour_rate: data.per_hour_rate } });
            return false;
          }
          this.router.navigateByUrl("jobseeker");
        }
        else {
          this.toastr.error("You are not valid user");
          this.loginStart1 = false;
        }

      }
      else {
        this.toastr.error('Invalid user name or password');
        this.loginStart1 = false;
      }

    }, (err) => {
      console.log(err);
      this.spinner.hideSpinner();
      this.loginStart1 = false;
      if (err.error.status == 401) {
        this.toastr.error('Invalid username or password');
        this.loginStart1 = false;
      } else {
        this.toastr.error(err.error.message);
      }

    });
  }

  Chechterm(e) {
    this.theterms = e.target.checked;
  }

  //*********************************Register********** */


  onCheckboxChange(index, e) {
    debugger

    const checkArray: FormArray = this.form.get('checkArray') as FormArray;

    checkArray.controls.forEach((control, i) => {
      debugger
      if (i == index) {
        // (<FormArray>this.form.controls['checkArray']).at(i).setValue({
        //   i: true
        // });
        e.target.checked = true;
      }
      else {
        // (<FormArray>this.form.controls['checkArray']).at(i).setValue({
        //   i: false
        // });
        //e.target.checked=false;
      }

    });

    // if (e.target.checked) {
    //   checkArray.push(new FormControl(e.target.value));
    // } else {
    //   let i: number = 0;
    //   checkArray.controls.forEach((item: FormControl) => {

    //     if (item.value == e.target.value) {
    //       checkArray.removeAt(i);
    //       return;
    //     }
    //     i++;
    //   });
    // }
  }



  register() {
    debugger;
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    if (!this.theterms) {
      this.showMessage('Please check the terms and conditions');
      return;

    }
    //const selectedOrderIds = this.form.value.checkArray;

    //const selectedOrderIds = this.form.value.checkArray.map((checked, i) => checked ? this.RoleData[i].Roleid : null).filter(v => v !== null);

    //return;
    //this.registerStart = true;.
    var r = ["jobseeker", "freelancer", "employer"]
    let d = {
      "username": this.form.get('email').value,
      "mobile": this.form.get('mobile').value,
      "email_confirmed": 'Y',
      "mobile_confirmed": "Y",
      "role": { "role_id": this.form.get('role').value, "role_name": r[this.form.get('role').value] },
      "reset_link": null,
      "jdbc_url": null
    }
    this.spinner.showSpinner();
    this.accountService.register(d).subscribe((res: any) => {
      //this.registerStart = false;
      this.spinner.hideSpinner();
      if (res) {
        document.getElementById('nav-register-tab').classList.remove('active');
        document.getElementById('nav-register').classList.remove('active');
        document.getElementById('nav-register').classList.remove('show');
        document.getElementById('nav-login-tab').classList.add('active');
        document.getElementById('nav-login').classList.add('active');
        document.getElementById('nav-login').classList.add('show');
        this.form.markAsPristine();
        this.form.markAsUntouched();
        this.form.reset();
        this.form.clearValidators();
        this.form.updateValueAndValidity();
        this.verified = false;
        this.mobileverified = false;
        this.emailOTP = false;
        this.mobileverified = false;
        this.validate = false;

        this.toastr.success('Register successfully! Please login', '', {
          timeOut: 300000,
          closeButton: true,
          tapToDismiss: false
        });
        this.submitted = false;
      }
      else {
        this.toastr.error('Not register! Please try again');
      }

    }, (err) => {
      //this.registerStart = false;
      this.spinner.hideSpinner();
      this.toastr.error(err.error);
      console.log(err);
    });
  }


  //*****************************Validate Email && call checkEmailAlreadyExist function************/
  validateAndCheckEmail() {
    if (this.f.email.errors) {
      if (this.f.email.errors.required) {
        this.showMessage('Email required');
      } else {
        this.showMessage('Invalid Email');
      }
    } else {
      this.checkEmailAlreadyExist();
    }
  }

  //*****************************Check Email Already Exist in the database or not*********************/
  checkEmailAlreadyExist() {
    //this.otpSendStart = true;
    this.accountService.checkEmailAllReadyRegisteredOrNot(this.form.get('email').value).subscribe((res: any) => {
      if (res == false) {

        this.sendemailOtp();
      } else {
        ///this.otpSendStart = false;
        this.showMessage('This email already registered');
      }
    }, error => {
      //this.otpSendStart = false;
      this.toastr.error(error);
    });
  }

  //*****************************send Email OTP*********************/
  sendemailOtp() {
    let d = {
      "username": this.form.value.email
    }
    this.toastr.success('OTP has been sent');
    this.emailOTP = true;
    this.accountService.sendEmailOtp(d).subscribe((res: any) => {
      if (res) {
        //  this.toastr.success('OTP has been sent');
        //  this.emailOTP = true;
        //this.otpSendStart = false;

      } else {
        //this.otpSendStart = false;
        this.emailOTP = false;
        //this.validate = true;
        this.showMessage('OTP has not been sent');
      }
    }, error => {
      // this.otpSendStart = false;

      //this.validate = true;
      this.toastr.error(error);
    });
  }

  /*****************************verify email OTP*********************/
  verifyEmailOtp() {
    debugger
    //this.submitted = true;

    if (this.f.code.errors) {
      if (this.f.code.errors.required) {
        this.showMessage('Email otp required');
      }
    }
    else {
      this.accountService.verify_email_otp(this.form.value.email, this.form.get('code').value).subscribe((res: any) => {
        debugger
        if (res == 0) {
          this.verified = true;
          this.emailOTP = false;
          this.validate = false;
        } else if (res == 1) {
          this.toastr.error('OTP expired! Please try again');

        } else if (res == 2) {

          this.toastr.error('Invalid OTP');
        } else {
          this.toastr.error('Exception Error');
        }
      }, (err) => {
        this.toastr.error(err.error);
      });
    }


  }
  //*****************************Validate mobile && call checkMobileAlreadyExist function************/
  validateAndCheckMobile() {
    debugger;
    if (this.f.mobile.errors) {
      if (this.f.mobile.errors.required) {
        this.showMessage('mobile required');
      } else {
        this.showMessage('Invalid mobile');
      }
    } else {
      this.checkMobileAlreadyExist();
    }
  }



  //*****************************Check mobile Already Exist in the database or not*********************/
  checkMobileAlreadyExist() {
    debugger;
    this.mobileotpSendStart = true;
    this.accountService.checkMobileAllReadyRegisteredOrNot(this.form.get('mobile').value).subscribe((res: any) => {

      if (res == false) {

        this.sendMobileOtp();
      } else {
        this.mobileotpSendStart = false;
        this.showMessage('This mobile already registered');
      }
    }, error => {
      this.mobileotpSendStart = false;
      this.toastr.error(error);
    });
  }

  //*****************************send mobile OTP************/
  sendMobileOtp() {

    debugger;
    let d = {
      "mobile": this.form.value.mobile
    }
    this.toastr.success('OTP has been sent');
    this.mobileOTP = true;
    this.accountService.sendmobileOtp(d).subscribe((res: any) => {
      if (res) {
        // this.toastr.success('OTP has been sent');
        // this.mobileOTP = true;
        //this.otpSendStart = false;
      } else {
        this.mobileotpSendStart = false;
        this.mobileOTP = false;
        //this.validate = true;
        this.showMessage('OTP has not been sent');
      }
    }, error => {
      this.mobileotpSendStart = false;

      //this.validate = true;
      this.toastr.error(error);
    });
  }
  /*****************************verify email OTP*********************/
  verifyMobileOtp() {
    debugger
    //this.submitted = true;

    if (this.f.mobilecode.errors) {
      if (this.f.mobilecode.errors.required) {
        this.showMessage('mobile otp required');
      }
    }
    else {
      this.accountService.verify_mobile_otp(this.form.value.mobile, this.form.get('mobilecode').value).subscribe((res: any) => {
        debugger
        if (res == 0) {
          this.mobileverified = true;
          this.mobileOTP = false;
          this.mvaldate = false;
        } else if (res == 1) {
          this.toastr.error('OTP expired! Please try again');

        } else if (res == 2) {

          this.toastr.error('Invalid OTP');
        } else {
          this.toastr.error('Exception Error');
        }
      }, (err) => {
        this.toastr.error(err.error);
      });
    }


  }

  onChange(e) {
    debugger
    this.isChecked = !this.isChecked;
    this.isCheckedName = e;
  }

  // onChange(name: string, isChecked: boolean) {
  //   debugger;
  //   const cartoons = (this.form.controls.name as FormArray);

  //   if (isChecked) {
  //     cartoons.push(new FormControl(name));
  //   } else {
  //     const index = cartoons.controls.findIndex(x => x.value === name);
  //     cartoons.removeAt(index);
  //   }
  // }
  get RoleFormArray() {
    return this.form.controls.checkArray as FormArray;
  }



  getrole(): Observable<Role[]> {
    let RoleData: Role[] = [
      { Roleid: 2, name: 'Job Seeker' },
      { Roleid: 1, name: 'Freelancer' },
      { Roleid: 3, name: 'Corportate User' },
      { Roleid: 4, name: 'Admin' },
    ];
    return of(RoleData).pipe(delay(10));
  }

  //******************************Show Error message*/
  showMessage(str) {
    this.toastr.error(str);
  }





}

function minSelectedCheckboxes(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      // get a list of checkbox values (boolean)
      .map(control => control.value)
      // total up the number of checked checkboxes
      .reduce((prev, next) => next ? prev + next : prev, 0);

    // if the total is not greater than the minimum, return the error message
    return totalSelected >= min ? null : { required: true };
  };

  return validator;
}