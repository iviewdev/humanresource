import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StorageService } from '../../../../shared/services/storage.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from '../../service/account.service';

@Component({
  selector: 'app-page-login',
  templateUrl: './page-login.component.html',
  styleUrls: ['./page-login.component.scss']
})
export class PageLoginComponent implements OnInit {
  loginForm: FormGroup;
  loginStart = false;
  submitted = false;
  siteKey: string;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private accountService: AccountService,
    private storageService: StorageService,
    private fb: FormBuilder,
  ) {
    this.siteKey = '6LcI7doZAAAAAGFNWcwuxd2wVmxbKB-vDBWX0F_n';
    this.loginForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      recaptcha: ['', Validators.required]
    });
  }

  ngOnInit() {
    if (this.storageService.getItem('token') != null) {
      this.router.navigateByUrl('account/login');
    }
  }
  get f() {
    return this.loginForm.controls;
  }

  //***********************Login Here**************************/

  login() {
    debugger;
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.loginStart = true;

    this.accountService.login(this.loginForm.value).subscribe((res: any) => {
      this.loginStart = false;
      // console.log(res);
      if (res) {
        debugger;
        this.storageService.setItem("token", res.jwttoken);
        this.storageService.setItem('username', res.username);
        this.storageService.setItem('id', res.userid);
        this.storageService.setItem('mobile', res.mobile);
        this.storageService.setItem('role', res.role);

        

        if (res.role.toLowerCase() == 'freelancer') {
          this.router.navigateByUrl("freelancer");
        }
        else if (res.role.toLowerCase() == 'jobseeker') {
          this.router.navigateByUrl("jobseeker");
        }
        else if (res.role.toLowerCase() == 'admin') {
          this.router.navigateByUrl("admin/dashboard");
        }

      }
      else {
        this.toastr.error('Invalid user name or password');
      }

    }, (err) => {
      console.log(err);
      this.loginStart = false;
      if (err.error.status == 401) {
        this.toastr.error('Invalid username or password');
      } else {
        this.toastr.error(err.error.message);
      }

    });
  }

}
