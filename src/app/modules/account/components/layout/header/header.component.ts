import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public navSearch: FormGroup;
  public submitted: boolean = false;
  constructor(private fb: FormBuilder,
    private router: Router, private toastr: ToastrService) { }

  ngOnInit() {
    this.createNavSearch();
  }

  createNavSearch() {
    this.navSearch = this.fb.group({
      org_type: ['Freelancer', Validators.required],
      search_keyword: ['', Validators.required]
    });
  }

  get s() {
    return this.navSearch.controls;
  }

  // Search by Keyword
  search_by_keyword() {
    this.submitted = true;
    if (this.navSearch.invalid) {
      this.toastr.error('Keywords empty');
      return;
    }
    this.router.navigate(['/search'], { queryParams: this.navSearch.value });
  }

}
