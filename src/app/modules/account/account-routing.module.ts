import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './components/layout/layout.component';
import { CorpResetPasswordComponent } from './pages/employer/corp-reset-password/corp-reset-password.component';
import { EmployerComponent } from './pages/employer/employer.component';
import { FreelancerComponent } from './pages/freelancer/freelancer.component';
import { JobseekerComponent } from './pages/jobseeker/jobseeker.component';
import { PageLoginComponent } from './pages/page-login/page-login.component';
import { PageRegisterComponent } from './pages/page-register/page-register.component';
import { PageResetPasswordComponent } from './pages/page-reset-password/page-reset-password.component';
import { VerifyUserComponent } from './pages/verify-user/verify-user.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    redirectTo: 'login'
  },
  {
    path: 'login-pre',
    component: PageLoginComponent
  },

  {
    path: 'register',
    component: PageRegisterComponent
  },
  {
    path: 'verify-user',
    component: VerifyUserComponent
  },
  {
    path: 'resetpwd/:username/:reset_token',
    component: PageResetPasswordComponent,
  },
  {
    path: 'corpresetpwd/:username/:reset_token/:tenentid',
    component: CorpResetPasswordComponent,
  },
  {
    path: 'login',
    component: LayoutComponent,
    children: [
      {
        path: 'jobseeker',
        component: JobseekerComponent
      },
      {
        path: 'freelancer',
        component: FreelancerComponent
      },
      {
        path: 'employer',
        component: EmployerComponent
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
