import { Injectable } from '@angular/core';
import { DataService } from '../../../shared/services/data.service';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class FreelancerService {
  // baseUrl = 'auth/';
  // private _controllerName: string = "freelancer/";
  // _url = 'http://157.245.101.207:8080/';
  // _methodName: string;
  // _param: number;

  constructor(private dataService: DataService, private _http: HttpClient) { }


  // ******************gte basic profile*****************************
  getbasicprofile(username: string) {


    return this.dataService.get('freelancerreg/getbasicprofile/' + username);
  }
  getLanguages() {
    return this.dataService.get('elasticsearch/getLanguages');
  }
  createbasicprofile(basicprofile: any) {
    return this.dataService.put('freelancerreg/createbasicprofile/', basicprofile);
  }
  uploadIDfile(idProof: any) {
    return this.dataService.uploadFile('freelancerreg/uploadIDfile/', idProof);
  }
  uploadTaxfile(taxFile: any) {
    return this.dataService.uploadFile('freelancerreg/uploadTaxfile/', taxFile);
  }
  updateAddress(contact: any) {
    return this.dataService.put('freelancerreg/updateAddress/', contact);
  }
  uploadAddFile(addressProof: any) {
    return this.dataService.uploadFile('freelancerreg/uploadAddFile/', addressProof);
  }
  updateJobInfo(jobInfo: any) {
    return this.dataService.put('freelancerreg/updateJobInfo/', jobInfo);
  }
  addIdDetails(idDetail: any) {
    return this.dataService.put('freelancerreg/addIdDetails/', idDetail);

  }

  getEducation(username: string) {
    return this.dataService.get('freelancerreg/getEducation/' + username);
  }
  updateEducation(education: any) {
    return this.dataService.put('freelancerreg/updateEducation/', education);
  }
  updateEducationCertificate(educationCertificate: any) {
    return this.dataService.uploadFile('freelancerreg/updateEducationCertificate/', educationCertificate);
  }
  getExperience(username: string) {
    return this.dataService.get('freelancerreg/getExperience/' + username);
  }
  getOtherDetails(username: string) {
    return this.dataService.get('freelancerreg/getOtherDetails/' + username);
  }

  getAllRoles() {
    return this.dataService.get('elasticsearch/getallRoles');
  }
  updateProfile(otherdetails: any) {
    return this.dataService.put('freelancerreg/updateProfile', otherdetails);
  }

  updateOtherDetails(otherdetails: any) {
    return this.dataService.put('freelancerreg/updateOtherDetails', otherdetails);
  }


  updateExperienceDetails(jobDetails: any) {
    debugger;
    return this.dataService.put('freelancerreg/updateExperience/', jobDetails);
  }
  updateExperienceCertificate(experienceCertificate: any) {
    return this.dataService.uploadFile('freelancerreg/updateExperienceCertificate/', experienceCertificate);
  }
  updateJoiningLetter(joiningLetter: any) {
    return this.dataService.uploadFile('freelancerreg/updateJoiningLetter/', joiningLetter);
  }
  updateSkillDetails(skillDetails: any) {
    return this.dataService.put('freelancerreg/updateSkillDetails/', skillDetails);
  }
  updateResume(resume: any) {
    return this.dataService.uploadFile('freelancerreg/updateResume/', resume);
  }
  uploadResume(resume: any) {
    return this.dataService.uploadFile('freelancerreg/uploadResume/', resume);
  }

  submitDetails(username: string) {
    return this.dataService.put('freelancerreg/submitDetails/' + username, username);
  }
  getJobrolebyName(modeller: string) {
    return this.dataService.get('elasticsearch/getJobrolebyName/' + modeller);
  }
  deleteEducation(eid: number) {
    return this.dataService.delete('freelancerreg/deleteEducation/' + eid);
  }
  deleteExperience(exid: number) {
    return this.dataService.delete('freelancerreg/deleteExperience/' + exid);
  }
  getSkillsCompetencies(username: string) {
    return this.dataService.get('freelancerreg/getSkillsCompetencies/' + username);
  }
  submitSkills(username: string, skills: any) {
    return this.dataService.put('freelancerreg/submitSkills/' + username, skills);
  }
  deleteSkillCompetency(competencyId: number) {
    return this.dataService.delete('freelancerreg/DeleteSkillCompetency/' + competencyId);
  }


  getCompetencyByName(competenctyName: string) {
    return this.dataService.get('elasticsearch/getCompetencyByName/' + competenctyName);
  }
  getAllSkills(skillName: string) {
    return this.dataService.get('elasticsearch/getAllSkills/' + skillName);
  }
  getFreelancerCurrentEvent(username: string) {
    return this.dataService.get('freelanceriviewer/getCurrentCalendarEvents/' + username);
  }

  getMyInterviews(id: number) {
    return this.dataService.get('freelanceriviewer/getMyInterviews/' + id);
  }
  getMyInteviewDetails(id: number) {
    return this.dataService.get('freelanceriviewer/getInterviewDetail/' + id);
  }
  updatePassword(d: any) {
    return this.dataService.put('identity/updatepassword', d);
  }

  getSkillbyName(skillName: string) {
    return this.dataService.get('elasticsearch/getSkillbyName/' + skillName);
  }
  // getCountry(name: string) {
  //   return this.dataService.get('elasticsearch/getCountry/' + name);
  // }
  // getState(name: string) {
  //   return this.dataService.get('elasticsearch/getstate/' + name);
  // }
  // getDistrict(name: string) {
  //   return this.dataService.get('elasticsearch/getdistrict/' + name);
  // }
  getAllCountries() {
    return this.dataService.get('elasticsearch/getAllCountries');
  }

  getAllStatesForCountry(countryName: string) {
    return this.dataService.get('elasticsearch/getAllStatesForCountry/' + countryName);
  }

  getAllStatesForCountryname(countryName: string) {
    return this.dataService.get('elasticsearch/getAllStatesForCountryname/' + countryName);
  }

  getAllCitiesForState(stateName: string) {
    return this.dataService.get('elasticsearch/getAllCitiesForState/' + stateName);
  }
  
  getAllDistrictsForState(stateName: string) {
    return this.dataService.get('elasticsearch/getAllDistrictsForState/' + stateName);
  }

  // set workDays and Hours
  createWorkDayHours(d: any) {
    return this.dataService.put('freelanceriviewer/createWorkDayHours', d);
  }

  // get work day hours

  getWorkDayHours(username) {
    return this.dataService.get('freelanceriviewer/getWorkdayHours/' + username);
  }
  // 08-10-2020 work
  getCurrentIndustry() {
    return this.dataService.get('elasticsearch/getindustry');
  }

  getJobrolebyIndustry(industryname) {
    return this.dataService.get('elasticsearch/getJobrolebyIndustry/' + industryname);
  }

  getAllFunctionalAreas() {
    return this.dataService.get('elasticsearch/getAllFunctionalAreas');
  }

  getAllBanks() {
    return this.dataService.get('elasticsearch/getAllBanks');
  }

  updateBankDetails(d: any) {
    return this.dataService.put('freelancerreg/updateBankDetails', d);
  }

  getBankDetails(username) {
    return this.dataService.get('freelancerreg/getBankDetail/' + username);
  }

  sendInterviewResponse(data: any) {
    return this.dataService.put('freelanceriviewer/sendInterviewResponse', data);
  }

  getAllCompetencies() {
    return this.dataService.get('elasticsearch/getAllCompetencies');
  }

  getSkillByCompetecy(id) {
    return this.dataService.get('elasticsearch/getSkillbyCompetency/' + id);
  }



  saveAllInterviewResultDetail(d: any) {
    return this.dataService.put('freelancer/saveAllInterviewResultDetail', d);
  }

  saveAllInterviewQna(d: any) {
    return this.dataService.put('freelancer/saveAllInterviewQna', d)
  }



  getIntervieDetailsById(id) {
    return this.dataService.get('freelancer/getinterviewdetail/' + id);
  }

  getTechskillAllQuestionById(id) {
    return this.dataService.get('freelancer/getInterviewtechqna/' + id);
  }


  getDomainskillAllQuestionById(id) {
    return this.dataService.get('freelancer/getInterviewdomainqna/' + id);
  }

  getGeneralAptitudeskillAllQuestionById(id) {
    return this.dataService.get('freelancer/getInterviewgaqna/' + id);
  }

  

  

  getMyPendingPayments(id) {
    return this.dataService.get('freelancer/getmypendingPayments/' + id)
  }

  getmyApprovedPayments(id) {
    return this.dataService.get('freelancer/getmyApprovedPayments/' + id)
  }


  getProfileCompleteness(username) {
    return this.dataService.get('freelancerreg/getprofilecompleteness/' + username)
  }

  downloadFile(data: any) {
    return this.dataService.downloadFile('freelancerreg/DownloadFile', data);
  }

  // ************************************Freelancer feedback******************************************************************

  saveInterviewResultSummary(d: any) {
    return this.dataService.put('freelancerfeedback/saveInterviewResultSummary', d);
  }


  getInterviewSummaryByInterviewId(id) {
    return this.dataService.get('freelancerfeedback/getInterviewsummarybyinterviewid/' + id);
  }


  saveInterviewQna(d: any) {
    return this.dataService.put('freelancerfeedback/saveInterviewQna', d);
  }

 

  deleteInterviewQnA(id) {
    return this.dataService.delete('freelancerfeedback/deleteInterviewQnA/' + id)
  }

  getinterviewdetails(d:any){
    return this.dataService.post('freelancerfeedback/getinterviewdetails', d);
  }

  getInterviewDetailbySkillnCompetency(d:any){
    return this.dataService.post('freelancerfeedback/getInterviewDetailbySkillnCompetency', d);
  }


  validateiterviewdetail(d:any){
    return this.dataService.post('freelancerfeedback/validateinterviewdetail', d);
  }

  deleteInterviewDetail(id) {
    return this.dataService.delete('freelancerfeedback/deleteInterviewDetail/' + id);
  }


  saveInterviewResultDetail(d: any) {
    return this.dataService.put('freelancerfeedback/saveInterviewResultDetail', d);
  }


}
