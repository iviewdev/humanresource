import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {

  constructor(private spinner: NgxSpinnerService) { }

  showSpinner() {
  //  console.log("SHowing spinner");
    this.spinner.show();
  }

  hideSpinner() {
    //console.log("hiding spinner");
    this.spinner.hide();
  }
}
