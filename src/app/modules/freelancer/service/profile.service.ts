import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FreelancerService } from './freelancer.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  public profilecompleteness = new BehaviorSubject<number>(0);

  public profilestatus = new BehaviorSubject<string>('');

  constructor(private freelanceService: FreelancerService) { }

  setProfileCompleteness(username) {
   
    this.freelanceService.getProfileCompleteness(username).subscribe((res: any) => {
      if (res) {
        this.profilecompleteness.next(res.percentcomplete);
      }
    }, err => {
      console.log(err);
    });
  }

  setProfileStatus(val) {
    this.profilestatus.next(val);
  }


}
