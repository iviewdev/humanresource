import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './components/layout/layout.component';
import { AddProfileComponent } from './pages/add-profile/add-profile.component';
import { EntryCalenderComponent } from './pages/entry-calender/entry-calender.component';
import { MyCalendarComponent } from './pages/my-calendar/my-calendar.component';
import { FeedbackComponent } from './pages/feedback/feedback.component';
import { MyInterviewsComponent } from './pages/my-interviews/my-interviews.component';
import { MyInterviewDetailsComponent } from './pages/my-interview-details/my-interview-details.component';
import { UpdatePwdComponent } from './pages/update-pwd/update-pwd.component';
import { MyDocumentApprovalComponent } from './pages/my-document-approval/my-document-approval.component';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { ViewProfileComponent } from './pages/view-profile/view-profile.component';
import { MyPaymentsComponent } from './pages/my-payments/my-payments.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { StartMeetingComponent } from './pages/start-meeting/start-meeting.component';
//import { BasicInfoComponent } from './pages/add-profile/basic-info/basic-info.component';
import { BasicInfoComponent } from './pages/add-profile/basic-info/basic-info.component'
import { JobinfoComponent } from './pages/add-profile/jobinfo/jobinfo.component';
import { AddressComponent } from './pages/add-profile/address/address.component';
import { IdproofComponent } from './pages/add-profile/idproof/idproof.component';
import { ExperienceComponent } from './pages/add-profile/experience/experience.component';
import { EducationComponent } from './pages/add-profile/education/education.component';
import { BankComponent } from './pages/add-profile/bank/bank.component';
import { RolesComponent } from './pages/add-profile/roles/roles.component';
import { ReviewComponent } from './pages/add-profile/review/review.component';
import { RateinfoComponent } from './pages/add-profile/rateinfo/rateinfo.component';
const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'add-profile',
        component: AddProfileComponent,
        canActivate: [AuthGuard],
        children: [
          { 'path': '', redirectTo: 'basicinfo', pathMatch: 'full' },
          { 'path': 'basicinfo', component: BasicInfoComponent },
          { 'path': 'jobinfo', component: JobinfoComponent },
          { 'path': 'idproof', component: IdproofComponent },
          { 'path': 'address', component: AddressComponent },
          { 'path': 'experience', component: ExperienceComponent },
          { 'path': 'education', component: EducationComponent },
          { 'path': 'bank', component: BankComponent },
          { 'path': 'roles', component: RolesComponent },
          { 'path': 'otherinfo', component: RateinfoComponent },
          { 'path': 'review', component: ReviewComponent },
        ]
      },
      {
        path: 'entry-calender',
        component: EntryCalenderComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'my-calendar',
        component: MyCalendarComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'complete-feedback',
        component: FeedbackComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'my-interviews',
        component: MyInterviewsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'my-interview-details/:id',
        component: MyInterviewDetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'update-pwd',
        component: UpdatePwdComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'my-document-approval',
        component: MyDocumentApprovalComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'view-update-profile',
        component: ViewProfileComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'my-payments',
        component: MyPaymentsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'start-meeting',
        component: StartMeetingComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    [RouterModule.forChild(routes)]
  ],
  exports: [RouterModule]
})
export class FreelancerRoutingModule { }
