import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './components/layout/layout.component';
import { AddProfileComponent } from './pages/add-profile/add-profile.component';
import { FreelancerRoutingModule } from './freelancer-routing.module';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FormWizardModule } from 'angular2-wizard';
import { EntryCalenderComponent } from './pages/entry-calender/entry-calender.component';
import { MyCalendarComponent } from './pages/my-calendar/my-calendar.component';
import { ScheduleModule, WeekService, MonthService, DayService, DragAndDropService, ResizeService } from '@syncfusion/ej2-angular-schedule';
import { FeedbackComponent } from './pages/feedback/feedback.component';
import { MyInterviewsComponent } from './pages/my-interviews/my-interviews.component';
import { MyInterviewDetailsComponent } from './pages/my-interview-details/my-interview-details.component';
import { UpdatePwdComponent } from './pages/update-pwd/update-pwd.component';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { AmazingTimePickerModule } from 'amazing-time-picker';
import { MyDocumentApprovalComponent } from './pages/my-document-approval/my-document-approval.component';
import { ViewProfileComponent } from './pages/view-profile/view-profile.component';
import { MyPaymentsComponent } from './pages/my-payments/my-payments.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { StartMeetingComponent } from './pages/start-meeting/start-meeting.component';
import { VideoModule } from 'src/app/video/video.module';
import { BasicInfoComponent } from './pages/add-profile/basic-info/basic-info.component';
import { JobinfoComponent } from './pages/add-profile/jobinfo/jobinfo.component';
import { IdproofComponent } from './pages/add-profile/idproof/idproof.component';
import { AddressComponent } from './pages/add-profile/address/address.component';
import { ExperienceComponent } from './pages/add-profile/experience/experience.component';
import { EducationComponent } from './pages/add-profile/education/education.component';
import { BankComponent } from './pages/add-profile/bank/bank.component';
import { RolesComponent } from './pages/add-profile/roles/roles.component';
import { ReviewComponent } from './pages/add-profile/review/review.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AddroleComponent } from './pages/add-profile/roles/addrole/addrole.component';
import { RateinfoComponent } from './pages/add-profile/rateinfo/rateinfo.component';
import { AddEducationComponent } from './pages/add-profile/education/add-education/add-education.component';
import { AddExperienceComponent } from './pages/add-profile/experience/add-experience/add-experience.component';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { TeckSkillComponent } from './pages/feedback/teck-skill/teck-skill.component';
import { AddTeckSkillComponent } from './pages/feedback/teck-skill/add-teck-skill/add-teck-skill.component';
import { TeckQuestionComponent } from './pages/feedback/teck-skill/add-teck-skill/teck-question/teck-question.component';
import { AddInterviewSummaryComponent } from './pages/feedback/add-interview-summary/add-interview-summary.component';
import { DomainSkillComponent } from './pages/feedback/domain-skill/domain-skill.component';
import { AddDomainSkillComponent } from './pages/feedback/domain-skill/add-domain-skill/add-domain-skill.component';
import { AddDomainQuestionComponent } from './pages/feedback/domain-skill/add-domain-skill/add-domain-question/add-domain-question.component';
import { GeneralAptitudeComponent } from './pages/feedback/general-aptitude/general-aptitude.component';
import { AddGeneralAptitudeComponent } from './pages/feedback/general-aptitude/add-general-aptitude/add-general-aptitude.component';
import { AddGeneralAptitudeQuestiongComponent } from './pages/feedback/general-aptitude/add-general-aptitude/add-general-aptitude-questiong/add-general-aptitude-questiong.component';
import { FeedbackSummaryComponent } from './pages/feedback/feedback-summary/feedback-summary.component';
import { UpdateFeedbackSummaryComponent } from './pages/feedback/feedback-summary/update-feedback-summary/update-feedback-summary.component';

@NgModule({
  declarations: [
    LayoutComponent,
    AddProfileComponent,
    SidebarComponent,
    NavbarComponent,
    FooterComponent,
    EntryCalenderComponent,
    MyCalendarComponent,
    FeedbackComponent,
    MyInterviewsComponent,
    MyInterviewDetailsComponent,
    UpdatePwdComponent,
    MyDocumentApprovalComponent,
    ViewProfileComponent,
    MyPaymentsComponent,
    DashboardComponent,
    StartMeetingComponent,
    BasicInfoComponent,
    JobinfoComponent,
    IdproofComponent,
    AddressComponent,
    ExperienceComponent,
    EducationComponent,
    AddEducationComponent,
    AddExperienceComponent,
    BankComponent,
    RolesComponent,
    ReviewComponent,
    AddroleComponent,
    RateinfoComponent,
    TeckSkillComponent,
    AddTeckSkillComponent,
    TeckQuestionComponent,
    AddInterviewSummaryComponent,
    DomainSkillComponent,
    AddDomainSkillComponent,
    AddDomainQuestionComponent,
    GeneralAptitudeComponent,
    AddGeneralAptitudeComponent,
    AddGeneralAptitudeQuestiongComponent,
    FeedbackSummaryComponent,
    UpdateFeedbackSummaryComponent,
  ],
  imports: [
    CommonModule,
    FreelancerRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormWizardModule,
    NgbModule,
    NgMultiSelectDropDownModule,
    BsDatepickerModule.forRoot(),
    AutocompleteLibModule,
    ScheduleModule,
    AngularDateTimePickerModule,
    AmazingTimePickerModule,
    VideoModule,
    NgxSpinnerModule,
    NgbModalModule,
    DialogModule
  ],
  entryComponents: [AddExperienceComponent, AddEducationComponent,
    AddroleComponent, MyInterviewDetailsComponent,
    AddTeckSkillComponent, TeckQuestionComponent,
    AddInterviewSummaryComponent, AddDomainSkillComponent, AddDomainQuestionComponent,
    AddGeneralAptitudeComponent, AddGeneralAptitudeQuestiongComponent,
    UpdateFeedbackSummaryComponent],
  providers: [DayService, WeekService, MonthService, DragAndDropService, ResizeService,]
})
export class FreelancerModule { }
