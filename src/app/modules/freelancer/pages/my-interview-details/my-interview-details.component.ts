import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FreelancerService } from '../../service/freelancer.service';
import swal from 'sweetalert2';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-my-interview-details',
  templateUrl: './my-interview-details.component.html',
  styleUrls: ['./my-interview-details.component.scss']
})
export class MyInterviewDetailsComponent implements OnInit {
  public interViewDetails: any;
  @Input() interview_id: number;
  public isLoading: boolean = false;
  constructor(
    private freelanceServie: FreelancerService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private router: Router,
    private modal: NgbActiveModal
  ) { }

  ngOnInit() {
    this.getInterviewDetails(this.interview_id);
  }


  close() {
    this.modal.close();
  }

  getInterviewDetails(id) {
    this.isLoading = true;
    this.freelanceServie.getMyInteviewDetails(id).subscribe((res: any) => {
      console.log(res);
      this.isLoading = false;
      if (res) {
        this.interViewDetails = res;
        this.interViewDetails.interviewstarttime
      } else {
        this.interViewDetails = {};
      }
    }, err => {
      this.isLoading = false;
      console.log(err);
    });
  }


  check_time(starttime, endtime) {

    return true;
  }

  sendInterviewResponse(status) {
    let d = {
      scheduleid: this.interViewDetails.scheduleid,
      interviewid: this.interViewDetails.interviewid,
      freelancerid: this.interViewDetails.freelancerid,
      jobseekerid: this.interViewDetails.jobseekerid,
      interviewstarttime: this.interViewDetails.interviewstarttime,
      interviewendtime: this.interViewDetails.interviewendtime,
      interviewstatus: status,
      interviewrecordingurl: null,
      interviewchannel: null,
      remarks: "",
    }


    this.freelanceServie.sendInterviewResponse(d).subscribe((res: any) => {
      if (res) {
        this.interViewDetails.interviewstatus = status;
        this.toastr.success(status);
        this.router.navigate(['freelancer/my-interviews']);
      }
    }, err => {
      this.toastr.error('Fail');
    });


  }

  //*************************Complete Feedback********************
  complete_feedback(interViewDetails) {
    console.log(this.interview_id);

    swal({
      title: "Do you want to proceed?",
      text: "",
      type: 'warning',
      showConfirmButton: true,
      showCancelButton: true
    })
      .then((willDelete) => {

        if (willDelete.value) {
          this.getInterview_details(this.interview_id, interViewDetails);
          //  swal("Success");
          //this.router.navigate(['freelancer/complete-feedback'], { queryParams: { jobseekerid: interViewDetails.jobseekerid, interviewid: interViewDetails.interviewid, interviewstatus: interViewDetails.interviewstatus, scheduleid: interViewDetails.scheduleid, freelancerid: interViewDetails.freelancerid } });
        } else {
          //  swal("Fail");
        }

        //  console.log(willDelete)
      });
  }

  //Get Interview Summary

  getInterview_details(id, interViewDetails) {
    this.freelanceServie.getMyInteviewDetails(id).subscribe((res: any) => {
      if (res) {
        console.log(res);
        let d = {
          scheduleid: res.scheduleid,
          interviewid: res.interviewid,
          freelancerid: res.freelancerid,
          jobseekerid: res.jobseekerid,
          interviewstarttime: res.interviewstarttime,
          interviewendtime: res.interviewendtime,
          interviewstatus: 'Interview Completed (Pending Feedback)',
          interviewrecordingurl: res.interviewrecordingurl,
          interviewchannel: res.interviewchannel,
          remarks: "",
        }
        this.freelanceServie.sendInterviewResponse(d).subscribe((res: any) => {
          if (res) {
            this.router.navigate(['freelancer/complete-feedback'], { queryParams: { jobseekerid: interViewDetails.jobseekerid, interviewid: interViewDetails.interviewid, interviewstatus: interViewDetails.interviewstatus, scheduleid: interViewDetails.scheduleid, freelancerid: interViewDetails.freelancerid } });
          }
        }, err => {
          this.toastr.error('Fail');
        });
      }
    }, err => {

    });
  }


  // Go to start Interview start page
  start_interview() {
    this.router.navigate(['/freelancer/start-meeting', this.interViewDetails.interviewid, this.interViewDetails.jobseekerid]);
  }

}
