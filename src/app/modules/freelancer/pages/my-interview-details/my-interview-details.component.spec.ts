import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyInterviewDetailsComponent } from './my-interview-details.component';

describe('MyInterviewDetailsComponent', () => {
  let component: MyInterviewDetailsComponent;
  let fixture: ComponentFixture<MyInterviewDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyInterviewDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyInterviewDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
