import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { EmitType } from '@syncfusion/ej2-base';
import { ToastrService } from 'ngx-toastr';
import { StorageService } from 'src/app/shared/services/storage.service';
import { FreelancerService } from '../../service/freelancer.service';
import { SpinnerService } from '../../service/spinner.service';
import { MyInterviewDetailsComponent } from '../my-interview-details/my-interview-details.component';
declare var $: any;

@Component({
  selector: 'app-my-interviews',
  templateUrl: './my-interviews.component.html',
  styleUrls: ['./my-interviews.component.scss']
})
export class MyInterviewsComponent implements OnInit {

  //----------------------------------
  // @ViewChild('ejDialog', { static: false }) ejDialog: DialogComponent;
  // Create element reference for dialog target element.
  @ViewChild('container', { read: ElementRef, static: false }) container: ElementRef;
  // The Dialog shows within the target element.
  public targetElement: HTMLElement;

  public width: string = '';
  public hide: any;
  public isFullScreen: Boolean;
  public dialogOldPositions: any;

  //-------------------------------------------
  public myInterviews: any = [];
  public completedInterviews: any = [];
  public isLoading: boolean = false;
  constructor(private freelancerService: FreelancerService,
    private storageService: StorageService,
    private modal: NgbModal,
    private spinner: SpinnerService,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit() {

    this.getMyOpenInterviews();

  }

  //get my all interviews list

  getMyOpenInterviews() {
    this.spinner.showSpinner();
    var id = +this.storageService.getItem('id') ? +this.storageService.getItem('id') : 0;
    this.freelancerService.getMyInterviews(id).subscribe((res: any) => {
      console.log(res);
      this.myInterviews = [];
      this.completedInterviews = [];
      if (res) {
        
        res.forEach(element => {
          if (element.interviewstatus == 'Completed & Closed' || element.interviewstatus == 'Interview Completed (Pending Feedback)') {
            this.completedInterviews.push(element);
          } else {
            this.myInterviews.push(element);
          }
        });
      } else {
        this.myInterviews = [];
        this.completedInterviews = [];
      }
      this.spinner.hideSpinner();
    }, err => {
      this.spinner.hideSpinner();
      console.log(err);
    });
  }


  changeSatatus(event, tb) {
    if (event.target.value == 'all') {
      this.getMyOpenInterviews();
    } else {
      this.getInterviewAndSort(event.target.value, tb);
    }
  }


  getInterviewAndSort(val, tb) {
    this.spinner.showSpinner();
    var id = +this.storageService.getItem('id') ? +this.storageService.getItem('id') : 0;
    this.freelancerService.getMyInterviews(id).subscribe((res: any) => {
      if (tb == 1) {
        this.myInterviews = [];
      }

      if (tb == 2) {
        this.completedInterviews = [];
      }

      if (res) {
        if (val == 'rejected' && tb == 1) {
          res.forEach(element => {
            if (element.interviewstatus == 'Interview Request Rejected') {
              this.myInterviews.push(element);
            }
          });
        }

        if (val == 'confirmed' && tb == 1) {
          res.forEach(element => {
            if (element.interviewstatus == 'Interview Request Confirmed') {
              this.myInterviews.push(element);
            }
          });
        }

        if (val == 'closed' && tb == 2) {
          res.forEach(element => {
            if (element.interviewstatus == 'Completed & Closed') {
              this.completedInterviews.push(element);
            }
          });
        }

        if (val == 'completed' && tb == 2) {
          res.forEach(element => {
            if (element.interviewstatus === 'Interview Completed (Pending Feedback)') {
              this.completedInterviews.push(element);
            }
          });
        }

      } else {
        this.myInterviews = [];
        this.completedInterviews = [];
      }
      this.spinner.hideSpinner();
    }, err => {
      this.spinner.hideSpinner();
      console.log(err);
    });
  }











  interviewDetails(id) {
    const modelRef = this.modal.open(MyInterviewDetailsComponent, {
      size: 'md',
      backdrop: 'static'
    });
    modelRef.componentInstance.interview_id = id;
  }




  start_interview(interview) {
    //  this.router.navigate(['/freelancer/start-meeting', interviewid, jobseekerid]);
    this.router.navigate(['/freelancer/start-meeting'], { queryParams: interview });
  }




  sendInterviewResponse(obj, status) {
    let d = {
      scheduleid: obj.scheduleid,
      interviewid: obj.interviewid,
      freelancerid: obj.freelancerid,
      jobseekerid: obj.jobseekerid,
      interviewstarttime: obj.interviewstarttime,
      interviewendtime: obj.interviewendtime,
      interviewstatus: status,
      interviewrecordingurl: null,
      interviewchannel: null,
      remarks: "",
    }

    debugger;
    this.freelancerService.sendInterviewResponse(d).subscribe((res: any) => {
      if (res) {
        console.log(res);
        var i = this.myInterviews.findIndex(x => x.interviewid === obj.interviewid);
        console.log(i);
        this.myInterviews[i].interviewstatus = status;

        this.toastr.success(status);
        // this.router.navigate(['freelancer/my-interviews']);
      }
    }, err => {
      console.log(err);
      this.toastr.error('Fail');
    });


  }

  complete_feedback(interViewDetails) {
    this.router.navigate(['freelancer/complete-feedback'], { queryParams: { jobseekerid: interViewDetails.jobseekerid, interviewid: interViewDetails.interviewid, interviewstatus: interViewDetails.interviewstatus, scheduleid: interViewDetails.scheduleid, freelancerid: interViewDetails.freelancerid } });
  }


}
