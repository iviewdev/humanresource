import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SpinnerService } from 'src/app/modules/employer/service/spinner.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { FreelancerService } from '../../../service/freelancer.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddroleComponent } from './addrole/addrole.component';
import { ProfileService } from '../../../service/profile.service';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';
@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {
  public username: any;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private spinner: SpinnerService,
    private freelancerService:FreelancerService,
    private storageService:StorageService,
    private toastr:ToastrService,
    private modelService: NgbModal,
    private profileService:ProfileService,
    private confirmDailogService:ConfirmationDialogService
  ) { }

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.profileService.setProfileCompleteness(this.username);
    this.getAllCompetency(this.username);
    this.getbasicprofile(this.username);
  }

  next() {
    this.router.navigate(['/freelancer/add-profile/otherinfo']);
  }

  previous() {
    console.log("in previous");
    this.router.navigate(['/freelancer/add-profile/bank']);
  }


  public basicProfile: any;
  getbasicprofile(username) {
    if (username) {
      this.freelancerService.getbasicprofile(username).subscribe((data: any) => {
        if (data) {
          if (data.status == 'Pending_Approval') {
            this.confirmDailogService.confirm(`Information`, `You cannot make changes to  Documents as they are pending admin approval.`,
              `OK`, '', 'md')
              .then((confirmed) => {
                if (confirmed) {

                }
              }).catch((err) => {
                console.log(err);

              });
          }
          this.basicProfile = data;
        }
      },
        err => {
          console.log(err);
        });

    }
  }



  public competencies: any = [];
  getAllCompetency(username) {
    this.spinner.showSpinner();
    this.freelancerService.getSkillsCompetencies(username).subscribe((res: any) => {
      if (res) {
        this.competencies = res;
        console.log(JSON.stringify(this.competencies));

      } else {
        this.competencies = [];
      }
      this.spinner.hideSpinner();
    }, err => {
      this.competencies = [];
      this.spinner.hideSpinner();
    });
  }

  competanceAddUpdate(compe?) {
    const modelRef = this.modelService.open(AddroleComponent, {
      size: 'md',
      backdrop: 'static'
    });
    modelRef.componentInstance.username = this.username;
    modelRef.componentInstance.compe = compe;
    modelRef.result.then((res: any) => {
      this.getAllCompetency(this.username);
    }, dismiss => {

    });
  }

  delete(competencyid) {
    this.spinner.showSpinner();
    this.freelancerService.deleteSkillCompetency(competencyid).subscribe((res: any) => {
      console.log(res);
      this.competencies = this.competencies.filter(x => x.competencyid !== competencyid);
      this.spinner.hideSpinner();
      this.toastr.success('Deleted successfully');
    }, err => {
      this.spinner.hideSpinner();
      console.log(err);
    });
  }

}
