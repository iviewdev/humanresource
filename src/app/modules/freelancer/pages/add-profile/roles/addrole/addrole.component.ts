import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SpinnerService } from 'src/app/modules/employer/service/spinner.service';
import { FreelancerService } from '../../../../service/freelancer.service';
@Component({
  selector: 'app-addrole',
  templateUrl: './addrole.component.html',
  styleUrls: ['./addrole.component.scss']
})
export class AddroleComponent implements OnInit {
  public competency: FormGroup;
  public submitted: boolean = false;
  @Input() username;
  @Input() compe;
  constructor(private fb: FormBuilder,
    private freelancerService: FreelancerService,
    private spinner: SpinnerService,
    private modelService: NgbActiveModal) { }

  ngOnInit() {
    this.create();
    this.getAllRoles();
    if (this.compe) {
      console.log(this.compe);
      this.competency.patchValue(this.compe);
    }
  }
  create() {
    this.competency = this.fb.group({
      username: [this.username],
      competencyid: [''],
      jobcategory: [''],
      years_of_exp: ['', Validators.required],
      totalmentored: ['', Validators.required],
    });
  }

  get f() {
    return this.competency.controls;
  }
  //Get Competance List
  public RoleData: any = [];
  getAllRoles() {
    this.freelancerService.getAllRoles().subscribe((data: any) => {
      this.RoleData = data.content;
    },
      (err) => {
        console.log(err);
      });
  }

  close() {
    this.modelService.dismiss();
  }

  submit() {
    this.submitted = true;
    if (this.competency.invalid) {
      console.log("in invalid");
      return;
    }
    this.spinner.showSpinner();
    this.freelancerService.submitSkills(this.username, this.competency.value).subscribe((res: any) => {
      this.spinner.hideSpinner();
      if (res) {
        this.modelService.close();
      }
    }, err => {
      this.spinner.hideSpinner();
      console.log(err);
    });
  }
}
