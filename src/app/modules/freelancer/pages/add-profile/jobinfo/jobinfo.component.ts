import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FreelancerService } from '../../../service/freelancer.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { ToastrService } from 'ngx-toastr';
import { SpinnerService } from 'src/app/modules/employer/service/spinner.service';
import { ProfileService } from '../../../service/profile.service';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';

@Component({
  selector: 'app-jobinfo',
  templateUrl: './jobinfo.component.html',
  styleUrls: ['./jobinfo.component.scss']
})
export class JobinfoComponent implements OnInit {
  public jobInfo: FormGroup;
  public submitted: boolean = false;
  public username: any;


  constructor(
    private router: Router,
    private fb: FormBuilder,
    private freelancerService: FreelancerService, 
    private storageService: StorageService,
    private toastr: ToastrService, 
    private spinner: SpinnerService,
    private profileService:ProfileService,
    private confirmDailogService:ConfirmationDialogService) { }

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.profileService.setProfileCompleteness(this.username);
    this.getCurrentIndustry();
    this.getAllFuctionArea();
    this.create();
    this.getbasicprofile(this.username);
  }
 
  create() {
    this.jobInfo = this.fb.group({
      currindustry: ['', Validators.required],
      functionalarea: ['', Validators.required],
      currrole: ['', Validators.required],
    });
  }
 
  public basicProfile: any;
  getbasicprofile(username) {
    if (username) {
      this.freelancerService.getbasicprofile(username).subscribe((data: any) => {
        if (data) {
          if (data.status == 'Pending_Approval') {
            this.confirmDailogService.confirm(`Information`, `You cannot make changes to  Documents as they are pending admin approval.`,
              `OK`, '', 'md')
              .then((confirmed) => {
                if (confirmed) {

                }
              }).catch((err) => {
                console.log(err);

              });
          }
          delete data.jobSeekerOther;
          delete data.experiences;
          delete data.educations;
          this.basicProfile = data;
          console.log(data.content);
          this.getJobRoleByCurrentIndustry(data.currindustry);
          this.jobInfo.patchValue(data);
        }
      },
        err => {
          console.log(err);
        });

    }
  }

  get f() {
    return this.jobInfo.controls;
  }

  submit() {
    console.log("in submit");
    this.submitted = true;
    
    if (this.jobInfo.invalid) {
      console.log("in INVALID");
      return;
    }
    if(!this.jobInfo.touched){
      console.log("in if");
      this.next();
      return false;
    }

    this.spinner.showSpinner();
    const jobInfoObj = Object.assign(this.basicProfile, this.jobInfo.value);
    this.freelancerService.updateJobInfo(jobInfoObj).subscribe(res => {
      this.spinner.hideSpinner();
      this.toastr.success('Job info saved successfully.');
      this.next();
    },
      err => {
        this.spinner.hideSpinner();
        this.toastr.error('Error in job info saved.');
        console.log(err);
      });

  }


  previous() {
    this.router.navigate(['/freelancer/add-profile/basicinfo']);
  }

  next() {
    this.router.navigate(['/freelancer/add-profile/idproof']);
  }

   //Get Current Industry
   public currentIndustryData: any = [];
   getCurrentIndustry() {
 
     this.freelancerService.getCurrentIndustry().subscribe((data: any) => {
       console.log(data.content);
       this.currentIndustryData = data.content;
 
     },
       (err) => {
         console.log(err);
       });
   }
 
   //Get Job role of current Industry
   public currentRoleData: any = [];
   getJobRoleByCurrentIndustry(val) {
 
     this.freelancerService.getJobrolebyIndustry(val).subscribe((data: any) => {
       this.currentRoleData = data;
     },
       (err) => {
         console.log(err);
       });
   }
 
   //Get Functional Area
   public currentFunctionalAreaData: any = [];
   getAllFuctionArea() {
 
     this.freelancerService.getAllFunctionalAreas().subscribe((data: any) => {
 
       this.currentFunctionalAreaData = data.content;
     },
       (err) => {
         console.log(err);
       });
   }


}
