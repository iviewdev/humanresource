import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FreelancerService } from '../../../service/freelancer.service';
import { SpinnerService } from 'src/app/modules/employer/service/spinner.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddExperienceComponent } from './add-experience/add-experience.component';
import { ProfileService } from '../../../service/profile.service';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {

  public username: any;
  public experiences: any = [];
  public title: string = 'None';
  public edit_mode: boolean = false;
  public otherDetails: any;
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private freelancerService: FreelancerService,
    public storageService: StorageService,
    private spinner: SpinnerService,
    private modalService: NgbModal,
    private profileService: ProfileService,
    private toastr: ToastrService,
    private confirmDailogService:ConfirmationDialogService
  ) { }

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.profileService.setProfileCompleteness(this.username);
    this.getExperiences(this.username);
    this.getbasicprofile(this.username);
    this.getOtherDetails(this.username);
  }

  getExperiences(username) {
    // this.spinner.showSpinner();
    this.freelancerService.getExperience(username).subscribe((res: any) => {


      if (res) {
        this.experiences = res;
      } else {
        this.experiences = [];
      }
      this.spinner.hideSpinner();
    }, err => {
      this.spinner.hideSpinner();
      console.log(err);
      this.experiences = [];
    })
  }


  next() {
    this.router.navigate(['/freelancer/add-profile/education']);
  }

  previous() {
    //console.log("in previous");
    this.router.navigate(['/freelancer/add-profile/address']);
  }

  //#################################
  toggle_edit_mode(val) {
    if (val) {
      this.updateProfileTitle();
    } else {
      this.edit_mode = true;
    }
  }


  getOtherDetails(username) {
    this.freelancerService.getOtherDetails(username).subscribe((res: any) => {
      if (res) {
        //console.log("res="+JSON.stringify(res));
        this.otherDetails = res;
        this.title = res.profiletitle;
      }
    }, err => {
      console.log(err);
    })
  }

  updateProfileTitle() {
    console.log("in update profile");
    this.spinner.showSpinner();
    if (this.otherDetails) {
      let d = {
        other_id: this.otherDetails.other_id,
        username: this.username,
        profiletitle: this.title,
      }
      if (d.profiletitle == '') {
        this.toastr.error('Enpty value');
        this.spinner.hideSpinner();
        return false;
      }
      const obj = Object.assign(this.otherDetails, d);
      this.freelancerService.updateProfile(obj).subscribe((res: any) => {
        this.spinner.hideSpinner();
        this.edit_mode = false;
      }, err => {
        this.spinner.hideSpinner();
        console.log(err);
      })
    }
    else {
      console.log("in else");
      let d = {
        other_id: 0,
        username: this.username,
        profiletitle: this.title,
      }
      console.log(JSON.stringify(d));
      this.freelancerService.updateProfile(d).subscribe((res: any) => {
        this.spinner.hideSpinner();
        this.edit_mode = false;
      }, err => {
        this.spinner.hideSpinner();
        console.log(err);
      })
    }





    //console.log('D='+JSON.stringify(d));

  }

  //################################
  public basicProfile: any;
  getbasicprofile(username) {
    if (username) {
      this.freelancerService.getbasicprofile(username).subscribe((data: any) => {
        if (data) {
          if (data.status == 'Pending_Approval') {
            this.confirmDailogService.confirm(`Information`, `You cannot make changes to  Documents as they are pending admin approval.`,
              `OK`, '', 'md')
              .then((confirmed) => {
                if (confirmed) {

                }
              }).catch((err) => {
                console.log(err);

              });
          }
          this.basicProfile = data;
          //      this.title = data.freelancerOther.profiletitle;

        }
      },
        err => {
          console.log(err);
        });

    }
  }

  //addnew experience

  addNew(exp?) {
    const modelRef = this.modalService.open(AddExperienceComponent, {
      size: 'md',
      backdrop: 'static'
    });
    modelRef.componentInstance.exp = exp;
    modelRef.componentInstance.basicProfileId = this.basicProfile.id;
    modelRef.componentInstance.username = this.username;

    modelRef.result.then((res: any) => {
      this.getExperiences(this.username);
    }, dismiss => {
      console.log(dismiss);
    })
  }


  delete(exid) {
    this.spinner.showSpinner();
    this.freelancerService.deleteExperience(exid).subscribe((res: any) => {
      console.log(res);
      this.experiences = this.experiences.filter(x => x.exid !== exid);
      this.spinner.hideSpinner();
      this.toastr.success('Deleted successfully');
    }, err => {
      this.spinner.hideSpinner();
      console.log(err);
    });
  }

}
