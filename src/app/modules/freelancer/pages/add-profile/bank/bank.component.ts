import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FreelancerService } from '../../../service/freelancer.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { HttpResponse } from '@angular/common/http';
import { SpinnerService } from '../../../service/spinner.service';
import { ToastrService } from 'ngx-toastr';
import { ProfileService } from '../../../service/profile.service';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';
@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.scss']
})
export class BankComponent implements OnInit {
  public bankForm: FormGroup;
  public submitted: boolean = false;
  public bankList:any;
  public username: String;
  constructor(
    private router: Router, 
    private fb: FormBuilder,
    private freelancerService:FreelancerService,
    private storageService: StorageService,
    private spinner: SpinnerService,
    private toastr: ToastrService,
    private profileService:ProfileService,
    private confirmDailogService:ConfirmationDialogService
  ) { }

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.profileService.setProfileCompleteness(this.username);
    this.getbasicprofile(this.username);
    this.create();
    this.getBanks();
    this.getBankDetail();
  }

  //************************* Create Basic form  ***********/
 create() { 
  this.bankForm = this.fb.group({
    bankaccountid: [''],
    bankname: ['', Validators.required],
    accountnumber: ['', Validators.required],
    ifsccode: ['', Validators.required],
    username: [this.username]
  });
}


public basicProfile: any;
  getbasicprofile(username) {
    if (username) {
      this.freelancerService.getbasicprofile(username).subscribe((data: any) => {
        if (data) {
          if (data.status == 'Pending_Approval') {
            this.confirmDailogService.confirm(`Information`, `You cannot make changes to  Documents as they are pending admin approval.`,
              `OK`, '', 'md')
              .then((confirmed) => {
                if (confirmed) {

                }
              }).catch((err) => {
                console.log(err);

              });
          }
          this.basicProfile = data;
        }
      },
        err => {
          console.log(err);
        });

    }
  }


submit(){
  this.submitted = true;
  if (this.bankForm.invalid) {
    console.log("in if invalid");
    return;
  }
   
  if (!this.bankForm.touched) {
    console.log("in if");

    this.next();
    return false;
  }

  //console.log(JSON.stringify(this.bankForm.value));
  this.spinner.showSpinner();
  this.freelancerService.updateBankDetails(this.bankForm.value).subscribe(res => {
    this.spinner.hideSpinner();
    this.toastr.success('Bank info saved successfully.');
    this.next();
  },
    err => {
      this.spinner.hideSpinner();
      this.toastr.error('Error in Saving Bank info .');
      console.log(err);
    });
}



get f() {
  return this.bankForm.controls;
}

next() {
  console.log("in next");
  this.router.navigate(['/freelancer/add-profile/roles']);
}

previous(){
  
  this.router.navigate(['/freelancer/add-profile/education']);
}

getBanks(){
  this.freelancerService.getAllBanks().subscribe((data: any) => {
       this.bankList = data.content;
     },
       (err) => {
         console.log(err);
       });
}

getBankDetail(){
  //this.spinner.showSpinner();
  this.freelancerService.getBankDetails(this.username).subscribe((res:any) =>{
    if(res){
      console.log(JSON.stringify(res));
      this.bankForm.patchValue(res);
      this.spinner.hideSpinner();
    }
    else{
      this.spinner.hideSpinner();
    }
  },
   err => {
    console.log(err);
    this.spinner.hideSpinner();

  })

}



}
