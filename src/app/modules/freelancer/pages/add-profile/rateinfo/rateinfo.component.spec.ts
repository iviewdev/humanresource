import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateinfoComponent } from './rateinfo.component';

describe('RateinfoComponent', () => {
  let component: RateinfoComponent;
  let fixture: ComponentFixture<RateinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RateinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
