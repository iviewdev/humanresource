import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { FreelancerService } from '../../../service/freelancer.service';
import { SpinnerService } from '../../../service/spinner.service';

@Component({
  selector: 'app-rateinfo',
  templateUrl: './rateinfo.component.html',
  styleUrls: ['./rateinfo.component.scss']
})
export class RateinfoComponent implements OnInit {
  public otherInfo: FormGroup;
  public username: any;
  public submitted: boolean = false;
  constructor(private router: Router,
    private fb: FormBuilder,
    private freelancerService: FreelancerService,
    private toastr: ToastrService,
    private storageService: StorageService,
    private spinner: SpinnerService,
    private confirmDailogService:ConfirmationDialogService) { }

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.getOtherDetails(this.username);
    this.getbasicprofile(this.username);
    this.createForm();
  }

  public otherDetails: any;

  getOtherDetails(username) {
    this.freelancerService.getOtherDetails(username).subscribe((res: any) => {
      if (res) {
        this.otherDetails = res;
        this.otherInfo.patchValue(this.otherDetails);
      }
    }, err => {
      console.log(err);
    })
  }

  createForm() {
    this.otherInfo = this.fb.group({
      perhourrate: ['', Validators.required],
      currency: ['INR', Validators.required],
      keywords: ['', Validators.required]
    });
  }

  get f() {
    return this.otherInfo.controls;
  }

  submit() {
    this.submitted = true;
    if (this.otherInfo.invalid) {
      return;
    }
    if (!this.otherInfo.dirty) {
     this.next();
      return false;
    }
    const otherObj = Object.assign(this.otherDetails, this.otherInfo.value);
    this.spinner.showSpinner();
    this.freelancerService.updateOtherDetails(otherObj).subscribe((res: any) => {
      console.log(res);
      this.spinner.hideSpinner();
      this.toastr.success('Saved successfully');
      this.next();
    }, err => {
      console.log(err);
      this.spinner.hideSpinner();
    })
  }

  next() {
    this.router.navigate(['/freelancer/add-profile/review']);
  }

  previous() {
    console.log("in previous");
    this.router.navigate(['/freelancer/add-profile/roles']);
  }


  //################################
  public basicProfile: any;
  getbasicprofile(username) {
    if (username) {
      this.freelancerService.getbasicprofile(username).subscribe((data: any) => {
        if (data) {
          if (data.status == 'Pending_Approval') {
            this.confirmDailogService.confirm(`Information`, `You cannot make changes to  Documents as they are pending admin approval.`,
              `OK`, '', 'md')
              .then((confirmed) => {
                if (confirmed) {

                }
              }).catch((err) => {
                console.log(err);

              });
          }
          this.basicProfile = data;
          //      this.title = data.freelancerOther.profiletitle;

        }
      },
        err => {
          console.log(err);
        });

    }
  }

}
