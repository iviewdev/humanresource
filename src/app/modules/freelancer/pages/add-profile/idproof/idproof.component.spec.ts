import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdproofComponent } from './idproof.component';

describe('IdproofComponent', () => {
  let component: IdproofComponent;
  let fixture: ComponentFixture<IdproofComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdproofComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdproofComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
