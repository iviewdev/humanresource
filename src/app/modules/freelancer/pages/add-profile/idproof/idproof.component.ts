import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SpinnerService } from 'src/app/modules/employer/service/spinner.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { FreelancerService } from '../../../service/freelancer.service';
import { HttpResponse } from '@angular/common/http';
import { Console } from 'console';
import { ProfileService } from '../../../service/profile.service';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';

@Component({
  selector: 'app-idproof',
  templateUrl: './idproof.component.html',
  styleUrls: ['./idproof.component.scss']
})
export class IdproofComponent implements OnInit {
  public idProof: FormGroup;
  public submitted: boolean = false;
  public username: string = '';
  
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private storageService: StorageService,
    private spinner: SpinnerService,
    private freelancerService: FreelancerService,
    private toastr: ToastrService,
    private profileService:ProfileService,
    private confirmDailogService:ConfirmationDialogService
  ) { } 

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.profileService.setProfileCompleteness(this.username);
    this.create();
    this.getbasicprofile(this.username);

  }
  
  create() {
    this.idProof = this.fb.group({
      identityprooftype: ['', Validators.required],
      identityNumber: ['', Validators.required],
      idproofurl: ['', Validators.required],
      taxidnumber: ['', Validators.required],
      taxproofurl: ['', Validators.required],
    });
  }
  
  next() {
    console.log("in next");
    this.router.navigate(['/freelancer/add-profile/address']);
  }

  previous() {
    this.router.navigate(['/freelancer/add-profile/jobinfo']);
  }

  //getbasicprofile
  public basicProfile: any;
  getbasicprofile(username) {
    if (username) {
      this.freelancerService.getbasicprofile(username).subscribe((data: any) => {
        if (data) {
          if (data.status == 'Pending_Approval') {
            this.confirmDailogService.confirm(`Information`, `You cannot make changes to  Documents as they are pending admin approval.`,
              `OK`, '', 'md')
              .then((confirmed) => {
                if (confirmed) {

                }
              }).catch((err) => {
                console.log(err);

              });
          }
          delete data.jobSeekerOther;
          delete data.experiences;
          delete data.educations;
          this.basicProfile = data;
          this.idProof.patchValue(data);
        }
      },
        err => {
          console.log(err);
        });

    }
  }

  //###########################
  get f() {
    return this.idProof.controls;
  }

  submit() {
    this.submitted = true;
    if (this.idProof.invalid) {
      console.log("in invalid");
      return;
    }

    if (!this.idProof.touched) {
      this.next();
      return false;
    }

    this.spinner.showSpinner();
    const idProofObj = Object.assign(this.basicProfile, this.idProof.value);

    if (this.idProofAttachment != null && this.taxProofAttachment != null) {

      this.freelancerService.uploadIDfile(this.idProofAttachment).toPromise()
        .then(event => {
          if (event instanceof HttpResponse) {
            var url = this.getEscapeStringUrl(event.body as string);
            idProofObj.idproofurl = url;
            this.idProofAttachment = null;
            this.freelancerService.uploadTaxfile(this.taxProofAttachment).toPromise()
              .then(event => {
                if (event instanceof HttpResponse) {
                  var url = this.getEscapeStringUrl(event.body as string);
                  idProofObj.taxproofurl = url;
                  this.taxProofAttachment = null;
                  this.freelancerService.addIdDetails(idProofObj).subscribe(res => {
                    this.spinner.hideSpinner();
                    this.toastr.success('Id proof saved successfully.');
                    this.next();
                  },
                    err => {
                      this.spinner.hideSpinner();
                      this.toastr.error('Error in id proof saved.');
                      console.log(err);
                    });
                }
              }).catch(err => {
                this.spinner.hideSpinner();
                console.log(err);
              });
          }
        }).catch(err => {
          this.spinner.hideSpinner();
          console.log(err);
        });






    } else if (this.idProofAttachment != null && this.taxProofAttachment == null) {

      this.freelancerService.uploadIDfile(this.idProofAttachment).toPromise()
        .then(event => {
          if (event instanceof HttpResponse) {
            var url = this.getEscapeStringUrl(event.body as string);
            idProofObj.idproofurl = url;
            this.idProofAttachment = null;
            this.freelancerService.addIdDetails(idProofObj).subscribe(res => {
              this.spinner.hideSpinner();
              this.toastr.success('Id proof saved successfully.');
              this.next();
            },
              err => {
                this.spinner.hideSpinner();
                this.toastr.error('Error in id proof saved.');
                console.log(err);
              });
          }
        }).catch(err => {
          this.spinner.hideSpinner();
          console.log(err);
        });

    } else if (this.idProofAttachment == null && this.taxProofAttachment != null) {
      this.freelancerService.uploadTaxfile(this.taxProofAttachment).toPromise()
        .then(event => {
          if (event instanceof HttpResponse) {
            var url = this.getEscapeStringUrl(event.body as string);
            idProofObj.taxproofurl = url;
            this.taxProofAttachment = null;
            this.freelancerService.addIdDetails(idProofObj).subscribe(res => {
              this.spinner.hideSpinner();
              this.toastr.success('Id proof saved successfully.');
              this.next();
            },
              err => {
                this.spinner.hideSpinner();
                this.toastr.error('Error in id proof saved.');
                console.log(err);
              });
          }
        }).catch(err => {
          this.spinner.hideSpinner();
          console.log(err);
        });
    } else {
      this.freelancerService.addIdDetails(idProofObj).subscribe(res => {
        this.spinner.hideSpinner();
        this.toastr.success('Id proof saved successfully.');
        this.next();
      },
        err => {
          this.spinner.hideSpinner();
          this.toastr.error('Error in id proof saved.');
          console.log(err);
        });
    }

  }

  //########################################
  public idProofAttachment: FormData;
  uploadIdProofFile(event: any) {
    console.log("in upload idproof");
    if (event.target.files.length > 0) {
      const uploadfile: File = event.target.files[0];
      if (this.validateFile(uploadfile)) {
        
        this.idProof.patchValue({
          idproofurl: uploadfile.name
        });
        this.idProof.markAsTouched();
        const formData: FormData = new FormData();
        formData.append('file', uploadfile);
        formData.append('bucket', 'iviewid' + this.basicProfile.id);
        formData.append('username', this.username);
        this.idProofAttachment = formData; // this.getFormData(uploadfile);
      }

    }
  }

  //########################################
  public taxProofAttachment: FormData;
  uploadTaxProofFile(event: any) {
    if (event.target.files.length > 0) {
      const uploadfile: File = event.target.files[0];
      if (this.validateFile(uploadfile)) {
        this.idProof.patchValue({
          taxproofurl: uploadfile.name
        });
        this.idProof.markAsTouched();
        const formData: FormData = new FormData();
        formData.append('file', uploadfile);
        formData.append('bucket', 'iviewid' + this.basicProfile.id);
        formData.append('username', this.username);
        this.taxProofAttachment = formData; // this.getFormData(uploadfile);
      }

    }
  }

  validateFile(file: File): boolean {

    let IsValid = true;
    const extension = file.name.substr(file.name.lastIndexOf('.') + 1);
    if (file.size > (1024 * 1024)) {
      this.toastr.error('Please select file size less than 1 MB');
      IsValid = false;
    }
    if (extension.toLowerCase() != 'doc' && extension.toLowerCase() != 'docx' &&
      extension.toLowerCase() != 'pdf' && extension.toLowerCase() != 'jpeg'
      && extension.toLowerCase() != 'jpg' && extension.toLowerCase() != 'png') {
      this.toastr.error("Only doc, docx, JPEG, PNG and PDF files are allowed.");
      IsValid = false;
    }
    return IsValid;
  }



  //##########################
  getEscapeStringUrl(url: string): string {
    if (url) {
      return url.replace('\ "', '').replace('"', '').replace('"', '');
    }
    return '';
  }

}
