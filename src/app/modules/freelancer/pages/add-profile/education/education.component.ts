import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SpinnerService } from 'src/app/modules/employer/service/spinner.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { AddEducationComponent } from './add-education/add-education.component';
import { FreelancerService } from '../../../service/freelancer.service';
import { ProfileService } from '../../../service/profile.service';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';
@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent implements OnInit {
  public username: any;
  public educations: any = [];
  public title: string = '';
  public edit_mode: boolean = false;
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private freelancerService: FreelancerService,
    private storageService: StorageService,
    private spinner: SpinnerService,
    private profileService:ProfileService,
    private toastr:ToastrService,
    private confirmDailogService:ConfirmationDialogService  ) { }

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.profileService.setProfileCompleteness(this.username);
    this.getbasicprofile(this.username);
    this.getEducationList(this.username);
  }


  
  next() {
    this.router.navigate(['/freelancer/add-profile/bank']);
  }

  previous() {
    console.log("in previous");
    this.router.navigate(['/freelancer/add-profile/experience']);
  }


  openEducation(edu?) {
    const modelRef = this.modalService.open(AddEducationComponent, {
      size: 'md',
      backdrop: 'static'
    });
    modelRef.componentInstance.username = this.username;
    modelRef.componentInstance.basicProfileId = this.basicProfile.id;
    modelRef.componentInstance.edu = edu;
    modelRef.result.then((res: any) => {
      this.getEducationList(this.username);
    }, dissmiss => {

    });
  }

  public basicProfile: any;
  getbasicprofile(username) {
    if (username) {
      this.freelancerService.getbasicprofile(username).subscribe((data: any) => {
        if (data) {
          if (data.status == 'Pending_Approval') {
            this.confirmDailogService.confirm(`Information`, `You cannot make changes to  Documents as they are pending admin approval.`,
              `OK`, '', 'md')
              .then((confirmed) => {
                if (confirmed) {

                }
              }).catch((err) => {
                console.log(err);

              });
          }
          this.basicProfile = data;
        }
      },
        err => {
          console.log(err);
        });

    }
  }


 //##########################
 getEducationList(username) {
   //this.spinner.showSpinner();
   this.freelancerService.getEducation(username).subscribe((res: any) => {
     if (res) {
       this.educations = res;
     } else {
       this.educations = [];
     }
     this.spinner.hideSpinner();
   }, err => {
     this.spinner.hideSpinner();
     this.educations = [];
     console.log(err);
   })
 }

 delete(eid) {
  this.spinner.showSpinner();
  this.freelancerService.deleteEducation(eid).subscribe((res: any) => {
    console.log(res);
    this.educations = this.educations.filter(x => x.eid !== eid);
    this.spinner.hideSpinner();
    this.toastr.success('Deleted successfully');
  }, err => {
    this.spinner.hideSpinner();
    console.log(err);
  });
}

}
