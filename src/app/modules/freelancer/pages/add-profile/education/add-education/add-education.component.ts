import { HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { SpinnerService } from 'src/app/modules/employer/service/spinner.service';
import { FreelancerService } from 'src/app/modules/freelancer/service/freelancer.service';
import { StorageService } from 'src/app/shared/services/storage.service';

@Component({
  selector: 'app-add-education',
  templateUrl: './add-education.component.html',
  styleUrls: ['./add-education.component.scss']
})
export class AddEducationComponent implements OnInit {
  public education: FormGroup;
  public submitted: boolean = false;
  @Input() basicProfileId;
  @Input() username;
  @Input() edu: any;
  constructor(private fb: FormBuilder,
    private feeelancerService: FreelancerService, private spinner: SpinnerService,
    private modelService: NgbActiveModal,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.create();
    this.getYears();
    console.log(JSON.stringify(this.edu));
    if (this.edu) {
      this.education.patchValue(this.edu);
    }
  } 

  //##################################
  create() {
    this.education = this.fb.group({
      eid: [''],
      username: [this.username],
      education_type: ['', Validators.required],
      institute_name: ['', Validators.required],
      institute_address: ['', Validators.required],
      board: [''],
      university: ['', Validators.required],
      yearofpassing: ['', Validators.required],
      gpa: [''],
      gpa_ind: ['N',Validators.required],
      percent_marks: ['', Validators.required],
      certificate_url: ['', Validators.required]
    });

  }

  //#######################
  get f() {
    return this.education.controls;
  }

  //################################

  submit() {
    this.submitted = true;
    if (this.education.invalid) {
      return
    }
    this.spinner.showSpinner();
    debugger;
    this.feeelancerService.updateEducation(this.education.value).subscribe((res: any) => {
      if (res) {

        if (this.attachedEduCert != null) {
          this.attachedEduCert.append('eid', res.eid);
          this.feeelancerService.updateEducationCertificate(this.attachedEduCert).toPromise()
            .then(event => {
              if (event instanceof HttpResponse) {
                this.attachedEduCert = null;
                this.spinner.hideSpinner();
                this.modelService.close();
              }
            }).catch(err => {
              this.spinner.hideSpinner();
              console.log(err);
            })
        } else {
          this.spinner.hideSpinner();
          this.modelService.close();
        }
      }
    }, err => {
      console.log(err);
    });
  }

  //############################

  dismiss() {
    this.modelService.dismiss();
  }

  public years: any = [];
  //#############################
  getYears() {
    const year = new Date().getFullYear();
    for (let i = 0; i < 60; i++) {
      this.years.push({
        label: year - i,
        value: parseInt(String(year - i))
      });
    }
  }

  //########################################
  public attachedEduCert: FormData;

  attachEduCertificate(event: any) {
    if (event.target.files.length > 0) {
      const uploadfile: File = event.target.files[0];
      if (this.validateFile(uploadfile)) {
        this.education.patchValue({
          certificate_url: uploadfile.name
        });
        this.education.markAsTouched();
        const formData: FormData = new FormData();
        formData.append('file', uploadfile);
        formData.append('bucket', 'iviewid' + this.basicProfileId);
        formData.append('username', this.username);
        this.attachedEduCert = formData; // this.getFormData(uploadfile);
      }

    }
  }

  validateFile(file: File): boolean {

    let IsValid = true;
    const extension = file.name.substr(file.name.lastIndexOf('.') + 1);
    if (file.size > (1024 * 1024)) {
      this.toastr.error('Please select file size less than 1 MB');
      IsValid = false;
    }
    if (extension.toLowerCase() != 'doc' && extension.toLowerCase() != 'docx' &&
      extension.toLowerCase() != 'pdf' && extension.toLowerCase() != 'jpeg'
      && extension.toLowerCase() != 'jpg' && extension.toLowerCase() != 'png') {
      this.toastr.error("Only doc, docx, JPEG, PNG and PDF files are allowed.");
      IsValid = false;
    }
    return IsValid;
  }



  //##########################
  getEscapeStringUrl(url: string): string {
    if (url) {
      return url.replace('\ "', '').replace('"', '').replace('"', '');
    }
    return '';
  }

 


}
