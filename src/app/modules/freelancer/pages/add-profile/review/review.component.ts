import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/shared/services/storage.service';
import { FreelancerService } from '../../../service/freelancer.service';
import { saveAs } from 'file-saver';
import { SortPipe } from 'src/app/shared/pipes/sort.pipe';
import { SpinnerService } from '../../../service/spinner.service';
import { ProfileService } from '../../../service/profile.service';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
  public username: any;
  constructor(
    private router: Router,
    private freelancerService: FreelancerService,
    private storageService: StorageService,
    private sortPipe: SortPipe,
    private spinner: SpinnerService,
    private profileService:ProfileService,
    private confirmDialogService:ConfirmationDialogService,
    private toastr:ToastrService
  ) { }

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.profileService.setProfileCompleteness(this.username);
    this.getAllCountries();
    this.getOtherDetails(this.username);
    this.getExperiences(this.username);
    this.getEducationList(this.username);
    this.getBankDetail(this.username);
    this.getAllCompetency(this.username);
  }
  previous() {
    console.log("in previous");
    this.router.navigate(['/freelancer/add-profile/otherinfo']);
  }

  public basicProfile: any;
  getbasicprofile(username) {
    if (username) {
      this.spinner.showSpinner();
      this.freelancerService.getbasicprofile(username).subscribe((data: any) => {
        if (data) {
          console.log(data);
          this.getAllStatesForCountry(data.country);
          this.getAllCitiesForState(data.state);
          data.country = this.countryList.find(x => x.id == data.country)['countryname'];
          this.basicProfile = data;
          this.spinner.hideSpinner();
        }
      },
        err => {
          this.spinner.hideSpinner();
          console.log(err);
        });

    }
  }


  //##################################
  public countryList: any = [];
  getAllCountries() {
    this.spinner.showSpinner();
    this.freelancerService.getAllCountries().subscribe((data: any) => {
      console.log(data);
      if (data) {
        this.countryList = this.sortPipe.transform(data.content, "asc", "countryname");
        this.getbasicprofile(this.username);
        this.spinner.hideSpinner();
      }
    },
      (err) => {
        console.log(err);
        this.spinner.hideSpinner();
      });
  }
  onChangeCountry(countryCode: string) {
    if (countryCode) {
      this.getAllStatesForCountry(countryCode);
    }
  }
  public stateList: any = [];
  getAllStatesForCountry(countryCode: string) {
    this.freelancerService.getAllStatesForCountry(countryCode).subscribe((data: any) => {
      console.log(data);
      this.stateList = this.sortPipe.transform(data, "asc", "statename");
      this.basicProfile.state = this.stateList.find(x => x.statecode == this.basicProfile.state)['statename'];
    },
      (err) => {
        console.log(err);
      });
  }
  onChangeState(stateCode: string) {
    if (stateCode) {
      this.getAllCitiesForState(stateCode);
    }
  }


  public cityList: any = [];
  getAllCitiesForState(stateCode: string) {
    this.freelancerService.getAllCitiesForState(stateCode).subscribe((data: any) => {
      this.cityList = this.sortPipe.transform(data, "asc", "cityname");
      this.basicProfile.city = this.cityList.find(x => x.citycode == this.basicProfile.city)['cityname'];
    },
      (err) => {
        console.log(err);
      });
  }

  public experiences: any = [];

  getExperiences(username) {
    // this.spinner.showSpinner();
    this.freelancerService.getExperience(username).subscribe((res: any) => {


      if (res) {
        this.experiences = res;
      } else {
        this.experiences = [];
      }
      this.spinner.hideSpinner();
    }, err => {
      this.spinner.hideSpinner();
      console.log(err);
      this.experiences = [];
    })
  }




  public title: any;
  public otherDetails: any;
  getOtherDetails(username) {
    this.freelancerService.getOtherDetails(username).subscribe((res: any) => {
      if (res) {
        //console.log("res="+JSON.stringify(res));
        this.otherDetails = res;
        this.title = res.profiletitle;
      }
    }, err => {
      console.log(err);
    })
  }


  //##########################
  public educations: any = [];
  getEducationList(username) {
    //this.spinner.showSpinner();
    this.freelancerService.getEducation(username).subscribe((res: any) => {
      if (res) {
        this.educations = res;
      } else {
        this.educations = [];
      }
      this.spinner.hideSpinner();
    }, err => {
      this.spinner.hideSpinner();
      this.educations = [];
      console.log(err);
    })
  }

  public bankDetails: any;
  getBankDetail(username) {
    this.spinner.showSpinner();
    this.freelancerService.getBankDetails(username).subscribe((res: any) => {
      if (res) {
        console.log(res);
        this.bankDetails = res;
        this.spinner.hideSpinner();
      }
      else {
        this.spinner.hideSpinner();
      }
    },
      err => {
        console.log(err);
        this.spinner.hideSpinner();

      })

  }

  public competencies: any = [];
  getAllCompetency(username) {
    this.spinner.showSpinner();
    this.freelancerService.getSkillsCompetencies(username).subscribe((res: any) => {
      if (res) {
        this.competencies = res;
        console.log(JSON.stringify(this.competencies));

      } else {
        this.competencies = [];
      }
      this.spinner.hideSpinner();
    }, err => {
      this.competencies = [];
      this.spinner.hideSpinner();
    });
  }


  downloadFile(url) {
    debugger
    var v = url.split('/');
    var n = v[v.length - 1];
    var f = new FormData();
    f.append('bucket', 'iviewid' + this.basicProfile.id);
    f.append('key', n);
    this.freelancerService.downloadFile(f).subscribe((res: any) => {
      saveAs(res, n);
    }, err => {
      console.log(err);
    })
  }


  submit() {
    this.confirmDialogService.confirm(`Please confirm`, `All The Documents You have Attached would be submitted for verification to Admin. Your Profile would be Visible for Search your Profile is 90% completed. Once your docs are admin verified, your profile would be visible with "Verified Internally" Label. This would improve your Prospects for Shortlisting by Employees.`,
      `Submit Details`, 'Cancel', 'md')
      .then((confirmed) => {
        if (confirmed) {
          this.spinner.showSpinner();
          this.freelancerService.submitDetails(this.username).subscribe((res: any) => {
            console.log(res);
            this.basicProfile.status = res.status;
            this.spinner.hideSpinner();
            this.toastr.success('Details submitted successfully.');
          },
            err => {
              this.toastr.error('Error in submitted details.');
              console.log(err);
              this.spinner.hideSpinner();
            });
        }
      }).catch((err) => {
        console.log(err);
        this.spinner.hideSpinner();
      });
  }

}
