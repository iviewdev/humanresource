import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FreelancerService } from '../../../service/freelancer.service';
import { ToastrService } from 'ngx-toastr';
import { SortPipe } from 'src/app/shared/pipes/sort.pipe';
import { StorageService } from 'src/app/shared/services/storage.service';
import { HttpResponse } from '@angular/common/http';
import { SpinnerService } from '../../../service/spinner.service';
import { logging } from 'selenium-webdriver';
import { ProfileService } from '../../../service/profile.service';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';

@Component({
  selector: 'app-basic-info',
  templateUrl: './basic-info.component.html',
  styleUrls: ['./basic-info.component.scss']
})
export class BasicInfoComponent implements OnInit {
  public basicForm: FormGroup;
  public submitted: boolean = false;
  public username: any;
  public attachedResume: FormData;
  mobile:String;
  id: String;
  constructor(
    private router: Router, 
    private fb: FormBuilder,
    private FreelancerService: FreelancerService,
    private sortPipe: SortPipe, 
    private toastr: ToastrService,
    private storageService: StorageService,
    private spinner: SpinnerService,
    private cd: ChangeDetectorRef,
    private profileService:ProfileService,
    private confirmDailogService:ConfirmationDialogService

  ) { }

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.profileService.setProfileCompleteness(this.username);
    this.getAllCountries();
    this.getbasicprofile(this.username);
    this.create();

     
  }

 //************************* Create Basic form  ***********/
 create() {
  this.basicForm = this.fb.group({
    email: [''],
    title: ['', Validators.required],
    gender: ['Male', Validators.required],
    nationality: ['', Validators.required],
    dob: ['', Validators.required],
    fname: ['', Validators.required],
    lname: ['', Validators.required],
    mobile: ['', Validators.required],
    mobilevalid: [''],
    resumeurl: [''],  
  });
}
  // next() {
  //   console.log("in next");
  //   this.router.navigate(['/freelancer/add-profile/jobinfo']);
  // }

  

  next() {
    console.log("in next");
    this.router.navigate(['/freelancer/add-profile/jobinfo']);
  }

  submit(){
    this.submitted = true;
    if (this.basicForm.invalid) {
      return;
    }
    if (!this.basicForm.dirty) {
      //console.log("in if");

      this.next();
      return false;
    }

    
    this.spinner.showSpinner();

    const basicObj = Object.assign(this.basicProfile, this.basicForm.value);
    //console.log(JSON.stringify(basicObj));
    //console.log("dob="+this.basicForm.get('dob').value);
    if (this.basicForm.get('dob').value != null) {
      basicObj.dob = this.formatYYYYMMDD(this.basicForm.get('dob').value);
      //console.log(basicObj.dob);
    }
    //console.log(" attachedresume"+this.attachedResume.getAll);
    if (this.attachedResume != null) {
      this.FreelancerService.uploadResume(this.attachedResume).toPromise()
        .then(event => {
          if (event instanceof HttpResponse) {
            var url = this.getEscapeStringUrl(event.body as string);
            basicObj.resumeurl = url;
            this.attachedResume = null;
            this.FreelancerService.createbasicprofile(basicObj).subscribe(res => {
              
              this.toastr.success('Personal details saved successfully.');
              this.spinner.hideSpinner();
              this.next();
            },
              err => {
                this.spinner.hideSpinner();
                this.toastr.error('Error in personal details saved.');
                console.log(err);
              });
          }
        }).catch(err => {
          this.spinner.hideSpinner();
          console.log(err);
        });
    } else {
      console.log("else attachedresume!=null");
      this.FreelancerService.createbasicprofile(basicObj).subscribe(res => {
        this.spinner.hideSpinner();
        this.toastr.success('Personal details saved successfully.');
        this.next();
      },
        err => {
          this.spinner.hideSpinner();
          this.toastr.error('Error in personal details saved.');
          console.log(err);
        });
    }
  }


  //Function Defintions
  public countryList: any = [];
  getAllCountries() {
    this.FreelancerService.getAllCountries().subscribe((data: any) => {
      if (data) {
        //console.log(data.content);
        this.countryList = this.sortPipe.transform(data.content, "asc", "countryname");

      }
    },
      (err) => {
        console.log(err);
      });
  }
  
  public basicProfile: any;
  getbasicprofile(username) {
    if (username) {
      this.FreelancerService.getbasicprofile(username).subscribe((data: any) => {
        if (data) {
          if (data.status == 'Pending_Approval') {
            this.confirmDailogService.confirm(`Information`, `You cannot make changes to  Documents as they are pending admin approval.`,
              `OK`, '', 'md')
              .then((confirmed) => {
                if (confirmed) {

                }
              }).catch((err) => {
                console.log(err);

              });
          }
          console.log(data);
          this.basicProfile = data;
          this.mobile=data.mobile;
          this.id=data.id;
          data.dob = new Date(data.dob).toISOString().split('T')[0];
          this.basicForm.patchValue(data);
          console.log(this.basicForm);
          this.basicForm.updateValueAndValidity();
          this.basicForm.markAsUntouched();
          this.basicForm.markAsPristine();
          this.cd.detectChanges();
        }
      },
        err => {

        });

    }
  }


  //#################################
  formatYYYYMMDD(date: string): string {
    const d = new Date(date);
    return d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2);
    // + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);

  }

 //##########################
 getEscapeStringUrl(url: string): string {
  if (url) {
    return url.replace('\ "', '').replace('"', '').replace('"', '');
  }
  return '';
}

get f() {
  return this.basicForm.controls;
}

//######################


  uploadResumeFile(event: any) {
    if (event.target.files.length > 0) {
      const uploadfile: File = event.target.files[0];
      if (this.validateFile(uploadfile)) {
        console.log("In apppend file "+uploadfile.name);
        this.basicForm.patchValue({resumeurl: uploadfile.name});
        this.basicForm.updateValueAndValidity();
        this.basicForm.markAsDirty();
        var formData: FormData = new FormData();
        formData.append('file', uploadfile);
        formData.append('bucket', 'iviewid' + this.basicProfile.id);
        formData.append('username', this.username);
        console.log("formData="+formData.get("file"));
        this.attachedResume = formData; // this.getFormData(uploadfile);
      }

    }
  }

  validateFile(file: File): boolean {

    let IsValid = true;
    const extension = file.name.substr(file.name.lastIndexOf('.') + 1);
    if (file.size > (1024 * 1024)) {
      this.toastr.error('Please select file size less than 1 MB');
      IsValid = false;
    }
    if (extension.toLowerCase() != 'doc' && extension.toLowerCase() != 'docx' &&
      extension.toLowerCase() != 'pdf' && extension.toLowerCase() != 'jpeg'
      && extension.toLowerCase() != 'jpg' && extension.toLowerCase() != 'png') {
      this.toastr.error("Only doc, docx, JPEG, PNG and PDF files are allowed.");
      IsValid = false;
    }
   // console.log(IsValid);
    return IsValid;
  }
}
