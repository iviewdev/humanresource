import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/shared/services/storage.service';
import { FreelancerService } from '../../service/freelancer.service';

@Component({
  selector: 'app-my-payments',
  templateUrl: './my-payments.component.html',
  styleUrls: ['./my-payments.component.scss']
})
export class MyPaymentsComponent implements OnInit {
  public freelanceid: number;
  public myPendingPayments: any = [];
  public myApprovedPayments: any = [];
  constructor(private freelancerService: FreelancerService,
    private storageService: StorageService) { }

  ngOnInit() {
    this.freelanceid = +this.storageService.getItem('id');
    console.log(this.freelanceid);
    this.getMyPendingPayment(this.freelanceid);
    this.getmyApprovedPayments(this.freelanceid);

  }

  getMyPendingPayment(id) {
    this.freelancerService.getMyPendingPayments(id).subscribe((res: any) => {
      this.myPendingPayments = res;
    }, err => {
      console.log(err);
    });
  }


  // Get Approved Payments

  getmyApprovedPayments(id) {
    this.freelancerService.getmyApprovedPayments(id).subscribe((res: any) => {
      this.myApprovedPayments = res;
    }, err => {
      console.log(err);
    });
  }

}
