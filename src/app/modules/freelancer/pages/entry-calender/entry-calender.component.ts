import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { StorageService } from 'src/app/shared/services/storage.service';
import { FreelancerService } from '../../service/freelancer.service';


@Component({
  selector: 'app-entry-calender',
  templateUrl: './entry-calender.component.html',
  styleUrls: ['./entry-calender.component.scss']
})
export class EntryCalenderComponent implements OnInit {
  public workHoursForm: FormGroup;
  public checkbox_selected: boolean = true;
  public isLoading: boolean = false;
  public workDayHours: any;
  constructor(private fb: FormBuilder, private freelancerService: FreelancerService, private storageService: StorageService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.createForm();
    this.getWorkDayHours(this.storageService.getItem('username'));
  }

  createForm() {
    this.workHoursForm = this.fb.group({
      sun: [''],
      mon: [''],
      tue: [''],
      wed: [''],
      thu: [''],
      fri: [''],
      sat: [''],
      start_time: ['', Validators.required],
      end_time: ['', Validators.required]
    });
  }

  //get form control
  get f() {
    return this.workHoursForm.controls;
  }

  checkboxselected() {
    this.checkbox_selected = false;
    var day = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
    var selected_value = [];
    day.forEach((element, inedx) => {
      if (this.workHoursForm.get(element).value) {
        selected_value.push(inedx);
      }
    });
    if (selected_value.length > 0) {
      this.checkbox_selected = false;
    } else {
      this.checkbox_selected = true;
    }
  }

  //submit form

  submit() {
    this.isLoading = true;
    var day = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
    var day_value = [];
    day.forEach((element, inedx) => {
      if (this.workHoursForm.get(element).value) {
        day_value.push(inedx);
      }
    });

    let d = {
      id: this.workDayHours ? this.workDayHours.id : "",
      username: this.storageService.getItem('username'),
      dayofweek: day_value.join(),
      starthour: this.f.start_time.value,
      endhour: this.f.end_time.value
    };

    this.freelancerService.createWorkDayHours(d).subscribe((res: any) => {
      this.isLoading = false;
      this.toastr.success('submit successfully');
    }, err => {
      this.isLoading = false;
    });
  }

  getWorkDayHours(username) {
    this.freelancerService.getWorkDayHours(username).subscribe((res: any) => {
      if (res) {
        this.checkbox_selected = false;
        var d = res.dayofweek.split(',');
        var day = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
        this.workDayHours = res;
        d.forEach(element => {
          //    console.log(element);
          this.workHoursForm.get(day[element]).setValue(true);
        });
        this.workHoursForm.get('start_time').setValue(res.starthour);
        this.workHoursForm.get('end_time').setValue(res.endhour);
      } else {
        this.checkbox_selected = true;
      }
    }, err => {
      console.log(err);
    });
  }

}
