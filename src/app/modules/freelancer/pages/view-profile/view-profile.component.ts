import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/shared/services/storage.service';
import { FreelancerService } from '../../service/freelancer.service';

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.scss']
})
export class ViewProfileComponent implements OnInit {
  public isLoading: boolean = false;
  public username: string;
  public basicProfile: any;
  public educationDetail: any = [];
  public jobDetail: any = [];
  public jobCompetencyDetail: any = [];
  public jobProfile: any;
  constructor(private storageService: StorageService,
    private freelancerService: FreelancerService) { }

  ngOnInit() {
    this.getbasicprofile();
    this.getEducation();
    this.getExperience();
    this.getJobCompetency();
    this.getJobProfile();
  }

  //###################################
  getbasicprofile() {
    this.isLoading = true;
    if (this.storageService.getItem('username') != null) {
      this.username = this.storageService.getItem('username');
      this.freelancerService.getbasicprofile(this.username).subscribe((data: any) => {
        console.log(data);
        this.isLoading = false;
        if (data) {
          this.basicProfile = data;
        }
      });
    }
  }
  //#########################################
  getFileName(url: string): string {
    if (url) {
      return url.split('/').pop();
    }
    return '';
  }


  //##########################################

  getEducation() {
    //  
    if (this.storageService.getItem('username') != null) {
      this.username = this.storageService.getItem('username');
      if (this.educationDetail.length > 0) {
        this.isLoading = true;
      }
      this.freelancerService.getEducation(this.username).subscribe((res: any) => {
        this.isLoading = false;
        this.educationDetail = res;
      });
    }
  }

  //################################
  getExperience() {
    this.isLoading = true;
    if (this.storageService.getItem('username') != null) {
      this.username = this.storageService.getItem('username');

      this.freelancerService.getExperience(this.username).subscribe((res: any) => {
        this.isLoading = false;
        //  console.log(res);
        this.jobDetail = res;
      });
    }
  }

  //########################################
  getJobCompetency() {
    //    
    if (this.storageService.getItem('username') != null) {
      this.username = this.storageService.getItem('username');
      this.freelancerService.getSkillsCompetencies(this.username).subscribe((res: any) => {
        //    
        // console.log(res);
        this.jobCompetencyDetail = res;

      });
    }
  }

  //#######################################
  getJobProfile() {

    if (this.storageService.getItem('username') != null) {
      this.username = this.storageService.getItem('username');
      this.isLoading = true;
      this.freelancerService.getOtherDetails(this.username).subscribe((res: any) => {

        this.isLoading = false;
        this.jobProfile = res;
      })
    }
  }

}
