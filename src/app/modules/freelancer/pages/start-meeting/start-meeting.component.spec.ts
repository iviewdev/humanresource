import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartMeetingComponent } from './start-meeting.component';

describe('StartMeetingComponent', () => {
  let component: StartMeetingComponent;
  let fixture: ComponentFixture<StartMeetingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartMeetingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartMeetingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
