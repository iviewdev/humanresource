import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from 'src/app/shared/services/storage.service';

@Component({
  selector: 'app-start-meeting',
  templateUrl: './start-meeting.component.html',
  styleUrls: ['./start-meeting.component.scss']
})
export class StartMeetingComponent implements OnInit {
  public user_role: string;
  public interviewid: any;
  public jobseekerid: any;
  public userId:number;
  public interviewstarttime;
  public interviewendtime;
  public scheduleid;
  public freelancerid;
  constructor(private storage: StorageService, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.route.queryParams.subscribe(param=>{
      console.log(param);
      this.interviewid = param.interviewid;
      this.jobseekerid = param.jobseekerid;
      this.interviewstarttime = param.interviewstarttime;
      this.interviewendtime = param.interviewendtime;
      this.scheduleid = param.scheduleid;
      this.freelancerid = param.freelancerid;
    });
    this.user_role = this.storage.getItem('role');
    this.userId = +this.storage.getItem('id');
    
  }

}
