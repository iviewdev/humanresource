import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { StorageService } from 'src/app/shared/services/storage.service';
import { FreelancerService } from '../../service/freelancer.service';
import { saveAs } from 'file-saver';
@Component({
  selector: 'app-my-document-approval',
  templateUrl: './my-document-approval.component.html',
  styleUrls: ['./my-document-approval.component.scss']
})
export class MyDocumentApprovalComponent implements OnInit {
  public experienceData: Observable<any>;
  public educationData: Observable<any>;
  public basicProfile: any;
  constructor(private freelancerService: FreelancerService,
    private storageService: StorageService) { }

  ngOnInit() {
    this.getExperience(this.storageService.getItem('username'));
    this.getEducation(this.storageService.getItem('username'));
    this.getBasicProfile(this.storageService.getItem('username'));
  }

  //get Education


  getEducation(username) {
    this.educationData = this.freelancerService.getEducation(username);

  }

  //get Experience


  getExperience(username) {
    this.experienceData = this.freelancerService.getExperience(username);
    this.experienceData.subscribe((res: any) => {
      console.log(res);
    })
  }

  // get basic profile

  getBasicProfile(username) {
    this.freelancerService.getbasicprofile(username).subscribe((res: any) => {
      console.log(res);
      if (res) {
        this.basicProfile = res;
      }
    });
  }



  downloadFile(url) {
    debugger
    var v = url.split('/');
    var n = v[v.length - 1];
    var f = new FormData();
    f.append('bucket', 'iviewid' + this.basicProfile.id);
    f.append('key', n);
    this.freelancerService.downloadFile(f).subscribe((res: any) => {
      saveAs(res, n);
    }, err => {
      console.log(err);
    })
  }

}
