import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyDocumentApprovalComponent } from './my-document-approval.component';

describe('MyDocumentApprovalComponent', () => {
  let component: MyDocumentApprovalComponent;
  let fixture: ComponentFixture<MyDocumentApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyDocumentApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyDocumentApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
