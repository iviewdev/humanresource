import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddGeneralAptitudeComponent } from './add-general-aptitude.component';

describe('AddGeneralAptitudeComponent', () => {
  let component: AddGeneralAptitudeComponent;
  let fixture: ComponentFixture<AddGeneralAptitudeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddGeneralAptitudeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGeneralAptitudeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
