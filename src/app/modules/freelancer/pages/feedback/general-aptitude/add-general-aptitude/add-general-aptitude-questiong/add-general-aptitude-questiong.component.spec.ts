import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddGeneralAptitudeQuestiongComponent } from './add-general-aptitude-questiong.component';

describe('AddGeneralAptitudeQuestiongComponent', () => {
  let component: AddGeneralAptitudeQuestiongComponent;
  let fixture: ComponentFixture<AddGeneralAptitudeQuestiongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddGeneralAptitudeQuestiongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGeneralAptitudeQuestiongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
