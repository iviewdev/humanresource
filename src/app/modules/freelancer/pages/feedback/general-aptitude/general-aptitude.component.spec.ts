import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralAptitudeComponent } from './general-aptitude.component';

describe('GeneralAptitudeComponent', () => {
  let component: GeneralAptitudeComponent;
  let fixture: ComponentFixture<GeneralAptitudeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralAptitudeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralAptitudeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
