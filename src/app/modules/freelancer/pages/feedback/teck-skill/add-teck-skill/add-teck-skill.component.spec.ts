import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTeckSkillComponent } from './add-teck-skill.component';

describe('AddTeckSkillComponent', () => {
  let component: AddTeckSkillComponent;
  let fixture: ComponentFixture<AddTeckSkillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTeckSkillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTeckSkillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
