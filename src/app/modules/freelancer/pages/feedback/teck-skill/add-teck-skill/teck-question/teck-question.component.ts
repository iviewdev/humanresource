import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { FreelancerService } from 'src/app/modules/freelancer/service/freelancer.service';
import { SpinnerService } from 'src/app/modules/freelancer/service/spinner.service';

@Component({
  selector: 'app-teck-question',
  templateUrl: './teck-question.component.html',
  styleUrls: ['./teck-question.component.scss']
})
export class TeckQuestionComponent implements OnInit {
  @Input() competency_id;
  @Input() competencyname;
  @Input() skill_id;
  @Input() skillname;
  @Input() resultid;
  @Input() detailid;
  @Input() question;
  public techQuestion: FormGroup;
  public techskillsubmit: boolean = false;
  constructor(private activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private freelancerService: FreelancerService,
    private spinner: SpinnerService,
    private toastr: ToastrService) {

  }

  ngOnInit() {
    debugger;
    if (this.question != '') {
      this.createForm(this.question);
    } else {
      this.createForm();
    }
  }

  createForm(ele?) {
    this.techQuestion = this.fb.group({
      qnaid: [ele ? ele.qnaid : ''],
      detailid: ele ? ele.detailid : this.detailid,
      resultid: ele ? ele.resultid : this.resultid,
      competencyid: ele ? ele.competencyid : this.competency_id,
      skillid: ele ? ele.skillid : this.skill_id,
      questiontext: [ele ? ele.questiontext : '', [Validators.required]],
      questiontype: [ele ? ele.questiontype : '', [Validators.required]],
      expectedanswer: [ele ? ele.expectedanswer : '', [Validators.required]],
      answergiven: [ele ? ele.answergiven : '', [Validators.required]],
      marks: [ele ? ele.marks : '', [Validators.required]],
      weight: [ele ? ele.weight : '', [Validators.required]],
      weightedscore: [ele ? ele.weightedscore : '', [Validators.required]],
      questioncategory: "Tech",
      competencyname: this.competencyname,
      skillname: this.skillname
    });
  }

  get f() {
    return this.techQuestion.controls;
  }

  //Change Tech Skill complexity
  change_complexity_of_tech_skill(value) {
    if (value == 'simple') {
      this.techQuestion.get('weight').patchValue(1);
    }
    if (value == 'average') {
      this.techQuestion.get('weight').patchValue(2);
    }
    if (value == 'complex') {
      this.techQuestion.get('weight').patchValue(3);
    }
  }

  //Change Tech Skill marks
  change_marks_of_tech_skill(value) {
    var wtage = this.techQuestion.get('weight').value;
    var wtscr = value * wtage;
    this.techQuestion.get('weightedscore').patchValue(wtscr);
  }


  //Submit Tech Skill
  submitTechSkill() {
    this.techskillsubmit = true;
    if (this.techQuestion.invalid) {
      return;
    }
    debugger;
    this.spinner.showSpinner();
    this.freelancerService.saveInterviewQna(this.techQuestion.value).subscribe((res: any) => {
      this.spinner.hideSpinner();
      if (res) {
        this.activeModal.close(res);
      }
    }, err => {
      this.toastr.error('Error');
      console.log(err);
    });
  }

  close(val?) {
    this.activeModal.close(val);
  }

}
