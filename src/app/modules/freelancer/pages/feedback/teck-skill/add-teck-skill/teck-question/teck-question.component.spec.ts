import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeckQuestionComponent } from './teck-question.component';

describe('TeckQuestionComponent', () => {
  let component: TeckQuestionComponent;
  let fixture: ComponentFixture<TeckQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeckQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeckQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
