import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeckSkillComponent } from './teck-skill.component';

describe('TeckSkillComponent', () => {
  let component: TeckSkillComponent;
  let fixture: ComponentFixture<TeckSkillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeckSkillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeckSkillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
