import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInterviewSummaryComponent } from './add-interview-summary.component';

describe('AddInterviewSummaryComponent', () => {
  let component: AddInterviewSummaryComponent;
  let fixture: ComponentFixture<AddInterviewSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddInterviewSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInterviewSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
