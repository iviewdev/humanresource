import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FreelancerService } from '../../../service/freelancer.service';

@Component({
  selector: 'app-add-interview-summary',
  templateUrl: './add-interview-summary.component.html',
  styleUrls: ['./add-interview-summary.component.scss']
})
export class AddInterviewSummaryComponent implements OnInit {
  public interviewSummaryForm: FormGroup;
  public submitInterviewSummary: boolean = false;
  @Input() interviewSummary;
  constructor(private fb: FormBuilder,
    private freelancerService: FreelancerService,
    private activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.getCurrentIndustry();
    this.createForm();
    if(this.interviewSummary){
      this.interviewSummaryForm.patchValue({
        industrytype:this.interviewSummary.industrytype,
        target_job:this.interviewSummary.targetjob
      });
    }
  }



  createForm() {
    this.interviewSummaryForm = this.fb.group({
      industrytype: ['', Validators.required],
      target_job: ['', Validators.required]
    })
  }

  get t() {
    return this.interviewSummaryForm.controls;
  }

  submit() {
    this.submitInterviewSummary = true;
    if (this.interviewSummaryForm.invalid) {
      return false;
    }
    this.close(this.interviewSummaryForm.value);
  }


  // Get Current Industry
  public currentIndustry: any = [];
  getCurrentIndustry() {
    this.freelancerService.getCurrentIndustry().subscribe((res: any) => {
      if (res) {
        this.currentIndustry = res.content;
        if(this.interviewSummary){
          this.onchange_currentindustry(this.interviewSummary.industrytype);
        }
      }
    }, err => {
      console.log(err);
    });
  }


  //Get JobRole
  public targetJobList = [];
  onchange_currentindustry(industry) {
    this.freelancerService.getJobrolebyIndustry(industry).subscribe((res: any) => {
      this.targetJobList = res;
    }, err => {

    });
  }

  // Close
  close(val?) {
    this.activeModal.close(val);
  }

}
