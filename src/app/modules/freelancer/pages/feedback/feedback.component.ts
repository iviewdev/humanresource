import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbTabChangeEvent, NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { FreelancerService } from '../../service/freelancer.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {
  currentJustify = 'fill';
  public techskillForm: FormGroup;
  public techskillsubmit: boolean = false;
  public techskill_isLoading: boolean = false;
  public domainskillForm: FormGroup;
  public domainskillsubmit: boolean;
  public generalAptitudeForm: FormGroup;
  public gasubmit: boolean;
  public summaryForm: FormGroup;
  public interViewDetails: any;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private freelancerService: FreelancerService,
    private toastr: ToastrService
  ) { }

  @ViewChild('pTabs', { static: true }) pTabs: NgbTabset;

  ngOnInit() {
    this.createTechSkill();
    this.createDomainSkill();
    this.createGeneralAptitude();
    this.createSummaryForm();
    this.getAllCompetancyList();
    this.getCurrentIndustry();

    this.route.queryParams.subscribe(params => {
      this.interViewDetails = params;
      this.getInterviewSummaryByIterviewId(this.interViewDetails.scheduleid);
    });

    

  }

  //Create Tech Skill Form 

  createTechSkill() {
    this.techskillForm = this.fb.group({
      industrytype: ['', Validators.required],
      target_job: ['', Validators.required],
      resultid: [''],
      interview_status: [''],
      techskill: this.fb.array([], Validators.minLength(1))
    });
  }

  //get techsill control
  get t() {
    return this.techskillForm.controls;
  }

  // get Tech skill row
  get techSkill() {
    return this.techskillForm.get('techskill') as FormArray;

  }



  // Add Tech Skill Questions

  addTechSkill(ele?) {
    this.techSkill.push(
      this.fb.group({
        competency_name: [ele ? ele.competencyid : '', [Validators.required]],
        skill_name: [ele ? ele.skillid : '', [Validators.required]],
        competencyname: [ele ? ele.competencyname : ''],
        skillname: [ele ? ele.skillname : ''],
        resultdetailid: [ele ? ele.resultdetailid : ''],
        improvementguidance: [ele ? ele.improvementguidance : ''],
        strengthweakness: [ele ? ele.strengthweakness : ''],
        asked_question: this.fb.array([], Validators.minLength(1)),
      })
    );
    console.log(this.techskillForm.value);
  }

  // remove TeckSkill
  removeTechSkill(index) {
    if (this.techSkill.at(index).get('resultdetailid').value) {
      this.freelancerService.deleteInterviewDetail(this.techSkill.at(index).get('resultdetailid').value).subscribe((res: any) => {

        this.toastr.success('Deleted successfull');
        this.techSkill.removeAt(index);

      }, err => {
        console.log(err);
      })
    } else {
      this.toastr.success('Deleted successfull');
      this.techSkill.removeAt(index);
    }

  }

  //get tech skill  asked question
  getTechSkillAskedQuestion(index): FormArray {
    return this.techSkill.at(index).get('asked_question') as FormArray;
  }
  // new tech skill asked question
  newTechSkillAskedQuestion(ele?): FormGroup {
    return this.fb.group({
      qnaid: [ele ? ele.qnaid : ''],
      detailid: [ele ? ele.detailid : ''],
      question: [ele ? ele.questiontext : '', [Validators.required]],
      question_complexity: [ele ? ele.questiontype : '', [Validators.required]],
      expected_answer: [ele ? ele.expectedanswer : '', [Validators.required]],
      actual_answer: [ele ? ele.answergiven : '', [Validators.required]],
      marks: [ele ? ele.marks : '', [Validators.required]],
      weightage: [ele ? ele.weight : '', [Validators.required]],
      weightage_score: [ele ? ele.weightedscore : '', [Validators.required]]
    });
  }

  // add tech skill asked question

  addTechSkillAskedQuestion(index, ele?) {
    this.getTechSkillAskedQuestion(index).push(this.newTechSkillAskedQuestion(ele));
  }

  // remove Tech Skill Asked Question
  removeTechSkillQuestion(techSkillIndex: number, askedQuestionIndex: number) {
    debugger;
    if (this.getTechSkillAskedQuestion(techSkillIndex).at(askedQuestionIndex).get('qnaid').value) {
      this.freelancerService.deleteInterviewQnA(this.getTechSkillAskedQuestion(techSkillIndex).at(askedQuestionIndex).get('qnaid').value).subscribe((res: any) => {
        this.toastr.success('Deleted successfully');
        this.getTechSkillAskedQuestion(techSkillIndex).removeAt(askedQuestionIndex);

      }, err => {
        console.log(err);
      });
    } else {
      this.toastr.success('Deleted successfully');
      this.getTechSkillAskedQuestion(techSkillIndex).removeAt(askedQuestionIndex);
    }

  }

  //Submit Tech Skill
  submitTechSkill() {
    this.techskillsubmit = true;
    if (this.techskillForm.invalid) {
      this.toastr.error('Invalid Form Field');
      return;
    }


    //Save Interview Summary
    var InterViewSummary = {
      resultid: this.techskillForm.get('resultid').value,
      scheduleid: this.interViewDetails.scheduleid,
      interviewerid: this.interViewDetails.freelancerid,
      interviewid: this.interViewDetails.interviewid,
      interview_status: "",
      ai_status: "",
      targetjob: this.techskillForm.get('target_job').value,
      industrytype: this.techskillForm.get('industrytype').value,
      jobseekerid: this.interViewDetails.jobseekerid,
      overallComments: "None"
    };


    this.techskill_isLoading = true;
    var result_id = '';
    this.freelancerService.saveInterviewResultSummary(InterViewSummary).subscribe((res: any) => {
      if (res) {
        result_id = res.resultid;
        this.techskillForm.get('resultid').patchValue(result_id);
        // Save Interview Details
        var InterviewResultDetails = [];
        for (let t of this.techSkill.controls) {
          var wtg = 0;
          var wtgs = 0;
          t.value.asked_question.forEach(element1 => {
            wtg += +element1.weightage;
            wtgs += +element1.weightage_score;
          });

          InterviewResultDetails.push({
            resultdetailid: t.value.resultdetailid,
            questioncategory: 'Tech',
            resultid: result_id,
            competencyid: t.value.competency_name,
            skillid: t.value.skill_name,
            competencyname: t.value.competencyname,
            skillname: t.value.skillname,
            weightedscore: wtgs / (wtg * 10),
            questiontype: "",
            improvementguidance: t.value.improvementguidance,
            strengthweakness: t.value.strengthweakness,
            status: ""
          });

        }

        this.freelancerService.saveAllInterviewResultDetail(InterviewResultDetails).subscribe((res: any) => {
          if (res) {

            res.forEach((element, index) => {
              this.techSkill.at(index).get('resultdetailid').patchValue(element.resultdetailid);
            });
            var InterviewQna = [];
            for (let t of this.techSkill.controls) {
              t.value.asked_question.forEach((element1, index) => {
                InterviewQna.push({
                  qnaid: element1.qnaid,
                  resultid: result_id,
                  detailid: t.value.resultdetailid,
                  competencyid: t.value.competency_name,
                  skillid: t.value.skill_name,
                  questiontext: element1.question,
                  answergiven: element1.actual_answer,
                  expectedanswer: element1.expected_answer,
                  marks: element1.marks,
                  weight: element1.weightage,
                  weightedscore: element1.weightage_score,
                  questiontype: element1.question_complexity,
                  questioncategory: "Tech"
                })
              });
            }

            this.freelancerService.saveAllInterviewQna(InterviewQna).subscribe((res: any) => {
              if (res) {
                var i = 0;
                for (let t of this.techSkill.controls) {
                  t.value.asked_question.forEach((element1, index) => {
                    this.getTechSkillAskedQuestion(i).at(index).get('qnaid').patchValue(res[index].qnaid);
                  });
                  i = i + 1;
                }



                this.techskill_isLoading = false;
                this.loadDomainSkills();
              }
            }, err => {
              this.techskill_isLoading = false;
            });
          }
        }, err => {
          this.techskill_isLoading = false;
        })
      }
    }, err => {
      this.techskill_isLoading = false;
    });
  }


  //###################################Domain Skill#####################################


  //Create Domain Skill Form 

  createDomainSkill() {
    this.domainskillForm = this.fb.group({
      domainskills: this.fb.array([])
    });
  }

  // get Domain skill row
  get domainSkill() {
    return this.domainskillForm.get('domainskills') as FormArray;
  }



  // Add Domain Skill Questions

  addDomainSkill(ele?) {
    this.domainSkill.push(
      this.fb.group({
        competency_name: [ele ? ele.competencyid : '', [Validators.required]],
        skill_name: [ele ? ele.skillid : '', [Validators.required]],
        resultdetailid: [ele ? ele.resultdetailid : ''],
        competencyname: [ele ? ele.competencyname : ''],
        skillname: [ele ? ele.skillname : ''],
        improvementguidance: [ele ? ele.improvementguidance : ''],
        strengthweakness: [ele ? ele.strengthweakness : ''],
        asked_question: this.fb.array([], Validators.minLength(1)),
      })
    );

  }


  // remove DomainSkill
  removeDomainSkill(index) {

    if (this.domainSkill.at(index).get('resultdetailid').value) {
      this.freelancerService.deleteInterviewDetail(this.domainSkill.at(index).get('resultdetailid').value).subscribe((res: any) => {

        this.toastr.success('Deleted successfull');
        this.domainSkill.removeAt(index);

      }, err => {
        console.log(err);
      })
    } else {
      this.toastr.success('Deleted successfull');
      this.domainSkill.removeAt(index);
    }

  }

  //get domain skill  asked question
  getDomainSkillAskedQuestion(index): FormArray {
    return this.domainSkill.at(index).get('asked_question') as FormArray;
  }
  // new domain skill asked question
  newDomainSkillAskedQuestion(ele?): FormGroup {
    return this.fb.group({
      qnaid: [ele ? ele.qnaid : ''],
      detailid: [ele ? ele.detailid : ''],
      question: [ele ? ele.questiontext : '', [Validators.required]],
      question_complexity: [ele ? ele.questiontype : '', [Validators.required]],
      expected_answer: [ele ? ele.expectedanswer : '', [Validators.required]],
      actual_answer: [ele ? ele.answergiven : '', [Validators.required]],
      marks: [ele ? ele.marks : '', [Validators.required]],
      weightage: [ele ? ele.weight : '', [Validators.required]],
      weightage_score: [ele ? ele.weightedscore : '', [Validators.required]]
    });
  }

  // add Domain skill asked question

  addDomainSkillAskedQuestion(index, ele?) {
    this.getDomainSkillAskedQuestion(index).push(this.newDomainSkillAskedQuestion(ele));
  }

  // remove Domain Skill Asked Question
  removeDomainSkillQuestion(domainSkillIndex: number, askedQuestionIndex: number) {

    if (this.getDomainSkillAskedQuestion(domainSkillIndex).at(askedQuestionIndex).get('qnaid').value) {
      this.freelancerService.deleteInterviewQnA(this.getDomainSkillAskedQuestion(domainSkillIndex).at(askedQuestionIndex).get('qnaid').value).subscribe((res: any) => {
        this.toastr.success('Deleted successfully');
        this.getDomainSkillAskedQuestion(domainSkillIndex).removeAt(askedQuestionIndex);

      }, err => {
        console.log(err);
      });
    } else {
      this.toastr.success('Deleted successfully');
      this.getTechSkillAskedQuestion(domainSkillIndex).removeAt(askedQuestionIndex);
    }


  }

  //Submit Domain Skill
  public domainskill_isLoading: boolean = false;
  submitDomainSkill() {
    this.domainskillsubmit = true;
    if (this.domainskillForm.invalid) {
      this.toastr.error('Invalid Form Field');
      return;
    }

    this.domainskill_isLoading = true;
    // Save Interview Details
    var InterviewResultDetails = [];
    for (let t of this.domainSkill.controls) {
      var wtg = 0;
      var wtgs = 0;
      t.value.asked_question.forEach(element1 => {
        wtg += +element1.weightage;
        wtgs += +element1.weightage_score;
      });

      InterviewResultDetails.push({
        resultdetailid: t.value.resultdetailid,
        resultid: this.techskillForm.get('resultid').value,
        competencyid: t.value.competency_name,
        skillid: t.value.skill_name,
        competencyname: t.value.competencyname,
        skillname: t.value.skillname,
        weightedscore: wtgs / (wtg * 10),
        questiontype: "",
        improvementguidance: t.value.improvementguidance,
        strengthweakness: t.value.strengthweakness,
        status: "",
        questioncategory: "Domain"
      });

    }
    debugger;
    this.freelancerService.saveAllInterviewResultDetail(InterviewResultDetails).subscribe((res: any) => {
      if (res) {

        res.forEach((element, index) => {
          this.domainSkill.at(index).get('resultdetailid').patchValue(element.resultdetailid);
        });
        var InterviewQna = [];
        for (let t of this.domainSkill.controls) {
          t.value.asked_question.forEach((element1, index) => {
            InterviewQna.push({
              qnaid: element1.qnaid,
              resultid: this.techskillForm.get('resultid').value,
              detailid: t.value.resultdetailid,
              competencyid: t.value.competency_name,
              skillid: t.value.skill_name,
              questiontext: element1.question,
              answergiven: element1.actual_answer,
              expectedanswer: element1.expected_answer,
              marks: element1.marks,
              weight: element1.weightage,
              weightedscore: element1.weightage_score,
              questiontype: element1.question_complexity,
              questioncategory: "Domain"
            })
          });
        }
        debugger;
        this.freelancerService.saveAllInterviewQna(InterviewQna).subscribe((res: any) => {
          if (res) {
            var i = 0;
            for (let t of this.domainSkill.controls) {
              t.value.asked_question.forEach((element1, index) => {
                this.getDomainSkillAskedQuestion(i).at(index).get('qnaid').patchValue(res[index].qnaid);
              });
              i = i + 1;
            }

            this.domainskill_isLoading = false;
            this.loadGeneralAptitude();
            this.toastr.success('Saved successfully');
          }
        }, err => {
          this.domainskill_isLoading = false;
        });
      }
    }, err => {
      this.domainskill_isLoading = false;
    })




  }



  //###################################################################################



  //###################################General Aptitude#####################################


  //Create General Aptitude Form 

  createGeneralAptitude() {
    this.generalAptitudeForm = this.fb.group({
      general_aptitude: this.fb.array([])
    });
  }

  // get General Aptitude row
  get generalAptitude() {
    return this.generalAptitudeForm.get('general_aptitude') as FormArray;
  }



  // Add General Aptitude

  addGeneralAptitude(ele?) {
    this.generalAptitude.push(
      this.fb.group({
        competency_name: [ele ? ele.competencyid : '', [Validators.required]],
        skill_name: [ele ? ele.skillid : '', [Validators.required]],
        resultdetailid: [ele ? ele.resultdetailid : ''],
        competencyname: [ele ? ele.competencyname : ''],
        skillname: [ele ? ele.skillname : ''],
        improvementguidance: [ele ? ele.improvementguidance : ''],
        strengthweakness: [ele ? ele.strengthweakness : ''],
        asked_question: this.fb.array([], Validators.minLength(1)),
      })
    );
  }

  // remove GeneralAptitude
  removeGeneralAptitude(index) {
    if (this.generalAptitude.at(index).get('resultdetailid').value) {
      this.freelancerService.deleteInterviewDetail(this.generalAptitude.at(index).get('resultdetailid').value).subscribe((res: any) => {

        this.toastr.success('Deleted successfull');
        this.generalAptitude.removeAt(index);

      }, err => {
        console.log(err);
      })
    } else {
      this.toastr.success('Deleted successfull');
      this.generalAptitude.removeAt(index);
    }

  }

  //get general aptitude  asked question
  getGeneralAptitudeAskedQuestion(index): FormArray {
    return this.generalAptitude.at(index).get('asked_question') as FormArray;
  }
  // new general aptitude asked question
  newGeneralAptitudeAskedQuestion(ele): FormGroup {
    return this.fb.group({
      qnaid: [ele ? ele.qnaid : ''],
      detailid: [ele ? ele.detailid : ''],
      question: [ele ? ele.questiontext : '', [Validators.required]],
      question_complexity: [ele ? ele.questiontype : '', [Validators.required]],
      expected_answer: [ele ? ele.expectedanswer : '', [Validators.required]],
      actual_answer: [ele ? ele.answergiven : '', [Validators.required]],
      marks: [ele ? ele.marks : '', [Validators.required]],
      weightage: [ele ? ele.weight : '', [Validators.required]],
      weightage_score: [ele ? ele.weightedscore : '', [Validators.required]]
    });

  }

  // add General Aptitude asked question

  addGeneralAptitudeAskedQuestion(index, ele?) {
    this.getGeneralAptitudeAskedQuestion(index).push(this.newGeneralAptitudeAskedQuestion(ele));
  }

  // remove General Aptitude Asked Question
  removeGeneralAptitudeAskedQuestion(generalAptitudeIndex: number, askedQuestionIndex: number) {

    if (this.getGeneralAptitudeAskedQuestion(generalAptitudeIndex).at(askedQuestionIndex).get('qnaid').value) {
      this.freelancerService.deleteInterviewQnA(this.getGeneralAptitudeAskedQuestion(generalAptitudeIndex).at(askedQuestionIndex).get('qnaid').value).subscribe((res: any) => {
        this.toastr.success('Deleted successfully');
        this.getGeneralAptitudeAskedQuestion(generalAptitudeIndex).removeAt(askedQuestionIndex);

      }, err => {
        console.log(err);
      });
    } else {
      this.toastr.success('Deleted successfully');
      this.getGeneralAptitudeAskedQuestion(generalAptitudeIndex).removeAt(askedQuestionIndex);
    }



  }

  //Submit General Aptitude Skill
  public ga_isLoading: boolean = false;
  submitGeneralAptitude() {


    this.gasubmit = true;
    if (this.generalAptitudeForm.invalid) {
      this.toastr.error('Invalid Form Field');
      return;
    }

    this.ga_isLoading = true;
    // Save Interview Details
    var InterviewResultDetails = [];
    for (let t of this.generalAptitude.controls) {
      var wtg = 0;
      var wtgs = 0;
      t.value.asked_question.forEach(element1 => {
        wtg += +element1.weightage;
        wtgs += +element1.weightage_score;
      });

      InterviewResultDetails.push({
        resultdetailid: t.value.resultdetailid,
        resultid: this.techskillForm.get('resultid').value,
        competencyid: t.value.competency_name,
        skillid: t.value.skill_name,
        competencyname: t.value.competencyname,
        skillname: t.value.skillname,
        weightedscore: wtgs / (wtg * 10),
        questiontype: "",
        improvementguidance: t.value.improvementguidance,
        strengthweakness: t.value.strengthweakness,
        status: "",
        questioncategory: "Ga"
      });

    }
    debugger;
    this.freelancerService.saveAllInterviewResultDetail(InterviewResultDetails).subscribe((res: any) => {
      if (res) {

        res.forEach((element, index) => {
          this.generalAptitude.at(index).get('resultdetailid').patchValue(element.resultdetailid);
        });

        var InterviewQna = [];
        for (let t of this.generalAptitude.controls) {
          t.value.asked_question.forEach((element1, index) => {
            InterviewQna.push({
              qnaid: element1.qnaid,
              resultid: this.techskillForm.get('resultid').value,
              detailid: t.value.resultdetailid,
              competencyid: t.value.competency_name,
              skillid: t.value.skill_name,
              questiontext: element1.question,
              answergiven: element1.actual_answer,
              expectedanswer: element1.expected_answer,
              marks: element1.marks,
              weight: element1.weightage,
              weightedscore: element1.weightage_score,
              questiontype: element1.question_complexity,
              questioncategory: "Ga"
            })
          });
        }

        this.freelancerService.saveAllInterviewQna(InterviewQna).subscribe((res: any) => {
          if (res) {
            var i = 0;
            for (let t of this.generalAptitude.controls) {
              t.value.asked_question.forEach((element1, index) => {
                this.getGeneralAptitudeAskedQuestion(i).at(index).get('qnaid').patchValue(res[index].qnaid);
              });
              i = i + 1;
            }
            this.ga_isLoading = false;
            this.toastr.success('Saved successfully');
            this.loadSummary();
          }
        }, err => {
          this.ga_isLoading = false;
        });
      }
    }, err => {
      this.ga_isLoading = false;
    })






  }



  //###################################################################################



  //####################################Create Summary Form####################


  //Create summary form 

  createSummaryForm() {
    this.summaryForm = this.fb.group({
      summary: this.fb.array([])
    });
  }

  //get Summary Controls

  get get_summary() {
    return this.summaryForm.get('summary') as FormArray;
  }

  // add Summary 

  addSummary(ele?) {
    this.get_summary.push(
      this.fb.group({
        resultdetailid: [ele ? ele.resultdetailid : ''],
        resultid: [ele ? ele.resultid : ''],
        competency: [ele ? ele.competencyid : ''],
        skill: [ele ? ele.skillid : ''],
        competencyname: [ele ? ele.competencyname : ''],
        skillname: [ele ? ele.skillname : ''],
        weighted_score: [ele ? ele.weightedscore : ''],
        strength_weaknesses: [ele ? ele.strengthweakness : '', [Validators.required]],
        improvement_guidance: [ele ? ele.improvementguidance : '', [Validators.required]],
        questioncategory: [ele ? ele.questioncategory : ''],

      })
    );
  }


  // remove summary

  removeSummary(index) {
    this.get_summary.removeAt(index);
  }


  //Submit Summary
  public summarysubmit: boolean = false;
  public summary_isLoading: boolean = false;
  submitSummary() {
    this.summarysubmit = true;
    if (this.summaryForm.invalid) {
      this.toastr.error('Invalid Form Field');
      return;
    }

    this.summary_isLoading = true;
    // Save Interview Details
    var InterviewResultDetails = [];
    for (let t of this.get_summary.controls) {
      InterviewResultDetails.push({
        resultdetailid: t.value.resultdetailid,
        resultid: t.value.resultid,
        competencyid: t.value.competency,
        competencyname: t.value.competencyname,
        skillname: t.value.skillname,
        skillid: t.value.skill,
        weightedscore: t.value.weighted_score,
        questiontype: "",
        improvementguidance: t.value.improvement_guidance,
        strengthweakness: t.value.strength_weaknesses,
        status: "Published",
        questioncategory: t.value.questioncategory
      });

    }
    debugger;
    this.freelancerService.saveAllInterviewResultDetail(InterviewResultDetails).subscribe((res: any) => {
      if (res) {

        //Save Interview Summary
        var InterViewSummary = {
          resultid: this.techskillForm.get('resultid').value,
          scheduleid: this.interViewDetails.scheduleid,
          interviewerid: this.interViewDetails.freelancerid,
          interviewid: this.interViewDetails.interviewid,
          interview_status: "Feedback Completed",
          ai_status: "",
          targetjob: this.techskillForm.get('target_job').value,
          industrytype: this.techskillForm.get('industrytype').value,
          jobseekerid: this.interViewDetails.jobseekerid,
          overallComments: "None"
        };

        this.freelancerService.saveInterviewResultSummary(InterViewSummary).subscribe((res: any) => {
          this.getInterview_details(this.interViewDetails.scheduleid);
        }, err => {
          console.log(err);
        });
        this.toastr.success('Saved successfully');
        this.summary_isLoading = false;
      }
    }, err => {
      console.log(InterviewResultDetails);
      console.log(err);
      this.toastr.error('Fail');
      this.summary_isLoading = false;
    });


  }


  // Submit the Interview Final Status

  getInterview_details(id) {
    // alert(id);
    this.freelancerService.getMyInteviewDetails(id).subscribe((res: any) => {
      if (res) {
        console.log(res);
        let d = {
          scheduleid: res.scheduleid,
          interviewid: res.interviewid,
          freelancerid: res.freelancerid,
          jobseekerid: res.jobseekerid,
          interviewstarttime: res.interviewstarttime,
          interviewendtime: res.interviewendtime,
          interviewstatus: 'Completed & Closed',
          interviewrecordingurl: res.interviewrecordingurl,
          interviewchannel: res.interviewchannel,
          remarks: "",
        }

        this.freelancerService.sendInterviewResponse(d).subscribe((res: any) => {
          if (res) {
            swal('Interview has been successfully completed & closed');
            // this.router.navigate(['freelancer/complete-feedback'], { queryParams: { jobseekerid: interViewDetails.jobseekerid, interviewid: interViewDetails.interviewid, interviewstatus: interViewDetails.interviewstatus, scheduleid: interViewDetails.scheduleid, freelancerid: interViewDetails.freelancerid } });
          }
        }, err => {
          // this.toastr.error('Fail');
        });
      }
    }, err => {

    });
  }


  //Prevent Tab Navigation
  preventChanges(event: NgbTabChangeEvent) {
    // event.preventDefault();
  }

  //Go to Tech Skills
  loadTechSkills() {
    this.pTabs.select('techSkill');
  }

  //Go to Domain Skills
  loadDomainSkills() {
    this.pTabs.select('domainSkill');
  }

  //Go to General Aptitude
  loadGeneralAptitude() {
    this.pTabs.select('generalAptitude');
  }

  //Go to Summary
  loadSummary() {
    this.getInterviewDetailByResultId();
    this.pTabs.select('summary');
  }


  //Get All Competancy List
  public allCompetacyList: any = [];
  getAllCompetancyList() {
    this.freelancerService.getAllCompetencies().subscribe((res: any) => {
      if (res) {
        this.allCompetacyList = res.content;
        console.log(this.allCompetacyList);
      }
    }, err => {

    });
  }

  // Get all Skill by Competancy id
  public skillList: any[] = [];
  public domainskillList: any[] = [];
  public gaList: any[] = [];
  public SummaryList: any[] = [];
  getAllSkillListByCompetency(index, type, id) {
    if (id) {
      this.freelancerService.getSkillByCompetecy(id).subscribe((res: any) => {
        if (res) {
          // console.log(res);
          if (type == 'tech') {
            this.skillList[index] = res;
          }

          if (type == 'domain') {
            this.domainskillList[index] = res;
          }

          if (type == 'ga') {
            this.gaList[index] = res;
          }

          if (type == 'summary') {
            this.SummaryList[index] = res;
          }

        }
      }, err => {
      });
    }
    console.log(this.domainskillList);
  }

  onchagecompetance(args, index, type) {
    if (type == 'tech') {
      var text = args.target.options[args.target.options.selectedIndex].text;
      this.techSkill.at(index).get('competencyname').patchValue(text);
    }

    if (type == 'domain') {
      var text = args.target.options[args.target.options.selectedIndex].text;
      this.domainSkill.at(index).get('competencyname').patchValue(text);
    }

    if (type == 'ga') {
      var text = args.target.options[args.target.options.selectedIndex].text;
      this.generalAptitude.at(index).get('competencyname').patchValue(text);
    }

  }

  changeskill(args, index, type) {
    if (type == 'tech') {
      var text = args.target.options[args.target.options.selectedIndex].text;
      this.techSkill.at(index).get('skillname').patchValue(text);
    }

    if (type == 'domain') {
      var text = args.target.options[args.target.options.selectedIndex].text;
      this.domainSkill.at(index).get('skillname').patchValue(text);
    }

    if (type == 'ga') {
      var text = args.target.options[args.target.options.selectedIndex].text;
      this.generalAptitude.at(index).get('skillname').patchValue(text);
    }

  }



  // Get Current Industry
  public currentIndustry: any = [];
  getCurrentIndustry() {
    this.freelancerService.getCurrentIndustry().subscribe((res: any) => {
      if (res) {
        this.currentIndustry = res.content;
        console.log(this.currentIndustry);
      }
    }, err => {

    });
  }

  //Get JobRole
  public targetJobList = [];
  onchange_currentindustry(industry) {
    this.freelancerService.getJobrolebyIndustry(industry).subscribe((res: any) => {
      this.targetJobList = res;
    }, err => {

    });
  }


  //Change Tech Skill complexity
  change_complexity_of_tech_skill(techIndex, questionIndex, value) {
    if (value == 'simple') {
      this.getTechSkillAskedQuestion(techIndex).at(questionIndex).get('weightage').patchValue(1);
    }
    if (value == 'average') {
      this.getTechSkillAskedQuestion(techIndex).at(questionIndex).get('weightage').patchValue(2);
    }
    if (value == 'complex') {
      this.getTechSkillAskedQuestion(techIndex).at(questionIndex).get('weightage').patchValue(3);
    }
  }

  //Change Tech Skill marks
  change_marks_of_tech_skill(techIndex, questionIndex, value) {
    var wtage = this.getTechSkillAskedQuestion(techIndex).at(questionIndex).get('weightage').value;
    var wtscr = value * wtage;
    this.getTechSkillAskedQuestion(techIndex).at(questionIndex).get('weightage_score').patchValue(wtscr);
  }


  //Change Domain Skill complexity
  change_complexity_of_domain_skill(dmIndex, questionIndex, value) {
    if (value == 'simple') {
      this.getDomainSkillAskedQuestion(dmIndex).at(questionIndex).get('weightage').patchValue(1);
    }
    if (value == 'average') {
      this.getDomainSkillAskedQuestion(dmIndex).at(questionIndex).get('weightage').patchValue(2);
    }
    if (value == 'complex') {
      this.getDomainSkillAskedQuestion(dmIndex).at(questionIndex).get('weightage').patchValue(3);
    }
  }

  //Change Domain Skill marks
  change_marks_of_domain_skill(dmIndex, questionIndex, value) {
    var wtage = this.getDomainSkillAskedQuestion(dmIndex).at(questionIndex).get('weightage').value;
    var wtscr = value * wtage;
    this.getDomainSkillAskedQuestion(dmIndex).at(questionIndex).get('weightage_score').patchValue(wtscr);
  }

  //Change Ga complexity
  change_complexity_of_ga(gaIndex, questionIndex, value) {
    if (value == 'simple') {
      this.getGeneralAptitudeAskedQuestion(gaIndex).at(questionIndex).get('weightage').patchValue(1);
    }
    if (value == 'average') {
      this.getGeneralAptitudeAskedQuestion(gaIndex).at(questionIndex).get('weightage').patchValue(2);
    }
    if (value == 'complex') {
      this.getGeneralAptitudeAskedQuestion(gaIndex).at(questionIndex).get('weightage').patchValue(3);
    }
  }

  //Change Ga marks
  change_marks_of_ga(gaIndex, questionIndex, value) {
    var wtage = this.getGeneralAptitudeAskedQuestion(gaIndex).at(questionIndex).get('weightage').value;
    var wtscr = value * wtage;
    this.getGeneralAptitudeAskedQuestion(gaIndex).at(questionIndex).get('weightage_score').patchValue(wtscr);
  }

  public interResultSummary: any;
  getInterviewSummaryByIterviewId(id) {

    this.freelancerService.getInterviewSummaryByInterviewId(id).subscribe((res: any) => {
      console.log(res);
      if (res) {
        this.interResultSummary = res;
        console.log(res);
        return false;
        this.techskillForm.get('industrytype').patchValue(res.industrytype);
        this.onchange_currentindustry(res.industrytype);
        this.techskillForm.get('target_job').patchValue(res.targetjob);
        this.techskillForm.get('resultid').patchValue(res.resultid);
        this.techskillForm.get('interview_status').patchValue(res.interview_status);
        this.freelancerService.getIntervieDetailsById(res.resultid).subscribe((res1: any) => {
          if (res1) {

            //Create Feedback Summary

            res1.forEach((element, index) => {
              this.addSummary(element);
              this.getAllSkillListByCompetency(index, 'summary', element.competencyid);
            });

            //Get Tech Skill All Question
            this.freelancerService.getTechskillAllQuestionById(res.resultid).subscribe((res2: any) => {
              if (res2) {

                var ti1 = 0;

                res1.forEach((element1, index1) => {
                  if (element1.questioncategory == 'Tech') {
                    this.getAllSkillListByCompetency(ti1, 'tech', element1.competencyid);
                    this.addTechSkill(element1);
                    var competencyid = this.techSkill.at(ti1).get('competency_name').value;
                    var skillid = this.techSkill.at(ti1).get('skill_name').value;

                    res2.forEach((element, index) => {

                      if ((competencyid == element.competencyid) && (skillid == element.skillid) && (element.questioncategory == 'Tech')) {
                        this.addTechSkillAskedQuestion(ti1, element);
                      }
                    });
                    ti1 = ti1 + 1;
                  }
                });
              }
            }, err => {
              console.log(err);
            });



            //Get Domain Skill All Question
            this.freelancerService.getDomainskillAllQuestionById(res.resultid).subscribe((res2: any) => {
              if (res2) {
                var di1 = 0;
                res1.forEach((Domelement1, Domindex1) => {
                  if (Domelement1.questioncategory == 'Domain') {
                    this.getAllSkillListByCompetency(di1, 'domain', Domelement1.competencyid);
                    this.addDomainSkill(Domelement1);
                    var competencyid = this.domainSkill.at(di1).get('competency_name').value;
                    var skillid = this.domainSkill.at(di1).get('skill_name').value;
                    res2.forEach((Domelement, Domindex) => {
                      console.log(Domelement);
                      if ((competencyid == Domelement.competencyid) && (skillid == Domelement.skillid) && (Domelement.questioncategory == 'Domain')) {
                        this.addDomainSkillAskedQuestion(di1, Domelement);
                      }
                    });
                    di1 = di1 + 1;
                  }
                });
              }
            }, err => {
              console.log(err);
            });


            //Get General Aptitude All Question
            this.freelancerService.getGeneralAptitudeskillAllQuestionById(res.resultid).subscribe((res2: any) => {
              if (res2) {
                var di1 = 0;
                res1.forEach((Domelement1, Domindex1) => {
                  if (Domelement1.questioncategory == 'Ga') {
                    this.getAllSkillListByCompetency(di1, 'ga', Domelement1.competencyid);
                    this.addGeneralAptitude(Domelement1);
                    var competencyid = this.generalAptitude.at(di1).get('competency_name').value;
                    var skillid = this.generalAptitude.at(di1).get('skill_name').value;
                    res2.forEach((Domelement, Domindex) => {
                      console.log(Domelement);
                      if ((competencyid == Domelement.competencyid) && (skillid == Domelement.skillid) && (Domelement.questioncategory == 'Ga')) {
                        this.addGeneralAptitudeAskedQuestion(di1, Domelement);
                      }
                    });
                    di1 = di1 + 1;
                  }
                });
              }
            }, err => {
              console.log(err);
            });




          }

        }, err => {
          console.log(err);
        });

      }
    }, err => {
      console.log(err);
    });
  }

  getInterviewDetailByResultId() {
    var resultid = this.techskillForm.get('resultid').value;
    this.freelancerService.getIntervieDetailsById(resultid).subscribe((res1: any) => {
      if (res1) {

        //Create Feedback Summary
        this.createSummaryForm();
        res1.forEach((element, index) => {
          this.addSummary(element);
          this.getAllSkillListByCompetency(index, 'summary', element.competencyid);
        });
      }
    });

  }


}
