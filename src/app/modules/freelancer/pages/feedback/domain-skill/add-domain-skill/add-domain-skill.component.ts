import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { FreelancerService } from 'src/app/modules/freelancer/service/freelancer.service';
import { SpinnerService } from 'src/app/modules/freelancer/service/spinner.service';
import { AddDomainQuestionComponent } from './add-domain-question/add-domain-question.component';


@Component({
  selector: 'app-add-domain-skill',
  templateUrl: './add-domain-skill.component.html',
  styleUrls: ['./add-domain-skill.component.scss']
})
export class AddDomainSkillComponent implements OnInit {

  @Input() resultid;
  @Input() details;//when come pro update mode
  public competency_id = 0;
  public competencyname: '';
  public skill_id = 0;
  public skillname = '';
  public detailid = 0;
  public allQuestionOfDetailId: any = []
  constructor(private activeModal: NgbActiveModal,
    private modal: NgbModal,
    private freelancerService: FreelancerService,
    private toastr: ToastrService,
    private spineer: SpinnerService) { }




  ngOnInit() {
    this.getAllCompetancyList();
    if (this.details != '') {
      this.detailid = this.details.resultdetailid;
      this.competency_id = this.details.competencyid;
      this.skill_id = this.details.skillid;
      this.competencyname = this.details.competencyname;
      this.skillname = this.details.skillname;
      this.getAllSkillListByCompetency(this.details.competencyid, '');
      this.on_change_skill(this.details.skillid, '');
    }
  }


  //Get All Competancy List
  public allCompetacyList: any = [];
  getAllCompetancyList() {
    this.freelancerService.getAllCompetencies().subscribe((res: any) => {
      if (res) {
        this.allCompetacyList = res.content;

      }
    }, err => {

    });
  }

  public skillList: any = [];
  getAllSkillListByCompetency(id, event) {
    if(event != ''){
      this.competencyname = event.target.options[event.target.options.selectedIndex].text;
    }
    if (id) {
      this.freelancerService.getSkillByCompetecy(id).subscribe((res: any) => {
        if (res) {
          this.skillList = res;
        }
      }, err => {

      });
    }
  }

  close() {
    this.activeModal.close(1);
  }



  // Add teck Question

  addTechQuestion(val?) {
    if (!val && !this.competency_id || !this.skill_id) {
      this.toastr.info("Please select Competency and skill name");
      return false;
    }
    const modal = this.modal.open(AddDomainQuestionComponent, {
      size: 'md',
      backdrop: "static",
    });
    if (val) {
      this.detailid = val.detailid;
      this.competency_id = val.competencyid;
      this.skill_id = val.skillid;
    }
    modal.componentInstance.competency_id = this.competency_id;
    modal.componentInstance.skill_id = this.skill_id;
    modal.componentInstance.resultid = this.resultid;
    modal.componentInstance.detailid = this.detailid ? this.detailid : '';
    modal.componentInstance.question = val ? val : '';
    modal.componentInstance.competencyname = this.competencyname;
    modal.componentInstance.skillname = this.skillname;

    modal.result.then(res => {
      debugger;
      if (res.qnaid) {
        this.detailid = res.detailid;
        var i = this.allQuestionOfDetailId.findIndex(x => x.qnaid == res.qnaid);
        if (i >= 0) {
          this.allQuestionOfDetailId[i] = res;
          this.toastr.success('Updated successfully');
        } else {
          this.detailid = res.detailid;
          this.allQuestionOfDetailId.push(res);
          this.toastr.success('Added successfully');
        }

      }
    }, dismiss => {
      debugger
    });
  }

  update(val) {
    this.addTechQuestion(val);
  }

  delete(id) {
    this.spineer.showSpinner();
    this.freelancerService.deleteInterviewQnA(id).subscribe((res: any) => {
      this.spineer.hideSpinner();

      var i = this.allQuestionOfDetailId.findIndex(x => x.qnaid == id);
      this.allQuestionOfDetailId.splice(i, 1);
      this.toastr.success('Deleted successfully');

    }, err => {
      this.spineer.hideSpinner();
      this.toastr.error('Error');
      console.log(err);
    });
  }


  public disable_add_question_btn: boolean = true;
  on_change_skill(val, event) {
    if(event != ''){
      this.skillname = event.target.options[event.target.options.selectedIndex].text;
    }
    
    if (this.resultid && this.competency_id && this.skill_id) {
      let d = {
        competencyid: this.competency_id,
        resultid: this.resultid,
        skillid: this.skill_id,
        questioncategory: 'Domain'
      }
      this.freelancerService.validateiterviewdetail(d).subscribe((res: any) => {
        if (res) {
          this.detailid = res.resultdetailid;
          this.disable_add_question_btn = false;
          this.get_all_questions_by_details_id(res.competencyid, res.resultdetailid, res.skillid)
        } else {
          this.disable_add_question_btn = false;
        }
      }, err => {
        console.log(err);
      });
    }
  }




  get_all_questions_by_details_id(competencyid, detailid, skillid) {

    let d = {
      "competencyid": competencyid,
      "resultdetailid": detailid,
      "skillid": skillid
    }
    console.log(d);
    this.spineer.showSpinner();
    this.freelancerService.getInterviewDetailbySkillnCompetency(d).subscribe((res: any) => {
      this.spineer.hideSpinner();
      console.log(res);
      if (res) {
        this.allQuestionOfDetailId = res.questions;
      }

    }, err => {
      this.spineer.hideSpinner();
      console.log(err);

    });
  }

}
