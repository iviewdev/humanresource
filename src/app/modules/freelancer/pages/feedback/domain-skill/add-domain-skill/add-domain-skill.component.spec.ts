import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDomainSkillComponent } from './add-domain-skill.component';

describe('AddDomainSkillComponent', () => {
  let component: AddDomainSkillComponent;
  let fixture: ComponentFixture<AddDomainSkillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDomainSkillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDomainSkillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
