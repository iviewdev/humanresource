import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDomainQuestionComponent } from './add-domain-question.component';

describe('AddDomainQuestionComponent', () => {
  let component: AddDomainQuestionComponent;
  let fixture: ComponentFixture<AddDomainQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDomainQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDomainQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
