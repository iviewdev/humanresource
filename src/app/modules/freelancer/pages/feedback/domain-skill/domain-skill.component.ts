import { Component, Input, OnInit, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ThemeService } from 'ng2-charts';
import { FreelancerService } from '../../../service/freelancer.service';
import { SpinnerService } from '../../../service/spinner.service';
import { AddInterviewSummaryComponent } from '../add-interview-summary/add-interview-summary.component';
import { AddDomainSkillComponent } from './add-domain-skill/add-domain-skill.component';

@Component({
  selector: 'app-domain-skill',
  templateUrl: './domain-skill.component.html',
  styleUrls: ['./domain-skill.component.scss']
})
export class DomainSkillComponent implements OnInit {

  @Input() interResultSummary: any;
  @Input() interViewDetails: any;
  public resultid: number = 0;
  constructor(private modal: NgbModal,
    private freelancerService: FreelancerService,
    private spinner: SpinnerService) { }

  ngOnChanges(changes: SimpleChanges) {
    const interViewResultSummary: SimpleChange = changes.interResultSummary;
    this.resultid = interViewResultSummary.currentValue.resultid;
    if (this.resultid) {
      this.getInterviewDetails(this.resultid);
    }
  }

  ngOnInit() {
   
    console.log(this.interViewDetails);
  }

  public domain_details_list: any = [];
  getInterviewDetails(id) {
    let d = {
      "resultid": id,
      "questioncategory": "Domain"
    }
    this.freelancerService.getinterviewdetails(d).subscribe((res: any) => {
      console.log(res);
      this.domain_details_list = res;
    }, err => {
      console.log(err);
    });
  }

  public questinons: any = [];
  public loading: boolean = false;
  get_all_questions_by_details_id(dl) {

    this.loading = true;
    this.questinons = [];

    let d = {
      "competencyid": dl.competencyid,
      "resultdetailid": dl.resultdetailid,
      "skillid": dl.skillid
    }

    this.freelancerService.getInterviewDetailbySkillnCompetency(d).subscribe((res: any) => {
      if (res) {
        this.questinons = res.questions;
      }
      this.loading = false;
    }, err => {
      console.log(err);
      this.loading = false;
    });
  }

  // Add Interview Summary
  addInterviewSummary(val?) {
    const modal = this.modal.open(AddInterviewSummaryComponent, {
      size: 'sm',
      backdrop: 'static'
    });
    modal.componentInstance.interviewSummary = val;

    modal.result.then(res => {
      if (res) {
        this.spinner.showSpinner();
        let d = {
          resultid: this.interResultSummary ? this.interResultSummary.resultid : '',
          scheduleid: this.interViewDetails.scheduleid,
          interviewid: this.interViewDetails.interviewid,
          interviewerid: this.interViewDetails.freelancerid,
          interview_status: "",
          ai_status: "",
          targetjob: res.target_job,
          industrytype: res.industrytype,
          jobseekerid: this.interViewDetails.jobseekerid,
          overallComments: "None"
        }

        this.freelancerService.saveInterviewResultSummary(d).subscribe((res: any) => {
          this.spinner.hideSpinner()
          if (res) {
            this.interResultSummary = res;
          }
        }, err => {
          this.spinner.hideSpinner()
          console.log(err);
        })
      }
    }, err => {

    });

  }

  // Add Domain Skill
  addDomainSkill(val?) {
    if (this.interResultSummary) {
      const modal = this.modal.open(AddDomainSkillComponent, {
        size: 'lg',
        backdrop: 'static',
        windowClass: 'Domain-skill'
      });
      modal.componentInstance.resultid = this.interResultSummary.resultid;
      modal.componentInstance.details = val ? val : '';
      modal.result.then(res => {
        this.getInterviewDetails(this.resultid);
      }, dismiss => {

      });
    } else {
      this.addInterviewSummary();
    }
  }


  deleteDetails(id) {
    debugger;
    this.spinner.showSpinner();
    this.freelancerService.deleteInterviewDetail(id).subscribe((res: any) => {
      this.spinner.hideSpinner();
      var i = this.domain_details_list.findIndex(x => x.resultdetailid == id);
      this.domain_details_list.splice(i, 1);

    }, err => {
      this.spinner.hideSpinner();
      console.log(err);
    })
  }


}
