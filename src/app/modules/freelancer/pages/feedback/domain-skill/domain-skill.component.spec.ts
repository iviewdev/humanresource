import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomainSkillComponent } from './domain-skill.component';

describe('DomainSkillComponent', () => {
  let component: DomainSkillComponent;
  let fixture: ComponentFixture<DomainSkillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomainSkillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomainSkillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
