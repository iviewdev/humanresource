import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFeedbackSummaryComponent } from './update-feedback-summary.component';

describe('UpdateFeedbackSummaryComponent', () => {
  let component: UpdateFeedbackSummaryComponent;
  let fixture: ComponentFixture<UpdateFeedbackSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFeedbackSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFeedbackSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
