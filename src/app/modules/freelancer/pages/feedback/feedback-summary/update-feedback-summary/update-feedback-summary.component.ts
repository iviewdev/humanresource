import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { FreelancerService } from 'src/app/modules/freelancer/service/freelancer.service';
import { SpinnerService } from 'src/app/modules/freelancer/service/spinner.service';

@Component({
  selector: 'app-update-feedback-summary',
  templateUrl: './update-feedback-summary.component.html',
  styleUrls: ['./update-feedback-summary.component.scss']
})
export class UpdateFeedbackSummaryComponent implements OnInit {
  @Input() details: any;
  public summaryForm: FormGroup;
  public submitted: boolean = false;
  constructor(private freelancerService: FreelancerService,
    private fb: FormBuilder,
    private spinner: SpinnerService,
    private toastr: ToastrService,
    private modal: NgbActiveModal) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.summaryForm = this.fb.group({
      strengthweakness: ['', [Validators.required]],
      improvementguidance: ['', [Validators.required]]
    });
  }

  get f() {
    return this.summaryForm.controls;
  }

  submit() {
    this.submitted = true;
    if (this.summaryForm.invalid) {
      return;
    }
    this.spinner.showSpinner();
   
    this.details.strengthweakness = this.summaryForm.get('strengthweakness').value;
        this.details.improvementguidance = this.summaryForm.get('improvementguidance').value;
        this.details.status = 'publish';

    this.freelancerService.saveInterviewResultDetail(this.details).subscribe((res: any) => {
      this.spinner.hideSpinner();
      if (res) {
        this.toastr.success('Updated successfully');
        
        this.close(this.details);
      }
    }, err => {
      this.spinner.hideSpinner();
      console.log(err);
    });
  }

  close(val?) {
    this.modal.close(val);
  }

}
