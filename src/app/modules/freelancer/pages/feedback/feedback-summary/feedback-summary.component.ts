import { Component, Input, OnInit, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FreelancerService } from '../../../service/freelancer.service';
import { UpdateFeedbackSummaryComponent } from './update-feedback-summary/update-feedback-summary.component';

@Component({
  selector: 'app-feedback-summary',
  templateUrl: './feedback-summary.component.html',
  styleUrls: ['./feedback-summary.component.scss']
})
export class FeedbackSummaryComponent implements OnInit, OnChanges {
  @Input() interResultSummary: any;
  @Input() interViewDetails: any;
  public resultid: number = 0;
  constructor(private freelancerService: FreelancerService,
    private modal: NgbModal) { }

  ngOnChanges(changes: SimpleChanges) {
    const interViewResultSummary: SimpleChange = changes.interResultSummary;
    this.resultid = interViewResultSummary.currentValue.resultid;
    if (this.resultid) {
      this.getInterviewDetails(this.resultid);
    }
  }

  ngOnInit() {
  }

  public all_skill_details: any = [];
  getInterviewDetails(id) {
    let d = {
      "resultid": id,
      "questioncategory": null
    }
    this.freelancerService.getinterviewdetails(d).subscribe((res: any) => {
      this.all_skill_details = res;
    }, err => {
      console.log(err);
    });
  }


  filter_skill_details_by_category(category_value) {
    return this.all_skill_details.filter(x => x.questioncategory === category_value);
  }

  get_category_wise_length(category_value) {
    var skillDetails = this.all_skill_details.filter(x => x.questioncategory === category_value);
    return skillDetails.length;
  }

  update(val) {
    const modal = this.modal.open(UpdateFeedbackSummaryComponent, {
      size: 'md',
      backdrop: 'static'
    });
    modal.componentInstance.details = val;
    modal.result.then(res => {
      if (res) {
        var i = this.all_skill_details.findIndex(x => x.resultdetailid == res.resultdetailid);
        if (i >= 0) {
          this.all_skill_details[i] = res;
        }
      }
    }, dismiss => {

    })
  }

}
