import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { HttpClientModule } from '@angular/common/http';
import { FormWizardModule } from 'angular2-wizard';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

//import { DragulaModule } from 'ng2-dragula';
//import { ClipboardModule } from 'ngx-clipboard';
//import { ContextMenuModule } from 'ngx-contextmenu';
//import { NouisliderModule } from 'ng2-nouislider';
//import { CarouselModule } from 'ngx-owl-carousel-o';
//import { DropzoneModule } from 'ngx-dropzone-wrapper';
//import { ColorPickerModule } from 'ngx-color-picker';
//import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
//import { AmazingTimePickerModule } from 'amazing-time-picker';
//import { TagInputModule } from 'ngx-chips';
//import { NgxSummernoteModule } from 'ngx-summernote';
//import { TinymceModule } from 'angular2-tinymce';
//import { AceEditorModule } from 'ng2-ace-editor';
//import { CodemirrorModule } from 'ng2-codemirror';
//import { ChartsModule } from 'ng2-charts';
//import { CustomFormsModule } from 'ng2-validation';
//import { Select2Module } from 'ng2-select2';
//import { MorrisJsModule } from 'angular-morris-js';
//import { ChartistModule } from 'ng-chartist';
//import { DataTablesModule } from 'angular-datatables';
//import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
//import { ToastrModule } from 'ngx-toastr';
//import { AgmCoreModule } from '@agm/core';
//import { BarRatingModule } from 'ngx-bar-rating';
//import { FullCalendarModule } from 'ng-fullcalendar';
//import { ScrollToModule } from 'ng2-scroll-to-el';

//import { SidebarComponent } from 'src/app/shared/sidebar/sidebar.component';
import { AdminRoutingModule } from './admin-routing.module';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LayoutComponent } from './components/layout/layout.component';
import { NavbarComponent } from 'src/app/shared/navbar/navbar.component';
import { FooterComponent } from 'src/app/shared/footer/footer.component';
import { ApprovalComponent } from './pages/approval/approval.component';
import { ApplicationDetailComponent } from './pages/application-detail/application-detail.component';
import { AdminSidebarComponent } from './components/admin-sidebar/admin-sidebar.component';
import { AddCompetencyComponent } from './pages/add-competency/add-competency.component';
import { AddSkillComponent } from './pages/add-skill/add-skill.component';
import { AddJobroleComponent } from './pages/add-jobrole/add-jobrole.component';
import { AddCountryComponent } from './pages/add-country/add-country.component';
import { AddStateComponent } from './pages/add-state/add-state.component';
import { AddDistrictComponent } from './pages/add-district/add-district.component';
import { AddCityComponent } from './pages/add-city/add-city.component';
import { LoginComponent } from './account/pages/login/login.component';
import { AccountLayoutComponent } from './account/components/account-layout/account-layout.component';
import { FreelancerPaymentComponent } from './pages/freelancer-payment/freelancer-payment.component';
import { ViewFeedbackComponent } from './pages/view-feedback/view-feedback.component';


@NgModule({
  declarations: [DashboardComponent,
    LayoutComponent,
    NavbarComponent,
    ApprovalComponent,
    ApplicationDetailComponent,
    AdminSidebarComponent,
    FooterComponent,
    AddCompetencyComponent,
    AddSkillComponent,
    AddJobroleComponent,
    AddCountryComponent,
    AddStateComponent,
    AddDistrictComponent,
    AddCityComponent,
    LoginComponent,
    AccountLayoutComponent,
    FreelancerPaymentComponent,
    ViewFeedbackComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    HttpClientModule,
    NgbModule,
    FormWizardModule,
    FormsModule,
    ReactiveFormsModule,
    AutocompleteLibModule

    // DragulaModule.forRoot(),
    // ClipboardModule,
    // ContextMenuModule.forRoot(),
    // NouisliderModule,
    // CarouselModule,
    // DropzoneModule,
    // ColorPickerModule,
    // AngularDateTimePickerModule,
    // AmazingTimePickerModule,
    // TagInputModule,
    // NgxSummernoteModule,
    // TinymceModule.withConfig({}),
    // AceEditorModule,
    // CodemirrorModule,
    // ChartsModule,
    // CustomFormsModule,
    // Select2Module,
    // MorrisJsModule,
    // ChartistModule,
    // DataTablesModule,
    // SweetAlert2Module.forRoot(),
    // ToastrModule.forRoot(),
    // AgmCoreModule.forRoot({ apiKey: 'AIzaSyCnT63XUjqjPgXZ0lFTU_pdpfUX7swzTTM' }),
    // BarRatingModule,
    // FullCalendarModule,
    // ScrollToModule.forRoot(),
  ]
})
export class AdminModule { }
