import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LayoutComponent } from './components/layout/layout.component';
import { ApprovalComponent } from './pages/approval/approval.component';
import { ApplicationDetailComponent } from './pages/application-detail/application-detail.component';
import { AddCompetencyComponent } from './pages/add-competency/add-competency.component';
import { AddSkillComponent } from './pages/add-skill/add-skill.component';
import { AddJobroleComponent } from './pages/add-jobrole/add-jobrole.component';
import { AddCountryComponent } from './pages/add-country/add-country.component';
import { AddStateComponent } from './pages/add-state/add-state.component';
import { AddDistrictComponent } from './pages/add-district/add-district.component';
import { AddCityComponent } from './pages/add-city/add-city.component';
import { AuthGuard } from './account/auth/auth.guard';
import { LoginComponent } from './account/pages/login/login.component';
import { AccountLayoutComponent } from './account/components/account-layout/account-layout.component';
import { FreelancerPaymentComponent } from './pages/freelancer-payment/freelancer-payment.component';
import { ViewFeedbackComponent } from './pages/view-feedback/view-feedback.component';


const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'approval',
        component: ApprovalComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'applicationDetail/:id',
        component: ApplicationDetailComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'addCompetency',
        component: AddCompetencyComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'addSkill',
        component: AddSkillComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'addJobrole',
        component: AddJobroleComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'addCountry',
        component: AddCountryComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'addState',
        component: AddStateComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'addDistrict',
        component: AddDistrictComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'addCity',
        component: AddCityComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'freelancer-payments',
        component: FreelancerPaymentComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'view-feedback',
        component: ViewFeedbackComponent,
        canActivate: [AuthGuard]
      },
    ]
  },
  {
    path: 'account',
    component: AccountLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent
      }
    ]

  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
