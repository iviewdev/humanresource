import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { StorageService } from 'src/app/shared/services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private storageService: StorageService,
    private router: Router) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    var url = state.url.split('/');
    if (this.storageService.getItem('token') != null) {
      var role = this.storageService.getItem('role');
      if (url[1] == role.toLowerCase()) {
        return true;
      } else {
        this.router.navigate(['/' + role.toLowerCase()]);
        return false;
      }
    } else {
      this.router.navigate(['admin/account/login']);
      return false;
    }
  }

}
