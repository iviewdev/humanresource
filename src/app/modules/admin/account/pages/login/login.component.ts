import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from 'src/app/modules/account/service/account.service';
import { StorageService } from 'src/app/shared/services/storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loginStart = false;
  submitted = false;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private accountService: AccountService,
    private storageService: StorageService,
    private fb: FormBuilder,
  ) {

    this.loginForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
    if (this.storageService.getItem('token') != null) {
      this.router.navigateByUrl('account/login');
    }
  }
  get f() {
    return this.loginForm.controls;
  }

  //***********************Login Here**************************/

  login() {
    debugger;
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.loginStart = true;

    this.accountService.login(this.loginForm.value).subscribe((res: any) => {
      this.loginStart = false;
      //console.log(res);
      if (res) {
        debugger;
        this.storageService.setItem("token", res.jwttoken);
        this.storageService.setItem('username', res.username);
        this.storageService.setItem('id', res.userid);
        this.storageService.setItem('mobile', res.mobile);
        this.storageService.setItem('role', res.role);

        if (res.role.toLowerCase() == 'admin') {
          this.router.navigateByUrl("admin");
        }

      }
      else {
        this.toastr.error('Invalid user name or password');
      }

    }, (err) => {
      this.loginStart = false;
      if (err.error.status == 401) {
        this.toastr.error('Invalid username or password');
      } else {
        this.toastr.error(err.error.message);
      }

    });
  }


}
