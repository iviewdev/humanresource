import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from '../../service/admin.service';

@Component({
  selector: 'app-add-district',
  templateUrl: './add-district.component.html',
  styleUrls: ['./add-district.component.scss']
})
export class AddDistrictComponent implements OnInit {
  districtForm: FormGroup;
  dfsubmitted = false;
  countries: any[];
  states: any[];

  constructor(
    private toastr: ToastrService,
    private adminService: AdminService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.fillCountries();
    this.createDistrictForm();
  }

  fillCountries() {
    this.adminService.GetAllCountries().subscribe((res: any) => {
      this.countries = res.content;
      this.countries.sort((a, b) => a.countryname.localeCompare(b.countryname));
    }, (err) => {
      this.toastr.error(err.error.massege);
    });
  }

  onCountryChange(selectedCountry) {
    this.adminService.GetAllStatesForCountry(selectedCountry).subscribe((res: any) => {
      this.states = res;
      this.states.sort((a, b) => a.statename.localeCompare(b.statename));
    }, (err) => {
      this.toastr.error(err.error.massege);
    });
  }

  get df() { return this.districtForm.controls; }
  createDistrictForm() {
    this.districtForm = this.fb.group({
      countrycode: ['', Validators.required],
      statecode: ['', Validators.required],
      districtcode: ['', Validators.required],
      districtname: ['', Validators.required]
    });

  }

  createDistrict() {
    debugger;
    this.dfsubmitted = true;
    if (this.districtForm.invalid) {
      return;
    }

    const district = this.districtForm.value;
    const districtObj = {
      country_code: district.countrycode,
      state_code: district.statecode,
      district_code: district.districtcode,
      district_name: district.districtname,
    };

    this.adminService.CreateDistrict(districtObj).subscribe(res => {
      this.toastr.success('District saved successfully');
    }, (err) => {
      this.toastr.error(err.error.message);
    });

  }


}