import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from '../../service/admin.service';

@Component({
  selector: 'app-add-country',
  templateUrl: './add-country.component.html',
  styleUrls: ['./add-country.component.scss']
})
export class AddCountryComponent implements OnInit {
  countryForm: FormGroup;
  cfsubmitted = false;

  constructor(
    private toastr: ToastrService,
    private adminService: AdminService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createCountryForm();
  }

  get cf() { return this.countryForm.controls; }
  createCountryForm() {
    this.countryForm = this.fb.group({
      countryid: ['', Validators.required],
      countrycode: ['', Validators.required],
      countryname: ['', Validators.required]
    });

  }

  createCountry() {
    //debugger;
    this.cfsubmitted = true;
    if (this.countryForm.invalid) {
      return;
    }

    const country = this.countryForm.value;
    const countryObj = {
      id: country.countryid,
      country_code: country.countrycode,
      country_name: country.countryname,
    };

    this.adminService.CreateCountry(countryObj).subscribe(res => {
      this.toastr.success('Country saved successfully');
    }, (err) => {
      this.toastr.error(err.error.message);
    });

  }

}