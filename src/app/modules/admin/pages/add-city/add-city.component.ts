import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from '../../service/admin.service';

@Component({
  selector: 'app-add-city',
  templateUrl: './add-city.component.html',
  styleUrls: ['./add-city.component.scss']
})
export class AddCityComponent implements OnInit {
  cityForm: FormGroup;
  cfsubmitted = false;
  countries: any[];
  states: any[];
  districts: any[];

  constructor(
    private toastr: ToastrService,
    private adminService: AdminService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.fillCountries();
    this.createCityForm();
  }

  fillCountries() {
    this.adminService.GetAllCountries().subscribe((res: any) => {
      this.countries = res.content;
      this.countries.sort((a, b) => a.countryname.localeCompare(b.countryname));
    }, (err) => {
      this.toastr.error(err.error.massege);
    });
  }

  onCountryChange(selectedCountry) {
    this.adminService.GetAllStatesForCountry(selectedCountry).subscribe((res: any) => {
      this.states = res;
      this.states.sort((a, b) => a.statename.localeCompare(b.statename));
    }, (err) => {
      this.toastr.error(err.error.massege);
    });
  }

  onSateChange(selectedSate) {
    this.adminService.GetAllDistrictsForState(selectedSate).subscribe((res: any) => {
      this.districts = res;
      this.districts.sort((a, b) => a.districtname.localeCompare(b.districtname));
    }, (err) => {
      this.toastr.error(err.error.massege);
    });
  }

  get cf() { return this.cityForm.controls; }
  createCityForm() {
    this.cityForm = this.fb.group({
      countrycode: ['', Validators.required],
      statecode: ['', Validators.required],
      districtcode: ['', Validators.required],
      citycode: ['', Validators.required],
      cityname: ['', Validators.required]
    });

  }

  createCity() {
    debugger;
    this.cfsubmitted = true;
    if (this.cityForm.invalid) {
      return;
    }

    const city = this.cityForm.value;
    const cityObj = {
      country_code: city.countrycode,
      state_code: city.statecode,
      district_code: city.districtcode,
      city_code: city.citycode,
      city_name: city.cityname,
    };

    this.adminService.CreateCity(cityObj).subscribe(res => {
      this.toastr.success('City saved successfully');
    }, (err) => {
      this.toastr.error(err.error.message);
    });

  }


}