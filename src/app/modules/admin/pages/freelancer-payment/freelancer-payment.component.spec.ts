import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreelancerPaymentComponent } from './freelancer-payment.component';

describe('FreelancerPaymentComponent', () => {
  let component: FreelancerPaymentComponent;
  let fixture: ComponentFixture<FreelancerPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreelancerPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreelancerPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
