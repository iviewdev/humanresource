import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../service/admin.service';

@Component({
  selector: 'app-freelancer-payment',
  templateUrl: './freelancer-payment.component.html',
  styleUrls: ['./freelancer-payment.component.scss']
})
export class FreelancerPaymentComponent implements OnInit {
  public pendingPayments: any = [];
  public approvedPayments: any = [];
  
  constructor(private adminService: AdminService) { }

  ngOnInit() {
    this.getPendingPayments();
    this.getApprovedPayments();
  }

  getPendingPayments() {
    this.adminService.getPendingPayments().subscribe((res: any) => {
       console.log(res);
      this.pendingPayments = res;
    }, err => {
      console.log(err);
    });
  }


  getApprovedPayments() {
    this.adminService.getApprovedPayments().subscribe((res: any) => {
       console.log(res);
      this.approvedPayments = res;
    }, err => {
      console.log(err);
    });
  }

}
