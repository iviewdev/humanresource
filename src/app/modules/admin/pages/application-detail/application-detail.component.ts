import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from '../../service/admin.service';

@Component({
  selector: 'app-application-detail',
  templateUrl: './application-detail.component.html',
  styleUrls: ['./application-detail.component.scss']
})
export class ApplicationDetailComponent implements OnInit {

  username: string;
  Documents: any[];
  EducationDocuments: any[];
  ExperienceDocuments: any[];
  MasterDocument: any;
  ValidateData: Boolean;

  constructor(private route: ActivatedRoute,
    private toastr: ToastrService,
    private adminService: AdminService,) {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.username = params.get('id');
    })
  }

  ngOnInit() {

    this.adminService.GetMasterDocuments(this.username).subscribe((res: any) => {
      if (res) {
        this.MasterDocument = res;
        console.log(this.MasterDocument);
      }
      else {

        this.toastr.error('error occured');
      }

    }, (err) => {
      this.toastr.error(err.error.message);

    });

    this.adminService.GetDocumentDetails(this.username).subscribe((res: any) => {
      if (res) {
        this.Documents = res;
        this.EducationDocuments = this.Documents.filter(t => t.cert_type === 'Education');
        this.ExperienceDocuments = this.Documents.filter(t => t.cert_type === 'Experience');
      }
      else {

        this.toastr.error('error occured');
      }

    }, (err) => {
      this.toastr.error(err.error.message);

    });
  }

  UpdateStatus(): void {
    debugger;
    this.ValidateData = true;

    // validate for reject reason of education & experience data
    for (let d of this.Documents) {
      if (d.cert_status === "Rejected" && this.StringNullorEmpty(d.reject_reason)) {
        this.ValidateData = false;
        break;
      }
    }

    // validate for reject reason of master data
    if (this.MasterDocument.idproof_status === "Rejected" && this.StringNullorEmpty(this.MasterDocument.idproof_reject_reason)) {
      this.ValidateData = false;
    }
    else if (this.MasterDocument.addproof_status === "Rejected" && this.StringNullorEmpty(this.MasterDocument.addroof_reject_reason)) {
      this.ValidateData = false;
    }
    else if (this.MasterDocument.taxproof_status === "Rejected" && this.StringNullorEmpty(this.MasterDocument.taxproof_reject_reason)) {
      this.ValidateData = false;
    }

    if (!this.ValidateData) {
      this.toastr.error("Please give reason for rejection.");
      return;
    }

    // call api to save education & experience data
    this.adminService.ApproveDetailDocuments(this.Documents).subscribe((res: any) => {
      if (res) {
        //this.toastr.success('Success');
        this.UpdateMasterDocuments();
      }
      else {
        this.toastr.error('error occured');
      }

    }, (err) => {
      this.toastr.error(err.error.message);
    });

  }

  UpdateMasterDocuments() {
    // call api to save master data
    this.adminService.ApproveMasterDocuments(this.MasterDocument).subscribe((res: any) => {
      if (res) {
        this.toastr.success('Application details saved successfully');
      }
      else {
        this.toastr.error('error occured');
      }

    }, (err) => {
      this.toastr.error(err.error.message);
    });

  }

  StringNullorEmpty(s: any): boolean {
    s = s ? s.replace(/^\s+|\s+$/gm, '') : '';
    return s == '';
  }

}
