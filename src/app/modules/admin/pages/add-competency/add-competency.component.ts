import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from '../../service/admin.service';

@Component({
  selector: 'app-add-competency',
  templateUrl: './add-competency.component.html',
  styleUrls: ['./add-competency.component.scss']
})
export class AddCompetencyComponent implements OnInit {
  competencyForm: FormGroup;
  cfsubmitted = false;

  constructor(
    private toastr: ToastrService,
    private adminService: AdminService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createCompetencyForm();
  }

  get cf() { return this.competencyForm.controls; }
  createCompetencyForm() {
    this.competencyForm = this.fb.group({
      competencyname: ['', Validators.required],
      competencydescription: ['', Validators.required]
    });

  }

  createCompetency() {
    //debugger;
    this.cfsubmitted = true;
    if (this.competencyForm.invalid) {
      return;
    }

    const competency = this.competencyForm.value;
    const competencyObj = {
      competency_name: competency.competencyname,
      competency_desc: competency.competencydescription,
    };

    this.adminService.CreateCompetency(competencyObj).subscribe(res => {
      this.toastr.success('Competency details saved successfully');
    }, (err) => {
      this.toastr.error(err.error.message);
    });

  }

}
