import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from '../../service/admin.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-view-feedback',
  templateUrl: './view-feedback.component.html',
  styleUrls: ['./view-feedback.component.scss']
})
export class ViewFeedbackComponent implements OnInit {
  public pendingPayments: any;
  public feedbackObj: any;
  public isLoading: boolean = false;
  constructor(private route: ActivatedRoute,
    private adminService: AdminService,
    private toastr:ToastrService,
    private location:Location) { }

  ngOnInit() {
    // this.interviewid = +this.route.snapshot.paramMap.get('interviewid');
    this.route.queryParams.subscribe(val => {
      console.log(val);
      this.pendingPayments = val;
    });
    this.getFeebackforInterview(this.pendingPayments.interviewid);
  }

  getFeebackforInterview(id) {
    this.adminService.getFeebackforInterview(id).subscribe((res: any) => {
      console.log(res);
      this.feedbackObj = res;
    }, err => {
      console.log(err);
    });
  }

  //#########################Approve Payments####################
  approve_payment(val) {
    if (val) {
      this.isLoading = true;
      let d = {
        "paymentid": this.pendingPayments.paymentid,
        "interviewscheduleid": this.pendingPayments.interviewscheduleid,
        "freelancerid": this.pendingPayments.freelancerid,
        "interviewid": this.pendingPayments.interviewid,
        "paymentstatus": "Payment Approved",
        "amountexpected": this.pendingPayments.amountexpected,
        "amountpaid": val,
        "currency": this.pendingPayments.currency,
        "holdreason": "None"
      }

      this.adminService.sendPaymentResponse(d).subscribe((res: any) => {
        this.isLoading = false;
        this.toastr.success('Payment approved sucessfully');
        this.location.back();
      }, err => {
        this.isLoading = false;
      });
    }
  }



  //#########################Hold Reason####################
  hold_payment(val) {
    if (val) {
      this.isLoading = true;
      let d = {
        "paymentid": this.pendingPayments.paymentid,
        "interviewscheduleid": this.pendingPayments.interviewscheduleid,
        "freelancerid": this.pendingPayments.freelancerid,
        "interviewid": this.pendingPayments.interviewid,
        "paymentstatus": "pending approval",
        "amountexpected": this.pendingPayments.amountexpected,
        "amountpaid": null,
        "currency": this.pendingPayments.currency,
        "holdreason": val
        
      }

      this.adminService.sendPaymentResponse(d).subscribe((res: any) => {
        this.isLoading = false;
        this.toastr.success('Payment hold sucessfully');
        this.location.back();
      }, err => {
        this.isLoading = false;
      });
    }
  }

}
