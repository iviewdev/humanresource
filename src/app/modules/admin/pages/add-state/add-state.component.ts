import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from '../../service/admin.service';

@Component({
  selector: 'app-add-state',
  templateUrl: './add-state.component.html',
  styleUrls: ['./add-state.component.scss']
})
export class AddStateComponent implements OnInit {
  stateForm: FormGroup;
  sfsubmitted = false;
  countries: any[];

  constructor(
    private toastr: ToastrService,
    private adminService: AdminService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.fillCountries();
    this.createStateForm();
  }

  fillCountries() {
    this.adminService.GetAllCountries().subscribe((res: any) => {
      this.countries = res.content;
      this.countries.sort((a, b) => a.countryname.localeCompare(b.countryname));
    }, (err) => {
      this.toastr.error(err.error.massege);
    });
  }

  get sf() { return this.stateForm.controls; }
  createStateForm() {
    this.stateForm = this.fb.group({
      countrycode: ['', Validators.required],
      stateid: ['', Validators.required],
      statecode: ['', Validators.required],
      statename: ['', Validators.required]
    });

  }

  createState() {
    //debugger;
    this.sfsubmitted = true;
    if (this.stateForm.invalid) {
      return;
    }

    const state = this.stateForm.value;
    const stateObj = {
      country_code: state.countrycode,
      state_id: state.stateid,
      state_code: state.statecode,
      state_name: state.statename,
    };

    this.adminService.CreateState(stateObj).subscribe(res => {
      this.toastr.success('State saved successfully');
    }, (err) => {
      this.toastr.error(err.error.message);
    });

  }

}