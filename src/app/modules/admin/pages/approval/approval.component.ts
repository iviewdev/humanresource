import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from '../../service/admin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-approval',
  templateUrl: './approval.component.html',
  styleUrls: ['./approval.component.scss']
})
export class ApprovalComponent implements OnInit {

  applications: any[];

  constructor(
    private toastr: ToastrService,
    private adminService: AdminService,
    private router: Router
  ) { }

  ngOnInit() {
    this.adminService.GetPendingApproval().subscribe((res: any) => {
      debugger;
      if (res) {
        this.applications = res;
      }
      else {

        this.toastr.error('error occured');
      }

    }, (err) => {
      debugger;
      this.toastr.error('error occured');

    });
  }

  Send(userName: any) {
    debugger;
    this.router.navigateByUrl("admin/applicationDetail/" + userName);
  }

}
