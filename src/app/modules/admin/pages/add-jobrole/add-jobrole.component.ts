import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from '../../service/admin.service';
import { stringify } from 'querystring';
import { precision } from 'chartist';

@Component({
  selector: 'app-add-jobrole',
  templateUrl: './add-jobrole.component.html',
  styleUrls: ['./add-jobrole.component.scss']
})
export class AddJobroleComponent implements OnInit {

  jobroleForm: FormGroup;
  jfsubmitted = false;

  constructor(
    private toastr: ToastrService,
    private adminService: AdminService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createJobroleForm();
  }

  get jf() { return this.jobroleForm.controls; }
  createJobroleForm() {
    this.jobroleForm = this.fb.group({
      jobrolecode: ['', Validators.required],
      jobrolename: ['', Validators.required],
      jobroledesc: ['', Validators.required],
      competencies: ['', Validators.required]
    });

  }

  keyword = 'competencyname';
  competencyList = [];
  selectedCompetency: any;
  selectedCompetencyList = [];

  selectCompetencyEvent(item: any) {
    // do something with selected item
    this.selectedCompetency = item;
  }

  onCompetencyChangeSearch(modeller: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    if (modeller && modeller.length >= 0) {
      //debugger
      this.adminService.GetCompetencyByName(modeller)
        .subscribe((res: any) => {
          if (res == undefined) {
            this.competencyList = [];
          }
          if (res && res.length > 0) {
            // alert('found');
            //debugger;
            this.competencyList = res.map((opt: any) => {
              return opt;
            });
          }
        },
          err => {
            //  alert(JSON.stringify(err));
          });
    }
  }

  onCompetencyFocused(e) {
    // do something when input is focused
  }

  selectCompetency() {
    if (this.selectedCompetencyList.indexOf(this.selectedCompetency) === -1) {
      this.selectedCompetencyList.push(this.selectedCompetency);

      var prevalue: String = "";
      if (this.selectedCompetencyList.length > 1) {
        prevalue = this.jobroleForm.controls["competencies"].value + "\n";
      }
      this.jobroleForm.patchValue({
        competencies: prevalue + this.selectedCompetency.competencyname
      });
      this.jobroleForm.updateValueAndValidity()
    }
  }

  createJobrole() {
    debugger;
    this.jfsubmitted = true;
    if (this.jobroleForm.invalid) {
      return;
    }

    const jobrole = this.jobroleForm.value;
    const jobroleObj = {
      jobrolecode: jobrole.jobrolecode,
      jobrolename: jobrole.jobrolename,
      jobroledesc: jobrole.jobroledesc,
      competencies: this.selectedCompetencyList
    };

    this.adminService.CreateJobrole(jobroleObj).subscribe(res => {
      this.toastr.success('Job Role details saved successfully');
    }, (err) => {
      this.toastr.error(err.error.message);
    });

  }

}
