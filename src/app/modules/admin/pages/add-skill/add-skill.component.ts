import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from '../../service/admin.service';

@Component({
  selector: 'app-add-skill',
  templateUrl: './add-skill.component.html',
  styleUrls: ['./add-skill.component.scss']
})
export class AddSkillComponent implements OnInit {
  skillForm: FormGroup;
  sfsubmitted = false;

  constructor(
    private toastr: ToastrService,
    private adminService: AdminService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createSkillForm();
  }

  get sf() { return this.skillForm.controls; }
  createSkillForm() {
    this.skillForm = this.fb.group({
      competency: ['', Validators.required],
      skillname: ['', Validators.required],
      skilldescription: ['', Validators.required],
      skilltype: ['', Validators.required]
    });

  }

  keyword = 'competencyname';
  competencyList = [];

  selectCompetencyEvent(item: any) {
    // do something with selected item
     //alert(JSON.stringify(item));
     //debugger;
     //this.skillForm.controls['competency'] = item.competencyname;
    this.skillForm.patchValue({
      // role: item.jobrolename
      competency: item.competencyname
    });
    this.skillForm.updateValueAndValidity()
     //console.log(this.skillForm.value);
  }

  onCompetencyChangeSearch(modeller: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    if (modeller && modeller.length >= 0) {
      //debugger
      this.adminService.GetCompetencyByName(modeller)
        .subscribe((res: any) => {
          if (res == undefined) {
             this.competencyList = [];
          }
          if (res && res.length > 0) {
            // alert('found');
            //debugger;
            this.competencyList = res.map((opt: any) => {
              return opt;
            });
          }
        },
          err => {
            //  alert(JSON.stringify(err));
          });
    }
  }

  onCompetencyFocused(e) {
    // do something when input is focused
  }

  createSkill() {
    debugger;
    this.sfsubmitted = true;
    if (this.skillForm.invalid) {
      return;
    }

    const skill = this.skillForm.value;
    const skillObj = {
      skillname: skill.skillname,
      competency: {"competencyid":skill.competency.competencyid,"competencyname":skill.competency.competencyname},
      skill_desc: skill.skilldescription,
      skilltype: skill.skilltype
    };

    this.adminService.CreateSkill(skillObj).subscribe(res => {
      this.toastr.success('Skill details saved successfully');
    }, (err) => {
      this.toastr.error(err.error.message);
    });

  }

}
