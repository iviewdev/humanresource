import { Injectable } from '@angular/core';
import { DataService } from '../../../shared/services/data.service';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private dataService: DataService) { }



  GetPendingApproval() {
    return this.dataService.get("admin/getApprovalQueue/Pending_Approval");
  }

  GetMasterDocuments(username: string) {
    return this.dataService.get("admin/getApprovalMaster/" + username);
  }

  GetDocumentDetails(username: string) {
    return this.dataService.get("admin/getDocumentDetails/" + username);
  }

  ApproveMasterDocuments(data: any) {
    return this.dataService.put("admin/approveMasterDocuments", data);
  }

  ApproveDetailDocuments(data: any) {
    return this.dataService.put("admin/approveDetailDocuments", data);
  }

  CreateCompetency(data: any) {
    return this.dataService.post("admin/createCompetency", data);
  }

  GetCompetencyByName(data: string) {
    return this.dataService.get("elasticsearch/getCompetencyByName/" + data);
  }

  CreateSkill(data: any) {
    return this.dataService.post("admin/createSkill", data);
  }

  CreateJobrole(data: any) {
    return this.dataService.put("admin/createjobrole", data);
  }

  CreateCountry(data: any) {
    return this.dataService.put("admin/createcountry", data);
  }

  GetAllCountries() {
    return this.dataService.get("elasticsearch/getAllCountries");
  }

  CreateState(data: any) {
    return this.dataService.put("admin/createstate", data);
  }

  GetAllStatesForCountry(data: string) {
    return this.dataService.get("elasticsearch/getAllStatesForCountry/" + data);
  }

  CreateDistrict(data: any) {
    return this.dataService.put("admin/createDistrict", data);
  }

  GetAllDistrictsForState(data: string) {
    return this.dataService.get("elasticsearch/getAllDistrictsForState/" + data);
  }

  CreateCity(data: any) {
    return this.dataService.put("admin/createcity", data);
  }

  getPendingPayments() {
    return this.dataService.get('admin/getPendingPayments');
  }

  getApprovedPayments() {
    return this.dataService.get('admin/getApprovedPayments');
  }

  
  getFeebackforInterview(interviewid) {
    return this.dataService.get('admin/getFeebackforInterview/' + interviewid);
  }

  sendPaymentResponse(d: any) {
    return this.dataService.put('admin/sendpaymentresponse', d);
  }

}
