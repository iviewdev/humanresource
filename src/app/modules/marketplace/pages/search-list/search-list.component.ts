import { Component, OnInit } from '@angular/core';
import { MarketplaceService } from '../../service/marketplace.service';


@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.scss']
})
export class SearchListComponent implements OnInit {
  public searchData: any = [];
  public isLoading: boolean = false;
  constructor(private marketPlaceService: MarketplaceService) {

  }

  ngOnInit() {

  }

  pushSearchData($event) {
    this.isLoading = true;
    this.marketPlaceService.search($event).subscribe((res: any) => {
      this.isLoading = false;
      if (res) {
        this.searchData = res;
        console.log(this.searchData);
      }
    }, err => {
      this.isLoading = false;
    });
  }



}
