import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Select2OptionData } from 'ng2-select2';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { debounceTime, distinctUntilChanged, filter, finalize, map, switchMap, tap } from 'rxjs/operators';
import { MarketplaceService } from '../../service/marketplace.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-search-option',
  templateUrl: './search-option.component.html',
  styleUrls: ['./search-option.component.scss']
})
export class SearchOptionComponent implements OnInit {
  @Output() onSearchClick: any = new EventEmitter();
  @Input() loading: boolean;
  public currentIndustryData: any = [];
  public jobRoleData: any = []
  public CompetencyData: any = [];
  public countryData: any = [];
  public stateData: any = [];
  public cityData: any = [];
  public experienceYearData: any = [];
  public languageData: any = [];
  public searchOption: FormGroup;
  public submitted: boolean = false;
  public isLoading: boolean = false;
  public currentIndustry: any = []
  public IndustryBoxSetting = {};
  public JobRoleBoxSetting = {};
  public LangauageSettings = {};
  public CountryBoxSetting = {};
  public StateBoxSetting = {};
  public CityBoxSetting = {};
  public CompetencyBoxSetting = {};
  constructor(private fb: FormBuilder, public marketPlaceService: MarketplaceService) {

  }

  //JobRole Auto-Complete 

  jobRole = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      switchMap(value => this.marketPlaceService.getJobRole(value))
    );

  resultFormatJobRoleValue(value: any) {
    return value.jobrolename;
  }

  inputFormatJobroleValue(value: any) {
    if (value.jobrolename)
      return value.jobrolename
    return value;
  }

  //JobRole Autocomplete end here




  ngOnInit() {
    this.createForm();
    this.currentIndustryRecord();
    this.getAllCountries();
    this.experienceLoop();
    this.getAllLanguages();
    this.IndustryBoxSetting = {
      singleSelection: true,
      idField: 'industrycode',
      textField: 'industryname',
      // selectAllText: 'Select All',
      // unSelectAllText: 'UnSelect All',
      // itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.JobRoleBoxSetting = {
      singleSelection: true,
      idField: 'jobrolecode',
      textField: 'jobrolename',
      allowSearchFilter: true
    };
    this.CompetencyBoxSetting = {
      singleSelection: false,
      idField: 'competencyid',
      textField: 'competencyname',
      allowSearchFilter: true
    };
    this.CountryBoxSetting = {
      singleSelection: true,
      idField: 'country_code',
      textField: 'countryname',
      allowSearchFilter: true
    };
    this.StateBoxSetting = {
      singleSelection: true,
      idField: 'statecode',
      textField: 'statename',
      allowSearchFilter: true
    };
    this.CityBoxSetting = {
      singleSelection: true,
      idField: 'citycode',
      textField: 'cityname',
      allowSearchFilter: true
    };

    this.LangauageSettings = {
      singleSelection: false,
      idField: 'langid',
      textField: 'language',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

  }




  createForm() {
    this.searchOption = this.fb.group({
      keyword: [''],
      current_industry: [''],
      job_role: [''],
      competency: [''],
      country: [''],
      state: [''],
      city: [''],
      experience_years: [''],
      languages: [''],
    });
  }

  get f() {
    return this.searchOption.controls;
  }

  //Current Industry Record
  currentIndustryRecord() {
    this.marketPlaceService.getCurrentIndustry().subscribe((res: any) => {
      if (res) {
        this.currentIndustryData = res.content;
      } else {
        this.currentIndustryData = [];
      }
    });

  }

  //On Industry Select 
  onIndustrySelectSelect($event) {
    this.marketPlaceService.getJobRoleByIndustryName($event.industryname).subscribe((res: any) => {
      if (res) {
        this.jobRoleData = res;
      } else {
        this.jobRoleData = [];
      }
    });
  }


  //On Job Role Selection 
  onJobRoleSelect($event) {
    debugger;
    var CompetencyData = [];
    this.marketPlaceService.getCompetencyByJobRole($event.jobrolename).subscribe((res: any) => {
      if (res) {
        res.forEach(element => {
          element.competencies.forEach(ele => {
            CompetencyData.push(ele);
          });
        });
        this.CompetencyData = CompetencyData;
      } else {
        this.CompetencyData = [];
      }
    });
  }


  //Get All Country
  getAllCountries() {
    this.marketPlaceService.getAllCountry().subscribe((res: any) => {
      if (res) {
        this.countryData = res.content;
      } else {
        this.countryData = [];
      }
    });
  }

  //ON Country Select 

  onCountrySelect($event) {
    this.marketPlaceService.getAllStateForCountry($event.country_code).subscribe((res: any) => {
      if (res) {
        this.stateData = res;
      } else {
        this.stateData = [];
      }
    });
  }

  //ON State Select 

  onStateSelect($event) {
    this.marketPlaceService.getAllCityForState($event.statecode).subscribe((res: any) => {
      if (res) {
        this.cityData = res;
      } else {
        this.cityData = [];
      }
    });
  }

  //Get All Languages 

  getAllLanguages() {
    this.marketPlaceService.getAllLanguage().subscribe((res: any) => {
      if (res) {
        this.languageData = res.content;
      } else {
        this.languageData = [];
      }
    });
  }


  //Loop for Experience
  experienceLoop() {
    for (var i = 0; i <= 35; i++) {
      this.experienceYearData.push(i);
    }
  }


  search() {
    this.submitted = true;
    if (this.searchOption.invalid) {
      return;
    }
    //  this.isLoading = true;
    // console.log(this.searchOption.get('current_industry').value[0]['text']);
    debugger;
    let d = {
      keywords: this.f.keyword.value ? this.f.keyword.value : "",
      currindustry: this.f.current_industry.value ? this.f.current_industry.value[0]['industryname'] : "",
      currrole: this.f.job_role.value ? this.f.job_role.value[0]['jobrolename'] : "",
      skills: this.getCompetency(this.f.competency.value),
      country: this.f.country.value ? this.f.country.value[0]['countryname'] : "",
      state: this.f.state.value ? this.f.state.value[0]['statename'] : "",
      city: this.f.city.value ? this.f.city.value[0]['cityname'] : "",
      totalexperience: this.f.experience_years.value ? this.f.experience_years.value : 0,
      languages: (this.getLanguageId(this.f.languages.value)) ? this.getLanguageId(this.f.languages.value).join() : ""
    };
    this.onSearchClick.emit(d);
  }

  getLanguageId(languageList: any): Array<any>[] {
    const arrList = [];
    if (languageList) {
      languageList.forEach((selVal: any) => {
        arrList.push(selVal.language);
      });
    }
    return arrList;
  }

  getCompetency(competencyList: any): any {
    const arrList = [];
    if (competencyList) {
      competencyList.forEach(element => {
        arrList.push({ competency_name: element.competencyname });
      });
    }
    return arrList;
  }



}
