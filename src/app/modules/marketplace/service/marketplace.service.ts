import { Injectable } from '@angular/core';
import { DataService } from 'src/app/shared/services/data.service';

@Injectable({
  providedIn: 'root'
})
export class MarketplaceService {

  constructor(private dataService: DataService) { }

  getJobRole(term: string) {
    return this.dataService.get('elasticsearch/getJobrolebyName/' + term);
  }

  getCurrentIndustry() {
    return this.dataService.get('elasticsearch/getindustry');
  }

  getJobRoleByIndustryName(industry) {
    return this.dataService.get('elasticsearch/getJobrolebyIndustry/' + industry);
  }

  getCompetencyByJobRole(jobrole) {
    return this.dataService.get('elasticsearch/getJobrolebyName/' + jobrole);
  }

  getAllCountry() {
    return this.dataService.get('elasticsearch/getAllCountries');
  }

  getAllStateForCountry(countrycode) {
    return this.dataService.get('elasticsearch/getAllStatesForCountry/' + countrycode);
  }
  getAllCityForState(statecode) {
    return this.dataService.get('elasticsearch/getAllCitiesForState/' + statecode);
  }
  getAllLanguage() {
    return this.dataService.get('elasticsearch/getLanguages');
  }

  search(d:any){
    return this.dataService.post('elasticsearch/getfreelancerbySearchParams',d);
  }
}
