import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketplaceRoutingModule } from './marketplace-routing.module';
import { SearchOptionComponent } from './pages/search-option/search-option.component';
import { SearchListComponent } from './pages/search-list/search-list.component';
import { LayoutComponent } from './components/layout/layout.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Select2Module } from 'ng2-select2';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FooterComponent } from './components/footer/footer.component';



@NgModule({
  declarations: [
    LayoutComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    SearchOptionComponent,
    SearchListComponent
  ],
  imports: [
    CommonModule,
    MarketplaceRoutingModule,
    NgbModule,
    Select2Module,
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class MarketplaceModule { }
