import {CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularAgoraRtcModule, AgoraConfig } from 'angular-agora-rtc';

import { DatePipe } from '@angular/common'
import { EmployerRoutingModule } from './employer-routing.module';
import { LayoutComponent } from './components/layout/layout.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Select2Module } from 'ng2-select2';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FooterComponent } from './components/footer/footer.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { MyjobpostingComponent } from './pages/myjobposting/myjobposting.component';
import { AddjobpostingComponent } from './pages/addjobposting/addjobposting.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { JobApplicationComponent } from './pages/job-application/job-application.component';
import { InterviewLoopScheduleComponent } from './pages/interview-loop-schedule/interview-loop-schedule.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { MyinterviewsComponent } from './pages/myinterviews/myinterviews.component';
import { MyinetrviewsDetailsComponent } from './pages/myinetrviews-details/myinetrviews-details.component';
import { VideoComponent } from './pages/video/video.component';

const agoraConfig: AgoraConfig = {
  // AppID: 'ksdfjlkdsjfklsdjf',
  AppID : '5d58a33c07774b27bcef860c9baeb84b'
};

@NgModule({
  declarations: [
    LayoutComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    DashboardComponent,
    MyjobpostingComponent,
    AddjobpostingComponent,
    JobApplicationComponent,
    InterviewLoopScheduleComponent,
    MyinterviewsComponent,
    MyinetrviewsDetailsComponent,
    VideoComponent
  ],
  imports: [
    AngularAgoraRtcModule.forRoot(agoraConfig),
    CommonModule,
    EmployerRoutingModule,
    NgbModule,
    Select2Module,
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule,
    AutocompleteLibModule
  ],
  providers: [DatePipe,BsModalService]
  //schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class EmployerModule { }
