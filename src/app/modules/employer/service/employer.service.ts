import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/shared/services/data.service';

@Injectable({
  providedIn: 'root'
})
export class EmployerService {
  userData:any;
  constructor(private dataService: DataService) {
   // this.userData= {};
   }

  // setUserData(val: object){
  //   this.userData= val;
  // }
  // getUserData(){
  //   return this.userData;
  // }
  
  getAllRoles() {
    return this.dataService.get('elasticsearch/getallRoles');
  }
  getAllIndus(){
    return this.dataService.get('elasticsearch/getindustry')
  }
  getAllCountry(){
    return this.dataService.get('elasticsearch/getAllCountries')
  }
  getAllState(obj){
    return this.dataService.get(`elasticsearch/getAllStatesForCountry/${obj}`)
  }
  getAllCity(obj){
    return this.dataService.get(`elasticsearch/getAllCitiesForState/${obj}`)
  }
  getjdTemp(){
    return this.dataService.get('corpjobprofile/jDTemplates');
  }

  addJd(obj){
    return this.dataService.put('corpjobprofile/jdTemplate', obj);
  }
  delJdTemp(obj){
    return this.dataService.get(`corpjobprofile/jDTemplate/${obj}`);
  }
  getmcqTemplate(){
    return this.dataService.get('corptesting/mcqtemplates/Initial Tech Screen');
  }
  gethrTemplate(){
    return this.dataService.get('corptesting/mcqtemplates/Initial HR Screen');
  }
  getaiTemplate(){
    return this.dataService.get('corpats/AITemplates')
  }
  getsearchEmpId(obj){
    return this.dataService.get(`corpidentity/SearchEmployeeByID/${obj}`)
  }
  getsearchEmpEmail(obj){
    return this.dataService.get(`corpidentity/SearchEmployeeByEmail/${obj}`)
  }
  getAllJobPosting(page,pageSize){
    return this.dataService.get(`corpjobprofile/jobpostings?pageNo=${page}&pageSize=${pageSize}`)
  }
  getJobPostingById(jdid){
    return this.dataService.get(`corpjobprofile/jobposting/${jdid}`)
  }
  updatejobPost(obj){
    return this.dataService.get(`corpjobprofile/jobpostingdto/${obj}`);
  }
  savejobpost(obj){
    return this.dataService.put('corpjobprofile/jobposting', obj);
  }
  UpdateStatus(obj){
    return this.dataService.put('corpjobprofile/jobpostingUpdateStatus', obj);
  }
  showApplication():Observable<any>{
    return this.dataService.get(`corpats/Applications`, {observe: 'response'})
  }
  getApplicationDataById(id){
    return this.dataService.get(`corpats/Applicationbyid/${id}`);
  }
  putinterviewDetail(obj){
    return this.dataService.put('corpats/interviewsummary', obj)
  }
  getinterviewDetails(id){
    return this.dataService.get(`corpats/interviewbyappid/${id}`)
  }
  employeeSearchauto(obj){
    return this.dataService.get(`corpidentity/SearchEmployeeByEmailKW/${obj}`)
  }
  saveinterviewDetail(obj){
    return this.dataService.put("corpats/interviewdetail", obj)
  }
  getInterviewSummary(obj){
    return this.dataService.get(`corpats/interviewdetail/interviewer/${obj}`)
  }
  getsummaryId(summaryid){
    return this.dataService.get(`corpats/interviewsummary/${summaryid}`);
  }
  getInterviewDetails(summaryid){
    return this.dataService.get(`corpats/interviewdetail/${summaryid}`);
  }
}
