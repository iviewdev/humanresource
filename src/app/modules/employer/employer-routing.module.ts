import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './components/layout/layout.component';
import { AddjobpostingComponent } from './pages/addjobposting/addjobposting.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { InterviewLoopScheduleComponent } from './pages/interview-loop-schedule/interview-loop-schedule.component';
import { JobApplicationComponent } from './pages/job-application/job-application.component';
import { MyjobpostingComponent } from './pages/myjobposting/myjobposting.component';
import { MyinterviewsComponent } from './pages/myinterviews/myinterviews.component';
import { MyinetrviewsDetailsComponent } from './pages/myinetrviews-details/myinetrviews-details.component';
import { VideoComponent } from './pages/video/video.component';

const routes: Routes = [
  { 
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },{
        path: 'myjobposting',
        component: MyjobpostingComponent
      },{
        path: 'addjobposting/:id',
        component: AddjobpostingComponent
      },{
        path: 'addjobposting',
        component: AddjobpostingComponent
      },{
        path: 'jobApplication/:id',
        component: JobApplicationComponent
      },{
        path: 'interviewloopschedule/:id',
        component: InterviewLoopScheduleComponent
      },{
        path: 'interviewDetails/:id',
        component: MyinetrviewsDetailsComponent
      },{
        path: 'myinterviews',
        component: MyinterviewsComponent
      },{ path: 'preview/:id', 
      component: VideoComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployerRoutingModule { }
