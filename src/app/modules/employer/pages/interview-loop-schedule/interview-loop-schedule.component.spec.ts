import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterviewLoopScheduleComponent } from './interview-loop-schedule.component';

describe('InterviewLoopScheduleComponent', () => {
  let component: InterviewLoopScheduleComponent;
  let fixture: ComponentFixture<InterviewLoopScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterviewLoopScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterviewLoopScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
