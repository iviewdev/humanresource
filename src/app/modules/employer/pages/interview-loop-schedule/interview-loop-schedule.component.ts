import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployerService } from '../../service/employer.service';
import { ToastrService } from 'ngx-toastr';
import { SpinnerService } from 'src/app/shared/services/spinner.service';

@Component({
  selector: 'app-interview-loop-schedule',
  templateUrl: './interview-loop-schedule.component.html',
  styleUrls: ['./interview-loop-schedule.component.scss']
})
export class InterviewLoopScheduleComponent implements OnInit {
  params: any;
  applicationData: any = [];
  postingReply:any = [];
  dataApps:any = [];
  dataGet: any;
  dateRange: any;
  timeRange: any;
  hiringManager:any;
  hrowner:any;
  f2frounds:any;
  getserviceRecords : any
  dataAppsLen:boolean = false;
  constructor(private service: EmployerService,
    private Activateroute: ActivatedRoute, public toastr: ToastrService, private spinner: SpinnerService) {
     }

  ngOnInit() {
    // console.log("getRecords", this.getserviceRecords)
    // if(this.getserviceRecords == undefined){
    this.params = this.Activateroute.snapshot.params.id
    this.applicationRecords();
    // }else{
    //  var id =  this.getserviceRecords.interviewid
    //   this.summarydetails(id)
    // var appData = this.getserviceRecords[0]
    // this.service.getsummaryId(appData.interviewid).subscribe(res =>{
    //   this.applicationData = res;
    // });
   // }
  }

  openLink(url: string){
    debugger
    window.open(url, "_blank");
}

  applicationRecords() {
    this.service.showApplication().subscribe(res => {
      if (res.jdid = this.params) {
        this.applicationData = res;
        this.dataGet = this.applicationData.body[0]
        this.getHiringandHrDetails(this.dataGet.jdid);
        if (this.dataGet.jobapplicationid != "") {
          this.getAplicationData(this.dataGet.jobapplicationid);
        }
      }
    });
  }
  jobrole:any;
  getHiringandHrDetails(jdid){
    this.service.getJobPostingById(jdid).subscribe(res => {
      this.postingReply= res;
      this.hiringManager = this.postingReply.hiringmgr;
      this.hrowner = this.postingReply.hrowner
      this.f2frounds = this.postingReply.f2frounds;
      this.jobrole = this.postingReply.jobrole
    });
  }

  sentInterviewDetails(dataGet) {
    var interviewObj = {}
    interviewObj["jobseekername"] = dataGet.jobseekerfname + " " + dataGet.jobseekerlname;
    interviewObj["applicationid"] = dataGet.jobapplicationid;
    interviewObj["jobseekerusername"] = dataGet.jobseekerusername;
    interviewObj["jobseekermobile"] = dataGet.mobile;
    interviewObj["interview_status"] = "Scheduled";
    interviewObj["jobseekerid"] = dataGet.jobseekerid;
    interviewObj["statuscode"] = "Scheduled";
    interviewObj["decision"] = "select";
    interviewObj["postingid"] = dataGet.jdid;
    interviewObj["jobrole"] = this.jobrole;
    interviewObj["hiringmanager"] = this.hiringManager
    interviewObj["hrowner"] = this.hrowner
    interviewObj["rounds"] = this.f2frounds
    this.service.putinterviewDetail(interviewObj).subscribe(res => {
      this.applicationData = res;
    })
  }
  rounds: any = [];
  getAplicationData(id) {
    this.service.getinterviewDetails(id).subscribe(res => {
      console.log("response", res);
      if (res != null) {
        this.applicationData = res
        this.service.getInterviewDetails(this.applicationData.interviewid).subscribe(res =>{
          this.dataApps = res;
          if(this.dataApps.length > 0){
            this.dataAppsLen = true;
          }
        })
        for (let i = 1; i <= this.applicationData.rounds; i++) {
          this.rounds.push(i)
        }
      } else {
        this.sentInterviewDetails(this.dataGet);
      }
    })
  }

  data: any = [];
  keyword = "username"
  onChangeSearch(val: string) {
    if (val.length > 2) {
      this.service.employeeSearchauto(val).subscribe(res => {
        this.data = res
      })
    }
  }

  selectedItem: any
  selectEvent(item) {
    this.selectedItem = item
  }

  saveinterviewDetails() {
    var interviewObj = {}
    interviewObj["corpid"] = this.applicationData.corpid;
    interviewObj["corpname"] = this.applicationData.corpname;
    interviewObj["interviewid"] = this.applicationData.interviewid;
    interviewObj["status"] = "Scheduled";
    interviewObj["jobseekerid"] = this.applicationData.jobseekerid;
    interviewObj["decisioncode"] = this.applicationData.decisioncode;
    interviewObj["interviewer"] = this.selectedItem.id;
    interviewObj["loopid"] = 1;
    interviewObj["round"] = 1;
    interviewObj["idatetime"] = this.dateRange + "T" + this.timeRange; //"2021-07-15T03:22:18.000+00:00"
    interviewObj["descreption"] = "Check";
    interviewObj["interviewurl"] = "";
    interviewObj["intervieweremail"] = this.selectedItem.username;
    this.service.saveinterviewDetail(interviewObj).subscribe(res => {
      this.toastr.success(this.selectedItem.username + "" + 'Successfull');
    })
  }

}
