import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { StorageService } from 'src/app/shared/services/storage.service';
import { EmployerService } from '../../service/employer.service';

@Component({
  selector: 'app-myinterviews',
  templateUrl: './myinterviews.component.html',
  styleUrls: ['./myinterviews.component.scss']
})
export class MyinterviewsComponent implements OnInit {
  summaryDetails:any= [];
  constructor(private service: EmployerService,public toastr: ToastrService,private storageService: StorageService) { }

  ngOnInit() {
    var tokenjdId = this.storageService.getItem('id');
    //var tokenjdId = 64;
    this.service.getInterviewSummary(tokenjdId).subscribe(res =>{
      debugger
      this.summaryDetails.push(res[0]);
     // this.service.setUserData(this.summaryDetails);
      // if(interviewerid !=undefined){
      //   this.getinterviewDetails(interviewerid)
      // }
    });
  }

  openLink(url: string){
    debugger
    window.open(url, "_blank");
}

  // getinterviewDetails(id){
  //   debugger
  //   this.service.getinterviewDetails(id).subscribe(res =>{
  //     // var interviewId = res.interviewid;
  //     // if(interviewId != undefined){
  //     // this.getsummaryId(interviewId);
  //     // }
  //   })
  // }
  // getsummaryId(id){
  //   debugger
  //   this.service.getsummaryId(id).subscribe(res =>{
  //     this.summaryDetails.push(res);
  //     console.log(res)
  //   });
    
  //   console.log(this.summaryDetails)
  // }

}
