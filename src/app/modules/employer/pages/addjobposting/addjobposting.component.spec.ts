import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddjobpostingComponent } from './addjobposting.component';

describe('AddjobpostingComponent', () => {
  let component: AddjobpostingComponent;
  let fixture: ComponentFixture<AddjobpostingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddjobpostingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddjobpostingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
