import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmployerService } from '../../service/employer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { SpinnerService } from 'src/app/shared/services/spinner.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import pdfMake from "pdfmake/build/pdfmake";  
import pdfFonts from "pdfmake/build/vfs_fonts";  
pdfMake.vfs = pdfFonts.pdfMake.vfs; 

@Component({
  selector: 'app-addjobposting',
  templateUrl: './addjobposting.component.html',
  styleUrls: ['./addjobposting.component.scss']
})
export class AddjobpostingComponent implements OnInit {
  employerForm: FormGroup;
  tenantid: any;
  constructor(private fb: FormBuilder, private service: EmployerService,
    private router: ActivatedRoute, public datepipe: DatePipe,
    public toastr: ToastrService, private spinner: SpinnerService,
    public routechange: Router, private modalService: NgbModal) {
  }

  /*************conditionbased intialization********/
  isShown: boolean = true;
  isShown2: boolean = false;
  isShown3: boolean = false;
  isSubmitted = false;
  isState:boolean = false;
  iscity:boolean =false;
  ishr: boolean = false;
  ismcq: boolean = false;
  isaibased: boolean = false;
  submitEmp = false;
  /*********nonconditional intialzation*********/
  showModal: boolean;
  jobtype: any;
  txtData: any;
  jobrole: any = [];
  currindustry:any;
  country:any;
  countryName:any;
  state:any;
  stateName:any;
  city:any;
  cityName:any;
  industryType:any;
  salrange: any;
  expReqMonths: any;
  expreqyears: any;
  type: any;
  otherDetails: any;
  othersRole: any = [];
  listofJobRes: any = [];
  listofRequiremnts: any = [];
  jdTemplate: any = [];
  addJD: any;
  jdDesc: any;
  desctype: any;
  editjdData: any;
  choseAITemp: any = [];
  choseMCQTemp: any = [];
  choseHRTemp: any = [];
  obj: any;
  empId: any;
  tenantname: any;
  hiringmgr: any;
  hrowner:any;
  editJDObj: any;
  empEmail: any;
  result: any;
  dataarray: any = [];
  keywordSearch: any;
  closeResult = '';
  jobtitle: any;
  requestData:any;
  listReq:any;

  ngOnInit() {
    var today = new Date().toISOString().split('T')[0];
    document.getElementsByName("somedate")[0].setAttribute('min', today);
    this.employerForm = this.fb.group({
      jptitle: ['', Validators.required],
      jobrole: ['', Validators.required],
      currindustry: ['', Validators.required],
      countryName : ['', Validators.required],
      statename : ['', Validators.required],
      cityname : ['', Validators.required],
      jdtemplate: ['', Validators.required],
      jobtype: ['', Validators.required],
      salrange: ['', Validators.required],
      jobOpenDate: ['', Validators.required],
      jobOpenTime: ['', Validators.required],
      hiringmgr: ['', Validators.required],
      hrowner: ['', Validators.required],
      expreqyears: ['', Validators.required],
      expReqMonths: ['', Validators.required],
      hrflag: [''],
      mcqflag: [''],
      hmflag: [''],
      debriefflag: [''],
      allowaiflag: [''],
      hrtemplate: [''],
      mcqtemplate: [''],
      aitemplate: [''],
      loopflag: [''],
      elimflag: [''],
      f2frounds: ['', Validators.required]
    });

    this.callGetAllRoles();
    this.callGetAllIndus();
    this.getjdTemplate();
    this.getAllCountry();
    this.service.updatejobPost(this.router.snapshot.params.id).subscribe((res) => {
      this.result = res;
      this.employerForm.patchValue({
        jptitle: this.result.jptitle,
        hiringmgr: this.result.hiringmgr,
        hrowner: this.result.hrowner,
        f2frounds: this.result.f2frounds,
        jobtype: this.result.jobtype,
        salrange : this.result.salrange,
        keywordSearch : this.result.keywords,
        expReqMonths : this.result.expreqmonths,
        expreqyears : this.result.expreqyears,
        jobrole : this.result.jobrole,
       // currindustry : this.result.currindustry
      });
    })
  }

 // subscribing the job Role service 
  callGetAllRoles() {
    this.service.getAllRoles().subscribe((res) => {
      this.jobrole = res;
    });
  }

  callGetAllIndus(){
    debugger
    this.service.getAllIndus().subscribe((res) => {
      console.log("response", res)
      this.currindustry = res;
      this.industryType = this.currindustry.content;
      console.log("response", this.industryType)
    });
  }

  getAllCountry(){
    this.service.getAllCountry().subscribe((res) => {
      this.country = res;
      this.countryName = this.country.content;
    });
  }
  getAllState(value){
    this.service.getAllState(value).subscribe((res) => {
      this.state = res;
      if(this.state.length > 0){
      this.isState = true
      this.stateName = res;
      console.log("stateName", this.stateName)
      }
    });
  }
  getAllCity(value){
    this.service.getAllCity(value).subscribe((res) => {
      console.log("response", res)
      this.city = res;
      if(this.city.length > 0){
        this.iscity = true;
       this.cityName = res;
      }
    });
  }
  // suscribing the jd Template
  getjdTemplate() {
    this.service.getjdTemp().subscribe((res) => {
      this.jdTemplate = res;
    });
  }
  // get the values of job discription and list of requirements using ******subscribing the jd Template service ******
  getSelected(value,id) {
    debugger
    if(id=="countryName"){
      this.getAllState(value);
    }if(id=='stateName'){
      this.getAllCity(value);
    }else{
    this.listofJobRes = [];
    this.listofRequiremnts = [];
    this.othersRole = this.jdTemplate.filter((res) => {
      if (res.id == value) {
        this.listofJobRes.push(res.jddesc);
        this.listofRequiremnts.push(res.desctype);
      }
    });
  }
  }
//add new Job Responsibilties
  addNewJobRes(){
    this.listofJobRes.push(this.jdDesc)
  }
// add List of Requirents
addNewListReq(){
  this.listofRequiremnts.push(this.listReq)
}
  // edit the jobRes and ListRequirements
  editJobRes(data, value, name) {
    debugger
    if (value == 'edit') {
      this.editjdData = data;
      this.jdTemplate.filter(res => {
        if (res.jddesc == data && name == 'jobRes') {
          this.editJDObj = {
            "jdcode": res.jdcode,
            "jdname": res.jdname,
            "desctype": res.desctype,
            "id": res.id
          }
        } if (res.desctype == data && name == 'listReq') {
          this.editJDObj = {
            "jdcode": res.jdcode,
            "jdname": res.jdname,
            "desctype": res.desctype,
            "id": res.id
          }
        }
      });
    } else if (value == 'del') {
      this.service.delJdTemp(this.editjdData).subscribe(res => {
        console.log("delete", res)
        this.toastr.success("deleted Successfully")
      })
    }
  }
// Save Edited JD
   editJD(val,name) {
     debugger
      if (val == 'edit' && name == "jobRes") {
      this.editJDObj['jddesc'] = this.editjdData;
    }else if(val == 'edit' && name == 'listReq'){
      this.editJDObj['desctype'] == this.editjdData
    }
    this.service.addJd(this.editJDObj).subscribe(res => {
       this.toastr.success("Successfully edited")
    })

  }
  //select Rounds to show the related template (AI/HR/MCQ)
  roundSelected(e) {
    if (e == 'mcq') {
      this.ismcq = !this.ismcq;
      this.getMCQtemp();
    } if (e == 'hr') {
      this.ishr = !this.ishr;
      this.getHRtemp();
    } if (e == 'aibased') {
      this.isaibased = !this.isaibased;
      this.getAItemp();
    }
  }
  // Get MCQ template
  getMCQtemp() {
    this.service.getmcqTemplate().subscribe(res => {
      this.choseMCQTemp = res;
    });
  }
  // Get HR template
  getHRtemp() {
    this.service.gethrTemplate().subscribe(res => {
      this.choseHRTemp = res;
    });
  }
  // Get AI template
  getAItemp() {
    this.service.getaiTemplate().subscribe(res => {
      this.choseAITemp = res;
    });
  }
  // hiring manager Search
  clickSearch(value) {
    debugger
    var commonArray = [];
    if (this.empId != undefined) {
      this.service.getsearchEmpId(this.empId).subscribe(res => {
        commonArray.push(res);
        commonArray.filter(res => {
          if(value == 'hiringmgr'){
          return this.hiringmgr = res.username;
          }else{
            return this.hrowner = res.username
          }
        })
      });
    }
    if (this.empEmail != undefined) {
      this.service.getsearchEmpEmail(this.empEmail).subscribe(res => {
        commonArray.push(res);
        commonArray.filter(res => {
          if(value == 'hiringmgr'){
          return this.hiringmgr = res.username;
          }else{
            return this.hrowner = res.username
          }
        })
      });
    }
  }

  get fc() {
    return this.employerForm.controls;
  }
  // save and next open model
  saveandnext(content) {
    debugger
    this.requestData =[];
    this.submitEmp = true;
    var obj = this.employerForm.value
    console.log(obj);
    var sys = new Date();
    obj['openedon'] = this.datepipe.transform(sys, 'yyyy-MM-dd');
    obj['statusactive'] = 'Active';
    obj['keywords'] = obj.jptitle;
    if (obj.hrflag == true) {
      obj['hrflag'] = 'Y'
    }else if(obj.hrflag == false) {
      obj['hrflag'] = ''
    } if (obj.mcqflag == true) {
      obj['mcqflag'] = 'Y'
    }else if (obj.mcqflag == false) {
      obj['mcqflag'] = ''
    } if (obj.hmflag == true) {
      obj['hmflag'] = 'Y'
    }else  if (obj.hmflag == false) {
      obj['hmflag'] = ''
    } if (obj.debriefflag == true) {
      obj['debriefflag'] = 'Y'
    }else  if (obj.debriefflag == false) {
      obj['debriefflag'] = ''
    } if (obj.allowaiflag == true) {
      obj['allowaiflag'] = 'Y'
    }else if (obj.allowaiflag == false) {
      obj['allowaiflag'] = ''
    } if (obj.loopflag == true) {
      obj['loopflag'] = 'Y'
    } else if (obj.loopflag == '') {
      obj['loopflag'] = 'N'
    } if (obj.elimflag == true) {
      obj['elimflag'] = 'Y'
    } else if (obj.elimflag == '') {
      obj['elimflag'] = 'N'
    }
    var jobpostingdetails = [];
    this.listofJobRes.forEach((res) => {
      var objListReq = {
        "requirement": res,
        "type": "Requirement",
        "jobresp": null
      }
      jobpostingdetails.push(objListReq)
    });
    this.listofRequiremnts.forEach((res)=>{
      var objJobRes = {
        "requirement": null,
        "type": "Responsibility",
        "jobresp": res
      }
      jobpostingdetails.push(objJobRes)
    })

    this.employerForm.value['jobpostingdetails'] = jobpostingdetails;
    this.jobtitle = this.employerForm.value.jptitle;
    this.cleanObj(obj)
    if (this.employerForm.invalid) {
      return this.toastr.error('PLease Enter Fields');
    }else{
      this.modalService.open(content, { size: 'xl' });
      this.requestData = obj
    }
  }
// save both
  saveJobPost(){
    debugger
    this.requestData['keywords'] = this.keywordSearch;
    this.spinner.showSpinner();
    this.service.savejobpost(this.requestData).subscribe(res => {
      this.spinner.hideSpinner();
      if (res) {
        this.toastr.success('Successfull');
        this.dataarray = res
        this.jobtitle = this.dataarray.jptitle  
        //this.routechange.navigateByUrl('/employer/myjobposting');
      }
    }), (err) => {
      this.toastr.error(err.error.message);
    }
  }

  cleanObj(obj) {
    for (var propName in obj) {
      if (obj[propName] === '' || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
    return obj
  }

  // create pdf
  generatePDF() {  
    let docDefinition = {  
      content: [
        {
          text: 'JD Preview',
          bold: true,
          fontSize: 20,
          alignment: 'center',
          margin: [20, 0, 0, 20],
          color: 'blue',
        },
        {
          columns: [
            [{
              text: 'Job Title',
              fontSize: 15,
              alignment: 'left',
              bold:true,
              margin: [0, 20, 0, 5],
              decoration: 'underline'
            },{
              columns: [
                this.jobtitle,
                ]
            },{
              text: 'Job Description',
              fontSize: 15,
              alignment: 'left',
              bold:true,
              margin: [0, 20, 0, 5],
              decoration: 'underline'
            },{
              columns : [
                {
                  ol : [
                    this.listofJobRes.filter(s => s).map(d => d)
                  ]
                },
              ]
            }
           ],
          ], 
          styles: {
            header: {
              bold: true,
              fontSize: 15,
              margin: [10, 20, 10, 0],
            }
          },defaultStyle: {
            fontSize: 10,
            alignment: 'left',
            margin: [20, 0, 0, 0],
          }
        },
      ],
    };    
    pdfMake.createPdf(docDefinition).open();  
  }  
}
