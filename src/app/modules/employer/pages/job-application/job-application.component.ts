import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmployerService } from '../../service/employer.service';

@Component({
  selector: 'app-job-application',
  templateUrl: './job-application.component.html',
  styleUrls: ['./job-application.component.scss']
})
export class JobApplicationComponent implements OnInit {
  activateID:any
  jobApplicant:any;
  jobsApplication:any =[];
  checkedList:any;
  jobAppLength:any;
  dataLength:boolean=true;
  masterSelected:boolean;
  checkCompleted:boolean = true;
  completed:boolean = false;
  constructor(private activateRoute : ActivatedRoute,private service: EmployerService,) { 
    this.masterSelected = false;
    this.showApplication();
  }

  ngOnInit() {
    this.activateID = this.activateRoute.snapshot.params.id;
    console.log(this.activateID);
  }

  showApplication(){
    debugger
    this.service.showApplication().subscribe((res)=>{
      if(res.status == 200){
      this.jobApplicant = res.body;
      if(res.body.length){
        this.dataLength = false;
        this.jobApplicant.filter(result =>{
          if(result.jdid == this.activateID){
            result['isSelected'] = false
            this.jobsApplication.push(result)
            this.jobAppLength = this.jobsApplication.length;
            this.checkForLoopRound();
          }else{
            this.dataLength = true
          }
          console.log("selected",this.jobsApplication)
        })
      }else{
        this.dataLength = true
      }
      }
    })
  }
  checkForLoopRound(){
    debugger
    var dataChange = this.jobsApplication[0];
    if(dataChange.automatedaistatus == 'Passed' && dataChange.automatedhrstatus == 'Passed'
    && dataChange.automatedintialstatus == 'Passed'){
      this.checkCompleted = false;
      this.completed = true
    }
  }

// for select and unselect checkbox start
  checkUncheckAll() {
    for (var i = 0; i < this.jobsApplication.length; i++) {
      this.jobsApplication[i].isSelected = this.masterSelected;
    }
    this.getCheckedItemList();
  }
  isAllSelected() {
    this.jobsApplication.every(function(item:any) {
        return item.isSelected == true;
      })
    this.getCheckedItemList();
  }

  getCheckedItemList(){
    this.checkedList = [];
    for (var i = 0; i < this.jobsApplication.length; i++) {
      if(this.jobsApplication[i].isSelected)
      this.checkedList.push(this.jobsApplication[i]);
    }
    this.checkedList = JSON.stringify(this.checkedList);
    console.log(this.checkedList)
  }
// for select and unselect checkbox end
}
