import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyjobpostingComponent } from './myjobposting.component';

describe('MyjobpostingComponent', () => {
  let component: MyjobpostingComponent;
  let fixture: ComponentFixture<MyjobpostingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyjobpostingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyjobpostingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
