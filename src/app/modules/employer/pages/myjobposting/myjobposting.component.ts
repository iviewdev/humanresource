import { Component, OnInit } from '@angular/core';
import { EmployerService } from '../../service/employer.service';
import { ToastrService } from 'ngx-toastr';
import { SpinnerService } from 'src/app/shared/services/spinner.service';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-myjobposting',
  templateUrl: './myjobposting.component.html',
  styleUrls: ['./myjobposting.component.scss']
})
export class MyjobpostingComponent implements OnInit {
  // jobpostingList:any=[];
  jobpostingLists: any = [];
  totalJobPost: any;
  status: any;
  getId: any;
  items = [];
  jobpostingList: Array<any>;
  popupUpdatepost: any = [];
  jobPostID: any;
  jobpostTitle: any;
  pager = {};
  pageSize: any = 5;
  dataNext: number = 0;
  currentStatus: any;
  dataLength:boolean=true;
  pageHide:boolean = false;
  userData:any
  constructor(private service: EmployerService, public toastr: ToastrService,
    private spinner: SpinnerService, private route: ActivatedRoute, private router: Router) {
     // this.service.setUserData(this.userData);
     }

  ngOnInit() {
    this.getalljobpostings();
   }

  // Pagination
  next() {
    this.dataNext = this.dataNext + 1
    this.getalljobpostings();
    $(".prev").removeClass('disabled')
  }
  prev() {
    this.dataNext = this.dataNext - 1;
    this.getalljobpostings();
    $(".next").removeClass("disabled");
    if (this.dataNext == 0) {
      $(".prev").addClass('disabled')
    }
  }
  //chnaging the type of postings
  onChangePage(jobpostingList: Array<any>) {
    this.jobpostingLists = jobpostingList;
  }
  // get the all job posting datas
  getalljobpostings() {
    this.spinner.showSpinner();
    this.service.getAllJobPosting(this.dataNext, this.pageSize).subscribe(res => {
      if (res) {
        this.spinner.hideSpinner();
        this.jobpostingLists = res;
        if (this.jobpostingLists.length > 0) {
          this.jobpostingList = this.jobpostingLists
          this.totalJobPost = this.jobpostingList.length;
          this.dataLength =false;
          this.pageHide = true;
        } else {
          this.toastr.error('data not available');
          $(".next").addClass("disabled");
          this.dataNext = this.dataNext - 1;
          this.dataLength =true;
          this.pageHide = false
        }
      }
    });
  }

  // filter the job post as per type of postings
  getStatus(e) {
    if (e == 'allPost') {
      this.jobpostingList = [];
      this.jobpostingList = this.jobpostingLists
      this.totalJobPost = this.jobpostingList.length;
    }
    if (e == 'openPost' || e == 'closePost') {
      this.jobpostingList = [];
      if (e == 'openPost') {
        this.status = 'Active';
      } else {
        this.status = 'Closed'
      }
      let openJobList = [];
      this.jobpostingLists.filter(res => {
        if (res.statusactive == this.status) {
          openJobList.push(res)
        }
      });
      this.jobpostingList = openJobList;
      this.totalJobPost = this.jobpostingList.length
    }
  }
  //open the status popup after clicking the job id
  updateStatus(id) {
    this.jobpostingLists.filter((res) => {
      if (res.id == id) {
        this.popupUpdatepost = res
      }
    });
    this.jobPostID = this.popupUpdatepost.id;
    this.jobpostTitle = this.popupUpdatepost.jptitle
  }
  // changing the job status hiring to closed vice versa
  getCurrentStatus(e) {
    this.currentStatus = e
  }
  //change status
  changeStatus() {
    this.popupUpdatepost['statusactive'] = this.currentStatus
    this.service.UpdateStatus(this.popupUpdatepost).subscribe(res => {
      if(res){
        this.toastr.success("Update Status Change")
      }
    })
  }

  // updateJOBs(id){
  //   this.service.updatejobPost(id).subscribe(res =>{
  //     if(res){
  //       console.log(res)
  //      // this.router.navigateByUrl(`/employer/addjobposting/${id}`)
  //     }
  //   })
  // }
}
