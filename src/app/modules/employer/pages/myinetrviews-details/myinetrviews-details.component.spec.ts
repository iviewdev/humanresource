import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyinetrviewsDetailsComponent } from './myinetrviews-details.component';

describe('MyinetrviewsDetailsComponent', () => {
  let component: MyinetrviewsDetailsComponent;
  let fixture: ComponentFixture<MyinetrviewsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyinetrviewsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyinetrviewsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
