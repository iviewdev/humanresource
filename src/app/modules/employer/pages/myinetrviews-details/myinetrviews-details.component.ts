import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployerService } from '../../service/employer.service';
import { ToastrService } from 'ngx-toastr';
import { SpinnerService } from 'src/app/shared/services/spinner.service';

@Component({
  selector: 'app-myinetrviews-details',
  templateUrl: './myinetrviews-details.component.html',
  styleUrls: ['./myinetrviews-details.component.scss']
})
export class MyinetrviewsDetailsComponent implements OnInit {
  params: any;
  applicationData: any = [];
  postingReply: any = [];
  dataApps: any = [];
  dataGet: any;
  dateRange: any;
  timeRange: any;
  hiringManager: any;
  hrowner: any;
  f2frounds: any;
  getserviceRecords: any
  constructor(private service: EmployerService,
    private Activateroute: ActivatedRoute, public toastr: ToastrService, private spinner: SpinnerService) {
  }

  // startVideo() {
  //   debugger
  //   $("body").addClass("modal-open");
  //   $("#int-screen").removeClass('d-none');
  //   $("#min-screen, .intvdetails").addClass('d-none');
  // }

  // minimizeVideo() {
  //   $("#int-screen").addClass('d-none');
  //   $("#min-screen, .intvdetails").removeClass('d-none');
  //   $("body").removeClass("modal-open");
  // }

  // maximizeVideo() {
  //   this.startVideo();
  // }

  ngOnInit() {
    this.params = this.Activateroute.snapshot.params.id
    this.applicationRecords();
  }

  openLink(url: string) {
    debugger
    window.open(url, "_blank");
  }

  applicationRecords() {
    this.service.showApplication().subscribe(res => {
      if (res.jdid = this.params) {
        this.applicationData = res;
        this.dataGet = this.applicationData.body[0]
        this.getHiringandHrDetails(this.dataGet.jdid);
        if (this.dataGet.jobapplicationid != "") {
          this.getAplicationData(this.dataGet.jobapplicationid);
        }
      }
    });
  }
  jobrole: any;
  getHiringandHrDetails(jdid) {
    this.service.getJobPostingById(jdid).subscribe(res => {
      this.postingReply = res;
      this.hiringManager = this.postingReply.hiringmgr;
      this.hrowner = this.postingReply.hrowner
      this.f2frounds = this.postingReply.f2frounds;
      this.jobrole = this.postingReply.jobrole
    });
  }

 
  rounds: any = [];
  getAplicationData(id) {
    this.service.getinterviewDetails(id).subscribe(res => {
      console.log("response", res);
      if (res != null) {
        this.applicationData = res
        this.service.getInterviewDetails(this.applicationData.interviewid).subscribe(res => {
          this.dataApps = res
        })
        for (let i = 1; i <= this.applicationData.rounds; i++) {
          this.rounds.push(i)
        }
      }
    })
  }

}