import { Component, OnInit } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { NavigationStart, Router } from '@angular/router';
import { StorageService } from 'src/app/shared/services/storage.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [NgbDropdownConfig]
})
export class NavbarComponent implements OnInit {

  public iconOnlyToggled = false;
  public sidebarToggled = false;
  public username: string;
  public role: string;
  public mobile: string;
  public navSearch: FormGroup;
  public submitted: boolean = false;
  toggleSidebar() {
    let body = document.querySelector('body');
    if ((!body.classList.contains('sidebar-toggle-display')) && (!body.classList.contains('sidebar-absolute'))) {
      this.iconOnlyToggled = !this.iconOnlyToggled;
      if (this.iconOnlyToggled) {
        body.classList.add('sidebar-icon-only');
      } else {
        body.classList.remove('sidebar-icon-only');
      }
    } else {
      this.sidebarToggled = !this.sidebarToggled;
      if (this.sidebarToggled) {
        body.classList.add('sidebar-hidden');
      } else {
        body.classList.remove('sidebar-hidden');
      }
    }
  }

  constructor(config: NgbDropdownConfig,
    private router: Router,
    private storageService: StorageService,
    private fb: FormBuilder, private toastr: ToastrService) {
    config.placement = 'bottom-right';
  }

  ngOnInit() {
    this.username = this.storageService.getItem('username');
    this.role = this.storageService.getItem('role');
    this.mobile = this.storageService.getItem('mobile');
    this.createNavSearch();
    this.router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        var url = event['url'].split('/')[2];
        url = url.toLocaleLowerCase();
        if (url.includes('freelancer-search')) {

        } else {
        //  this.navSearch.get('search_keyword').patchValue('');
        }
      }
    });
  }

  Logout() {
    this.storageService.removeItem("token");
    this.storageService.removeItem('username');
    this.storageService.removeItem('id');
    this.storageService.removeItem('mobile');
    this.storageService.removeItem('role');
    this.router.navigate(['/account/login/jobseeker']);
  }

  closeSettingsSidebar() {
    document.querySelector('#right-sidebar').classList.toggle('open');
  }

  focusInput() {
    const navbarSearchInput = <HTMLElement>document.querySelector('#navbar-search-input');
    navbarSearchInput.focus();
  }

  toggleRightSidebar() {
    document.querySelector('.sidebar-offcanvas').classList.toggle('active');
  }

  createNavSearch() {
    this.navSearch = this.fb.group({
      org_type: ['Freelancer', Validators.required],
      search_keyword: ['', Validators.required]
    });
  }

  get s() {
    return this.navSearch.controls;
  }

  // Search by Keyword
  search_by_keyword() {
    this.submitted = true;
    if (this.navSearch.invalid) {
      this.toastr.error('Keywords empty');
      return;
    }
    this.router.navigate(['/jobseeker/freelancer-search'], { queryParams: this.navSearch.value });
  }

}
