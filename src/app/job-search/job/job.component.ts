import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery'
import { StorageService } from 'src/app/shared/services/storage.service';
import { SearchService } from 'src/app/shared/services/search.service';
import { JobSearchService } from '../job-search.service';
declare function showPanel(id);
declare function hidePanel();

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.scss']
})
export class JobComponent implements OnInit {
  public searchRecords: any = [];
  public search_keywords: string = '';
  public org_type: string = '';
  public search: string;
  public country;
  public d = {
    "keywords": "",
    "jptitle": "",
    "jpresp": "",
    "jobtype": "",
    "salrange": "",
    "openedon": "",
    "hiringmgr": "",
    "expreqyears": "",
    "statusactive": "",
    "jobrole": "",
    "currindustry": ""
  };


  public offset: number = 0;
  public limit: number = 5;
  public applied: number = 0;
  constructor(private route: ActivatedRoute,
    private jobsearch: JobSearchService, private router: Router,
    private storageService: StorageService,
    private searchService: SearchService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.org_type = params['org_type'];
      this.d.keywords = params['search_keyword'];
      this.getjobpostingbySearchParams();
    });
    var d = {
      org_type: this.org_type,
      search_keyword: this.d.keywords
    }
    this.searchService.setSearchParam(d);
    this.currentIndustryRecord();
  }

  // next page
  nextPage() {
    this.offset = this.offset + 1;
    this.getjobpostingbySearchParams();
  }

  //previous page

  priviousPage() {
    if (this.offset > 0) {
      this.offset = this.offset - 1;
      this.getjobpostingbySearchParams();
    }
  }


  // Get Freelancer By Search params
  public total_page: number = 0;
  getjobpostingbySearchParams() {
    this.jobsearch.getjobpostingbySearchParams(this.d, this.offset).subscribe((res: any) => {
      console.log(res);
      if (res.length > 0) {
        // this.jobsearch.getQueryHits(this.d).subscribe((q: any) => {
        //   if (q) {
        //     this.total_page = q;
        //     this.searchRecords = res;
        //   }
        // }, err => {
        //   console.log(err);
        // });
        this.searchRecords = res;
      } else {
        this.total_page = 0;
        this.offset = 0;
        this.searchRecords = [];
      }

    }, err => {
      this.searchRecords = [];
      console.log(err);
    });
  }

  forwardDisable() {
    if ((this.total_page - (this.limit * this.offset)) > this.limit) {
      return false;
    } else {
      return true;
    }
  }

  backwardDisable() {
    if (this.offset != 0) {
      return false;
    } else {
      return true;
    }
  }

  // Filter Education
  getEducation(records) {
  //  return Array.prototype.map.call(records, function (item) { return item.education_type; }).join(",")
  }


  //######################### Clear All Filters####################

  clearAllFilter() {

    // Clear Current industries
    this.currentIndustryData.map(item => {
      return item.checked = false;
    });
    this.jobRoleData.map(item => {
      return item.checked = false;
    });


    this.applied = 0
    this.d = {
      "keywords": this.search_keywords,
      "jptitle": "",
      "jpresp": "",
      "jobtype": "",
      "salrange": "",
      "openedon": "",
      "hiringmgr": "",
      "expreqyears": "",
      "statusactive": "",
      "jobrole": "",
      "currindustry": ""
    }
    this.searchRecords = [];
    this.total_page = 0;
    this.offset = 0;
    this.getjobpostingbySearchParams();
  }


  //Current Industry Record
  public currentIndustryData: any = [];
  currentIndustryRecord() {
    this.jobsearch.getCurrentIndustry().subscribe((res: any) => {
      console.log(res);
      if (res) {
        this.currentIndustryData = res.content;
      } else {
        this.currentIndustryData = [];
      }
    });

  }

  //On Industry Select 
  public jobRoleData: any = [];
  public currentindustry: any;
  onIndustrySelectSelect($event) {

    if (!this.d.currindustry) {
      this.applied = this.applied + 1;
    }

    this.currentIndustryData.forEach((element, index) => {
      if (element.industryname == $event.target.value) {
        this.currentIndustryData[index].checked = true;
      } else {
        this.currentIndustryData[index].checked = false;
      }
    });
    var i = this.currentIndustryData.findIndex(x => x.industryname == $event.target.value);
    if (i >= 0) {
      if (i > 4) {
        var element = this.currentIndustryData[i];
        element.checked = true;
        this.currentIndustryData.splice(i, 1);
        this.currentIndustryData.unshift(element);
      }
    }

    this.d.currindustry = $event.target.value;
    this.getjobpostingbySearchParams();
    hidePanel();
    this.jobsearch.getJobRoleByIndustryName($event.target.value).subscribe((res: any) => {
      console.log(res);
      if (res) {

        this.jobRoleData = res;
      } else {
        this.jobRoleData = [];
      }
    });
  }


  //On Job Role Selection 
  public CompetencyData: any = [];
  public jobrole: any;
  onJobRoleSelect($event) {
    this.jobRoleData.forEach((element, index) => {
      if (element.jobrolename == $event.target.value) {
        this.jobRoleData[index].checked = true;
      } else {
        this.jobRoleData[index].checked = false;
      }
    });
    var i = this.jobRoleData.findIndex(x => x.jobrolename == $event.target.value);
    if (i >= 0) {
      if (i > 4) {
        var element = this.jobRoleData[i];
        element.checked = true;
        this.jobRoleData.splice(i, 1);
        this.jobRoleData.unshift(element);
      }
    }
    if (!this.d.jobrole) {
      this.applied = this.applied + 1;
    }
    this.d.jobrole = $event.target.value;
    this.getjobpostingbySearchParams();
    hidePanel();
    var CompetencyData = [];
    this.jobsearch.getCompetencyByJobRole($event.target.value).subscribe((res: any) => {
      if (res) {
        res.forEach(element => {
          element.competencies.forEach(ele => {
            CompetencyData.push(ele);
          });
        });
        this.CompetencyData = CompetencyData;
      } else {
        this.CompetencyData = [];
      }
    });
  }


  //show Panel
  public title: string;
  public popuprecords: any = [];
  showPan(id, title) {
    this.search = '';
    hidePanel();


    if (title == 'Current Industry') {
      this.title = title;
      this.popuprecords = this.currentIndustryData;
    }

    if (title == 'Job Role') {
      this.title = title;
      this.popuprecords = this.jobRoleData;
    }


    showPanel(id);
  }

}
