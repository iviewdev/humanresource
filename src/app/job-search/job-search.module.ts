import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobSearchRoutingModule } from './job-search-routing.module';
import { JobComponent } from './job/job.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilterPipe } from '../modules/jobseeker/pages/search/filter.pipe';


@NgModule({
  declarations: [
    JobComponent,
    FilterPipe
  ],
  imports: [
    CommonModule,
    JobSearchRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class JobSearchModule { }
