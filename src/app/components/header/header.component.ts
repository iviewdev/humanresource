import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SearchService } from 'src/app/shared/services/search.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public navSearch: FormGroup;
  public submitted: boolean = false;
  constructor(private fb: FormBuilder,
    private router: Router, private toastr: ToastrService,
    private searchService: SearchService) { }

  ngOnInit() {
    this.createNavSearch();
    this.searchService.searchParam.subscribe(val => {
      this.navSearch.patchValue(val);
    });
  }

  createNavSearch() {
    this.navSearch = this.fb.group({
      org_type: ['', Validators.required],
      search_keyword: ['', Validators.required]
    });
  }

  get s() {
    return this.navSearch.controls;
  }

  // Search by Keyword
  search_by_keyword() {
    this.submitted = true;
    if (this.navSearch.invalid) {
      this.toastr.error('Keywords empty');
      return;
    }
    if (this.navSearch.get('org_type').value == 'Jobs') {
      this.router.navigate(['/job-search/job'], { queryParams: this.navSearch.value });
    } else {
      this.router.navigate(['/search'], { queryParams: this.navSearch.value });
    }

  }

}
