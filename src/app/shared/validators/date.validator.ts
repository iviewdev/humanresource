import { FormGroup, ValidatorFn } from '@angular/forms';

export function dateLessThan(firstDateField: string, secondDateField: string): ValidatorFn {
    return (form: FormGroup): { [key: string]: boolean } | null => {
        debugger;
        const firstDateValue = form.get(firstDateField).value;
        const secondDateValue = form.get(secondDateField).value;

        if (!firstDateValue || !secondDateValue) {
            return { missing: true };
        }
        const firstDate = new Date(firstDateValue).getTime();
        const secondDate = new Date(secondDateValue).getTime();

        if (firstDate >= secondDate) {
            const err = { dateLessThan: true }
            form.get(firstDateField).setErrors(err);
            return err;
        }
        else {
         const dateLessError= form.get(firstDateField).hasError('dateLessThan');
         if(dateLessError){
             delete form.get(firstDateField).errors['dateLessThan'];
             form.get(firstDateField).updateValueAndValidity();
         }

        }
    };
}

export function dateGreaterThan(firstDateField: string, secondDateField: string): ValidatorFn {
    return (form: FormGroup): { [key: string]: boolean } | null => {
        debugger;
        const firstDateValue = form.get(firstDateField).value;
        const secondDateValue = form.get(secondDateField).value;

        if (!firstDateValue || !secondDateValue) {
            return { missing: true };
        }
        const firstDate = new Date(firstDateValue).getTime();
        const secondDate = new Date(secondDateValue).getTime();

        if (firstDate >= secondDate) {
            const err = { dateGreaterThan: true }
            form.get(secondDateField).setErrors(err);
            return err;
        }
        else {
         const dateGreaterError= form.get(secondDateField).hasError('dateGreaterThan');
         if(dateGreaterError){
             delete form.get(secondDateField).errors['dateGreaterThan'];
             form.get(secondDateField).updateValueAndValidity();
         }

        }
    };
}