import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public baseURL = environment.BASE_API_URL;
  loginURL = 'http://35.198.194.180:8080/';
  __RequestVerificationToken = '';

  constructor(private httpClient: HttpClient, private storage: StorageService) {

  }
  headers = new HttpHeaders();
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })

  };

  httpOptions1 = {
    headers: new HttpHeaders({
      'Content-Type': ''
    })

  };


  get(method, headerOptions?) {
    return this.httpClient.get(this.baseURL + method, headerOptions);
  }
  getWithParam(method, _params) {
    return this.httpClient.get(this.baseURL + method, _params);
  }
  post(method, data, headerOptions?) {
    return this.httpClient.post(this.baseURL + method, data, headerOptions);
  }
  put(method, data, headerOptions?) {
    return this.httpClient.put(this.baseURL + method, data, headerOptions);
  }

  login(method, data) {
    debugger;
    return this.httpClient.post(this.baseURL + method, data);
  }
  delete(method) {
    return this.httpClient.delete(this.baseURL + method);
  }

  downloadFile(method, data,) {
    return this.httpClient.put(this.baseURL + method, data, {responseType:'blob'});
  }

  uploadFile(method, formData) {
    // return this.httpClient.post(method, formData, {
    //     headers: {
    //       'Content-Type': 'multipart/form-data',
    //       'Access-Control-Allow-Headers': 'Authorization'
    //     }
    //   });
    //const data: FormData = new FormData();
    // data.append('file', file);
    method = this.baseURL + method;
    const newRequest = new HttpRequest('PUT', method, formData, {
      //  reportProgress: true,
      responseType: 'text'
    });
    return this.httpClient.request(newRequest);
  }
}
