import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor() { }
  
  public param = {
    'org_type': 'Jobs',
    'search_keyword': ''
  }

  public searchParam = new BehaviorSubject<any>(this.param);

  setSearchParam(val) {
    this.searchParam.next(val);
  }

}
