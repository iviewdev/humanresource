import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ChangeDetectorRef, Component, ElementRef, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AngularAgoraRtcService, Stream } from 'angular-agora-rtc';
import { RateYourExperienceComponent } from 'src/app/modules/jobseeker/pages/rate-your-experience/rate-your-experience.component';
import { JobSeekerService } from 'src/app/modules/jobseeker/service/jobseeker.service';
import { Location } from '@angular/common';
declare var $: any;
import { Resume } from './resume';
import { FreelancerService } from 'src/app/modules/freelancer/service/freelancer.service';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';
import { SpinnerService } from 'src/app/modules/freelancer/service/spinner.service';


@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {
  title = 'AgoraDemo';
  localStream: Stream;
  sreenStream: Stream;
  remoteCalls: any = []; // Add
  remoteStream: any = [];
  @Input() user_role;
  @Input() userId;
  @Input() interviewid;
  @Input() jobseekerid;
  @Input() freelancerid;
  @Input() interviewstarttime;
  @Input() interviewendtime;
  @Input() remotecalls;
  @Input() scheduleid;
  public appId: string = '5d58a33c07774b27bcef860c9baeb84b';
  public resouceId: string;
  public w_resouceId: string;
  public sid: string;
  public w_sid: string;
  public cname: string = '';
  public uid: string = '';
  public w_uid: string = '';
  public show_start_meeting: boolean = true;
  public show_shart_sharing: boolean = false;
  public show_start_recording: boolean = false;
  public show_end_recording: boolean = false;
  public disable_end_meeting: boolean = false;

  public j_id = "";
  @ViewChild('pdf', { static: false }) pdf: ElementRef;
  @ViewChild('remote_video', { static: false }) remote_video: ElementRef;
  @ViewChild('textpanel', { static: false }) textpanel: ElementRef;
  @ViewChild('sitecontent', { static: false }) sitecontent: ElementRef;
  @ViewChild('resume', { static: false }) resume: ElementRef;
  @ViewChild('videosreen', { static: false }) videosreen: ElementRef;
  constructor(
    private agoraService: AngularAgoraRtcService,
    private http: HttpClient,
    private jobseeker: JobSeekerService,
    private modalService: NgbModal,
    private elementRef: ElementRef,
    private location: Location,
    private cd: ChangeDetectorRef,
    private renderer: Renderer2,
    private jobseekerservice: JobSeekerService,
    private freelancerService: FreelancerService,
    private confirmationDailog: ConfirmationDialogService,
    private spinner: SpinnerService
  ) {
    this.agoraService.createClient('rtc');

  }

  ngOnInit() {
    this.user_role = this.user_role.toLowerCase();
    this.cname = this.interviewid;
    this.uid = Math.floor((Math.random() * 10000000) + 1).toString();
    this.w_uid = Math.floor((Math.random() * 10000000) + 1).toString();
    localStorage.setItem('script-loaded', '0');
    this.getJobseekerBasicProfile(this.jobseekerid);
  }


  // setDynamicHeight(h) {
  //   this.textpanel.nativeElement.style.height = h;
  //   this.sitecontent.nativeElement.style.height = h;
  //   this.resume.nativeElement.style.height = h;
  //   this.pdf.nativeElement.style.height = h;
  //   this.pdf.nativeElement.style.width = '100%';
  //   this.videosreen.nativeElement.style.height = h;
  // }



  // ngAfterViewInit() {
  //   var v = window.innerHeight - 190;
  //   var h = v + "px";
  //   this.setDynamicHeight(h);
  // }

  public resumeurl: string;
  public jobseekername: string;
  getJobseekerBasicProfile(id) {
    this.jobseekerservice.getbasicprofilebyid(id).subscribe((res: any) => {
      if (res) {
        console.log(res);
        this.resumeurl = res.resumeurl;
        this.jobseekername = res.fname + ' ' + res.lname;
        // this.getFilenameFromUrl(res.resumeurl);
      }
    }, err => {
      console.log(err);
    })
  }

  public loading: boolean = false;






  async getSignUrl() {
    const rm = new Resume(this.jobseekerid, this.resumeurl);
    var url = await rm.signedurl();
    this.renderer.removeAttribute(this.pdf.nativeElement, "src");
    this.renderer.setAttribute(this.pdf.nativeElement, "src", url);
  }





  zoom() {
    var elem = document.documentElement;
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    }
    if (document.exitFullscreen) {
      document.exitFullscreen();
    }

  }

  startCall() {
    this.agoraService.client.join(null, this.cname, null, (uid) => {
      this.remoteStream.push(uid);
      this.localStream = this.agoraService.createStream(uid, true, null, null, true, false);
      this.localStream.setVideoProfile('720p_3');
      this.subscribeToStreams();
    });
  }


  private subscribeToStreams() {
    this.localStream.on("accessAllowed", () => {
      console.log("accessAllowed");
    });
    // The user has denied access to the camera and mic.
    this.localStream.on("accessDenied", () => {
      console.log("accessDenied");
    });

    this.localStream.init(() => {
      this.show_start_meeting = false;
      console.log("getUserMedia successfully");
      this.localStream.play('agora_local');
      this.agoraService.client.publish(this.localStream, function (err) {
        console.log("Publish local stream error: " + err);
      });
      this.agoraService.client.on('stream-published', function (evt) {
        console.log("Publish local stream successfully");
      });
    }, function (err) {
      console.log("getUserMedia failed", err);
    });



    // Add
    this.agoraService.client.on('error', (err) => {
      console.log("Got error msg:", err.reason);
      if (err.reason === 'DYNAMIC_KEY_TIMEOUT') {
        this.agoraService.client.renewChannelKey("", () => {
          console.log("Renew channel key successfully");
        }, (err) => {
          console.log("Renew channel key failed: ", err);
        });
      }
    });

    // Add
    this.agoraService.client.on('stream-added', (evt) => {
      const stream = evt.stream;
      this.agoraService.client.subscribe(stream, (err) => {
        console.log("Subscribe stream failed", err);
      });
    });

    // Add
    this.agoraService.client.on('stream-subscribed', (evt) => {

      this.show_shart_sharing = true;
      this.show_start_recording = true;
      this.show_end_recording = false;
      const stream = evt.stream;

      if (!this.remoteCalls.includes(`agora_remote${stream.getId()}`)) this.remoteCalls.push(`agora_remote${stream.getId()}`);
      setTimeout(() => stream.play(`agora_remote${stream.getId()}`), 2000);
    });

    // Add
    this.agoraService.client.on('stream-removed', (evt) => {
      const stream = evt.stream;
      stream.stop();
      this.remoteCalls = this.remoteCalls.filter(call => call !== `#agora_remote${stream.getId()}`);
      console.log(`Remote stream is removed ${stream.getId()}`);
    });

    // Add
    this.agoraService.client.on('peer-leave', (evt) => {

      const stream = evt.stream;
      if (stream) {
        stream.stop();
        this.remoteCalls = this.remoteCalls.filter(call => call === `#agora_remote${stream.getId()}`);
        console.log(`${evt.uid} left from this channel`);

      }
    });
  }


  leave() {
    this.agoraService.client.leave(() => {
      this.localStream.stop();
      this.localStream.close();
      this.show_shart_sharing = false;
      this.show_start_recording = false;
      this.show_end_recording = false;
      console.log("Leavel channel successfully");

      if (this.user_role == 'jobseeker') {
        this.location.back();
        const modelRef = this.modalService.open(RateYourExperienceComponent, {
          backdrop: true,
        });
        modelRef.componentInstance.jobseekerid = this.jobseekerid;
        modelRef.componentInstance.freelancerid = this.freelancerid;
        modelRef.componentInstance.interviewid = this.interviewid;
      }
      if (this.user_role == 'freelancer') {
        this.confirmationDailog.confirm('Please Confirm', 'Send Interview Completed Response', 'Confirm', '', 'sm')
          .then((confirmed) => {
            if (confirmed) {
              this.sendInterviewStatus();
            }
          }).catch((err) => {
            console.log(err);
          })
      }

    }, (err) => {
      console.log("Leave channel failed");
    });
  }



  sendInterviewStatus() {
    debugger
    this.spinner.showSpinner();
    let d = {
      scheduleid: this.scheduleid,
      interviewid: this.interviewid,
      freelancerid: this.freelancerid,
      jobseekerid: this.jobseekerid,
      interviewstarttime: this.interviewstarttime,
      interviewendtime: this.interviewendtime,
      interviewstatus: 'Interview Completed (Pending Feedback)',
      interviewrecordingurl: null,
      interviewchannel: this.cname,
      remarks: "",
    }
    this.freelancerService.sendInterviewResponse(d).subscribe((res: any) => {
      this.spinner.hideSpinner();
      if (res) {
        this.location.back();
      }
    }, err => {
      this.spinner.hideSpinner();
      this.location.back();
    });
  }




  //*******************************COMPOSITE RECORDING********************************* */

  startRecording() {
    var acquieUrl = 'https://api.agora.io/v1/apps/' + this.appId + '/cloud_recording/acquire';
    var username = 'b9feec21a504498d9078f6587e790843'; // customer Id;
    var password = 'b0f3108b52984b3c94b3b8bf5b23caaa'; // secret Id;
    let authorizationData = 'Basic ' + btoa(username + ':' + password);
    const headerOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': authorizationData
      })
    };
    debugger;
    let d = {
      "cname": this.cname,
      "uid": this.uid,
      "clientRequest": {
      }
    }

    this.http
      .post(acquieUrl, d, headerOptions)
      .subscribe(
        (data: any) => { // json data
          this.disable_end_meeting = true;
          console.log('Success: ', data.resourceId);
          this.resouceId = data.resourceId;
          var startUrl = 'https://api.agora.io/v1/apps/' + this.appId + '/cloud_recording/resourceid/' + this.resouceId + '/mode/mix/start';

          this.j_id = this.remoteStream[0];

          let e = {
            "cname": this.cname,
            "uid": this.uid,
            "clientRequest": {
              "recordingConfig": {
                "channelType": 0,
                "streamTypes": 2,
                "audioProfile": 1,
                "videoStreamType": 0,
                "maxIdleTime": 120,
                "transcodingConfig": {
                  "width": 1920,
                  "height": 1080,
                  "fps": 30,
                  "bitrate": 3150,
                  "maxResolutionUid": "1",
                  "mixedVideoLayout": 1
                },
                "autoSubscribe": false,
                "subscribeVideoUids": [this.j_id.toString()],
                "subscribeAudioUids": [this.j_id.toString()],
              },
              "storageConfig": {
                "vendor": 1,
                "region": 14,
                "bucket": "iviewrecs",
                "accessKey": "AKIAJPBXLIJ5AXVOTJWQ",
                "secretKey": "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"
              }
            }
          };
          this.http.post(startUrl, e, headerOptions).subscribe((res: any) => {
            console.log(res);
            this.show_start_recording = false;
            this.show_end_recording = true;
            this.sid = res.sid;
            this.acquireWebPageResourceId();
          }, err => {
            console.log("Error" + JSON.stringify(err));
          });

        },
        error => {
          console.log('Error: ', error);
        });
  }


  //*************************************************ACQUIRE WEBPAGE RESOURCE ID*****************/
  acquireWebPageResourceId() {
    var acquieUrl = 'https://api.agora.io/v1/apps/' + this.appId + '/cloud_recording/acquire';
    var username = 'b9feec21a504498d9078f6587e790843'; // customer Id;
    var password = 'b0f3108b52984b3c94b3b8bf5b23caaa'; // secret Id;
    let authorizationData = 'Basic ' + btoa(username + ':' + password);
    const headerOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': authorizationData
      })
    };
    debugger;
    let d = {
      "cname": this.cname,
      "uid": this.w_uid,
      "clientRequest": {
        "resourceExpiredHour": 24,
        "scene": 1
      }
    }
    debugger;
    this.http
      .post(acquieUrl, d, headerOptions)
      .subscribe(
        (data: any) => { // json data
          this.w_resouceId = data.resourceId;
          this.startWebPageRecording();
        }, err => {
          console.log(err);
        })
  }

  //*********************************************WEB PAGE RECORDING**************************************/

  startWebPageRecording() {
    var url = 'https://api.agora.io/v1/apps/' + this.appId + '/cloud_recording/resourceid/' + this.w_resouceId + '/mode/web/start';
    var username = 'b9feec21a504498d9078f6587e790843'; // customer Id;
    var password = 'b0f3108b52984b3c94b3b8bf5b23caaa'; // secret Id;
    let authorizationData = 'Basic ' + btoa(username + ':' + password);
    const headerOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': authorizationData
      })
    };
    let e = {
      "cname": this.cname,
      "uid": this.w_uid,
      "clientRequest": {
        "extensionServiceConfig": {
          "errorHandlePolicy": "error_abort",
          "extensionServices": [{
            "serviceName": "web_recorder_service",
            "errorHandlePolicy": "error_abort",
            "serviceParam": {
              "url": "https://toovix.com/freelancer/start-meeting/" + this.interviewid + "/" + this.jobseekerid,
              "audioProfile": 0,
              "videoWidth": 1280,
              "videoHeight": 720,
              "maxRecordingHour": 72
            }
          }]
        },
        "recordingFileConfig": {
          "avFileType": [
            "hls",
            "mp4"
          ]
        },
        "storageConfig": {
          "vendor": 1,
          "region": 14,
          "bucket": "iviewrecs",
          "accessKey": "AKIAJPBXLIJ5AXVOTJWQ",
          "secretKey": "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"
        }
      }
    }

    this.http.post(url, e, headerOptions).subscribe((res: any) => {
      this.w_sid = res.sid;
    }, err => {
      console.log("Error" + JSON.stringify(err));
    });


  }


  //********************************************END COMPOSITE RECORDING *****/
  endRecord() {
    var url = 'https://api.agora.io/v1/apps/' + this.appId + '/cloud_recording/resourceid/' + this.resouceId + '/sid/' + this.sid + '/mode/mix/stop';
    var username = 'b9feec21a504498d9078f6587e790843'; // customer Id;
    var password = 'b0f3108b52984b3c94b3b8bf5b23caaa'; // secret Id;
    let authorizationData = 'Basic ' + btoa(username + ':' + password);
    const headerOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': authorizationData
      })
    };
    let d = {
      "cname": this.cname,
      "uid": this.uid,
      "clientRequest": {

      }
    }

    this.http.post(url, d, headerOptions).subscribe((res: any) => {
      console.log(res);
      let d = {
        "jobseekerid": this.jobseekerid,
        "interviewid": this.interviewid,
        "recordingurl": "https://iviewrecs.s3.ap-south-1.amazonaws.com/" + res.serverResponse.fileList
      };
      this.jobseeker.saveVideoRecordings(d).subscribe((res: any) => {
        this.show_start_recording = true;
        this.show_end_recording = false;
        this.disable_end_meeting = false;
        this.endWebPageRecording();
      }, err => {

      });

    }, err => {
      console.log(err);
    });

  }

  //********************************************END WEBPAGE RECORDING *****/
  endWebPageRecording() {
    var url = 'https://api.agora.io/v1/apps/' + this.appId + '/cloud_recording/resourceid/' + this.w_resouceId + '/sid/' + this.w_sid + '/mode/web/stop';
    var username = 'b9feec21a504498d9078f6587e790843'; // customer Id;
    var password = 'b0f3108b52984b3c94b3b8bf5b23caaa'; // secret Id;
    let authorizationData = 'Basic ' + btoa(username + ':' + password);
    const headerOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': authorizationData
      })
    };
    let d = {
      "cname": this.cname,
      "uid": this.w_uid,
      "clientRequest": {

      }
    }

    this.http.post(url, d, headerOptions).subscribe((res: any) => {
      console.log(res);
      let d = {
        "jobseekerid": this.jobseekerid,
        "interviewid": this.interviewid,
        "recordingurl": "https://iviewrecs.s3.ap-south-1.amazonaws.com/" + res.sid + "_" + this.interviewid + "_0.mp4"
      };
      this.jobseeker.saveVideoRecordings(d).subscribe((res: any) => {

      }, err => {

      });

    }, err => {
      console.log(err);
    });

  }



  // Start Screen Sharing

  startSharing() {
    this.agoraService.client.unpublish(this.localStream, function (err) {

    });
    this.localStream.stop();
    this.localStream.close();

    this.localStream = this.agoraService.createStream(this.uid, true, null, null, false, true);
    this.localStream.setScreenProfile('720p_1');
    this.localStream.init(() => {
      this.localStream.play('agora_local');
      this.agoraService.client.publish(this.localStream, (err) =>
        console.log('Publish local stream error: ' + err)
      );
      this.agoraService.client.on('stream-published', (evt) =>
        console.log('Publish local stream successfully')
      );

    }, (err) => {
      console.log(err);
    });
    this.localStream.on("stopScreenSharing", () => {
      this.agoraService.client.unpublish(this.localStream, function (err) {

      });
      this.localStream.stop();
      this.localStream.close();

      this.localStream = this.agoraService.createStream(this.uid, true, null, null, true, false);
      this.subscribeToStreams();
    });

  }

  back() {
    this.location.back();
  }






}
