import { aws } from './aws';
import * as AWS from 'aws-sdk'
export class Resume {
    public jobseekerid: number;
    public resumeurl: string;
    constructor(jobseekerid: number, resumeurl: string) {
        this.jobseekerid = jobseekerid;
        this.resumeurl = resumeurl;
    }

    public getFilenameFromUrl(url) {
        if (url) {
            var cuturl = 'https://' + 'iviewid' + this.jobseekerid + '.s3.' + aws.region + '.amazonaws.com/';
            var rtnurl = url.slice(cuturl.length, url.length);
            return decodeURI(rtnurl);
        }
    }

    public async signedurl() {
        var key = this.getFilenameFromUrl(this.resumeurl);
        const config = {
            accessKeyId: aws.accessKeyId,
            secretAccessKey: aws.secretAccessKey,
            signatureVersion: aws.signatureVersion,
            region: aws.region
        }
        AWS.config.update(config);
        const client = new AWS.S3();
        var url = await client.getSignedUrl('getObject', {
            Bucket: 'iviewid' + this.jobseekerid,
            Key: key,
            Expires: 3600
        });

        return url;
    }
}
