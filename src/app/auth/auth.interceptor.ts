import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StorageService } from '../shared/services/storage.service'
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private storage: StorageService, private router: Router,
    ) {

    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        
        if (this.storage.getItem('token') != null) {
            //debugger;
            const cloneReq = req.clone({
                headers: req.headers.set('Authorization', 'Bearer ' + this.storage.getItem('token'))

            });
            return next.handle(cloneReq).pipe(
                tap(
                    suc => { },
                    err => {
                        if (err.status == 401) {
                            this.storage.removeItem('token');
                            this.router.navigateByUrl('/account/login');
                        }
                    }
                )
            )
        } else {
           
            // this.router.navigateByUrl('/account/login');
            return next.handle(req.clone());
        }
    }
}