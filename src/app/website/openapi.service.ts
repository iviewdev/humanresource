import { Injectable } from '@angular/core';
import { DataService } from '../shared/services/data.service';

@Injectable({
  providedIn: 'root'
})
export class OpenapiService {

  constructor(public dataService: DataService) { }

  getfreelancerbySearchParams(data: any, offset, limit) {
    return this.dataService.post('elasticsearch/openapi/getfreelancerbySearchParams?pageNo=' + offset + '&pageSize=' + limit, data);
  }

  getQueryHits(data: any) {
    return this.dataService.post('elasticsearch/openapi/getQueryHits', data);
  }

  getCurrentIndustry() {
    return this.dataService.get('elasticsearch/getindustry');
  }

  getJobRoleByIndustryName(industry) {
    return this.dataService.get('elasticsearch/getJobrolebyIndustry/' + industry);
  }

  getCompetencyByJobRole(jobrole) {
    return this.dataService.get('elasticsearch/getJobrolebyName/' + jobrole);
  }

  getAllCountry() {
    return this.dataService.get('elasticsearch/getAllCountries');
  }

  getAllStateForCountry(countrycode) {
    return this.dataService.get('elasticsearch/getAllStatesForCountry/' + countrycode);
  }
  getAllCityForState(statecode) {
    return this.dataService.get('elasticsearch/getAllCitiesForState/' + statecode);
  }
  getAllLanguage() {
    return this.dataService.get('elasticsearch/getLanguages');
  }

  getFreelancerById(id) {
    return this.dataService.get('elasticsearch/getFreelancerbyId/' + id);
  }


}
